package com.epam.rd.november2019.core;

import com.epam.rd.november2019.interfaces.MyDeque;
import com.epam.rd.november2019.interfaces.MyList;

import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * Doubly-linked list implementation of the {@code MyList} and
 * {@code MyDeque} interfaces.
 *
 * @param <E> the type of elements in this list
 * @author Nataliia Soloviova
 * @version 1.0
 * @see MyList
 * @see MyDeque
 * @see MyArrayList
 */
public class MyLinkedList<E> implements MyList<E>, MyDeque<E> {

    /**
     * The number of elements MyLinkedList contains.
     */
    private int size = 0;

    /**
     * Pointer to first node.
     */
    private Element<E> first;

    /**
     * Pointer to last node.
     */
    private Element<E> last;

    /**
     * Constructs an empty list.
     */
    public MyLinkedList() {
    }

    /**
     * Inserts the specified element at the beginning of this list.
     *
     * @param e the element to add.
     */
    public void addFirst(E e) {
        Element<E> element = first;
        Element<E> newElement = new Element<E>(e, null, element);
        first = newElement;
        if (element == null) {
            last = newElement;
        } else {
            element.setPrevious(newElement);
        }
        size++;
    }

    /**
     * Appends the specified element to the end of this list.
     *
     * @param e the element to add.
     */
    public void addLast(E e) {
        Element<E> element = last;
        Element<E> newElement = new Element<E>(e, element, null);
        last = newElement;
        if (element == null) {
            addFirst(e);
        } else {
            element.setNext(newElement);
            size++;
        }
    }

    /**
     * Appends the specified element to the end of this list.
     *
     * @param element element to be appended to this list.
     */
    public void add(E element) {
        if (size == 0) {
            addFirst(element);
        } else {
            addLast(element);
        }
    }

    /**
     * Inserts the specified element at the specified position in this list.
     *
     * @param index   index at which the specified element is to be inserted
     * @param element element to be inserted
     * @throws ArrayIndexOutOfBoundsException
     */
    public void add(int index, E element) {
        checkIndex(index);
        if (index == 0) {
            addFirst(element);
        } else if (index == size) {
            addLast(element);
        } else {
            Element<E> nextElement = first;
            for (int i = 0; i < index; i++) {
                nextElement = nextElement.getNext();
            }
            Element<E> addElement = new Element<E>(element, nextElement.getPrevious(), nextElement);
            nextElement.setPrevious(addElement);
            nextElement.getPrevious().getPrevious().setNext(addElement);
            size++;
        }
    }

    /**
     * Returns the first element in this list.
     *
     * @return the first element in this list
     * @throws NoSuchElementException if this list is empty
     */
    public E getFirst() {
        Element<E> element = first;
        if (element == null) {
            throw new NoSuchElementException();
        }
        return element.getCurrent();
    }

    /**
     * Returns the last element in this list.
     *
     * @return the last element in this list
     * @throws NoSuchElementException if this list is empty
     */
    public E getLast() {
        Element<E> element = last;
        if (element == null) {
            throw new NoSuchElementException();
        }
        return element.getCurrent();
    }

    /**
     * Returns the element at the specified position in this list.
     *
     * @param index index of the element to return
     * @return the element at the specified position in this list
     * @throws ArrayIndexOutOfBoundsException
     */
    public E get(int index) {
        checkIndex(index);
        Element<E> returnElement = first;
        for (int i = 0; i < index; i++) {
            returnElement = returnElement.getNext();
        }
        return returnElement.getCurrent();
    }

    /**
     * Returns the number of elements in this list.
     *
     * @return the number of elements in this list
     */
    public int size() {
        return size;
    }

    /**
     * Removes the element at the specified position in this list.
     *
     * @param index the index of the element to be removed
     * @return the element previously at the specified position
     * @throws ArrayIndexOutOfBoundsException
     */
    public E remove(int index) {
        checkIndex(index);
        Element<E> currentElement = getElement(index);
        if (currentElement.getPrevious() == null) {
            currentElement.getNext().setPrevious(null);
            first = currentElement.getNext();
        } else {
            currentElement.getPrevious().setNext(currentElement.getNext());
        }
        if (currentElement.getNext() == null) {
            currentElement.getPrevious().setNext(null);
            last = currentElement.getPrevious();
        } else {
            currentElement.getNext().setPrevious(currentElement.getPrevious());
        }
        size--;
        return currentElement.getCurrent();
    }

    /**
     * Removes and returns the first element from this list.
     *
     * @return the first element from this list
     * @throws NoSuchElementException if this list is empty
     */
    public E removeFirst() {
        if (first == null) {
            throw new NoSuchElementException("This list is empty!");
        }
        return remove(0);
    }

    /**
     * Removes and returns the last element from this list.
     *
     * @return the last element from this list
     * @throws NoSuchElementException if this list is empty
     */
    public E removeLast() {
        if (first == null) {
            throw new NoSuchElementException("This list is empty!");
        }
        return remove(size - 1);
    }

    /**
     * Replaces the element at the specified position in this list with the
     * specified element.
     *
     * @param index   index of the element to replace
     * @param element element to be stored at the specified position
     * @return the element previously at the specified position
     * @throws ArrayIndexOutOfBoundsException
     */
    public E set(int index, E element) {
        E oldValue = getElement(index).getCurrent();
        getElement(index).setCurrent(element);
        return oldValue;
    }

    /**
     * Returns the index of the first occurrence of the specified element
     * in this list, or -1 if this list does not contain the element.
     *
     * @param element element to search for
     * @return the index of the first occurrence of the specified element in
     * this list, or -1 if this list does not contain the element
     */
    public int indexOf(E element) {
        int index = 0;
        for (Element<E> i = first; i != null; i = i.next) {
            if (element == null) {
                if (i.getCurrent() == element) {
                    return index;
                }
                index++;
            } else {
                if (element.equals(i.getCurrent())) {
                    return index;
                }
                index++;
            }
        }
        return -1;
    }

    /**
     * Returns the index of the last occurrence of the specified element
     * in this list, or -1 if this list does not contain the element.
     *
     * @param element element to search for
     * @return the index of the last occurrence of the specified element in
     * this list, or -1 if this list does not contain the element
     */
    public int lastIndexOf(E element) {
        int index = size - 1;
        for (Element<E> i = last; i != null; i = i.previous) {
            if (element == null) {
                if (i.getCurrent() == element) {
                    return index;
                }
                index--;
            } else {
                if (element.equals(i.getCurrent())) {
                    return index;
                }
                index--;
            }
        }
        return -1;
    }

    public void clear() {
        for (Element<E> x = first; x != null; ) {
            Element<E> next = x.next;
            x.current = null;
            x.next = null;
            x.previous = null;
            x = next;
        }
        first = last = null;
        size = 0;
    }

    /**
     * Checks if the index belongs to the array.
     *
     * @param index the index of the element
     * @throws ArrayIndexOutOfBoundsException
     */
    private void checkIndex(int index) {
        if ((index < 0) || (index > size)) {
            throw new ArrayIndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        }
    }

    private static class Element<E> {
        private E current;
        private Element<E> previous;
        private Element<E> next;

        Element(E current, Element<E> previous, Element<E> next) {
            this.current = current;
            this.previous = previous;
            this.next = next;
        }

        E getCurrent() {
            return current;
        }

        void setCurrent(E current) {
            this.current = current;
        }

        Element<E> getPrevious() {
            return previous;
        }

        void setPrevious(Element<E> previous) {
            this.previous = previous;
        }

        Element<E> getNext() {
            return next;
        }

        void setNext(Element<E> next) {
            this.next = next;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) return true;
            if (obj == null || getClass() != obj.getClass()) return false;
            Element<E> that = (Element<E>) obj;
            return this.getCurrent() == that.getCurrent();
        }

        @Override
        public int hashCode() {
            return super.hashCode();
        }
    }

    private Element<E> getElement(int index) {
        checkIndex(index);
        Element<E> returnElement = first;
        for (int i = 0; i < index; i++) {
            returnElement = returnElement.getNext();
        }
        return returnElement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyLinkedList<?> that = (MyLinkedList<?>) o;
        if (size != that.size) return false;
        for (int i = 0; i < size; i++) {
            if (this.get(i) != that.get(i)) return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(size, first, last);
    }

//    @Override
//    public String toString() {
//        String s = "";
//        for (int i = 0; i < size - 1; i++) {
//            s += get(i) + ", ";
//        }
//        return "{" + s + get(size - 1) + "}";
//    }

    @Override
    public String toString() {
        String s = "";
        String result = "";
        if (!isEmpty()) {
            for (int i = 0; i < size - 1; i++) {
                s += get(i) + ", ";
            }
            result = "[" + s + get(size - 1) + "]";
        } else {
            result = null;
        }
        return result;
    }
}

