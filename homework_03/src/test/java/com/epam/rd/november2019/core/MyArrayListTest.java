package com.epam.rd.november2019.core;


import com.epam.rd.november2019.interfaces.MyList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


class MyArrayListTest {

    private MyList<Integer> myArrayList;

    @BeforeEach
    void setUp() {
        myArrayList = new MyArrayList<>();
        myArrayList.add(100);
        myArrayList.add(200);
        myArrayList.add(300);
    }

    @AfterEach
    void tearDown() {
        myArrayList.clear();
    }

    @Test
    void testEqualsShouldReturnTrue() {
        //GIVEN
        MyList<Integer> newList = myArrayList;
        //WHEN
        boolean result = myArrayList.equals(newList);
        //THEN
        assertTrue(result);
    }

    @Test
    void testEqualsShouldReturnFalse() {
        //GIVEN
        MyList<Integer> newList = new MyArrayList<>();
        newList.add(100);
        newList.add(200);
        newList.add(300);
        //WHEN
        boolean result = myArrayList.equals(newList);
        //THEN
        assertFalse(result);
    }

    @Test
    void testToStringShouldReturnSuchString() {
        //GIVEN
        //WHEN
        String result = myArrayList.toString();
        //THEN
        assertThat("[100, 200, 300]", is(result));
    }
}