package com.epam.rd.november2019.core;


import com.epam.rd.november2019.interfaces.MyList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;


class MyListTest {

    private static Stream<Arguments> provideClasses() {
        return Stream.of(
                Arguments.of(new MyArrayList<Integer>()),
                Arguments.of(new MyLinkedList<Integer>())
        );
    }

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @ParameterizedTest
    @MethodSource("provideClasses")
    void addTestShouldReturnNumberOnTheThirdPlace(MyList<Integer> myList) {
        //GIVEN
        int number = 5;
        //WHEN
        myList.add(number);
        //THEN
        assertThat(number, is(myList.get(0)));
    }


    @ParameterizedTest
    @MethodSource("provideClasses")
    void clearTestShouldReturnEmptyList(MyList<Integer> myList) {
        //GIVEN
        myList.add(100);
        myList.add(200);
        myList.add(300);
        //WHEN
        myList.clear();
        //THEN
        assertThat(0, is(myList.size()));
        assertTrue(myList.isEmpty());
    }

    @ParameterizedTest
    @MethodSource("provideClasses")
    void isEmptyTestShouldReturnFalse(MyList<Integer> myList) {
        //GIVEN
        myList.add(100);
        //WHEN
        boolean result = myList.isEmpty();
        //THEN
        assertNotNull(myList);
        assertFalse(result);
    }

    @ParameterizedTest
    @MethodSource("provideClasses")
    void isEmptyTestShouldReturnTrue(MyList<Integer> myList) {
        //GIVEN
        //WHEN
        boolean result = myList.isEmpty();
        //THEN
        assertTrue(result);
    }

    @ParameterizedTest
    @MethodSource("provideClasses")
    void containsTestShouldReturnTrue(MyList<Integer> myList) {
        //GIVEN
        myList.add(200);
        //WHEN
        boolean result = myList.contains(200);
        //THEN
        assertTrue(result);
    }

    @ParameterizedTest
    @MethodSource("provideClasses")
    void containsTestShouldReturnFalse(MyList<Integer> myList) {
        //GIVEN
        //WHEN
        boolean result = myList.contains(800);
        //THEN
        assertFalse(result);
    }

    @ParameterizedTest
    @MethodSource("provideClasses")
    void sizeTestShouldBeThree(MyList<Integer> myList) {
        //GIVEN
        myList.add(100);
        myList.add(200);
        myList.add(300);
        //WHEN
        int length = myList.size();
        //THEN
        assertThat(3, is(length));
    }

    @ParameterizedTest
    @MethodSource("provideClasses")
    void getTestShouldReturnSecondElement(MyList<Integer> myList) {
        //GIVEN
        myList.add(100);
        myList.add(200);
        myList.add(300);
        int index = 1;
        //WHEN
        int secondElement = myList.get(index);
        //THEN
        assertThat(200, is(secondElement));
    }

    @ParameterizedTest
    @MethodSource("provideClasses")
    void getTestShouldReturnArrayIndexOutOfBoundsException(MyList<Integer> myList) {
        //GIVEN
        int index = 100;
        //WHEN
        //THEN
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> myList.get(index));
    }

    @ParameterizedTest
    @MethodSource("provideClasses")
    void addTestShouldReturnNumberFromTheSpecifiedPosition(MyList<Integer> myList) {
        //GIVEN
        myList.add(200);
        myList.add(300);
        int index = 1;
        int number = 800;
        //WHEN
        myList.add(index, number);
        //THEN
        assertThat(number, is(myList.get(index)));
    }

    @ParameterizedTest
    @MethodSource("provideClasses")
    void addTestShouldReturnArrayIndexOutOfBoundsException(MyList<Integer> myList) {
        //GIVEN
        int index = 100;
        int element = 100;
        //WHEN
        //THEN
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> myList.add(index, element));
    }

    @ParameterizedTest
    @MethodSource("provideClasses")
    void setTestShouldReturnOldAndNewValueFromList(MyList<Integer> myList) {
        //GIVEN
        myList.add(100);
        int index = 0;
        int newValue = 800;
        //WHEN
        int oldValue = myList.set(index, newValue);
        //THEN
        assertThat(100, is(oldValue));
        assertThat(newValue, is(myList.get(index)));
    }

    @ParameterizedTest
    @MethodSource("provideClasses")
    void setTestShouldReturnArrayIndexOutOfBoundsException(MyList<Integer> myList) {
        //GIVEN
        int index = 100;
        int newValue = 800;
        //WHEN
        //THEN
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> myList.set(index, newValue));
    }

    @ParameterizedTest
    @MethodSource("provideClasses")
    void removeTestShouldReturnOldAndNewValueAndSizeShouldBeTwo(MyList<Integer> myList) {
        //GIVEN
        int index = 1;
        myList.add(100);
        myList.add(200);
        myList.add(300);
        //WHEN
        int oldValue = myList.remove(index);
        //THEN
        assertThat(200, is(oldValue));
        assertThat(2, is(myList.size()));
        assertThat(300, is(myList.get(index)));
    }

    @ParameterizedTest
    @MethodSource("provideClasses")
    void removeTestShouldReturnArrayIndexOutOfBoundsException(MyList<Integer> myList) {
        //GIVEN
        int index = 100;
        //WHEN
        //THEN
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> myList.remove(index));
    }

    @ParameterizedTest
    @MethodSource("provideClasses")
    void indexOfTestShouldReturnOne(MyList<Integer> myList) {
        //GIVEN
        myList.add(100);
        myList.add(200);
        int element = 200;
        //WHEN
        int index = myList.indexOf(element);
        //THEN
        assertThat(1, is(index));
    }

    @ParameterizedTest
    @MethodSource("provideClasses")
    void indexOfTestShouldReturnMinusOne(MyList<Integer> myList) {
        //GIVEN
        int element = 1000;
        //WHEN
        int index = myList.indexOf(element);
        //THEN
        assertThat(-1, is(index));
    }

    @ParameterizedTest
    @MethodSource("provideClasses")
    void lastIndexOfShouldReturnThree(MyList<Integer> myList) {
        //GIVEN
        myList.add(100);
        myList.add(200);
        myList.add(300);
        myList.add(0, 300);
        //WHEN
        int lastIndex = myList.lastIndexOf(300);
        //THEN
        assertThat(3, is(lastIndex));
    }

    @ParameterizedTest
    @MethodSource("provideClasses")
    void lastIndexOfShouldReturnMinusOne(MyList<Integer> myList) {
        //GIVEN
        //WHEN
        int lastIndex = myList.lastIndexOf(500);
        //THEN
        assertThat(-1, is(lastIndex));
    }
//
//    @Test
//    void testEqualsShouldReturnTrue() {
//        //GIVEN
//        MyList<Integer> newList = myArrayList;
//        //WHEN
//        boolean result = myArrayList.equals(newList);
//        //THEN
//        assertTrue(result);
//    }
//
//    @Test
//    void testEqualsShouldReturnFalse() {
//        //GIVEN
//        MyList<Integer> newList = new MyArrayList<>();
//        newList.add(100);
//        newList.add(200);
//        newList.add(300);
//        //WHEN
//        boolean result = myArrayList.equals(newList);
//        //THEN
//        assertFalse(result);
//    }
//
//    @Test
//    void testToString() {
//        //GIVEN
//        //WHEN
//        String result = myArrayList.get(0).toString();
//        //THEN
//        assertThat("100", is(result));
//    }
}