<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${cookie['lang'].value}"/>
<fmt:setBundle basename="messages" />
<fmt:message key="placeholder.Login" var="loginPlaceholder"/>
<fmt:message key="placeholder.Password" var="passwordPlaceholder"/>
<fmt:message key="sign_in" var="sign_in"/>

<!DOCTYPE html>

<html lang="${cookie['lang'].value}">
    <head>
         <title>Main page</title>

         <style>
            input, button {
                width: 300px;
                height: 30px;
            }
         </style>
    </head>

    <body>
        <c:choose>
            <c:when test="${empty sessionScope.user}">
                <div>
                    <h1><fmt:message key="Login"/></h1>
                    <form action="/login" method="post">
                    <p>
                        <input type="text" name="loginName" width="30" placeholder="${loginPlaceholder}" required/>
                    </p>
                    <p>
                        <input type="password" name="password" width="10" placeholder="${passwordPlaceholder}" / required>
                    </p>
                    <p>
                        <input type="submit" value="${sign_in}" />
                    </p>
                    </form>
                    <a href="/registration">
                        <fmt:message key="label.Registration" />
                    </a>
                    <p style="color:red">${requestScope.errorMessage}</p>
                </div>
            </c:when>
            <c:otherwise>
                <form action="/profile" method="get">
                    <h1>You are already registered!</h1>
                    <button type="submit">View user profile</button>
                </form>
            </c:otherwise>
        </c:choose>
    </body>
</html>