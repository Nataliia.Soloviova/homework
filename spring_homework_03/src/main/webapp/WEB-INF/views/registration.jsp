<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false" %>


<fmt:setLocale value="${cookie['lang'].value}"/>
<fmt:setBundle basename="messages"/>
<fmt:message key="placeholder.Login" var="loginPlaceholder"/>
<fmt:message key="placeholder.Password" var="passwordPlaceholder"/>
<fmt:message key="register" var="register"/>


<!DOCTYPE html>

<html lang="${cookie['lang'].value}">
    <head>
        <title>Registration</title>

        <style>
           input, select {
            width: 300px;
            height: 30px;
           }
        </style>

    </head>
    <body>

        <h1><fmt:message key="Registration"/></h1>

        <form action="/registration" method="post">

            <p><input type="text" name="loginName" width="30" placeholder="${loginPlaceholder}" required/></p>
            <p><input type="password" name="password" width="10" placeholder="${passwordPlaceholder}" required/></p>
            <p><input type="email" name="email" width="30" placeholder="Email" required/></p>
            <p>
                <select name="userRole" width="50" required>
                    <option value=""><fmt:message key="label.Occupation"/></option>
                    <option value="ADMINISTRATOR"><fmt:message key="label.Administrator"/></option>
                    <option value="DISPATCHER"><fmt:message key="label.Dispatcher"/></option>
                </select>
            </p>

            <p><input type="submit" value="${register}"/></p>

        </form>
        <a href="/login">
            <fmt:message key="Login"/>
        </a>

        <p style="color:red">${requestScope.errorMessage}</p>

    </body>
</html>