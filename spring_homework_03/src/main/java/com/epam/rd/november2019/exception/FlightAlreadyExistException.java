package com.epam.rd.november2019.exception;

public class FlightAlreadyExistException extends ApplicationException {

    public FlightAlreadyExistException(String message) {
        super(message);
    }
}
