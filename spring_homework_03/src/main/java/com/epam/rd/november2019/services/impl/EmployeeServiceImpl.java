package com.epam.rd.november2019.services.impl;

import com.epam.rd.november2019.converter.EmployeeConverter;
import com.epam.rd.november2019.dao.EmployeeDao;
import com.epam.rd.november2019.dao.EmployeeScheduleDao;
import com.epam.rd.november2019.exception.EmployeeAlreadyExistException;
import com.epam.rd.november2019.model.Employee;
import com.epam.rd.november2019.model.EmployeeSchedule;
import com.epam.rd.november2019.services.EmployeeService;
import com.epam.rd.november2019.web.dto.EmployeeCreateDto;
import com.epam.rd.november2019.web.dto.EmployeeViewDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private EmployeeDao employeeDao;
    private EmployeeScheduleDao employeeScheduleDao;
    private EmployeeConverter employeeConverter;

    @Autowired
    public EmployeeServiceImpl(EmployeeDao employeeDao,
                               EmployeeScheduleDao employeeScheduleDao,
                               EmployeeConverter employeeConverter) {
        this.employeeDao = employeeDao;
        this.employeeScheduleDao = employeeScheduleDao;
        this.employeeConverter = employeeConverter;
    }

    @Override
    public List<EmployeeViewDto> getAllEmployees() {
        List<EmployeeViewDto> dtoList = new LinkedList<>();
        List<Employee> employees = employeeDao.getAll();
        for (Employee employee :
                employees) {
            dtoList.add(employeeConverter.asEmployeeDto(employee));
        }
        return dtoList;
    }

    @Override
    public EmployeeViewDto addEmployee(EmployeeCreateDto dto) {
        Employee employee = employeeConverter.asEmployee(dto);
        if (employeeDao.getId(employee) != -1) {
            throw new EmployeeAlreadyExistException("This employee already exist!");
        }
        employee = employeeDao.add(employee);
        employeeScheduleDao.add(new EmployeeSchedule(employee.getEmployeeId(), employee.getCity()));

        return employeeConverter.asEmployeeDto(employee);
    }

    @Override
    public boolean removeEmployee(int employeeId) {
        if (employeeDao.remove(employeeId)) {
            return employeeScheduleDao.removeByEmployeeId(employeeId);
        } else {
            return false;
        }
    }

    @Override
    public boolean editEmployee(int employeeId, EmployeeCreateDto dto) {
        String oldStartLocation = employeeDao.getById(employeeId).getCity();
        Employee employee = employeeConverter.asEmployee(dto);
        employee.setEmployeeId(employeeId);
        if (employeeDao.edit(employee)) {
            if (dto.getCity() != null) {
                return employeeScheduleDao.changeStartLocation(employeeId, dto.getCity(), oldStartLocation);
            }
            return true;
        }
        return false;
    }

    @Override
    public EmployeeViewDto getEmployeeById(int employeeId) {
        Employee employee = employeeDao.getById(employeeId);
        return employeeConverter.asEmployeeDto(employee);
    }

    @Override
    public List<EmployeeViewDto> findEmployees(EmployeeCreateDto dto) {
        List<EmployeeViewDto> dtoList = new LinkedList<>();
        List<Employee> employees = employeeDao.findEmployee(employeeConverter.asEmployee(dto));
        for (Employee employee :
                employees) {
            dtoList.add(employeeConverter.asEmployeeDto(employee));
        }
        return dtoList;
    }
}
