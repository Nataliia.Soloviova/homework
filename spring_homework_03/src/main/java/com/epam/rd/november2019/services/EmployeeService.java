package com.epam.rd.november2019.services;

import com.epam.rd.november2019.web.dto.EmployeeCreateDto;
import com.epam.rd.november2019.web.dto.EmployeeViewDto;

import java.util.List;

public interface EmployeeService {

    List<EmployeeViewDto> getAllEmployees();

    EmployeeViewDto addEmployee(EmployeeCreateDto dto);

    boolean removeEmployee(int employeeId);

    boolean editEmployee(int employeeId, EmployeeCreateDto dto);

    EmployeeViewDto getEmployeeById(int employeeId);

    List<EmployeeViewDto> findEmployees(EmployeeCreateDto dto);
}
