package com.epam.rd.november2019.model;

import java.util.Objects;

public class Request {

    private Flight flight;
    private String requestText;
    private EmployeeRole employeeRole;
    private RequestStatus requestStatus;

    public Request(Flight flight, EmployeeRole employeeRole) {
        this.flight = flight;
        this.employeeRole = employeeRole;
        requestText = "Please, find " + employeeRole.toString().toLowerCase() + " for flight " + flight;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public String getRequestText() {
        return requestText;
    }

    public void setRequestText(String requestText) {
        this.requestText = requestText;
    }

    public EmployeeRole getEmployeeRole() {
        return employeeRole;
    }

    public void setEmployeeRole(EmployeeRole employeeRole) {
        this.employeeRole = employeeRole;
    }

    public RequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(RequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Request request = (Request) o;
        return Objects.equals(flight, request.flight) &&
                Objects.equals(requestText, request.requestText) &&
                employeeRole == request.employeeRole &&
                requestStatus == request.requestStatus;
    }

    @Override
    public int hashCode() {
        return Objects.hash(flight, requestText, employeeRole, requestStatus);
    }

    @Override
    public String toString() {
        return requestText;
    }
}
