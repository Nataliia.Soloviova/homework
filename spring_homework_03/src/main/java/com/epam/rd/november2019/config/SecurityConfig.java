package com.epam.rd.november2019.config;

import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Component
public class SecurityConfig {

    private Set<String> protectedURLs = new HashSet<>();
    private Set<String> adminURLs = new HashSet<>();
    private Set<String> dispatcherURLs = new HashSet<>();

    public SecurityConfig() {

        adminURLs.add("/deleteFlight");
        adminURLs.add("/editEmployee");
        adminURLs.add("/admin_menu");

        dispatcherURLs.add("/freeEmployees");
        dispatcherURLs.add("/addFlightTeam");
        dispatcherURLs.add("dispatcher_menu");

        protectedURLs.add("/employees");
        protectedURLs.add("/flights");
        protectedURLs.add("/findFlight");
        protectedURLs.add("/editFlight");
        protectedURLs.add("/findEmployee");
        protectedURLs.add("/deleteFlight");
        protectedURLs.add("/profile");
        protectedURLs.addAll(adminURLs);
        protectedURLs.addAll(dispatcherURLs);
    }

    public Set<String> getProtectedURLs() {
        return Collections.unmodifiableSet(protectedURLs);
    }

    public Set<String> getAdminURLs() {
        return Collections.unmodifiableSet(adminURLs);
    }

    public Set<String> getDispatcherURLs() {
        return Collections.unmodifiableSet(dispatcherURLs);
    }
}
