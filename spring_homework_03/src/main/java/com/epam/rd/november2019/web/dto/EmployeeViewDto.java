package com.epam.rd.november2019.web.dto;

import com.epam.rd.november2019.model.EmployeeRole;
import com.epam.rd.november2019.model.Sex;

import java.io.Serializable;

public class EmployeeViewDto implements Serializable {

    private static final long serialVersionUID = 1853084038109202289L;

    private int employeeId;
    private String lastName;
    private String firstName;
    private EmployeeRole employeeRole;
    private Sex sex;
    private String city;

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public EmployeeRole getEmployeeRole() {
        return employeeRole;
    }

    public void setEmployeeRole(EmployeeRole employeeRole) {
        this.employeeRole = employeeRole;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return String.format("[%s : %s : %s : %s : %s : %s]", employeeId, employeeRole, firstName, lastName, sex, city);
    }
}
