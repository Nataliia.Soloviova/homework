package com.epam.rd.november2019.services.impl;


import com.epam.rd.november2019.converter.UserAccountConverter;
import com.epam.rd.november2019.dao.UserDao;
import com.epam.rd.november2019.exception.UserAlreadyExistException;
import com.epam.rd.november2019.exception.ValidationException;
import com.epam.rd.november2019.model.UserAccount;
import com.epam.rd.november2019.services.SecurityService;
import com.epam.rd.november2019.services.UserService;
import com.epam.rd.november2019.valid.UserValidator;
import com.epam.rd.november2019.web.dto.UserAccountCreateDto;
import com.epam.rd.november2019.web.dto.UserAccountViewDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private final UserDao userDao;
    private final UserValidator userValidator;
    private final UserAccountConverter userAccountConverter;
    private final SecurityService securityService;

    @Autowired
    public UserServiceImpl(UserDao userDao,
                           UserValidator userValidator,
                           UserAccountConverter userAccountConverter,
                           SecurityService securityService) {
        this.userDao = userDao;
        this.userValidator = userValidator;
        this.userAccountConverter = userAccountConverter;
        this.securityService = securityService;
    }

    @Override
    public UserAccountViewDto login(String userName, String password) throws ValidationException {
        userValidator.validateUserCredentials(userName, password);
        UserAccount userAccount = userDao.getByUserName(userName);
        if (userAccount == null) {
            throw new ValidationException("User with this login doesn't exist!");
        }
        if (!securityService.isCorrectPassword(userAccount, password)) {
            throw new ValidationException("Wrong password! Try again!");
        }
        return userAccountConverter.asUserDto(userAccount);
    }

    @Override
    public UserAccountViewDto registerUser(UserAccountCreateDto createDto) throws ValidationException {
        userValidator.validateNewUser(createDto);
        UserAccount userAccount = userAccountConverter.asUserAccount(createDto);
        if (userDao.getByUserName(createDto.getUserName()) != null || userDao.getByEmail(createDto.getEmail()) != null) {
            throw new UserAlreadyExistException("This user already exist! Please login!");
        }
        userAccount = userDao.add(userAccount);
        return userAccountConverter.asUserDto(userAccount);
    }
}
