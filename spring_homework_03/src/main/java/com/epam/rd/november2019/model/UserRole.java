package com.epam.rd.november2019.model;

public enum UserRole {
    ADMINISTRATOR, DISPATCHER;
}