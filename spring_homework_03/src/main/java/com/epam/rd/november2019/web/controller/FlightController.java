package com.epam.rd.november2019.web.controller;

import com.epam.rd.november2019.converter.DateTimeConverter;
import com.epam.rd.november2019.model.FlightStatus;
import com.epam.rd.november2019.services.FlightService;
import com.epam.rd.november2019.web.dto.FlightCreateDto;
import com.epam.rd.november2019.web.dto.FlightViewDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

@Controller
public class FlightController {

    @Autowired
    private FlightService flightService;

    @Autowired
    private DateTimeConverter timeConverter;

    @GetMapping("/flights")
    public String allFlights(Model model) {
        model.addAttribute("flight", new FlightViewDto());
        model.addAttribute("flights", flightService.getAllFlights());

        return "flights";
    }

    @PostMapping(value = "/flights/add")
    public String addFlight(HttpServletRequest req, Model model) {
        FlightCreateDto createDto = extractFlightFromRequest(req);
        flightService.addFlight(createDto);

        return "redirect:/flights";
    }

    private FlightCreateDto extractFlightFromRequest(HttpServletRequest req) {
        String departureCity = req.getParameter("departureCity");
        String departureAirport = req.getParameter("departureAirport");
        String arrivalCity = req.getParameter("arrivalCity");
        String arrivalAirport = req.getParameter("arrivalAirport");
        FlightStatus flightStatus = null;
        if (req.getParameter("flightStatus") != null) {
            flightStatus = FlightStatus.valueOf(req.getParameter("flightStatus").toUpperCase());
        }
        Timestamp departureDate = null;
        Timestamp arrivalDate = null;
        if (!req.getParameter("departureDate").isEmpty()) {
            departureDate = timeConverter.toTimestamp(req.getParameter("departureDate"));
        }
        if (!req.getParameter("arrivalDate").isEmpty()) {
            arrivalDate = timeConverter.toTimestamp(req.getParameter("arrivalDate"));
        }
        return new FlightCreateDto(departureAirport, arrivalAirport, departureCity, arrivalCity, departureDate, arrivalDate, flightStatus);
    }

    @RequestMapping("deleteFlight/{id}")
    public String removeFlight(@PathVariable("id") int id) {
        flightService.removeFlight(id);

        return "redirect:/flights";
    }

    @RequestMapping("editFlight/{id}")
    public String editFlight(@PathVariable("id") int id, Model model) {
        model.addAttribute("flight", flightService.getById(id));
        model.addAttribute("flights", flightService.getAllFlights());

        return "/flights";
    }

    @PostMapping(value = "/editFlight/{id}")
    public String editFlight(@PathVariable("id") int id, HttpServletRequest req) {
        FlightCreateDto createDto = extractFlightFromRequest(req);
        flightService.editFlight(id, createDto);

        return "redirect:/flights";
    }

    @PostMapping("/flights")
    public String findFlight(HttpServletRequest req, Model model) {
        List<FlightViewDto> flights = flightService.findFlights(extractFlightFromRequest(req));
        model.addAttribute("flights", flights);

        return "/flights";
    }

    @PostMapping(value = "/sort_flights")
    public String sortFlight(HttpServletRequest req, Model model) {
        String sortBy = req.getParameter("sortBy");
        List<FlightViewDto> flights = new LinkedList<>();
        if (sortBy.equals("Flight name")) {
            flights = flightService.sortByFlightName();
        } else if (sortBy.equals("Flight id")) {
            flights = flightService.sortById();
        } else {
            flights = flightService.getAllFlights();
        }
        model.addAttribute("flights", flights);

        return "flights";
    }
}
