package com.epam.rd.november2019.dao;

import com.epam.rd.november2019.model.UserAccount;

public interface UserDao {

    UserAccount add(UserAccount userAccount);

    boolean edit(UserAccount userAccount);

    UserAccount getById(int userId);

    UserAccount getByEmail(String email);

    UserAccount getByUserName(String email);
}
