package com.epam.rd.november2019.exception;

public class UserAlreadyExistException extends ApplicationException {


    public UserAlreadyExistException(String message) {
        super(message);
    }
}
