package com.epam.rd.november2019.pojo;

import java.io.*;
import java.util.Objects;

public class ResultFromLog implements Serializable, Comparable<ResultFromLog> {

    private static final long serialVersionUID = 100L;

    private String module;
    private String operation;
    private int executionTime;
    private String time;

    public ResultFromLog(String module, String operation, int executionTime, String time) {
        this.module = module;
        this.operation = operation;
        this.executionTime = executionTime;
        this.time = time;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public int getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(int executionTime) {
        this.executionTime = executionTime;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResultFromLog that = (ResultFromLog) o;
        return Objects.equals(module, that.module) &&
                Objects.equals(operation, that.operation) &&
                Objects.equals(executionTime, that.executionTime) &&
                Objects.equals(time, that.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(module, operation, executionTime, time);
    }

    @Override
    public String toString() {
        return operation + " " + executionTime + " ms, finished at " + time;
    }

    @Override
    public int compareTo(ResultFromLog o) {
        return o.getExecutionTime() - getExecutionTime();
    }

    private void readObject(ObjectInputStream input) throws IOException, ClassNotFoundException {
        input.defaultReadObject();
        module = input.readUTF();
        operation = input.readUTF();
        time = input.readUTF();
        executionTime = input.readInt();
    }

    private void writeObject(ObjectOutputStream output) throws IOException {
        output.defaultWriteObject();
        output.writeUTF(module);
        output.writeUTF(operation);
        output.writeUTF(time);
        output.writeInt(executionTime);
    }
}
