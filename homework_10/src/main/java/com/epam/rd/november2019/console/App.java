package com.epam.rd.november2019.console;

import com.epam.rd.november2019.core.impl.AnalysisLogFileImpl;
import com.epam.rd.november2019.pojo.ResultFromLog;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class App {
    public static void main(String[] args) {
        AnalysisLogFileImpl analysisLogFile = new AnalysisLogFileImpl();

        if (args != null) {
            Pattern patternFile = Pattern.compile("-file, (((\\w+|\\.)(/\\w+)+|\\w+)\\.\\w+)");
            Pattern patternLoad = Pattern.compile("-load, (((\\w+|\\.)(/\\w+)+|\\w+)\\.\\w+)");
            Pattern patternView = Pattern.compile("-view, (\\d+)");
            Pattern patternSave = Pattern.compile("-save, (((\\w+|\\.)(/\\w+)+|\\w+)\\.\\w+)");

            Matcher matcherFile = patternFile.matcher(Arrays.toString(args));
            Matcher matcherLoad = patternLoad.matcher(Arrays.toString(args));
            Matcher matcherView = patternView.matcher(Arrays.toString(args));
            Matcher matcherSave = patternSave.matcher(Arrays.toString(args));

            List<String> stringLogsList = null;
            List<ResultFromLog> listResult = null;
            int quantityLogsLines = 0;

            if (matcherFile.find()) {
                String fileName = matcherFile.group(1);
                stringLogsList = analysisLogFile.readLinesFromFile(fileName);
            }

            if (matcherView.find()) {
                if (stringLogsList != null) {
                    listResult = analysisLogFile.createObjectListFromLog(stringLogsList);
                }
                quantityLogsLines = Integer.parseInt(matcherView.group(1));
                analysisLogFile.printNResults(listResult, quantityLogsLines);
            }

            if (matcherSave.find()) {
                try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(matcherSave.group(1)))) {
                    for (ResultFromLog resultFromLog :
                            analysisLogFile.savedResults(listResult, quantityLogsLines)) {
                        objectOutputStream.writeObject(resultFromLog);
                    }
                } catch (IOException ex) {
                    ex.getStackTrace();
                }
            }

            if (matcherLoad.find()) {
                List<ResultFromLog> resultFromLogList = new ArrayList<>();
                try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(matcherLoad.group(1)))) {
                    ResultFromLog resultFromLog;
                    while ((resultFromLog = (ResultFromLog) objectInputStream.readObject()) != null) {
                        resultFromLogList.add(resultFromLog);
                    }
                } catch (EOFException e) {
                    e.getStackTrace();
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
                analysisLogFile.printResults(resultFromLogList);
            }
        }
    }
}
