package com.epam.rd.november2019.core;

import com.epam.rd.november2019.pojo.ResultFromLog;

import java.util.List;

/**
 * The {@code AnalysisLogFile} interface provides methods for analysing
 * log-file.
 *
 * @author unascribed
 * @version 1.0
 */
public interface AnalysisLogFile {

    /**
     * This method reads all lines from file.
     *
     * @param fileName path to log-file
     * @return string list with all lines from file
     */
    List<String> readLinesFromFile(String fileName);

    /**
     * This method creates list with ResultFromLog objects.
     *
     * @param stringList with all lines from log-file
     * @return ResultFromLog list
     */
    List<ResultFromLog> createObjectListFromLog(List<String> stringList);

    /**
     * This method sorts list with ResultFromLog in natural order by module.
     *
     * @param listResult ResultFromLog list
     * @param module module's name
     * @return sorted list in natural order by module
     */
    List<ResultFromLog> sortByModule(List<ResultFromLog> listResult, String module);

    /**
     * Returns new ResultFromLog list with specific length. Specific length is sum of
     * object's quantity in every module.
     *
     * @param listResult ResultFromLog list
     * @param n quantity objects in every module
     * @return new ResultFromLog list with specific length
     */
    List<ResultFromLog> savedResults(List<ResultFromLog> listResult, int n);

    /**
     * Prints in the console ResultFromLog list with specific length. Specific length is sum of
     * object's quantity in every module.
     *
     * @param listResult ResultFromLog list
     * @param n quantity objects in every module
     */
    void printNResults(List<ResultFromLog> listResult, int n);

    /**
     * Prints in the console ResultFromLog list.
     *
     * @param listResult ResultFromLog list
     */
    void printResults(List<ResultFromLog> listResult);
}
