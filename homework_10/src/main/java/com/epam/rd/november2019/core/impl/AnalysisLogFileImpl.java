package com.epam.rd.november2019.core.impl;

import com.epam.rd.november2019.core.AnalysisLogFile;
import com.epam.rd.november2019.pojo.Module;
import com.epam.rd.november2019.pojo.ResultFromLog;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * The class {@code AnalysisLogFileImpl} contains methods for analysing
 * log-file.
 *
 * The class {@code AnalysisLogFileImpl} implementation of the {@code AnalysisLogFile}
 * interface. Implements and overrides all operations.
 *
 * @author unascribed
 * @see AnalysisLogFile
 * @version 1.0
 */
public class AnalysisLogFileImpl implements AnalysisLogFile {

    @Override
    public List<String> readLinesFromFile(String fileName) {
        Path path = Paths.get(fileName);
        try {
            return Files.readAllLines(path);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<ResultFromLog> createObjectListFromLog(List<String> stringList) {
        List<ResultFromLog> listResult = new ArrayList<ResultFromLog>();
        for (String s : stringList) {

            String module = moduleFromLogLine(s);
            String operation = operationFromLogLine(s);
            int executionTime = executionTimeFromLogLine(s);
            String time = timeFromLogLine(s);

            listResult.add(new ResultFromLog(module, operation, executionTime, time));
        }
        return listResult;
    }

    @Override
    public List<ResultFromLog> sortByModule(List<ResultFromLog> listResult, String module) {
        return listResult.stream().filter(x -> x.getModule().equals(module)).sorted(Comparator.naturalOrder()).collect(Collectors.toList());
    }

    @Override
    public List<ResultFromLog> savedResults(List<ResultFromLog> listResult, int n) {
        List<ResultFromLog> list = new ArrayList<>();
        for (Module module:
             Module.values()) {
            for (int i = 0; i < n; i++) {
                list.add(sortByModule(listResult, module.toString()).get(i));
            }
        }
        return list;
    }

    @Override
    public void printResults(List<ResultFromLog> listResult) {
        for (Module module :
                Module.values()) {
            System.out.println(module + " Module:");
            for (ResultFromLog resultFromLog :
                    sortByModule(listResult, module.toString())) {
                System.out.println("\t\t" + resultFromLog.toString());
            }
        }
    }

    @Override
    public void printNResults(List<ResultFromLog> listResult, int n) {
        for (Module module:
             Module.values()) {
            System.out.println(module + " Module:");
            for (int i = 0; i < n; i++) {
                System.out.println("\t\t" + sortByModule(listResult, module.toString()).get(i));
            }
        }
    }

    private String timeFromLogLine(String logLine) {
        Pattern pattern = Pattern.compile("^(\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}.\\d{3})");
        Matcher m = pattern.matcher(logLine);
        if (m.find()) {
            return m.group();
        }
        return null;
    }

    private String moduleFromLogLine(String logLine) {
        Pattern pattern = Pattern.compile("Module=([A-Z]{3})");
        Matcher m = pattern.matcher(logLine);
        if (m.find()) {
            return m.group(1);
        }
        return null;
    }

    private String operationFromLogLine(String logLine) {
        Pattern pattern = Pattern.compile("Operation: (Login|Find_Product|Get_Order|Logout|Save_Order)");
        Matcher m = pattern.matcher(logLine);
        if (m.find()) {
            return m.group(1);
        }
        return null;
    }

    private int executionTimeFromLogLine(String logLine) {
        Pattern pattern = Pattern.compile("Execution time: (\\d+) ms");
        Matcher m = pattern.matcher(logLine);
        if (m.find()) {
            return Integer.parseInt(m.group(1));
        }
        return -1;
    }
}
