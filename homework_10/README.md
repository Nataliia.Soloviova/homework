# homework_10
This program analyzes log-file and displays statistics: N longest operations for each module, duration of operation and 
time when operation ended.
Operations are sorted by module name and in decreasing order of execution time.

### To build project execute in cmd:
* mvn clean install
### To run application execute in cmd:
* java -jar .\target\logAnalysis-jar-with-dependencies.jar 

### Commands:
* -file <path_to_logs_file> 
* -load <path_to_saved_results>
* -view N
* -save <path_to_saved_results>

### Checking:
* java -jar .\target\logAnalysis-jar-with-dependencies.jar -file ./src/main/resources/logs.txt -view 3 -save ./src/main/resources/saved_result.txt
* java -jar .\target\logAnalysis-jar-with-dependencies.jar -load ./src/main/resources/saved_result.txt