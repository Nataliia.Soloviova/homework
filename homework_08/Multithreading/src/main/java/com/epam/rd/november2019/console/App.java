package com.epam.rd.november2019.console;

import com.epam.rd.november2019.core.PrimeChecker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class App {
    public static void main(String[] args) {

        int numberOfThreads = 0;
        int firstNumber = 0;
        int lastNumber = 0;
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
            try {
                System.out.println("Please, enter a range of numbers.");
                System.out.println("First number:");
                firstNumber = Integer.parseInt(bufferedReader.readLine());
                System.out.println("Second number:");
                lastNumber = Integer.parseInt(bufferedReader.readLine());
                System.out.println("Number of Threads:");
                numberOfThreads = Integer.parseInt(bufferedReader.readLine());
            } catch (NumberFormatException e) {
                System.err.println("Invalid input data! Please, enter three numbers!");
            }
        } catch (IOException e) {
            throw new RuntimeException();
        }

        long startTime = System.currentTimeMillis();
        PrimeChecker.getFromDifferentLists(numberOfThreads, firstNumber, lastNumber);
        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println("Execution time getting from different lists = : " + elapsedTime);

        long startTime1 = System.currentTimeMillis();
        PrimeChecker.getFromOneList(numberOfThreads, firstNumber, lastNumber);
        long stopTime1 = System.currentTimeMillis();
        long elapsedTime1 = stopTime1 - startTime1;
        System.out.println("Execution time getting from one list = : " + elapsedTime1);
    }
}
