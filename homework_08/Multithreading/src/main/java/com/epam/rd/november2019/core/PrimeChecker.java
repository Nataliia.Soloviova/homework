package com.epam.rd.november2019.core;

import java.util.ArrayList;
import java.util.List;

public class PrimeChecker extends Thread {

    private int step, last, current;

    private List<Integer> primes;

    public PrimeChecker(int start, int step, int last, List<Integer> primes) {
        this.step = step;
        this.last = last;
        this.primes = primes;
        current = start;
    }

    private boolean isPrime(int number) {
        if (number <= 1) {
            return false;
        } else {
            for (int i = 2; i < number; i++) {
                if (number % i == 0) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void run() {
        while (current <= last) {
            if (isPrime(current)) {
                primes.add(current);
            }
            current += step;
        }
    }

    public static List<Integer> getFromDifferentLists(int numberOfThreads, int firstNumber, int lastNumber) {
        List<Integer> list = new ArrayList<Integer>();

        PrimeChecker[] checkers = new PrimeChecker[numberOfThreads];
        List[] results = new List[numberOfThreads];
        for (int i = 0; i < numberOfThreads; i++) {
            List<Integer> currentResult = new ArrayList<Integer>(list);
            checkers[i] = new PrimeChecker(firstNumber + i, numberOfThreads, lastNumber, currentResult);
            results[i] = currentResult;
        }

        checkers[0].run();
        for (int i = 1; i < numberOfThreads; i++) {
            checkers[i].start();
        }

        try {
            for (int i = 0; i < numberOfThreads; i++) {
                checkers[i].join();
            }
        } catch (InterruptedException ignored) {
        }

        for (int i = 0; i < numberOfThreads; i++) {
            for (int j = 0; j < results[i].size(); j++) {
                list.add((Integer) results[i].get(j));
            }
        }
        return list;
    }

    public static List<Integer> getFromOneList(int numberOfThreads, int firstNumber, int lastNumber) {
        List<Integer> list = new ArrayList<Integer>();

        PrimeChecker[] checkers = new PrimeChecker[numberOfThreads];
        for (int i = 0; i < numberOfThreads; i++) {
            checkers[i] = new PrimeChecker(firstNumber + i, numberOfThreads, lastNumber, list);
        }

        checkers[0].run();
        for (int i = 1; i < numberOfThreads; i++) {
            checkers[i].start();
        }

        try {
            for (int i = 0; i < numberOfThreads; i++) {
                checkers[i].join();
            }
        } catch (InterruptedException ignored) {
        }

        return list;
    }
}
