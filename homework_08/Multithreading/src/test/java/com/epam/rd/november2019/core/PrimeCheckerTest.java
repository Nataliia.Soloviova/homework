package com.epam.rd.november2019.core;


import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class PrimeCheckerTest {


    @Test
    void run() {
        //GIVEN
        int numberOfThreads = 4;
        int firstNumber = 1;
        int lastNumber = 10000;
        List<Integer> list = new ArrayList<Integer>();
        PrimeChecker primeChecker = new PrimeChecker(firstNumber, 1, lastNumber, list);
        //WHEN
        primeChecker.run();
        //THEN
        assertThat(1229, is(list.size()));
    }

    @Test
    void getFromDifferentListsTestShouldReturnQuantityOfPrimes() {
        //GIVEN
        int numberOfThreads = 4;
        int firstNumber = 1;
        int lastNumber = 10000;
        //WHEN
        List<Integer> result = PrimeChecker.getFromDifferentLists(numberOfThreads, firstNumber, lastNumber);
        //THEN
        assertThat(1229, is(result.size()));
    }

    @Test
    void getFromOneList() {
        //GIVEN
        int numberOfThreads = 4;
        int firstNumber = 1;
        int lastNumber = 10000;
        //WHEN
        List<Integer> result = PrimeChecker.getFromOneList(numberOfThreads, firstNumber, lastNumber);
        //THEN
        assertThat(1229, is(result.size()));
    }
}