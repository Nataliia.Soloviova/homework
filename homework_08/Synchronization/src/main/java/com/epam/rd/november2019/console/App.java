package com.epam.rd.november2019.console;

import com.epam.rd.november2019.core.FileService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class App {
    public static void main(String[] args) {

        String fileName = readFileName();

        Thread writeThread = new Thread(new Runnable() {
            @Override
            public void run() {
                FileService.writingIntoFileAccounts(fileName);
            }
        }, "A");

        Thread readThread = new Thread(new Runnable() {
            @Override
            public void run() {
                FileService.readingFile(fileName);
            }
        }, "B");

        writeThread.start();
        readThread.start();

    }

    private static String readFileName() {
        String fileName = "";
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.println("Please, enter path to your file: ");
            fileName = bufferedReader.readLine();
        } catch (IOException e) {
            e.getStackTrace();
        }
        return fileName;
    }
}
