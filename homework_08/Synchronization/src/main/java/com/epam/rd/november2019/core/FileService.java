package com.epam.rd.november2019.core;

import com.epam.rd.november2019.pojo.Account;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class FileService {

    private static final Logger logger = LoggerFactory.getLogger(FileService.class);

    public static synchronized void writingIntoFileAccounts(String fileName) {
        try (FileOutputStream fileOutputStream = new FileOutputStream(fileName);
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {

            logger.info("File Writing started by - {}", Thread.currentThread().getName());

            for (int i = 0; i < 10; i++) {
                Account ts = new Account();
                objectOutputStream.writeObject(ts);
            }

            logger.info("File Writing ended by - {}", Thread.currentThread().getName());

        } catch (IOException e) {
            e.getStackTrace();
        }
    }

    public static synchronized List<Object> readingFile(String fileName) {

        List<Object> list = new ArrayList<>();
        Object object;

        try (FileInputStream fileInputStream = new FileInputStream(fileName);
             ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
            logger.info("File Reading started by - {}", Thread.currentThread().getName());

            while ((object = objectInputStream.readObject()) != null) {
                list.add(object);
            }

        } catch (EOFException e) {
            e.getStackTrace();
            logger.info("File Reading ended by - {}", Thread.currentThread().getName());
        } catch (ClassNotFoundException | IOException e) {
            e.getStackTrace();
        }
        return list;
    }

}
