package com.epam.rd.november2019.core;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;


class MyLinkedListTest {

    private MyLinkedList<Integer> myLinkedList;

    @BeforeEach
    void setUp() {
        myLinkedList = new MyLinkedList<>();
        myLinkedList.add(100);
        myLinkedList.add(200);
        myLinkedList.add(300);
    }

    @AfterEach
    void tearDown() {
        myLinkedList.clear();
    }

    @Test
    void addFirstShouldReturnElement() {
        //GIVEN
        int element = 600;
        //WHEN
        myLinkedList.addFirst(element);
        //THEN
        assertThat(element, is(myLinkedList.get(0)));
    }

    @Test
    void addLastShouldReturnElement() {
        //GIVEN
        int element = 600;
        //WHEN
        myLinkedList.addLast(element);
        //THEN
        assertThat(element, is(myLinkedList.get(myLinkedList.size() - 1)));
    }

    @Test
    void getFirstShouldReturnFirstElement() {
        //GIVEN
        int firstElement = 555;
        myLinkedList.addFirst(firstElement);
        //WHEN
        int result = myLinkedList.getFirst();
        //THEN
        assertThat(firstElement, is(result));
    }

    @Test
    void getFirstShouldReturnNoSuchElementException() {
        //GIVEN
        myLinkedList.clear();
        //WHEN
        //THEN
        assertThrows(NoSuchElementException.class, () -> myLinkedList.getFirst());
    }

    @Test
    void getLastShouldReturnLastElement() {
        //GIVEN
        int lastElement = 555;
        myLinkedList.addLast(lastElement);
        //WHEN
        int result = myLinkedList.getLast();
        //THEN
        assertThat(lastElement, is(result));
    }

    @Test
    void getLastShouldReturnNoSuchElementException() {
        //GIVEN
        myLinkedList.clear();
        //WHEN
        //THEN
        assertThrows(NoSuchElementException.class, () -> myLinkedList.getLast());
    }

    @Test
    void removeFirstShouldReturnFirstElementFromTheListThatWasRemoved() {
        //GIVEN
        int expected = myLinkedList.getFirst();
        //WHEN
        int result = myLinkedList.removeFirst();
        //THEN
        assertThat(expected, is(result));
    }

    @Test
    void removeFirstShouldReturnNoSuchElementException() {
        //GIVEN
        myLinkedList.clear();
        //WHEN
        //THEN
        assertThrows(NoSuchElementException.class, () -> myLinkedList.removeFirst());
    }

    @Test
    void removeLastShouldReturnLastElementFromTheListThatWasRemoved() {
        //GIVEN
        int expected = myLinkedList.getLast();
        //WHEN
        int result = myLinkedList.removeLast();
        //THEN
        assertThat(expected, is(result));
    }

    @Test
    void removeLastShouldReturnNoSuchElementException() {
        //GIVEN
        myLinkedList.clear();
        //WHEN
        //THEN
        assertThrows(NoSuchElementException.class, () -> myLinkedList.removeLast());
    }

    @Test
    void testEqualsShouldReturnTrue() {
        //GIVEN
        MyLinkedList<Integer> newList = myLinkedList;
        //WHEN
        boolean result = myLinkedList.equals(newList);
        //THEN
        assertTrue(result);
    }

    @Test
    void testEqualsShouldReturnFalse() {
        //GIVEN
        MyLinkedList<Integer> newList = new MyLinkedList<>();
        newList.add(100);
        newList.add(200);
        newList.add(300);
        //WHEN
        boolean result = myLinkedList.equals(newList);
        //THEN
        assertFalse(result);
    }

    @Test
    void testToStringShouldReturnSuchString() {
        //GIVEN
        //WHEN
        String result = myLinkedList.toString();
        //THEN
        assertThat("[100, 200, 300]", is(result));
    }
}