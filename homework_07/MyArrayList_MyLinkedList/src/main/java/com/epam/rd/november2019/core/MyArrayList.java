package com.epam.rd.november2019.core;

import com.epam.rd.november2019.interfaces.MyList;


/**
 * Resizable-array implementation of the {@code MyList} interface.
 *
 * @param <E> the type of elements in this list
 * @author Nataliia Soloviova
 * @version 1.0
 * @see MyList
 * @see MyLinkedList
 */
public class MyArrayList<E> implements MyList<E> {

    /**
     * There are stored elements of the MyArrayList in array.
     */
    private Object[] list;

    /**
     * The number of elements MyArrayList contains
     */
    private int size;

    /**
     * Default initial size.
     */
    private static final int DEFAULT_CAPACITY = 0;

    /**
     * Empty array instance sed for empty instances.
     */
    private static final Object[] DEFAULT_LIST = {};

    /**
     * Default initial ArrayList.
     */
    private static final Object[] DEFAULT_CAPACITY_EMPTY_LIST = new Object[DEFAULT_CAPACITY];


    /**
     * Constructs an empty list with the specified initial capacity.
     */
    public MyArrayList() {
        this.list = DEFAULT_CAPACITY_EMPTY_LIST;
    }

    /**
     * Constructs an empty list.
     *
     * @param initialCapacity the initial capacity of the list.
     * @throws IllegalArgumentException if the specified initial capacity
     *                                  is negative.
     */
    public MyArrayList(int initialCapacity) {
        if (initialCapacity > 0) {
            this.list = createArray(initialCapacity);
        } else if (initialCapacity == 0) {
            this.list = DEFAULT_LIST;
        } else {
            throw new IllegalArgumentException("Illegal Capacity: " +
                    initialCapacity);
        }
    }

    /**
     * Returns the number of elements in this list.
     *
     * @return the number of elements in this list
     */
    public int size() {
        return size;
    }

    /**
     * Returns the element at the specified position in this list.
     *
     * @param index index of the element to return
     * @return the element at the specified position in this list
     * @throws ArrayIndexOutOfBoundsException
     */
    public E get(int index) {
        checkIndex(index);
        return list(index);
    }

    /**
     * Appends the specified element to the end of this list.
     *
     * @param element element to be appended to this list
     */
    public void add(E element) {
        E[] array = createArray(list.length + 1);
        System.arraycopy(list, 0, array, 0, list.length);
        array[array.length - 1] = element;
        list = array;
        size++;
    }

    /**
     * Inserts the specified element at the specified position in this
     * list.
     *
     * @param index   index at which the specified element is to be inserted
     * @param element element to be inserted
     * @throws ArrayIndexOutOfBoundsException
     */
    public void add(int index, E element) {
        checkIndex(index);
        E[] array = createArray(list.length + 1);
        for (int i = 0; i < list.length; i++) {
            if (i == index) {
                E e = list(i);
                array[i + 1] = e;
                array[i] = element;
                for (int j = 0; j < index; j++) {
                    array[j] = list(j);
                }
                for (int j = index + 2; j < array.length; j++) {
                    array[j] = list(j - 1);
                }
                list = array;
                size++;
            }
        }
    }

    /**
     * Replaces the element at the specified position in this list with
     * the specified element.
     *
     * @param index   index of the element to replace
     * @param element element to be stored at the specified position
     * @return the element previously at the specified position
     * @throws ArrayIndexOutOfBoundsException
     */
    public E set(int index, E element) {
        checkIndex(index);
        E oldValue = list(index);
        list[index] = element;
        return oldValue;
    }

    /**
     * Removes the element at the specified position in this list.
     *
     * @param index the index of the element to be removed
     * @return the element that was removed from the list
     * @throws ArrayIndexOutOfBoundsException
     */
    public E remove(int index) {
        checkIndex(index);
        E oldValue = list(index);
        E[] array = createArray(list.length - 1);
        System.arraycopy(list, index + 1, list, index, list.length - index - 1);
        for (int i = 0; i < array.length; i++) {
            array[i] = list(i);
        }
        list = array;
        size--;
        return oldValue;
    }

    /**
     * Returns the index of the first occurrence of the specified element
     * in this list, or -1 if this list does not contain the element.
     *
     * @param element element to be find at the specified position
     * @return index of the specified element or -1
     */
    public int indexOf(E element) {
        for (int i = 0; i < list.length; i++) {
            if (element == null) {
                if (element == list(i)) {
                    return i;
                }
            } else {
                if (element.equals(list(i))) {
                    return i;
                }
            }
        }
        return -1;
    }

    /**
     * Returns the index of the last occurrence of the specified element
     * in this list, or -1 if this list does not contain the element.
     *
     * @param element element to be find at the specified position
     * @return index of the specified element or -1
     */
    public int lastIndexOf(E element) {
        for (int i = list.length - 1; i >= 0; i--) {
            if (element == null) {
                if (element == list(i)) {
                    return i;
                }
            } else {
                if (element.equals(list(i))) {
                    return i;
                }
            }
        }
        return -1;
    }

    /**
     * Removes all of the elements from this list.
     */
    public void clear() {
        Object[] array = list;
        for (int i = 0; i < size; i++) {
            array[i] = null;
        }
        size = 0;
    }

    /**
     * Checks if the index belongs to the array.
     *
     * @param index the index of the element
     * @throws ArrayIndexOutOfBoundsException
     */
    private void checkIndex(int index) {
        if ((index < 0) || (index >= size)) {
            throw new ArrayIndexOutOfBoundsException("Index: " + index + ", Size: " + size);
        }
    }

    @SuppressWarnings({"unchecked"})
    private E list(int index) {
        return (E) list[index];
    }

    @SuppressWarnings({"unchecked"})
    private E[] createArray(int index) {
        return (E[]) new Object[index];
    }

    @Override
    public boolean equals(Object o) {
        boolean result = false;
        if (o == this) {
            result = true;
        }
        if (!(o instanceof MyList)) {
            result = false;
        }
        return result;
    }

    @Override
    public int hashCode() {
        final Object[] array = list;
        int hashCode = 1;
        for (int i = 0; i < size; i++) {
            Object e = array[i];
            hashCode = 31 * hashCode + (e == null ? 0 : e.hashCode());
        }
        return hashCode;
    }

    @Override
    public String toString() {
        String s = "";
        String result = "";
        if (!isEmpty()) {
            for (int i = 0; i < size - 1; i++) {
                s += get(i) + ", ";
            }
            result = "[" + s + get(size - 1) + "]";
        } else {
            result = null;
        }
        return result;
    }
}

