package com.epam.rd.november2019.interfaces;

/**
 * The user of this interface has precise control over where in the
 * list each element is inserted.  The user can access elements by
 * their integer index (position in the list), and search for elements
 * in the list.
 *
 * @param <E> the type of elements in this list
 * @author unascribed
 * @see MyDeque
 * @since 1.0
 */
public interface MyList<E> {

    /**
     * Appends the specified element to the end of this list.
     *
     * @param element element to be appended to this list
     */
    void add(E element);

    /**
     * Returns the element at the specified position in this list.
     *
     * @param index index of the element to return
     * @return the element at the specified position in this list
     * @throws ArrayIndexOutOfBoundsException
     * @throws IllegalArgumentException
     */
    E get(int index);

    /**
     * Returns the number of elements in this list.
     *
     * @return the number of elements in this list
     */
    int size();

    /**
     * This implementation returns {@code size() == 0}.
     */
    default boolean isEmpty() {
        return size() == 0;
    }

    /**
     * Inserts the specified element at the specified position in this
     * list.
     *
     * @param index   index at which the specified element is to be inserted
     * @param element element to be inserted
     * @throws ArrayIndexOutOfBoundsException
     * @throws IllegalArgumentException
     */
    void add(int index, E element);

    /**
     * Removes the element at the specified position in this list.
     *
     * @param index the index of the element to be removed
     * @return the element previously at the specified position
     * @throws IllegalArgumentException
     * @throws ArrayIndexOutOfBoundsException
     */
    E remove(int index);

    /**
     * Replaces the element at the specified position in this list with
     * the specified element.
     *
     * @param index   index of the element to replace
     * @param element element to be stored at the specified position
     * @return the element previously at the specified position
     * @throws ArrayIndexOutOfBoundsException
     * @throws IndexOutOfBoundsException
     */
    E set(int index, E element);

    /**
     * Returns the index of the first occurrence of the specified element
     * in this list, or -1 if this list does not contain the element.
     *
     * @param element element to search for
     * @return the index of the first occurrence of the specified element in
     * this list, or -1 if this list does not contain the element
     */
    int indexOf(E element);

    /**
     * Returns the index of the last occurrence of the specified element
     * in this list, or -1 if this list does not contain the element.
     *
     * @param element element to search for
     * @return the index of the last occurrence of the specified element in
     * this list, or -1 if this list does not contain the element
     */
    int lastIndexOf(E element);

    /**
     * Returns {@code true} if this list contains the specified element.
     *
     * @param element element to be find at this list
     * @return {@code true} if this list contains the specified element
     */
    default boolean contains(E element) {
        return indexOf(element) >= 0;
    }

    /**
     * Removes all of the elements from this list.
     */
    void clear();
}

