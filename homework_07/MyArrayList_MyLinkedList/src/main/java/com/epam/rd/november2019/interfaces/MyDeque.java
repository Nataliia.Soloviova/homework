package com.epam.rd.november2019.interfaces;

import java.util.NoSuchElementException;

/**
 * A linear collection that supports element insertion and removal
 * at both ends.
 *
 * @param <E> the type of elements held in this deque
 * @author unascribed
 * @since 1.0
 */
public interface MyDeque<E> {

    /**
     * Inserts the specified element at the beginning of this list.
     *
     * @param e the element to add.
     */
    void addFirst(E e);

    /**
     * Appends the specified element to the end of this list.
     *
     * @param e the element to add.
     */
    void addLast(E e);

    /**
     * Returns the first element in this list.
     *
     * @return the first element in this list
     * @throws NoSuchElementException if this list is empty
     */
    E getFirst();

    /**
     * Returns the last element in this list.
     *
     * @return the last element in this list
     * @throws NoSuchElementException if this list is empty
     */
    E getLast();

    /**
     * Removes and returns the first element from this list.
     *
     * @return the first element from this list
     * @throws NoSuchElementException if this list is empty
     */
    E removeFirst();

    /**
     * Removes and returns the last element from this list.
     *
     * @return the last element from this list
     * @throws NoSuchElementException if this list is empty
     */
    E removeLast();
}
