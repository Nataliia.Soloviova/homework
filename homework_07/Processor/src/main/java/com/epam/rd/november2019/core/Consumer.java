package com.epam.rd.november2019.core;

public class Consumer {
    public void consume(String value) {
        System.out.println("Consumed -> " + value);
    }
}
