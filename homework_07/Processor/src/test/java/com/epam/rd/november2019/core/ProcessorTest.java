package com.epam.rd.november2019.core;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class ProcessorTest {
    @Mock
    private Producer producer;

    @Mock
    private Consumer consumer;

    @InjectMocks
    private Processor processor = new Processor();

    @Captor
    private ArgumentCaptor<String> captor;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void whenCalledProducerProduceShouldCapturedString() {
        //GIVEN
        String expected = "Magic value";
        when(producer.produce()).thenCallRealMethod();
        //WHEN
        processor.process();
        //THEN
        verify(consumer).consume(captor.capture());
        verifyNoMoreInteractions(consumer);
        String captured = captor.getValue();
        assertEquals(expected, captured);
    }

    @Test
    void whenCalledProcessorProcessShouldBeUsedOneTime() {
        //GIVEN
        when(producer.produce()).thenCallRealMethod();
        //WHEN
        processor.process();
        //THEN
        verify(producer, times(1)).produce();
        verifyNoMoreInteractions(producer);
    }

    @Test
    void whenCalledProducerProduceThatReturnsNullShouldThrowsIllegalStateException() {
        //GIVEN
        when(producer.produce()).thenReturn(null);
        //WHEN
        //THEN
        assertThrows(IllegalStateException.class, () -> processor.process());
        verify(producer, times(1)).produce();
        verifyNoMoreInteractions(producer);
    }

    @Test
    void whenCalledProducerProduceThatReturnsAnotherStringShouldBeUsedOneTime() {
        //GIVEN
        String expected = "Action";
        when(producer.produce()).thenReturn(expected);
        //WHEN
        processor.process();
        //THEN
        verify(producer, times(1)).produce();
        verifyNoMoreInteractions(producer);
    }
}