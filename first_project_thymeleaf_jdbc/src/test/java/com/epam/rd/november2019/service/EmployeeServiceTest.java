package com.epam.rd.november2019.service;

import com.epam.rd.november2019.converter.EmployeeConverter;
import com.epam.rd.november2019.dao.EmployeeDao;
import com.epam.rd.november2019.dao.EmployeeScheduleDao;
import com.epam.rd.november2019.exception.EmployeeAlreadyExistException;
import com.epam.rd.november2019.model.Employee;
import com.epam.rd.november2019.model.EmployeeRole;
import com.epam.rd.november2019.model.EmployeeSchedule;
import com.epam.rd.november2019.model.Sex;
import com.epam.rd.november2019.service.impl.EmployeeServiceImpl;
import com.epam.rd.november2019.web.dto.EmployeeCreateDto;
import com.epam.rd.november2019.web.dto.EmployeeViewDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


public class EmployeeServiceTest {

    private static final int ID = 1;
    private static final String LAST_NAME = "Johnson";
    private static final String FIRST_NAME = "John";
    private static final EmployeeRole EMPLOYEE_ROLE = EmployeeRole.RADIO_OPERATOR;
    private static final Sex SEX = Sex.MAN;
    private static final String CITY = "New York";

    private final EmployeeDao employeeDao = mock(EmployeeDao.class);
    private final EmployeeScheduleDao employeeScheduleDao = mock(EmployeeScheduleDao.class);
    private final EmployeeConverter employeeConverter = mock(EmployeeConverter.class);
    private final EmployeeService employeeService = new EmployeeServiceImpl(employeeDao, employeeScheduleDao, employeeConverter);

    private Employee employee;
    private EmployeeCreateDto createDto;
    private EmployeeViewDto viewDto;
    private EmployeeSchedule employeeSchedule;

    @BeforeEach
    public void setUp() {
        employee = new Employee();
        employee.setEmployeeId(ID);
        employee.setFirstName(FIRST_NAME);
        employee.setLastName(LAST_NAME);
        employee.setSex(SEX);
        employee.setEmployeeRole(EMPLOYEE_ROLE);
        employee.setCity(CITY);

        createDto = new EmployeeCreateDto();
        createDto.setEmployeeId(ID);
        createDto.setFirstName(FIRST_NAME);
        createDto.setLastName(LAST_NAME);
        createDto.setSex(SEX);
        createDto.setEmployeeRole(EMPLOYEE_ROLE);
        createDto.setCity(CITY);

        viewDto = new EmployeeViewDto();
        viewDto.setEmployeeId(ID);
        viewDto.setFirstName(FIRST_NAME);
        viewDto.setLastName(LAST_NAME);
        viewDto.setSex(SEX);
        viewDto.setEmployeeRole(EMPLOYEE_ROLE);
        viewDto.setCity(CITY);

        employeeSchedule = new EmployeeSchedule(ID, CITY);
    }

    @Test
    public void getAllEmployeesTestShouldReturnList() {
        //Given
        when(employeeDao.getAll()).thenReturn(Collections.singletonList(employee));
        when(employeeConverter.asEmployeeDto(employee)).thenReturn(viewDto);

        //When
        List<EmployeeViewDto> result = employeeService.getAllEmployees();

        //Then
        verify(employeeDao, times(1)).getAll();
        verify(employeeConverter, times(1)).asEmployeeDto(employee);
        verifyNoMoreInteractions(employeeDao, employeeConverter);

        assertEquals(result, Collections.singletonList(viewDto));
    }

    @Test
    void addEmployeeTestShouldCreateEmployee() {
        //Given
        when(employeeConverter.asEmployee(createDto)).thenReturn(employee);
        when(employeeDao.getId(employee)).thenReturn(-1);
        when(employeeDao.add(employee)).thenReturn(employee);
        when(employeeConverter.asEmployeeDto(employee)).thenReturn(viewDto);

        //When
        EmployeeViewDto result = employeeService.addEmployee(createDto);

        //Then
        verify(employeeConverter).asEmployee(createDto);
        verify(employeeDao).getId(employee);
        verify(employeeDao).add(employee);
        verify(employeeConverter).asEmployeeDto(employee);
        verifyNoMoreInteractions(employeeConverter, employeeDao);

        assertEquals(result, viewDto);
    }

    @Test
    void addEmployeeTestShouldThrowException() {
        //Given
        when(employeeConverter.asEmployee(createDto)).thenReturn(employee);
        when(employeeDao.getId(employee)).thenReturn(1);

        //When
        EmployeeAlreadyExistException ex = assertThrows(EmployeeAlreadyExistException.class, () -> {
            employeeService.addEmployee(createDto);
        });

        //Then
        verify(employeeDao).getId(employee);
        verify(employeeConverter).asEmployee(createDto);
        verifyNoMoreInteractions(employeeDao, employeeConverter);

        assertTrue(ex.getMessage().contains("This employee already exist!"));
    }

    @Test
    void removeEmployeeTestShouldDeleteEmployee() {
        //Given
        when(employeeDao.remove(ID)).thenReturn(true);
        when(employeeScheduleDao.removeByEmployeeId(ID)).thenReturn(true);

        //When
        boolean result = employeeService.removeEmployee(ID);

        //Then
        verify(employeeDao).remove(ID);
        verify(employeeScheduleDao).removeByEmployeeId(ID);
        verifyNoMoreInteractions(employeeDao, employeeScheduleDao);

        assertTrue(result);
    }

    @Test
    void editEmployeeTestShouldUpdateEmployee() {
        //Given
        when(employeeDao.getById(ID)).thenReturn(employee);
        when(employeeConverter.asEmployee(createDto)).thenReturn(employee);
        when(employeeDao.edit(employee)).thenReturn(true);
        when(employeeScheduleDao.changeStartLocation(ID, createDto.getCity(), employee.getCity())).thenReturn(true);

        //When
        boolean result = employeeService.editEmployee(ID, createDto);

        //Then
        verify(employeeDao).getById(ID);
        verify(employeeConverter).asEmployee(createDto);
        verify(employeeDao).edit(employee);
        verify(employeeScheduleDao).changeStartLocation(ID, createDto.getCity(), employee.getCity());
        verifyNoMoreInteractions(employeeDao, employeeConverter, employeeScheduleDao);

        assertTrue(result);
    }

    @Test
    void getEmployeeByIdTestShouldReturnEmployee() {
        //Given
        when(employeeDao.getById(ID)).thenReturn(employee);
        when(employeeConverter.asEmployeeDto(employee)).thenReturn(viewDto);

        //When
        EmployeeViewDto result = employeeService.getEmployeeById(ID);

        //Then
        verify(employeeDao).getById(ID);
        verify(employeeConverter).asEmployeeDto(employee);
        verifyNoMoreInteractions(employeeDao, employeeConverter);

        assertEquals(result, viewDto);
    }

    @Test
    void getEmployeesByIdTestShouldReturnList() {
        //Given
        when(employeeDao.getById(ID)).thenReturn(employee);
        when(employeeConverter.asEmployeeDto(employee)).thenReturn(viewDto);

        //When
        List<EmployeeViewDto> result = employeeService.getEmployeesById(Collections.singletonList(ID));

        //Then
        verify(employeeDao).getById(ID);
        verify(employeeConverter).asEmployeeDto(employee);
        verifyNoMoreInteractions(employeeDao, employeeConverter);

        assertEquals(result, Collections.singletonList(viewDto));
    }

    @Test
    void findEmployeesTestShouldReturnList() {
        //Given
        when(employeeConverter.asEmployee(createDto)).thenReturn(employee);
        when(employeeDao.findEmployee(employee)).thenReturn(Collections.singletonList(employee));
        when(employeeConverter.asEmployeeDto(employee)).thenReturn(viewDto);

        //When
        List<EmployeeViewDto> result = employeeService.findEmployees(createDto);

        //Then
        verify(employeeConverter).asEmployee(createDto);
        verify(employeeDao).findEmployee(employee);
        verify(employeeConverter).asEmployeeDto(employee);
        verifyNoMoreInteractions(employeeConverter, employeeDao);

        assertEquals(result, Collections.singletonList(viewDto));
    }
}
