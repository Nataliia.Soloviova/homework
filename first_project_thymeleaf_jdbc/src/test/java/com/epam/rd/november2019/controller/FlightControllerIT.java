package com.epam.rd.november2019.controller;

import com.epam.rd.november2019.SpringIntegrationTest;
import com.epam.rd.november2019.model.FlightStatus;
import com.epam.rd.november2019.web.dto.FlightViewDto;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class FlightControllerIT extends SpringIntegrationTest {

    @ParameterizedTest
    @ValueSource(strings = {"/flights", "/deleteFlight/{id}", "/editFlight/{id}", "/flights/add", "/sort_flights", "/flight/{id}", "/editStatus/{id}"})
    public void shouldGetRedirectionToLoginPage(String link) throws Exception {
        //Given
        int id = 1;
        //When
        //Then
        mockMvc.perform(get(link, id)
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isFound());
    }

    @WithMockUser(roles = "ADMINISTRATOR")
    @Test
    public void shouldGetForbiddenResponseForAdmin() throws Exception {
        //Given
        int id = 1;
        //When
        //Then
        mockMvc.perform(get("/editStatus/{id}", id)
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isForbidden());
    }

    @WithMockUser(roles = "DISPATCHER")
    @ParameterizedTest
    @ValueSource(strings = {"/deleteFlight/{id}", "/editFlight/{id}", "/flights/add"})
    public void shouldGetForbiddenResponseForDispatcher(String link) throws Exception {
        //Given
        int id = 1;
        //When
        //Then
        mockMvc.perform(get(link, id)
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isForbidden());
    }

    @Disabled("Should prepare database before running test")
    @WithMockUser(roles = {"ADMINISTRATOR", "DISPATCHER"})
    @Test
    void testGetAllFlightsShouldReturnFlightList() throws Exception {
        //Given
        List<FlightViewDto> expected = new ArrayList<>();
        expected.add(new FlightViewDto(1, "Laa", "Paa", "London", "Paris", "12:00 20.02.2020", "15:00 20.02.2020", FlightStatus.PLANNED));
        expected.add(new FlightViewDto(2, "NYaa", "Paa", "New York", "Paris", "12:00 20.02.2020", "15:00 20.02.2020", FlightStatus.PLANNED));
        expected.add(new FlightViewDto(3, "Paa", "NYaa", "Paris", "New York", "12:00 20.02.2020", "15:00 20.02.2020", FlightStatus.PLANNED));
        //When
        //Then
        mockMvc.perform(get("/flights")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("flight"))
                .andExpect(model().attributeExists("flights"))
                .andExpect(model().attribute("flights", isA(List.class)))
                .andExpect(model().attribute("flights", hasSize(expected.size())))
                .andExpect(model().attribute("flights", hasToString(expected.toString())))
                .andExpect(view().name("flights"));
    }


    @WithMockUser(roles = {"ADMINISTRATOR", "DISPATCHER"})
    @Test
    void testFindFlightShouldReturnFlightsList() throws Exception {
        //Given
        FlightViewDto dto = new FlightViewDto(1, "Laa", "Paa", "London", "Paris", "2020-02-20T12:00", "2020-02-20T15:00", FlightStatus.PLANNED);
        FlightViewDto expectedDto = new FlightViewDto(1, "Laa", "Paa", "London", "Paris", "12:00 20.02.2020", "15:00 20.02.2020", FlightStatus.PLANNED);
        List<FlightViewDto> expected = new ArrayList<>();
        expected.add(expectedDto);
        //When
        mockMvc.perform(post("/flights")
                .param("flightId", String.valueOf(dto.getFlightId()))
                .param("departureCity", dto.getDepartureCity())
                .param("departureAirport", dto.getDepartureAirport())
                .param("departureDate", dto.getDepartureDate())
                .param("arrivalCity", dto.getArrivalCity())
                .param("arrivalAirport", dto.getArrivalAirport())
                .param("arrivalDate", dto.getArrivalDate())
                .param("status", dto.getStatus().toString())
                .accept(MediaType.TEXT_HTML))
                //Then
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("flights"))
                .andExpect(model().attribute("flights", isA(List.class)))
                .andExpect(model().attribute("flights", hasSize(expected.size())))
                .andExpect(model().attribute("flights", hasToString(expected.toString())))
                .andExpect(view().name("/flights"));
    }

    @Disabled("Should prepare database before running test")
    @WithMockUser(roles = "ADMINISTRATOR")
    @Test
    void testAddFlightShouldReturnFlightsPage() throws Exception {
        //Given
        int expectedSize = 4;
        FlightViewDto dto = new FlightViewDto("Waa", "Paa", "Washington", "Paris", "2020-02-20T12:00", "2020-02-20T15:00", FlightStatus.PLANNED);
        List<FlightViewDto> expectedList = new ArrayList<>();
        expectedList.add(new FlightViewDto(1, "Laa", "Paa", "London", "Paris", "12:00 20.02.2020", "15:00 20.02.2020", FlightStatus.PLANNED));
        expectedList.add(new FlightViewDto(2, "NYaa", "Paa", "New York", "Paris", "12:00 20.02.2020", "15:00 20.02.2020", FlightStatus.PLANNED));
        expectedList.add(new FlightViewDto(3, "Paa", "NYaa", "Paris", "New York", "12:00 20.02.2020", "15:00 20.02.2020", FlightStatus.PLANNED));
        expectedList.add(new FlightViewDto(4, "Waa", "Paa", "Washington", "Paris", "12:00 20.02.2020", "15:00 20.02.2020", FlightStatus.PLANNED));
        //When
        mockMvc.perform(post("/flights/add")
                .param("departureCity", dto.getDepartureCity())
                .param("departureAirport", dto.getDepartureAirport())
                .param("departureDate", dto.getDepartureDate())
                .param("arrivalCity", dto.getArrivalCity())
                .param("arrivalAirport", dto.getArrivalAirport())
                .param("arrivalDate", dto.getArrivalDate())
                .param("status", dto.getStatus().toString())
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/flights"));
        //Then
        mockMvc.perform(get("/flights")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("flight"))
                .andExpect(model().attributeExists("flights"))
                .andExpect(model().attribute("flights", isA(List.class)))
                .andExpect(model().attribute("flights", hasSize(expectedList.size())))
                .andExpect(model().attribute("flights", hasToString(expectedList.toString())))
                .andExpect(view().name("flights"));
    }

    @Disabled("Should prepare database before running test")
    @WithMockUser(roles = "ADMINISTRATOR")
    @Test
    void testRemoveFlightShouldRedirectToFlightsPage() throws Exception {
        //Given
        int id = 1;
        int expectedSize = 2;
        //When
        mockMvc.perform(get("/deleteFlight/{id}", id)
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/flights"));
        //Then
        mockMvc.perform(get("/flights")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("flight"))
                .andExpect(model().attributeExists("flights"))
                .andExpect(model().attribute("flights", isA(List.class)))
                .andExpect(model().attribute("flights", hasSize(expectedSize)))
                .andExpect(view().name("flights"));
    }

    @WithMockUser(roles = "ADMINISTRATOR")
    @Test
    void testEditFlightShouldReturnFlightDataPage() throws Exception {
        //Given
        int id = 1;
        //When
        mockMvc.perform(get("/editFlight/{id}", id)
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(view().name("flight_data"));
        //Then
        mockMvc.perform(get("/employees")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("employee"))
                .andExpect(model().attributeExists("employees"))
                .andExpect(view().name("employees"));
    }

    @WithMockUser(roles = "ADMINISTRATOR")
    @Test
    void testEditFlightShouldRedirectToFlightsPage() throws Exception {
        //Given
        int id = 2;
        FlightViewDto dto = new FlightViewDto("Waa", "Laa", "Washington", "London", "2020-02-20T12:00", "2020-02-20T15:00", FlightStatus.PLANNED);
        //When
        mockMvc.perform(post("/editFlight/{id}", id)
                .param("departureCity", dto.getDepartureCity())
                .param("departureAirport", dto.getDepartureAirport())
                .param("departureDate", dto.getDepartureDate())
                .param("arrivalCity", dto.getArrivalCity())
                .param("arrivalAirport", dto.getArrivalAirport())
                .param("arrivalDate", dto.getArrivalDate())
                .param("status", dto.getStatus().toString())
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/flights"));
        //Then
        mockMvc.perform(get("/flights")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("flight"))
                .andExpect(model().attributeExists("flights"))
                .andExpect(view().name("flights"));
    }

    @WithMockUser(roles = "DISPATCHER")
    @Test
    void testEditStatusShouldRedirectToSpecificFlightPage() throws Exception {
        //Given
        int id = 2;
        String status = FlightStatus.ON_TIME.toString();
        //When
        mockMvc.perform(post("/editStatus/{id}", id)
                .param("status", status)
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/flight/" + id));
        //Then
        mockMvc.perform(get("/flights")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("flight"))
                .andExpect(model().attributeExists("flights"))
                .andExpect(view().name("flights"));
    }

    @Disabled("Should prepare database before running test")
    @WithMockUser(roles = {"ADMINISTRATOR","DISPATCHER"})
    @ParameterizedTest
    @ValueSource(strings = {"Flight name", "Flight id"})
    void testSortFlightShouldReturnFlightsPageWithSortedList(String sortBy) throws Exception {
        //Given
        int expectedSize = 3;
        //When
        mockMvc.perform(post("/sort_flights")
                .param("sortBy", sortBy)
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(view().name("flights"));
        //Then
        mockMvc.perform(get("/flights")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("flight"))
                .andExpect(model().attributeExists("flights"))
                .andExpect(model().attribute("flights", hasSize(expectedSize)))
                .andExpect(view().name("flights"));
    }

    @WithMockUser(roles = {"ADMINISTRATOR","DISPATCHER"})
    @Test
    void testShowFlightShouldReturnSpecificFlightPage() throws Exception {
        //Given
        int id = 1;
        //When
        //Then
        mockMvc.perform(get("/flight/{id}", id)
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("flight"))
                .andExpect(model().attributeExists("employees"))
                .andExpect(view().name("flight_data"));
    }
}
