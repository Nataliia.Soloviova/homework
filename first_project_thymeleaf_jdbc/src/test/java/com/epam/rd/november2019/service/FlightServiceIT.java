package com.epam.rd.november2019.service;

import com.epam.rd.november2019.SpringIntegrationTest;
import com.epam.rd.november2019.converter.DateTimeConverter;
import com.epam.rd.november2019.converter.FlightConverter;
import com.epam.rd.november2019.dao.FlightDao;
import com.epam.rd.november2019.exception.FlightAlreadyExistException;
import com.epam.rd.november2019.model.Flight;
import com.epam.rd.november2019.model.FlightStatus;
import com.epam.rd.november2019.model.FlightTeam;
import com.epam.rd.november2019.web.dto.FlightCreateDto;
import com.epam.rd.november2019.web.dto.FlightViewDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class FlightServiceIT extends SpringIntegrationTest {

    private static final int ID = 4;
    private static final String DEPARTURE_CITY = "London";
    private static final String DEPARTURE_AIRPORT = "Laa";
    private static final String ARRIVAL_CITY = "Paris";
    private static final String ARRIVAL_AIRPORT = "Paa";
    private static final FlightStatus FLIGHT_STATUS = FlightStatus.BOARDING;
    private static final FlightTeam FLIGHT_TEAM = new FlightTeam(new ArrayList<>());
    private static Timestamp DEPARTURE_DATE;
    private static Timestamp ARRIVAL_DATE;

    static {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
            DEPARTURE_DATE = new Timestamp(simpleDateFormat.parse("2020-02-20T12:00").getTime());
            ARRIVAL_DATE = new Timestamp(simpleDateFormat.parse("2020-02-20T15:00").getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Autowired
    private FlightService flightService;

    @Autowired
    private FlightDao flightDao;

    @Autowired
    private DateTimeConverter converter;

    @Autowired
    private FlightConverter flightConverter;

    private Flight flight;
    private FlightCreateDto createDto;

    @BeforeEach
    public void init() {
        flight = new Flight();
        flight.setFlightId(ID);
        flight.setDepartureCity(DEPARTURE_CITY);
        flight.setDepartureDate(DEPARTURE_DATE);
        flight.setDepartureAirport(DEPARTURE_AIRPORT);
        flight.setArrivalCity(ARRIVAL_CITY);
        flight.setArrivalDate(ARRIVAL_DATE);
        flight.setArrivalAirport(ARRIVAL_AIRPORT);
        flight.setStatus(FLIGHT_STATUS);
        flight.setFlightTeam(FLIGHT_TEAM);

        createDto = new FlightCreateDto();
        createDto.setFlightId(ID);
        createDto.setDepartureCity(DEPARTURE_CITY);
        createDto.setDepartureDate(DEPARTURE_DATE);
        createDto.setDepartureAirport(DEPARTURE_AIRPORT);
        createDto.setArrivalCity(ARRIVAL_CITY);
        createDto.setArrivalDate(ARRIVAL_DATE);
        createDto.setArrivalAirport(ARRIVAL_AIRPORT);
        createDto.setStatus(FLIGHT_STATUS);
        createDto.setFlightTeam(FLIGHT_TEAM);
    }

    @Test
    void getAllFlightsTestShouldReturnList() {
        //Given
        List<Flight> expected = flightDao.getAll();

        //When
        List<FlightViewDto> result = flightService.getAllFlights();

        //Then
        assertEquals(result.size(), expected.size());
    }

    @Disabled("Should prepare database before running test")
    @Test
    void addFlightTestShouldCreateFlight() {
        //Given

        //When
        FlightViewDto result = flightService.addFlight(createDto);

        //Then
        assertEquals(DEPARTURE_CITY, result.getDepartureCity());
        assertEquals(DEPARTURE_AIRPORT, result.getDepartureAirport());
        assertEquals(DEPARTURE_DATE, converter.toTimestamp(result.getDepartureDate()));
        assertEquals(ARRIVAL_CITY, result.getArrivalCity());
        assertEquals(ARRIVAL_AIRPORT, result.getArrivalAirport());
        assertEquals(ARRIVAL_DATE, converter.toTimestamp(result.getArrivalDate()));
        assertEquals(FLIGHT_STATUS, result.getStatus());
        assertEquals(FLIGHT_TEAM, result.getFlightTeam());
    }

    @Test
    void addFlightTestShouldThrowException() {
        //Given
        flightDao.add(flight);

        //When
        FlightAlreadyExistException ex = assertThrows(FlightAlreadyExistException.class, () -> {
            flightService.addFlight(createDto);
        });

        //Then
        assertTrue(ex.getMessage().contains("This flight already exist!"));
    }

    @Test
    void removeFlightTestShouldDeleteFlight() {
        //Given
        flightDao.add(flight);

        //When
        flightService.removeFlight(ID);
        Flight flight = flightDao.getById(ID);

        //Then
        assertNull(flight);
    }

    @Test
    void editFlightTestShouldUpdateFlight() {
        //Given
        String newDepartureCity = "Paris";
        flightDao.add(flight);
        createDto.setDepartureCity(newDepartureCity);

        //When
        flightService.editFlight(ID, createDto);

        //Then
        assertEquals(newDepartureCity, flightDao.getById(ID).getDepartureCity());
    }

    @Test
    void editStatusShouldUpdateFlightStatus() {
        //Given
        FlightStatus newStatus = FlightStatus.ON_TIME;
        flightDao.add(flight);

        //When
        flightService.edit(ID, newStatus);

        //Then
        assertEquals(newStatus, flightDao.getById(ID).getStatus());
    }

    @Test
    void findFlightTestShouldReturnList() {
        //Given
        String searchString = "Paris";
        FlightCreateDto createDto = new FlightCreateDto();
        createDto.setArrivalCity(searchString);
        Flight flight = new Flight();
        flight.setArrivalCity(searchString);

        //When
        List<FlightViewDto> result = flightService.findFlights(createDto);

        //Then
        assertEquals(result.size(), flightDao.findFlights(flight).size());
    }

    @Test
    void sortByNameTestShouldReturnList() {
        //Given
        List<Flight> expected = flightDao.sortByFlightName();

        //When
        List<FlightViewDto> result = flightService.sortByFlightName();

        //Then
        assertEquals(result.size(), expected.size());
        assertEquals(result.get(0).getFlightId(), expected.get(0).getFlightId());
        assertEquals(result.get(1).getFlightId(), expected.get(1).getFlightId());
        assertEquals(result.get(2).getFlightId(), expected.get(2).getFlightId());
    }

    @Test
    void sortByIdTestShouldReturnList() {
        //Given
        List<Flight> expected = flightDao.sortById();

        //When
        List<FlightViewDto> result = flightService.sortById();

        //Then
        assertEquals(result.size(), expected.size());
        assertEquals(result.get(0).getFlightId(), expected.get(0).getFlightId());
        assertEquals(result.get(1).getFlightId(), expected.get(1).getFlightId());
        assertEquals(result.get(2).getFlightId(), expected.get(2).getFlightId());
    }

    @Test
    void getByIdTestShouldReturnFlight() {
        //Given
        flightDao.add(flight);

        //When
        FlightViewDto result = flightService.getById(ID);

        //Then
        assertEquals(flight.getDepartureCity(), result.getDepartureCity());
        assertEquals(flight.getDepartureAirport(), result.getDepartureAirport());
        assertEquals(flight.getDepartureDate(), converter.toTimestamp(result.getDepartureDate()));
        assertEquals(flight.getArrivalCity(), result.getArrivalCity());
        assertEquals(flight.getArrivalAirport(), result.getArrivalAirport());
        assertEquals(flight.getArrivalDate(), converter.toTimestamp(result.getArrivalDate()));
        assertEquals(flight.getFlightTeam(), result.getFlightTeam());
    }
}
