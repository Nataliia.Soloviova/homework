package com.epam.rd.november2019.service;

import com.epam.rd.november2019.converter.FlightConverter;
import com.epam.rd.november2019.dao.EmployeeScheduleDao;
import com.epam.rd.november2019.dao.FlightDao;
import com.epam.rd.november2019.exception.FlightAlreadyExistException;
import com.epam.rd.november2019.model.Flight;
import com.epam.rd.november2019.model.FlightStatus;
import com.epam.rd.november2019.model.FlightTeam;
import com.epam.rd.november2019.service.impl.FlightServiceImpl;
import com.epam.rd.november2019.web.dto.FlightCreateDto;
import com.epam.rd.november2019.web.dto.FlightViewDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class FlightServiceTest {

    private static final int ID = 1;
    private static final String DEPARTURE_CITY = "London";
    private static final String DEPARTURE_AIRPORT = "Laa";
    private static final Timestamp DEPARTURE_DATE = new Timestamp(new Date().getTime());
    private static final String ARRIVAL_CITY = "Paris";
    private static final String ARRIVAL_AIRPORT = "Paa";
    private static final Timestamp ARRIVAL_DATE = new Timestamp(new Date().getTime());
    private static final FlightStatus FLIGHT_STATUS = FlightStatus.BOARDING;
    private static final FlightTeam FLIGHT_TEAM = new FlightTeam(new ArrayList<>());

    private final FlightDao flightDao = mock(FlightDao.class);
    private final EmployeeScheduleDao employeeScheduleDao = mock(EmployeeScheduleDao.class);
    private final FlightConverter flightConverter = mock(FlightConverter.class);
    private final FlightService flightService = new FlightServiceImpl(flightDao, employeeScheduleDao, flightConverter);

    private Flight flight;
    private FlightCreateDto createDto;
    private FlightViewDto viewDto;

    @BeforeEach
    public void init() {
        flight = new Flight();
        flight.setFlightId(ID);
        flight.setDepartureCity(DEPARTURE_CITY);
        flight.setDepartureDate(DEPARTURE_DATE);
        flight.setDepartureAirport(DEPARTURE_AIRPORT);
        flight.setArrivalCity(ARRIVAL_CITY);
        flight.setArrivalDate(ARRIVAL_DATE);
        flight.setArrivalAirport(ARRIVAL_AIRPORT);
        flight.setStatus(FLIGHT_STATUS);
        flight.setFlightTeam(FLIGHT_TEAM);

        createDto = new FlightCreateDto();
        createDto.setFlightId(ID);
        createDto.setDepartureCity(DEPARTURE_CITY);
        createDto.setDepartureDate(DEPARTURE_DATE);
        createDto.setDepartureAirport(DEPARTURE_AIRPORT);
        createDto.setArrivalCity(ARRIVAL_CITY);
        createDto.setArrivalDate(ARRIVAL_DATE);
        createDto.setArrivalAirport(ARRIVAL_AIRPORT);
        createDto.setStatus(FLIGHT_STATUS);
        createDto.setFlightTeam(FLIGHT_TEAM);

        viewDto = new FlightViewDto();
        viewDto.setFlightId(ID);
        viewDto.setDepartureCity(DEPARTURE_CITY);
        viewDto.setDepartureDate(DEPARTURE_DATE.toString());
        viewDto.setDepartureAirport(DEPARTURE_AIRPORT);
        viewDto.setArrivalCity(ARRIVAL_CITY);
        viewDto.setArrivalDate(ARRIVAL_DATE.toString());
        viewDto.setArrivalAirport(ARRIVAL_AIRPORT);
        viewDto.setStatus(FLIGHT_STATUS);
        viewDto.setFlightTeam(FLIGHT_TEAM);
    }

    @Test
    void getAllFlightsTestShouldReturnList() {
        //Given
        when(flightDao.getAll()).thenReturn(Collections.singletonList(flight));
        when(flightConverter.asFlightViewDto(flight)).thenReturn(viewDto);
        when(employeeScheduleDao.getFlightTeam(flight.getFlightId())).thenReturn(flight.getFlightTeam());

        //When
        List<FlightViewDto> result = flightService.getAllFlights();

        //Then
        verify(flightDao, times(1)).getAll();
        verify(flightConverter, times(1)).asFlightViewDto(flight);
        verify(employeeScheduleDao, times(1)).getFlightTeam(flight.getFlightId());
        verifyNoMoreInteractions(flightDao, flightConverter, employeeScheduleDao);

        assertEquals(result, Collections.singletonList(viewDto));
    }

    @Test
    void addFlightTestShouldCreateFlight() {
        //Given
        when(flightConverter.asFlight(createDto)).thenReturn(flight);
        when(flightDao.getId(flight)).thenReturn(-1);
        when(flightDao.add(flight)).thenReturn(flight);
        when(flightConverter.asFlightDto(flight)).thenReturn(viewDto);

        //When
        FlightViewDto result = flightService.addFlight(createDto);

        //Then
        verify(flightDao).getId(flight);
        verify(flightDao).add(flight);
        verify(flightConverter).asFlight(createDto);
        verify(flightConverter).asFlightDto(flight);
        verifyNoMoreInteractions(flightDao, flightConverter);

        assertEquals(result, viewDto);
    }

    @Test
    void addFlightTestShouldThrowException() {
        //Given
        when(flightConverter.asFlight(createDto)).thenReturn(flight);
        when(flightDao.getId(flight)).thenReturn(1);

        //When
        FlightAlreadyExistException ex = assertThrows(FlightAlreadyExistException.class, () -> {
            flightService.addFlight(createDto);
        });

        //Then
        verify(flightDao).getId(flight);
        verify(flightConverter).asFlight(createDto);
        verifyNoMoreInteractions(flightDao, flightConverter);

        assertTrue(ex.getMessage().contains("This flight already exist!"));
    }

    @Test
    void removeFlightTestShouldDeleteFlight() {
        //Given
        when(flightDao.remove(flight.getFlightId())).thenReturn(true);
        when(employeeScheduleDao.removeByFlightId(flight.getFlightId())).thenReturn(true);

        //When
        boolean result = flightService.removeFlight(flight.getFlightId());

        //Then
        verify(flightDao, times(1)).remove(flight.getFlightId());
        verify(employeeScheduleDao, times(1)).removeByFlightId(flight.getFlightId());
        verifyNoMoreInteractions(flightDao, employeeScheduleDao);

        assertTrue(result);
    }

    @Test
    void editFlightTestShouldUpdateFlight() {
        //Given
        when(flightConverter.asFlight(createDto)).thenReturn(flight);
        when(flightDao.edit(ID, flight)).thenReturn(true);
        when(employeeScheduleDao.removeByFlightId(ID)).thenReturn(true);

        //When
        boolean result = flightService.editFlight(ID, createDto);

        //Then
        verify(flightConverter).asFlight(createDto);
        verify(flightDao).edit(ID, flight);
        verify(employeeScheduleDao).removeByFlightId(ID);
        verifyNoMoreInteractions(flightConverter, flightDao, employeeScheduleDao);

        assertTrue(result);
    }

    @Test
    void editStatusShouldUpdateFlightStatus() {
        //Given
        when(flightDao.edit(ID, FLIGHT_STATUS)).thenReturn(true);

        //When
        boolean result = flightService.edit(ID, FLIGHT_STATUS);

        //Then
        verify(flightDao).edit(ID, FLIGHT_STATUS);
        verifyNoMoreInteractions(flightDao);

        assertTrue(result);
    }

    @Test
    void findFlightTestShouldReturnList() {
        //Given
        when(flightConverter.asFlight(createDto)).thenReturn(flight);
        when(flightDao.findFlights(flight)).thenReturn(Collections.singletonList(flight));
        when(employeeScheduleDao.getFlightTeam(flight.getFlightId())).thenReturn(flight.getFlightTeam());
        when(flightConverter.asFlightViewDto(flight)).thenReturn(viewDto);
        //When
        List<FlightViewDto> result = flightService.findFlights(createDto);

        //Then
        verify(flightConverter).asFlight(createDto);
        verify(flightDao).findFlights(flight);
        verify(employeeScheduleDao).getFlightTeam(flight.getFlightId());
        verify(flightConverter).asFlightViewDto(flight);
        verifyNoMoreInteractions(flightConverter, flightDao, employeeScheduleDao);

        assertEquals(result, Collections.singletonList(viewDto));
    }

    @Test
    void sortByNameTestShouldReturnList() {
        //Given
        when(flightDao.sortByFlightName()).thenReturn(Collections.singletonList(flight));
        when(employeeScheduleDao.getFlightTeam(flight.getFlightId())).thenReturn(flight.getFlightTeam());
        when(flightConverter.asFlightDto(flight)).thenReturn(viewDto);

        //When
        List<FlightViewDto> result = flightService.sortByFlightName();

        //Then
        verify(flightDao).sortByFlightName();
        verify(employeeScheduleDao).getFlightTeam(flight.getFlightId());
        verify(flightConverter).asFlightDto(flight);
        verifyNoMoreInteractions(flightDao, employeeScheduleDao, flightConverter);

        assertEquals(result, Collections.singletonList(viewDto));
    }

    @Test
    void sortByIdTestShouldReturnList() {
        //Given
        when(flightDao.sortById()).thenReturn(Collections.singletonList(flight));
        when(employeeScheduleDao.getFlightTeam(flight.getFlightId())).thenReturn(flight.getFlightTeam());
        when(flightConverter.asFlightDto(flight)).thenReturn(viewDto);

        //When
        List<FlightViewDto> result = flightService.sortById();

        //Then
        verify(flightDao).sortById();
        verify(employeeScheduleDao).getFlightTeam(flight.getFlightId());
        verify(flightConverter).asFlightDto(flight);
        verifyNoMoreInteractions(flightDao, employeeScheduleDao, flightConverter);

        assertEquals(result, Collections.singletonList(viewDto));
    }

    @Test
    void getByIdTestShouldReturnFlight() {
        //Given
        when(flightDao.getById(ID)).thenReturn(flight);
        when(employeeScheduleDao.getFlightTeam(ID)).thenReturn(flight.getFlightTeam());
        when(flightConverter.asFlightDto(flight)).thenReturn(viewDto);

        //When
        FlightViewDto result = flightService.getById(ID);

        //Then
        verify(flightDao).getById(ID);
        verify(employeeScheduleDao).getFlightTeam(flight.getFlightId());
        verify(flightConverter).asFlightDto(flight);
        verifyNoMoreInteractions(flightDao, employeeScheduleDao, flightConverter);

        assertEquals(result, viewDto);
    }
}
