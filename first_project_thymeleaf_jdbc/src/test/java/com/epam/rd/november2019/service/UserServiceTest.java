package com.epam.rd.november2019.service;

import com.epam.rd.november2019.converter.UserAccountConverter;
import com.epam.rd.november2019.dao.UserDao;
import com.epam.rd.november2019.model.User;
import com.epam.rd.november2019.model.UserRole;
import com.epam.rd.november2019.service.impl.UserServiceImpl;
import com.epam.rd.november2019.valid.UserValidator;
import com.epam.rd.november2019.web.dto.UserCreateDto;
import com.epam.rd.november2019.web.dto.UserViewDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class UserServiceTest {

    private static final String EMAIL = "ivan_ivanov_000@corp.com";
    private static final String USERNAME = "ivan_ivanov_000";
    private static final String PASSWORD = "password12345";
    private static final UserRole USER_ROLE = UserRole.ROLE_ADMINISTRATOR;

    private final UserDao userDao = mock(UserDao.class);
    private final UserAccountConverter userAccountConverter = mock(UserAccountConverter.class);
    private final PasswordEncoder passwordEncoder = mock(PasswordEncoder.class);
    private final UserValidator userValidator = mock(UserValidator.class);
    private final UserService userService = new UserServiceImpl(userDao, userAccountConverter, passwordEncoder, userValidator);

    private User user;
    private UserCreateDto createDto;
    private UserViewDto viewDto;

    @BeforeEach
    void setUp() {
        user = new User();
        user.setEmail(EMAIL);
        user.setPassword(PASSWORD);
        user.setUserName(USERNAME);
        user.setUserRole(USER_ROLE);

        createDto = new UserCreateDto();
        createDto.setEmail(EMAIL);
        createDto.setPassword(PASSWORD);
        createDto.setUserName(USERNAME);
        createDto.setUserRole(USER_ROLE);

        viewDto = new UserViewDto();
        viewDto.setEmail(EMAIL);
        viewDto.setUserName(USERNAME);
        viewDto.setUserRole(USER_ROLE);
    }

    @Test
    void registerUserTestShouldReturnViewDto() {
        //Given
        when(userDao.getByEmail(createDto.getEmail())).thenReturn(null);
        when(passwordEncoder.encode(createDto.getPassword())).thenReturn(PASSWORD);
        when(userDao.add(user)).thenReturn(user);
        when(userAccountConverter.asUserDto(user)).thenReturn(viewDto);

        //When
        UserViewDto result = userService.registerUser(createDto);

        //THen
        verify(userDao).getByEmail(createDto.getEmail());
        verify(passwordEncoder).encode(createDto.getPassword());
        verify(userDao).add(user);
        verify(userAccountConverter).asUserDto(user);
        verifyNoMoreInteractions(userDao, userAccountConverter);

        assertEquals(result, viewDto);
    }
}
