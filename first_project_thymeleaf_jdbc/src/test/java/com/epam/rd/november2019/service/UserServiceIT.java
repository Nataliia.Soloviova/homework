package com.epam.rd.november2019.service;

import com.epam.rd.november2019.SpringIntegrationTest;
import com.epam.rd.november2019.dao.UserDao;
import com.epam.rd.november2019.model.UserRole;
import com.epam.rd.november2019.web.dto.UserCreateDto;
import com.epam.rd.november2019.web.dto.UserViewDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserServiceIT extends SpringIntegrationTest {

    private static final String EMAIL = "ivan_ivanov_000@corp.com";
    private static final String USERNAME = "ivan_ivanov_000";
    private static final String PASSWORD = "password12345";
    private static final UserRole USER_ROLE = UserRole.ROLE_ADMINISTRATOR;

    @Autowired
    private UserService userService;

    @Autowired
    private UserDao userDao;

    @Test
    void registerUserTestShouldReturnViewDto() {
        //Given
        UserCreateDto createDto = new UserCreateDto();
        createDto.setEmail(EMAIL);
        createDto.setPassword(PASSWORD);
        createDto.setUserName(USERNAME);
        createDto.setUserRole(USER_ROLE);

        //When
        UserViewDto result = userService.registerUser(createDto);

        //THen
        assertEquals(EMAIL, result.getEmail());
        assertEquals(USERNAME, result.getUserName());
        assertEquals(USER_ROLE, result.getUserRole());
    }
}
