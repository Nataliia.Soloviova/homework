package com.epam.rd.november2019.service;

import com.epam.rd.november2019.SpringIntegrationTest;
import com.epam.rd.november2019.converter.EmployeeConverter;
import com.epam.rd.november2019.dao.EmployeeDao;
import com.epam.rd.november2019.dao.EmployeeScheduleDao;
import com.epam.rd.november2019.exception.EmployeeAlreadyExistException;
import com.epam.rd.november2019.model.Employee;
import com.epam.rd.november2019.model.EmployeeRole;
import com.epam.rd.november2019.model.Sex;
import com.epam.rd.november2019.web.dto.EmployeeCreateDto;
import com.epam.rd.november2019.web.dto.EmployeeViewDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class EmployeeServiceIT extends SpringIntegrationTest {

    private static final int ID = 4;
    private static final String LAST_NAME = "Johnson";
    private static final String FIRST_NAME = "John";
    private static final EmployeeRole EMPLOYEE_ROLE = EmployeeRole.RADIO_OPERATOR;
    private static final Sex SEX = Sex.MAN;
    private static final String CITY = "New York";

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private EmployeeDao employeeDao;

    @Autowired
    private EmployeeScheduleDao employeeScheduleDao;

    @Autowired
    private EmployeeConverter employeeConverter;

    private Employee employee;
    private EmployeeCreateDto createDto;

    @BeforeEach
    public void setUp() {
        employee = new Employee();
        employee.setEmployeeId(ID);
        employee.setFirstName(FIRST_NAME);
        employee.setLastName(LAST_NAME);
        employee.setSex(SEX);
        employee.setEmployeeRole(EMPLOYEE_ROLE);
        employee.setCity(CITY);

        createDto = new EmployeeCreateDto();
        createDto.setEmployeeId(ID);
        createDto.setFirstName(FIRST_NAME);
        createDto.setLastName(LAST_NAME);
        createDto.setSex(SEX);
        createDto.setEmployeeRole(EMPLOYEE_ROLE);
        createDto.setCity(CITY);
    }

    @Test
    public void getAllEmployeesTestShouldReturnList() {
        //Given
        List<Employee> expected = employeeDao.getAll();

        //When
        List<EmployeeViewDto> result = employeeService.getAllEmployees();

        //Then
        assertEquals(result.size(), expected.size());
    }

    @Disabled("Should prepare database before running test")
    @Test
    void addEmployeeTestShouldCreateEmployee() {
        //Given

        //When
        EmployeeViewDto result = employeeService.addEmployee(createDto);

        //Then
        assertEquals(LAST_NAME, result.getLastName());
        assertEquals(FIRST_NAME, result.getFirstName());
        assertEquals(EMPLOYEE_ROLE, result.getEmployeeRole());
        assertEquals(CITY, result.getCity());
        assertEquals(SEX, result.getSex());
    }

    @Test
    void addEmployeeTestShouldThrowException() {
        //Given
        employeeDao.add(employee);

        //When
        EmployeeAlreadyExistException ex = assertThrows(EmployeeAlreadyExistException.class, () -> {
            employeeService.addEmployee(createDto);
        });

        //Then
        assertTrue(ex.getMessage().contains("This employee already exist!"));
    }

    @Test
    void removeEmployeeTestShouldDeleteEmployee() {
        //Given
        employeeDao.add(employee);

        //When
        employeeService.removeEmployee(ID);
        Employee employee = employeeDao.getById(ID);

        //Then
        assertNull(employee);
    }

    @Test
    void editEmployeeTestShouldUpdateEmployee() {
        //Given
        String newFirstName = "Jack";
        employeeDao.add(employee);
        createDto.setFirstName(newFirstName);

        //When
        employeeService.editEmployee(ID, createDto);

        //Then
        assertEquals(newFirstName, employeeDao.getById(ID).getFirstName());
    }

    @Test
    void getEmployeeByIdTestShouldReturnEmployee() {
        //Given
        employeeDao.add(employee);

        //When
        EmployeeViewDto result = employeeService.getEmployeeById(ID);

        //Then
        assertEquals(employee.getFirstName(), result.getFirstName());
        assertEquals(employee.getLastName(), result.getLastName());
        assertEquals(employee.getCity(), result.getCity());
        assertEquals(employee.getEmployeeRole(), result.getEmployeeRole());
        assertEquals(employee.getSex(), result.getSex());
    }

    @Test
    void getEmployeesByIdTestShouldReturnList() {
        //Given
        int firstId = 2;
        int secondId = 3;
        List<Integer> employeeIds = new ArrayList<>();
        employeeIds.add(firstId);
        employeeIds.add(secondId);

        //When
        List<EmployeeViewDto> result = employeeService.getEmployeesById(employeeIds);

        //Then
        assertEquals(result.get(0).getEmployeeId(), firstId);
        assertEquals(result.get(1).getEmployeeId(), secondId);
    }

    @Test
    void findEmployeesTestShouldReturnList() {
        //Given
        Sex searchString = Sex.MAN;
        EmployeeCreateDto createDto = new EmployeeCreateDto();
        createDto.setSex(searchString);
        Employee employee = new Employee();
        employee.setSex(searchString);

        //When
        List<EmployeeViewDto> result = employeeService.findEmployees(createDto);

        //Then
        assertEquals(result.size(), employeeDao.findEmployee(employee).size());
    }
}
