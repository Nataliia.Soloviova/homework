CREATE TABLE users (
user_email VARCHAR(50) NOT NULL,
user_name VARCHAR(50) NOT NULL,
user_password VARCHAR(100) NOT NULL,
role_id INT(11) NOT NULL,
PRIMARY KEY (user_email),
UNIQUE (user_email)
);

CREATE TABLE user_roles (
role_id INT(11) AUTO_INCREMENT,
role_name VARCHAR(50) NOT NULL,
PRIMARY KEY (role_id)
);

CREATE TABLE employees (
employee_id INT(11) AUTO_INCREMENT,
last_name VARCHAR(50) NOT NULL,
first_name VARCHAR(50) NOT NULL,
city VARCHAR(50) NOT NULL,
sex ENUM('MAN','WOMAN') NOT NULL,
role_id INT(11) NOT NULL,
PRIMARY KEY (employee_id)
);

CREATE TABLE employee_roles (
role_id INT(11) AUTO_INCREMENT,
role_name VARCHAR(50) NOT NULL,
PRIMARY KEY (role_id)
);

CREATE TABLE employee_schedule (
employee_id INT(11) NOT NULL,
flight_id INT(11) DEFAULT('0') NOT NULL,
start_workday TIMESTAMP DEFAULT ('0000-00-00 00:00:00') NOT NULL,
finish_workday TIMESTAMP DEFAULT ('0000-00-00 00:00:00') NOT NULL,
start_location VARCHAR(50) NOT NULL,
end_location VARCHAR(50)
);

CREATE TABLE flights (
flight_id INT(11) AUTO_INCREMENT,
departure_city VARCHAR(50) NOT NULL,
arrival_city VARCHAR(50) NOT NULL,
departure_date TIMESTAMP DEFAULT ('0000-00-00 00:00:00') NOT NULL,
arrival_date TIMESTAMP DEFAULT ('0000-00-00 00:00:00') NOT NULL,
departure_airport VARCHAR(50) NOT NULL,
arrival_airport VARCHAR(50) NOT NULL,
status_id INT(11) NOT NULL,
PRIMARY KEY (flight_id)
);

CREATE TABLE flight_statuses (
status_id INT(11) AUTO_INCREMENT,
status_name VARCHAR(50) NOT NULL,
PRIMARY KEY (status_id)
);

CREATE TABLE flight_requests (
request_id INT(11) AUTO_INCREMENT,
request_by VARCHAR(50) NOT NULL,
request_date TIMESTAMP DEFAULT ('0000-00-00 00:00:00') NOT NULL,
status_id INT(11),
requester_comment VARCHAR(500) NOT NULL,
approver_comment VARCHAR(500),
flight_id INT(11) NOT NULL,
PRIMARY KEY (request_id)
);

CREATE TABLE request_statuses (
status_id INT(11) AUTO_INCREMENT,
status_name VARCHAR(50) NOT NULL,
PRIMARY KEY (status_id)
);

INSERT INTO user_roles (role_name) VALUES ('ROLE_ADMINISTRATOR');
INSERT INTO user_roles (role_name) VALUES ('ROLE_DISPATCHER');

INSERT INTO flight_statuses (status_name) VALUES ('PLANNED');
INSERT INTO flight_statuses (status_name) VALUES ('FORMED');
INSERT INTO flight_statuses (status_name) VALUES ('ON_TIME');
INSERT INTO flight_statuses (status_name) VALUES ('CHECK_IN');
INSERT INTO flight_statuses (status_name) VALUES ('GATE_CLOSING');
INSERT INTO flight_statuses (status_name) VALUES ('BOARDING');
INSERT INTO flight_statuses (status_name) VALUES ('CANCELLED');
INSERT INTO flight_statuses (status_name) VALUES ('DELAYED');
INSERT INTO flight_statuses (status_name) VALUES ('DEPARTED');

INSERT INTO request_statuses (status_name) VALUES ('APPROVED');
INSERT INTO request_statuses (status_name) VALUES ('DISAPPROVED');

INSERT INTO employee_roles (role_name) VALUES ('PILOT');
INSERT INTO employee_roles (role_name) VALUES ('NAVIGATOR');
INSERT INTO employee_roles (role_name) VALUES ('RADIO_OPERATOR');
INSERT INTO employee_roles (role_name) VALUES ('STEWARDESS');

INSERT INTO users (user_name, user_email, user_password, role_id)
VALUES (
'Ivan_Ivanov_001',
'ivanov_ivan_001@corp.com',
'$2a$10$bN01BcipnAn3PXX6fosQ.eDEok4reCrIPCNo4OsVpF7ev.nsRS3aO',
(SELECT role_id FROM user_roles WHERE role_name='ROLE_ADMINISTRATOR')
);

INSERT INTO employees (last_name, first_name, sex, city, role_id) VALUES ('Jackson', 'Michael', 'MAN', 'London', '1');
INSERT INTO employees (last_name, first_name, sex, city, role_id) VALUES ('Lewis', 'Jack', 'MAN', 'London', '2');
INSERT INTO employees (last_name, first_name, sex, city, role_id) VALUES ('Williams', 'John', 'MAN', 'London', '3');

INSERT INTO flights (departure_city, departure_airport, arrival_city, arrival_airport, departure_date, arrival_date, status_id)
VALUES ('London', 'Laa', 'Paris', 'Paa', '2020-02-20 12:00:00', '2020-02-20 15:00:00', (SELECT status_id FROM flight_statuses WHERE status_name='PLANNED'));
INSERT INTO flights (departure_city, departure_airport, arrival_city, arrival_airport, departure_date, arrival_date, status_id)
VALUES ('New York', 'NYaa', 'Paris', 'Paa', '2020-02-20 12:00:00', '2020-02-20 15:00:00', (SELECT status_id FROM flight_statuses WHERE status_name='PLANNED'));
INSERT INTO flights (departure_city, departure_airport, arrival_city, arrival_airport, departure_date, arrival_date, status_id)
VALUES ('Paris', 'Paa', 'New York', 'NYaa', '2020-02-20 12:00:00', '2020-02-20 15:00:00', (SELECT status_id FROM flight_statuses WHERE status_name='PLANNED'));
