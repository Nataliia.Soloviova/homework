package com.epam.rd.november2019.service.impl;


import com.epam.rd.november2019.converter.FlightConverter;
import com.epam.rd.november2019.dao.EmployeeScheduleDao;
import com.epam.rd.november2019.dao.FlightDao;
import com.epam.rd.november2019.exception.FlightAlreadyExistException;
import com.epam.rd.november2019.model.Flight;
import com.epam.rd.november2019.model.FlightStatus;
import com.epam.rd.november2019.service.FlightService;
import com.epam.rd.november2019.web.dto.FlightCreateDto;
import com.epam.rd.november2019.web.dto.FlightViewDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Service
public class FlightServiceImpl implements FlightService {

    private final FlightDao flightDao;
    private final EmployeeScheduleDao employeeScheduleDao;
    private final FlightConverter flightConverter;

    @Autowired
    public FlightServiceImpl(FlightDao flightDao,
                             EmployeeScheduleDao employeeScheduleDao,
                             FlightConverter flightConverter) {
        this.flightDao = flightDao;
        this.employeeScheduleDao = employeeScheduleDao;
        this.flightConverter = flightConverter;
    }

    @Override
    public List<FlightViewDto> getAllFlights() {
        List<FlightViewDto> dtoList = new ArrayList<>();
        List<Flight> flights = flightDao.getAll();
        for (Flight flight :
                flights) {
            flight.setFlightTeam(employeeScheduleDao.getFlightTeam(flight.getFlightId()));
            dtoList.add(flightConverter.asFlightViewDto(flight));
        }
        return dtoList;
    }

    @Override
    public FlightViewDto addFlight(FlightCreateDto dto) {
        Flight flight = flightConverter.asFlight(dto);
        if (flightDao.getId(flight) != -1) {
            throw new FlightAlreadyExistException("This flight already exist!");
        }
        flight = flightDao.add(flight);
        return flightConverter.asFlightDto(flight);
    }

    @Override
    public boolean removeFlight(int flightId) {
        if (flightDao.remove(flightId)) {
            return employeeScheduleDao.removeByFlightId(flightId);
        } else {
            return false;
        }
    }

    @Override
    public boolean editFlight(int flightId, FlightCreateDto dto) {
        Flight newFlight = flightConverter.asFlight(dto);
        if (flightDao.edit(flightId, newFlight)) {
            return employeeScheduleDao.removeByFlightId(flightId);
        }
        return false;
    }

    @Override
    public boolean edit(int flightId, FlightStatus flightStatus) {
        return flightDao.edit(flightId, flightStatus);
    }

    @Override
    public List<FlightViewDto> findFlights(FlightCreateDto dto) {
        List<FlightViewDto> dtoList = new LinkedList<>();
        Flight searchFlight = flightConverter.asFlight(dto);
        List<Flight> flights = flightDao.findFlights(searchFlight);
        for (Flight flight :
                flights) {
            flight.setFlightTeam(employeeScheduleDao.getFlightTeam(flight.getFlightId()));
            dtoList.add(flightConverter.asFlightViewDto(flight));
        }
        return dtoList;
    }

    @Override
    public List<FlightViewDto> sortByFlightName() {
        List<FlightViewDto> dtoList = new ArrayList<>();
        List<Flight> flights = flightDao.sortByFlightName();
        for (Flight flight :
                flights) {
            flight.setFlightTeam(employeeScheduleDao.getFlightTeam(flight.getFlightId()));
            dtoList.add(flightConverter.asFlightDto(flight));
        }
        return dtoList;
    }

    @Override
    public List<FlightViewDto> sortById() {
        List<FlightViewDto> dtoList = new LinkedList<>();
        List<Flight> flights = flightDao.sortById();
        for (Flight flight :
                flights) {
            flight.setFlightTeam(employeeScheduleDao.getFlightTeam(flight.getFlightId()));
            dtoList.add(flightConverter.asFlightDto(flight));
        }
        return dtoList;
    }

    @Override
    public FlightViewDto getById(int flightId) {
        Flight flight = flightDao.getById(flightId);
        flight.setFlightTeam(employeeScheduleDao.getFlightTeam(flightId));

        return flightConverter.asFlightDto(flight);
    }
}
