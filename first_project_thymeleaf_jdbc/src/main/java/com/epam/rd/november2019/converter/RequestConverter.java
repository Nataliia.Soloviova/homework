package com.epam.rd.november2019.converter;

import com.epam.rd.november2019.model.Request;
import com.epam.rd.november2019.web.dto.RequestCreateDto;
import com.epam.rd.november2019.web.dto.RequestViewDto;
import org.springframework.stereotype.Component;

@Component
public class RequestConverter {

    public Request asRequest(RequestCreateDto dto) {
        Request request = new Request();

        return request;
    }

    public RequestViewDto asRequestDto(Request request) {
        RequestViewDto dto = new RequestViewDto();

        return dto;
    }
}
