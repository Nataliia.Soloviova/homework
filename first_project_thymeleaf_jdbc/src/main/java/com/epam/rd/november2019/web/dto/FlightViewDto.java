package com.epam.rd.november2019.web.dto;


import com.epam.rd.november2019.model.FlightStatus;
import com.epam.rd.november2019.model.FlightTeam;

import java.io.Serializable;
import java.util.Objects;

public class FlightViewDto implements Serializable {

    private static final long serialVersionUID = 4414342883098637633L;

    private int flightId;
    private String departureAirport;
    private String arrivalAirport;
    private String departureCity;
    private String arrivalCity;
    private String departureDate;
    private String arrivalDate;
    private FlightStatus status;
    private FlightTeam flightTeam;

    public FlightViewDto() {
    }

    public FlightViewDto(String departureAirport, String arrivalAirport, String departureCity, String arrivalCity, String departureDate, String arrivalDate, FlightStatus status) {
        this.departureAirport = departureAirport;
        this.arrivalAirport = arrivalAirport;
        this.departureCity = departureCity;
        this.arrivalCity = arrivalCity;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.status = status;
    }

    public FlightViewDto(int flightId, String departureAirport, String arrivalAirport, String departureCity, String arrivalCity, String departureDate, String arrivalDate, FlightStatus status) {
        this.flightId = flightId;
        this.departureAirport = departureAirport;
        this.arrivalAirport = arrivalAirport;
        this.departureCity = departureCity;
        this.arrivalCity = arrivalCity;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.status = status;
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public String getDepartureAirport() {
        return departureAirport;
    }

    public void setDepartureAirport(String departureAirport) {
        this.departureAirport = departureAirport;
    }

    public String getArrivalAirport() {
        return arrivalAirport;
    }

    public void setArrivalAirport(String arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
    }

    public String getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(String departureCity) {
        this.departureCity = departureCity;
    }

    public String getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public FlightStatus getStatus() {
        return status;
    }

    public void setStatus(FlightStatus status) {
        this.status = status;
    }

    public FlightTeam getFlightTeam() {
        return flightTeam;
    }

    public void setFlightTeam(FlightTeam flightTeam) {
        this.flightTeam = flightTeam;
    }

    @Override
    public String toString() {
        return "{" +
                "flightId=" + flightId +
                ", departureCity='" + departureCity + '\'' +
                ", departureAirport='" + departureAirport + '\'' +
                ", departureDate=" + departureDate +
                ", arrivalCity='" + arrivalCity + '\'' +
                ", arrivalAirport='" + arrivalAirport + '\'' +
                ", arrivalDate=" + arrivalDate +
                ", status=" + status +
                '}';
    }
}
