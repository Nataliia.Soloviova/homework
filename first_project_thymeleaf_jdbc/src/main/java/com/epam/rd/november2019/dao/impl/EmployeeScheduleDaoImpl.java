package com.epam.rd.november2019.dao.impl;


import com.epam.rd.november2019.dao.EmployeeScheduleDao;
import com.epam.rd.november2019.model.EmployeeSchedule;
import com.epam.rd.november2019.model.Flight;
import com.epam.rd.november2019.model.FlightTeam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class EmployeeScheduleDaoImpl implements EmployeeScheduleDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeScheduleDaoImpl.class);

    private static final int TWELVE_HOURS = 43_200_000;

    private static final String GET_ALL = "SELECT * FROM employee_schedule";

    private static final String CHANGE_START_LOCATION = "UPDATE employee_schedule SET start_location = ? WHERE employee_id = ? AND start_location = ? AND flight_id = 0";

    private static final String ADD_SCHEDULE = "INSERT INTO employee_schedule (employee_id, flight_id, start_workday, finish_workday, start_location, end_location) VALUES (?, ?, ?, ?, ?, ?)";

    private static final String REMOVE_SCHEDULE_BY_EMPLOYEE_ID = "DELETE FROM employee_schedule WHERE employee_id = ?";

    private static final String REMOVE_SCHEDULE_BY_FLIGHT_ID = "DELETE FROM employee_schedule WHERE flight_id = ?";

    private static final String REMOVE_FROM_FLIGHT_TEAM = "DELETE FROM employee_schedule WHERE employee_id= ? AND flight_id = ?";

    private static final String GET_FREE_EMPLOYEE_BY_ROLE_AND_LOCATION = "SELECT employee_id FROM employee_schedule WHERE start_location = ? AND start_workday NOT BETWEEN ? AND ?";

    private static final String GET_BY_IDS = "SELECT * FROM employee_schedule WHERE employee_id = ? AND flight_id = ?";

    private static final String GET_FLIGHT_TEAM = "SELECT employee_id FROM employee_schedule WHERE flight_id = ?";

    @Autowired
    private DataSource dataSource;

    @Override
    public List<EmployeeSchedule> getAll() {
        LOGGER.debug("Trying to get all elements from table: employee_schedule");
        ArrayList<EmployeeSchedule> employeeSchedules = new ArrayList<>();
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GET_ALL)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                employeeSchedules.add(new EmployeeSchedule(
                        resultSet.getInt("employee_id"),
                        resultSet.getInt("flight_id"),
                        resultSet.getTimestamp("start_workday"),
                        resultSet.getTimestamp("finish_workday"),
                        resultSet.getString("start_location"),
                        resultSet.getString("end_location")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (employeeSchedules.size() > 0) {
            LOGGER.debug("Return {} schedules after get all elements from table: employee_schedule", employeeSchedules.size());
        } else {
            LOGGER.warn("Return empty list after get all elements from table: employee_schedule");
        }
        return employeeSchedules;
    }

    @Override
    public boolean changeStartLocation(int employeeId, String newStartLocation, String oldStartLocation) {
        LOGGER.debug("Trying to change employee schedule for employee with id: {}", employeeId);
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(CHANGE_START_LOCATION)) {
            preparedStatement.setString(1, newStartLocation);
            preparedStatement.setInt(2, employeeId);
            preparedStatement.setString(3, oldStartLocation);
            preparedStatement.executeUpdate();
            LOGGER.debug("Editing employee schedule was successful");
            return true;
        } catch (SQLException e) {
            LOGGER.warn("Can't change employee schedule with employee id {}", employeeId);
            return false;
        }

    }

    @Override
    public EmployeeSchedule add(EmployeeSchedule employeeSchedule) {
        LOGGER.debug("Trying to add {} to employee_schedule table", employeeSchedule);
        if (!checkNotNull(employeeSchedule.getEmployeeId(), employeeSchedule.getFlightId())) {
            try (Connection conn = dataSource.getConnection();
                 PreparedStatement preparedStatement = conn.prepareStatement(ADD_SCHEDULE)) {
                preparedStatement.setInt(1, employeeSchedule.getEmployeeId());
                preparedStatement.setInt(2, employeeSchedule.getFlightId());
                preparedStatement.setTimestamp(3, employeeSchedule.getStartWorkDay());
                preparedStatement.setTimestamp(4, employeeSchedule.getFinishWorkDay());
                preparedStatement.setString(5, employeeSchedule.getStartLocation());
                preparedStatement.setString(6, employeeSchedule.getFinishLocation());
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            LOGGER.debug("Employee schedule {} added to employee_schedule table", employeeSchedule);
        } else {
            LOGGER.warn("Employee schedule for employee id {} and flight id {} already exist", employeeSchedule.getEmployeeId(), employeeSchedule.getFlightId());
        }
        return employeeSchedule;
    }

    @Override
    public boolean removeByEmployeeId(int employeeId) {
        LOGGER.debug("Trying to remove employee schedule for employee id {} from employee_schedule table", employeeId);
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(REMOVE_SCHEDULE_BY_EMPLOYEE_ID)) {
            preparedStatement.setInt(1, employeeId);
            preparedStatement.executeUpdate();
            LOGGER.debug("Schedule for employee id {} removed from employee_schedule table", employeeId);
            return true;
        } catch (SQLException e) {
            LOGGER.warn("Can't remove schedule for employeeId {}. It doesn't exist", employeeId);
            return false;
        }
    }

    @Override
    public boolean removeByFlightId(int flightId) {
        LOGGER.debug("Trying to remove employee schedule for flight id {} from employee_schedule table", flightId);
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(REMOVE_SCHEDULE_BY_FLIGHT_ID)) {
            preparedStatement.setInt(1, flightId);
            preparedStatement.executeUpdate();
            LOGGER.debug("Schedule for flight id {} removed from employee_schedule table", flightId);
            return true;
        } catch (SQLException e) {
            LOGGER.warn("Can't remove schedule for flight id {}. It doesn't exist", flightId);
            return false;
        }
    }

    @Override
    public void removeEmployeeFromFlightTeam(int employeeId, int flightId) {
        LOGGER.debug("Trying to remove employee from flight team id {} from employee_schedule table", flightId);
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(REMOVE_FROM_FLIGHT_TEAM)) {
            preparedStatement.setInt(1, employeeId);
            preparedStatement.setInt(2, flightId);
            preparedStatement.executeUpdate();
            LOGGER.debug("Employee {} removed from flight team id {}", employeeId, flightId);
        } catch (SQLException e) {
            LOGGER.warn("Can't remove employee {} from flight team id {}.", employeeId, flightId);
        }
    }

    @Override
    public synchronized EmployeeSchedule getByIds(int employeeId, int flightId) {
        LOGGER.debug("Trying to get employee schedule by employee id {} and flight id {}", employeeId, flightId);
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GET_BY_IDS)) {
            preparedStatement.setInt(1, employeeId);
            preparedStatement.setInt(2, flightId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                LOGGER.debug("Employee schedule with employee id {} and flight id {} is returned", employeeId, flightId);
                return new EmployeeSchedule(
                        resultSet.getInt("employee_id"),
                        resultSet.getInt("flight_id"),
                        resultSet.getTimestamp("start_workday"),
                        resultSet.getTimestamp("finish_workday"),
                        resultSet.getString("start_location"),
                        resultSet.getString("end_location")
                );
            } else {
                LOGGER.warn("Employee schedule with employee id {} and flight id {} doesn't exist", employeeId, flightId);
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public synchronized List<Integer> getFreeEmployee(Flight flight) {
        List<Integer> freeEmployees = new ArrayList<>();
        LOGGER.debug("Trying to get list of free employee id from employee_schedule table for flight {}", flight);
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GET_FREE_EMPLOYEE_BY_ROLE_AND_LOCATION)) {
            preparedStatement.setString(1, flight.getDepartureCity());
            preparedStatement.setTimestamp(2, startFreePeriod(flight.getDepartureDate()));
            preparedStatement.setTimestamp(3, endFreePeriod(flight.getArrivalDate()));
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                freeEmployees.add(resultSet.getInt("employee_id"));
            }
        } catch (SQLException e) {
            LOGGER.warn("Can not find free employee for flight {}", flight);
        }
        if (freeEmployees.size() > 0) {
            LOGGER.debug("List of free employee id for flight {} is returned", flight);
        } else {
            LOGGER.warn("There is no one free employee for flight {}", flight);
        }
        return freeEmployees;
    }

    @Override
    public FlightTeam getFlightTeam(int flightId) {
        List<Integer> employeeIds = new ArrayList<>();
        LOGGER.debug("Trying to get flight team from employee_schedule table for flight {}", flightId);
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GET_FLIGHT_TEAM)) {
            preparedStatement.setInt(1, flightId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                employeeIds.add(resultSet.getInt("employee_id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (employeeIds.size() > 0) {
            LOGGER.debug("Flight team has {} employees", employeeIds.size());
        } else {
            LOGGER.warn("There is no one employee in flight team for flight {}", flightId);
        }
        return new FlightTeam(employeeIds);
    }

    private Timestamp startFreePeriod(Timestamp startDate) {
        return new Timestamp(startDate.getTime() - TWELVE_HOURS);
    }

    private Timestamp endFreePeriod(Timestamp endDate) {
        return new Timestamp(endDate.getTime() + TWELVE_HOURS);
    }

    private boolean checkNotNull(int employeeId, int flightId) {
        return getByIds(employeeId, flightId) != null;
    }
}
