package com.epam.rd.november2019.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RequestController {

    @RequestMapping("/requests")
    public String getListRequest(Model model) {

        return "requests";
    }

    @GetMapping("/flight/{id}/request")
    public String getRequestForm(@PathVariable("id") int flightId, Model model) {
        model.addAttribute("flightId", flightId);

        return "request_data";
    }

    @PostMapping("/flight/{id}/request/add")
    public String postRequest() {

        return "redirect:/requests";
    }
}
