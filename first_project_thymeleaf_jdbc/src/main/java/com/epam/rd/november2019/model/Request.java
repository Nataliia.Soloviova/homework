package com.epam.rd.november2019.model;

import java.sql.Timestamp;
import java.util.Objects;

public class Request {

    private int requestId;
    private int flightId;
    private String requestBy;
    private Timestamp requestDate;
    private RequestStatus requestStatus;
    private String requesterComment;
    private String approverComment;

    public Request() {
    }

    public Request(int requestId, int flightId, String requestBy, Timestamp requestDate, String requesterComment) {
        this.requestId = requestId;
        this.flightId = flightId;
        this.requestBy = requestBy;
        this.requestDate = requestDate;
        this.requesterComment = requesterComment;
    }

    public Request(int requestId, int flightId, String requestBy, Timestamp requestDate, RequestStatus requestStatus, String requesterComment, String approverComment) {
        this.requestId = requestId;
        this.flightId = flightId;
        this.requestBy = requestBy;
        this.requestDate = requestDate;
        this.requestStatus = requestStatus;
        this.requesterComment = requesterComment;
        this.approverComment = approverComment;
    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public String getRequestBy() {
        return requestBy;
    }

    public void setRequestBy(String requestBy) {
        this.requestBy = requestBy;
    }

    public Timestamp getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Timestamp requestDate) {
        this.requestDate = requestDate;
    }

    public RequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(RequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getRequesterComment() {
        return requesterComment;
    }

    public void setRequesterComment(String requesterComment) {
        this.requesterComment = requesterComment;
    }

    public String getApproverComment() {
        return approverComment;
    }

    public void setApproverComment(String approverComment) {
        this.approverComment = approverComment;
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Request request = (Request) o;
        return requestId == request.requestId &&
                flightId == request.flightId &&
                Objects.equals(requestBy, request.requestBy) &&
                Objects.equals(requestDate, request.requestDate) &&
                requestStatus == request.requestStatus &&
                Objects.equals(requesterComment, request.requesterComment) &&
                Objects.equals(approverComment, request.approverComment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestId, requestBy, requestDate, requestStatus, requesterComment, approverComment, flightId);
    }

    @Override
    public String toString() {
        return "Request{" +
                "requestId=" + requestId +
                ", requestBy='" + requestBy + '\'' +
                ", requestDate=" + requestDate +
                ", requestStatus=" + requestStatus +
                ", requesterComment='" + requesterComment + '\'' +
                ", approverComment='" + approverComment + '\'' +
                ", flightId=" + flightId +
                '}';
    }
}
