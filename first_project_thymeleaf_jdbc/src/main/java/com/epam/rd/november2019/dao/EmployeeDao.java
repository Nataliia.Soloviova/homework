package com.epam.rd.november2019.dao;

import com.epam.rd.november2019.model.Employee;

import java.util.List;

public interface EmployeeDao {

    Employee add(Employee employee);

    boolean edit(Employee employee);

    boolean remove(int employeeId);

    Employee getById(int employeeId);

    List<Employee> getAll();

    int getId(Employee employee);

    List<Employee> findEmployee(Employee employee);
}
