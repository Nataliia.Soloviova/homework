package com.epam.rd.november2019.dao.impl;

import com.epam.rd.november2019.dao.RequestDao;
import com.epam.rd.november2019.model.Request;
import com.epam.rd.november2019.model.RequestStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class RequestDaoImpl implements RequestDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeDaoImpl.class);

    private static final String ADD_REQUEST = "INSERT INTO requests (flight_id, request_by, request_date, requester_comment, approver_comment, status_id) VALUES (?, ?, ?, ?, ? (SELECT status_id FROM request_statuses WHERE status_name=?))";

    private static final String GET_ALL = "SELECT request_id, flight_id, request_by, request_date, requester_comment, approver_comment, request_statuses.status_name FROM request_statuses JOIN requests ON requests.status_id=request_statuses.status_id";

    @Autowired
    private DataSource dataSource;

    @Override
    public List<Request> getAll() {
        LOGGER.debug("Trying to get all requests from table: requests");
        ArrayList<Request> requests = new ArrayList<>();
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GET_ALL)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                requests.add(new Request(
                        resultSet.getInt("request_id"),
                        resultSet.getInt("flight_id"),
                        resultSet.getString("request_by"),
                        resultSet.getTimestamp("request_date"),
                        RequestStatus.valueOf(resultSet.getString("request_statuses")),
                        resultSet.getString("requester_comment"),
                        resultSet.getString("approver_comment")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (requests.size() > 0) {
            LOGGER.debug("Return {} requests after get all requests", requests.size());
        } else {
            LOGGER.warn("Return empty list after get all requests");
        }

        return requests;
    }

    @Override
    public Request add(Request request) {
        LOGGER.debug("Trying to add request {} to requests table", request);
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(ADD_REQUEST, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setInt(1, request.getFlightId());
            preparedStatement.setString(2, request.getRequestBy());
            preparedStatement.setTimestamp(3, request.getRequestDate());
            preparedStatement.setString(4, request.getRequesterComment());
            preparedStatement.setString(5, request.getApproverComment());
            preparedStatement.setString(6, request.getRequestStatus().toString());
            preparedStatement.executeUpdate();

            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                request.setRequestId(generatedKeys.getInt("request_id"));
            }
        } catch (SQLException e) {
            LOGGER.warn("Failed to insert request into DB");
        }

        return request;
    }
}
