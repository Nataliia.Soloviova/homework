package com.epam.rd.november2019.web.dto;

import com.epam.rd.november2019.model.UserRole;

import java.io.Serializable;

public class UserCreateDto implements Serializable {

    private static final long serialVersionUID = 1108905749709775122L;

    private String email;
    private String userName;
    private String password;
    private UserRole userRole;

    public UserCreateDto() {
    }

    public UserCreateDto(String email, String userName, String password, UserRole userRole) {
        this.email = email;
        this.userName = userName;
        this.password = password;
        this.userRole = userRole;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    @Override
    public String toString() {
        return "UserAccountCreateDto{" +
                "userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", userRole=" + userRole +
                '}';
    }
}
