package com.epam.rd.november2019.service;

import com.epam.rd.november2019.model.FlightTeam;
import com.epam.rd.november2019.web.dto.EmployeeViewDto;

import java.util.List;

public interface FlightTeamService {

    FlightTeam createFlightTeam(List<Integer> employeeIds, int flightId);

    List<EmployeeViewDto> getFreeEmployees(int flightId);

    void removeEmployeeFromFlightTeam(int employeeId, int flightId);
}
