package com.epam.rd.november2019.converter;


import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class DateTimeConverter {

    public Timestamp toTimestamp(String param) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        try {
            Date date = simpleDateFormat.parse(param);
            return new Timestamp(date.getTime());
        } catch (ParseException e) {
            throw new RuntimeException("Can not parse");
        }
    }

    public String toDateTimeString(Timestamp timestamp) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        Date date = new Date(timestamp.getTime());

        return simpleDateFormat.format(date);
    }

    public String toViewString(Timestamp timestamp) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm dd.MM.yyyy");
        Date date = new Date(timestamp.getTime());

        return simpleDateFormat.format(date);
    }
}
