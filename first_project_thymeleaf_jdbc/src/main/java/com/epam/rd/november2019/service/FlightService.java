package com.epam.rd.november2019.service;


import com.epam.rd.november2019.model.FlightStatus;
import com.epam.rd.november2019.web.dto.FlightCreateDto;
import com.epam.rd.november2019.web.dto.FlightViewDto;

import java.util.List;

public interface FlightService {

    List<FlightViewDto> getAllFlights();

    FlightViewDto addFlight(FlightCreateDto dto);

    boolean removeFlight(int flightId);

    boolean editFlight(int flightId, FlightCreateDto dto);

    boolean edit(int flightId, FlightStatus flightStatus);

    List<FlightViewDto> findFlights(FlightCreateDto dto);

    List<FlightViewDto> sortByFlightName();

    List<FlightViewDto> sortById();

    FlightViewDto getById(int flightId);
}
