package com.epam.rd.november2019.web.dto;

import com.epam.rd.november2019.model.RequestStatus;

import java.io.Serializable;
import java.sql.Timestamp;

public class RequestCreateDto implements Serializable {

    private static final long serialVersionUID = -6107310279055303125L;

    private int requestId;
    private int flightId;
    private String requestBy;
    private Timestamp requestDate;
    private RequestStatus requestStatus;
    private String requesterComment;
    private String approverComment;

    public int getRequestId() {
        return requestId;
    }

    public int getFlightId() {
        return flightId;
    }

    public String getRequestBy() {
        return requestBy;
    }

    public Timestamp getRequestDate() {
        return requestDate;
    }

    public RequestStatus getRequestStatus() {
        return requestStatus;
    }

    public String getRequesterComment() {
        return requesterComment;
    }

    public String getApproverComment() {
        return approverComment;
    }
}
