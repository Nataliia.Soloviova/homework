package com.epam.rd.november2019.web.dto;


import com.epam.rd.november2019.model.FlightStatus;
import com.epam.rd.november2019.model.FlightTeam;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

public class FlightCreateDto implements Serializable {

    private static final long serialVersionUID = 3582227784241194540L;

    private int flightId;
    private String departureAirport;
    private String arrivalAirport;
    private String departureCity;
    private String arrivalCity;
    private Timestamp departureDate;
    private Timestamp arrivalDate;
    private FlightStatus status;
    private FlightTeam flightTeam;

    public FlightCreateDto() {
    }

    public FlightCreateDto(FlightStatus status) {
        this.status = status;
    }

    public FlightCreateDto(String departureAirport, String arrivalAirport, String departureCity, String arrivalCity, Timestamp departureDate, Timestamp arrivalDate, FlightStatus status) {
        this.departureAirport = departureAirport;
        this.arrivalAirport = arrivalAirport;
        this.departureCity = departureCity;
        this.arrivalCity = arrivalCity;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.status = status;
    }

    public FlightCreateDto(int flightId, String departureAirport, String arrivalAirport, String departureCity, String arrivalCity, Timestamp departureDate, Timestamp arrivalDate, FlightStatus status) {
        this.flightId = flightId;
        this.departureAirport = departureAirport;
        this.arrivalAirport = arrivalAirport;
        this.departureCity = departureCity;
        this.arrivalCity = arrivalCity;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.status = status;
        this.flightTeam = flightTeam;
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public String getDepartureAirport() {
        return departureAirport;
    }

    public void setDepartureAirport(String departureAirport) {
        this.departureAirport = departureAirport;
    }

    public String getArrivalAirport() {
        return arrivalAirport;
    }

    public void setArrivalAirport(String arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
    }

    public String getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(String departureCity) {
        this.departureCity = departureCity;
    }

    public String getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public Timestamp getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Timestamp departureDate) {
        this.departureDate = departureDate;
    }

    public Timestamp getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Timestamp arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public FlightStatus getStatus() {
        return status;
    }

    public void setStatus(FlightStatus status) {
        this.status = status;
    }

    public FlightTeam getFlightTeam() {
        return flightTeam;
    }

    public void setFlightTeam(FlightTeam flightTeam) {
        this.flightTeam = flightTeam;
    }

    @Override
    public String toString() {
        return "{" +
                "flightId=" + flightId +
                ", departureCity='" + departureCity + '\'' +
                ", departureAirport='" + departureAirport + '\'' +
                ", departureDate=" + departureDate +
                ", arrivalCity='" + arrivalCity + '\'' +
                ", arrivalAirport='" + arrivalAirport + '\'' +
                ", arrivalDate=" + arrivalDate +
                ", status=" + status +
                '}';
    }
}
