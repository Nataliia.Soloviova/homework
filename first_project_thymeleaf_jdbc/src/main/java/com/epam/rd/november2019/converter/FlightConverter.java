package com.epam.rd.november2019.converter;


import com.epam.rd.november2019.model.Flight;
import com.epam.rd.november2019.web.dto.FlightCreateDto;
import com.epam.rd.november2019.web.dto.FlightViewDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FlightConverter {

    @Autowired
    private DateTimeConverter dateTimeConverter;

    public Flight asFlight(FlightCreateDto dto) {
        Flight flight = new Flight();
        flight.setFlightId(dto.getFlightId());
        flight.setDepartureCity(dto.getDepartureCity());
        flight.setDepartureAirport(dto.getDepartureAirport());
        flight.setDepartureDate(dto.getDepartureDate());
        flight.setArrivalCity(dto.getArrivalCity());
        flight.setArrivalAirport(dto.getArrivalAirport());
        flight.setArrivalDate(dto.getArrivalDate());
        flight.setStatus(dto.getStatus());
        flight.setFlightTeam(dto.getFlightTeam());

        return flight;
    }

    public FlightViewDto asFlightDto(Flight flight) {
        FlightViewDto dto = new FlightViewDto();
        dto.setFlightId(flight.getFlightId());
        dto.setDepartureCity(flight.getDepartureCity());
        dto.setDepartureAirport(flight.getDepartureAirport());
        dto.setDepartureDate(dateTimeConverter.toDateTimeString(flight.getDepartureDate()));
        dto.setArrivalCity(flight.getArrivalCity());
        dto.setArrivalAirport(flight.getArrivalAirport());
        dto.setArrivalDate(dateTimeConverter.toDateTimeString(flight.getArrivalDate()));
        dto.setStatus(flight.getStatus());
        dto.setFlightTeam(flight.getFlightTeam());

        return dto;
    }

    public FlightViewDto asFlightViewDto(Flight flight) {
        FlightViewDto dto = new FlightViewDto();
        dto.setFlightId(flight.getFlightId());
        dto.setDepartureCity(flight.getDepartureCity());
        dto.setDepartureAirport(flight.getDepartureAirport());
        dto.setDepartureDate(dateTimeConverter.toViewString(flight.getDepartureDate()));
        dto.setArrivalCity(flight.getArrivalCity());
        dto.setArrivalAirport(flight.getArrivalAirport());
        dto.setArrivalDate(dateTimeConverter.toViewString(flight.getArrivalDate()));
        dto.setStatus(flight.getStatus());
        dto.setFlightTeam(flight.getFlightTeam());

        return dto;
    }
}
