# Hello! It's my study plan where you can see my progress in Java learning.

* homework_01 - GIT
* homework_02 - Maven. Logging 
* homework_03 - Java Core
* homework_04 - Stream API. Collections
* homework_05 - JDBC. Liquibase
* homework_06 - Java IO/NIO2
* homework_07 - JUnit. Mockito
* homework_08 - Concurrent. Multithreading
* homework_09 - Design Patterns
* homework_10 - Regular expressions. Serialization

### pet_project - My first pet project. Servlets, JSP, JSTL. Sessions. Localization.

* spring_homework_01 - Spring Core. IoC & DI. Beans. Configuration
* spring_homework_02 - Context Initialization. Bean definition. Bean factory. Bean post processors.
* spring_homework_03 - MVC implementation. MVC Architecture. Exceptions handling. Working with parameters/View Validation. File Upload.
* spring_homework_04 - Spring Batch. Spring Integration.
* spring_homework_05 - Spring Boot. Autoconfiguration. Health check. 
* spring_homework_06 - AOP. Annotation and xml
* spring_homework_07 - Spring security on pet project
* spring_homework_08 - Spring Testing. Unit and Integration testing
* spring_homework_09 - Spring Cloud
* spring_homework_10 - Hibernate
* docker-ht - Docker. Docker Compose
* spring_homework_12 - JMS. Correlation Identifier
* AWS homework where used S3, EC2, VPC, RDS 

### pet project - spring boot - jpa - data

### first_project - thymeleaf - jdbc
Used Spring Boot, Spring Security, Exception Handler, MariaDB, MVC Architecture, H2, Spring Testing

# Portfolio
### 1. pet-project bill-system:
Spring Boot, JSONDoc, REST, YAML, XML, H2, MariaDB, Spring Testing, Lombok, Design Patterns, JPA, Hibernate, Liquibase

