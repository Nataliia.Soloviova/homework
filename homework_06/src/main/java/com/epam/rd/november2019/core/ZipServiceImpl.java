package com.epam.rd.november2019.core;

import com.epam.rd.november2019.interfaces.ZipService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipServiceImpl implements ZipService {

    private static final Logger logger = LoggerFactory.getLogger(ZipServiceImpl.class);

    private static Path pathToZipFile;
    private static Path pathToFileOrFolder;
    private static BufferedReader bufferedReader;

    static {
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            logger.info("Please, enter the path to the file or directory to be archived:");
            pathToFileOrFolder = Paths.get(bufferedReader.readLine());

            logger.info("Please, enter name of zip-file without format:");
            String zipName = bufferedReader.readLine() + ".zip";

            logger.info("Create zip-file here? (Y/N)");
            while (true) {
                String answer = bufferedReader.readLine();
                if (answer.equalsIgnoreCase("N")) {
                    logger.info("Directory to create zip-file:");
                    pathToZipFile = Paths.get(bufferedReader.readLine() + "\\" + zipName);
                    break;
                } else if (answer.equalsIgnoreCase("Y")) {
                    if (pathToFileOrFolder.getName(pathToFileOrFolder.getNameCount() - 1).toString().contains(".")) {
                        pathToZipFile = Paths.get(pathToFileOrFolder.getRoot().toString()
                                + pathToFileOrFolder.subpath(0, pathToFileOrFolder.getNameCount() - 1).toString()
                                + "\\"
                                + zipName);
                        break;
                    } else {
                        pathToZipFile = Paths.get(pathToFileOrFolder.toString() + "\\" + zipName);
                        break;
                    }
                } else {
                    logger.info("Please, enter Y or N");
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void zipping() {
        if (Files.exists(pathToZipFile)) {
            logger.info("This file already exists! Do you want to replace it? (Y/N)");
            while (true) {
                try {
                    String secondAnswer = bufferedReader.readLine();
                    if (secondAnswer.equalsIgnoreCase("Y")) {
                        zippingFileOrFolder();
                        break;
                    } else if (secondAnswer.equalsIgnoreCase("N")) {
                        break;
                    } else {
                        logger.info("Please, enter Y or N!");
                    }
                } catch (IOException ex) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        } else {
            zippingFileOrFolder();
        }
    }

    private void zippingFileOrFolder() {
        if (pathToFileOrFolder.getName(pathToFileOrFolder.getNameCount() - 1).toString().contains(".")) {
            zippingFile();
        } else {
            zippingFolder();
        }
    }

    private void zippingFile() {
        try (ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(pathToZipFile.toString()));
             FileInputStream fileInputStream = new FileInputStream(pathToFileOrFolder.toString())) {
            ZipEntry zipEntry = new ZipEntry(pathToFileOrFolder.getName(pathToFileOrFolder.getNameCount() - 1).toString());
            zipOutputStream.putNextEntry(zipEntry);
            byte[] buffer = new byte[fileInputStream.available()];
            fileInputStream.read(buffer);
            zipOutputStream.write(buffer);
            zipOutputStream.closeEntry();
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    private void zippingFolder() {
        try (ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(pathToZipFile.toString()))) {
            Files.walkFileTree(pathToFileOrFolder, new SimpleFileVisitor<Path>() {
                        @Override
                        public FileVisitResult preVisitDirectory(final Path dir, final BasicFileAttributes attrs) throws IOException {
                            if (!pathToFileOrFolder.equals(dir)) {
                                zipOutputStream.putNextEntry(new ZipEntry(pathToFileOrFolder.relativize(dir).toString() + "/"));
                            }
                            return FileVisitResult.CONTINUE;
                        }

                        @Override
                        public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
                            if (!file.equals(pathToZipFile)) {
                                zipOutputStream.putNextEntry(new ZipEntry(pathToFileOrFolder.relativize(file).toString()));
                                Files.copy(file, zipOutputStream);
                            }
                            return FileVisitResult.CONTINUE;
                        }
                    }
            );
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
}
