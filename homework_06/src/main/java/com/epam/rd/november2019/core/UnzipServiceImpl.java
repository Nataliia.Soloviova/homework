package com.epam.rd.november2019.core;

import com.epam.rd.november2019.interfaces.UnzipService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class UnzipServiceImpl implements UnzipService {

    private static final Logger logger = LoggerFactory.getLogger(UnzipServiceImpl.class);

    private static String zipName;
    private static Path pathForUnzipFile;
    private static BufferedReader bufferedReader;

    static {
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            logger.info("Please, enter path to zip-file:");
            zipName = bufferedReader.readLine();
            if (!zipName.endsWith(".zip")) {
                throw new IllegalArgumentException("File must has zip-format!");
            }

            logger.info("Extract files here? (Y/N)");
            while (true) {
                String answer = bufferedReader.readLine();
                if (answer.equalsIgnoreCase("N")) {
                    logger.info("Files will be extracted to the folder:");
                    pathForUnzipFile = Paths.get(bufferedReader.readLine());
                    break;
                } else if (answer.equalsIgnoreCase("Y")) {
                    pathForUnzipFile = Paths.get(String.valueOf(Paths.get(zipName).getRoot()) +
                            Paths.get(zipName).subpath(0, Paths.get(zipName).getNameCount() - 1));
                    break;
                } else {
                    logger.info("Please, enter Y or N");
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void unzipping() {
        try (ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(zipName))) {
            ZipEntry zipEntry;
            while ((zipEntry = zipInputStream.getNextEntry()) != null) {
                String pathZipEntry = pathForUnzipFile + "\\" + zipEntry.getName();
                if (!zipEntry.isDirectory()) {
                    if (Files.exists(Paths.get(pathZipEntry))) {
                        logger.info("This file [{}] already exists! Do you want to replace it? (Y/N)", zipEntry.getName());
                        while (true) {
                            String answer = bufferedReader.readLine();
                            if (answer.equalsIgnoreCase("Y")) {
                                try (FileOutputStream fileOutputStream = new FileOutputStream(pathZipEntry)) {
                                    byte[] buffer = new byte[1024];
                                    int length;
                                    while ((length = zipInputStream.read(buffer)) > 0) {
                                        fileOutputStream.write(buffer, 0, length);
                                    }
                                    break;
                                }
                            } else if (answer.equalsIgnoreCase("N")) {
                                break;
                            } else {
                                logger.info("Please, enter Y or N");
                            }
                        }
                    }
                } else {
                    if (!Files.exists(Paths.get(pathZipEntry))) {
                        Files.createDirectory(Paths.get(pathZipEntry));
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            try {
                bufferedReader.close();
            } catch (IOException e) {
                e.getStackTrace();
            }
        }
    }
}
