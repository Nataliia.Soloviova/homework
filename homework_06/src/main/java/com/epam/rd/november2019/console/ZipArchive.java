package com.epam.rd.november2019.console;

import com.epam.rd.november2019.core.UnzipServiceImpl;
import com.epam.rd.november2019.core.ZipServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ZipArchive {

    private static final Logger logger = LoggerFactory.getLogger(ZipArchive.class);

    public static void main(String[] args) {
        logger.info("Do you want to zip or unzip? (0 or 1)");
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
            while (true) {
                String answer = bufferedReader.readLine();
                if (answer.equals("0")) {
                    ZipServiceImpl zipService = new ZipServiceImpl();
                    zipService.zipping();
                    break;
                } else if (answer.equals("1")) {
                    UnzipServiceImpl unzipService = new UnzipServiceImpl();
                    unzipService.unzipping();
                    break;
                } else {
                    logger.info("Please, enter 0 or 1!");
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
