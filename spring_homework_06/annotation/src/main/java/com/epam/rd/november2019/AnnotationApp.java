package com.epam.rd.november2019;

import com.epam.rd.november2019.service.AdminService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy
public class AnnotationApp {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(AnnotationApp.class);
        AdminService service = context.getBean(AdminService.class);
        service.saveEmployee("John");
    }
}
