package com.epam.rd.november2019.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class SaveLoggingAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(SaveLoggingAspect.class);

    @Pointcut("execution(* save*(..))")
    public void savePointcut() {
    }

    @Before("savePointcut()")
    public void logBeforeSaving(JoinPoint joinPoint) {
        LOGGER.info("Trying to save {} to database", joinPoint.getArgs());
    }

    @AfterReturning("savePointcut()")
    public void logAfterSaving(JoinPoint joinPoint) {
        try {
            LOGGER.info("Successful saving {} to database", joinPoint.getArgs());
        } catch (Throwable e) {
            LOGGER.warn("Unsuccessful saving {} to database", joinPoint.getArgs());
        }
    }

}
