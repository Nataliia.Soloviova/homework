package com.epam.rd.november2019.service;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class AdminService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminService.class);

    public String saveEmployee(String employee) {
        LOGGER.info("Saving employee: " + employee);

        return employee;
    }
}
