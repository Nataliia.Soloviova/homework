package com.epam.rd.november2019.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class AopNamespaceAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(AopNamespaceAspect.class);

    public void logAround(ProceedingJoinPoint joinPoint) {
        try {
            String declaringTypeName = joinPoint.getSignature().getDeclaringTypeName();
            String name = joinPoint.getSignature().getName();
            Object[] args = joinPoint.getArgs();

            LOGGER.info("Object type: {}", declaringTypeName);
            LOGGER.info("Method name: {}", name);

            LOGGER.info("Args:");
            if (args.length != 0) {
                for (Object arg :
                        args) {
                    LOGGER.info("              ---> " + arg);
                }
            } else {
                LOGGER.info("No one argument");
            }

            long start = System.nanoTime();

            joinPoint.proceed();

            long end = System.nanoTime();

            LOGGER.info("Execution time of method {}: {} ms", name, (end - start) / 1_000_000);

        } catch (Throwable e) {
            throw new IllegalArgumentException("Method " + joinPoint.getSignature().getName() + " failed!", e);
        }
    }
}
