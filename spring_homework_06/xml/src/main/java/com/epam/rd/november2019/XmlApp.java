package com.epam.rd.november2019;


import com.epam.rd.november2019.service.AdminService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class XmlApp {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        AdminService service = (AdminService) context.getBean("adminService");
        service.saveEmployee("John");
    }
}
