package com.epam.rd.november2019.service;

import com.epam.rd.november2019.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class AdminService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminService.class);

    @Timed
    public void saveEmployee(String employee) {
        LOGGER.info("Saving employee: " + employee);
    }
}
