package com.epam.rd.november2019.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "flights")
@DynamicInsert
@DynamicUpdate
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Flight {

    @Id
    @Column(name = "flight_id", length = 16, nullable = false, updatable = false)
    private UUID flightId = UUID.randomUUID();

    @Column(name = "departure_city", nullable = false)
    private String departureCity;

    @Column(name = "departure_airport", nullable = false)
    private String departureAirport;

    @Column(name = "departure_date", nullable = false)
    private Timestamp departureDate;

    @Column(name = "arrival_city", nullable = false)
    private String arrivalCity;

    @Column(name = "arrival_airport", nullable = false)
    private String arrivalAirport;

    @Column(name = "arrival_date", nullable = false)
    private Timestamp arrivalDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "flight_status", nullable = false)
    private FlightStatus status;

    @OneToMany(orphanRemoval = true, mappedBy = "flight")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private List<Request> requests = new ArrayList<>();


    public Flight(String departureCity, String departureAirport, Timestamp departureDate, String arrivalCity, String arrivalAirport, Timestamp arrivalDate, FlightStatus status) {
        this.departureCity = departureCity;
        this.departureAirport = departureAirport;
        this.departureDate = departureDate;
        this.arrivalCity = arrivalCity;
        this.arrivalAirport = arrivalAirport;
        this.arrivalDate = arrivalDate;
        this.status = status;
    }

    public Flight(UUID flightId, String departureCity, String departureAirport, Timestamp departureDate, String arrivalCity, String arrivalAirport, Timestamp arrivalDate, FlightStatus status) {
        this.flightId = flightId;
        this.departureCity = departureCity;
        this.departureAirport = departureAirport;
        this.departureDate = departureDate;
        this.arrivalCity = arrivalCity;
        this.arrivalAirport = arrivalAirport;
        this.arrivalDate = arrivalDate;
        this.status = status;
    }

    public void addRequest(Request request) {
        this.requests.add(request);
        request.setFlight(this);
    }

    @Override
    public String toString() {
        return String.format("Flight{id=%s : %s : %s : %s : %s : %s : %s : status=%s : requests=%s}",
                flightId, departureCity, departureAirport, departureDate, arrivalCity, arrivalAirport, arrivalDate, status, requests.size());    }
}
