package com.epam.rd.november2019.dao.impl;


import com.epam.rd.november2019.dao.FlightDao;
import com.epam.rd.november2019.entity.Flight;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Repository
public class FlightDaoImpl implements FlightDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(FlightDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public void add(Flight flight) {
        Session session = sessionFactory.getCurrentSession();
        session.save(flight);
        LOGGER.info("Flight has been successfully saved. Flight details: {}", flight);
    }

    @Override
    public void remove(UUID flightId) {
        Flight flight = getById(flightId);
        sessionFactory.getCurrentSession().delete(flight);
        LOGGER.info("Flight has been successfully deleted. Flight details: {}", flight);
    }

    @Override
    @Transactional(readOnly = true)
    public Flight getById(UUID flightId) {
        Session session = sessionFactory.getCurrentSession();
        Flight flight = session.get(Flight.class, flightId);
        Objects.requireNonNull(flight, "Flight isn't found by id: " + flightId);
        LOGGER.info("Flight has been successfully got. Flight details: {}", flight);
        return flight;
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    public List<Flight> getAll() {
        List<Flight> flights = sessionFactory.getCurrentSession().createQuery("from Flight").list();

        for (Flight flight :
                flights) {
            LOGGER.info("Flight list: {}", flight);
        }

        return flights;
    }
}
