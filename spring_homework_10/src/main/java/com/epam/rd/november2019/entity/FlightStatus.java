package com.epam.rd.november2019.entity;


public enum FlightStatus {

    PLANNED("Planned"), // when flight is created
    FORMED("Formed"), // when flight team is created
    ON_TIME("On time"), // registration will be open on time
    CHECK_IN("Check in"), // registration has been started
    GATE_CLOSING("Gate closing"), // registration will be closed in few minutes
    BOARDING("Boarding"), // boarding now
    CANCELLED("Cancelled"), // flight is cancelled
    DELAYED("Delayed"), // flight is delayed
    DEPARTED("Departed"); // flight already departed

    private final String displayValue;

    FlightStatus(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}
