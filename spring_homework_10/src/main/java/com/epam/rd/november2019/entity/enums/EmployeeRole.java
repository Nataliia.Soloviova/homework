package com.epam.rd.november2019.entity.enums;

public enum EmployeeRole {
    PILOT("Pilot"),
    NAVIGATOR("Navigator"),
    RADIO_OPERATOR("Radio operator"),
    STEWARDESS("Stewardess");

    private final String displayValue;

    EmployeeRole(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}
