package com.epam.rd.november2019.service.impl;

import com.epam.rd.november2019.dao.RequestDao;
import com.epam.rd.november2019.dto.RequestViewDto;
import com.epam.rd.november2019.entity.Request;
import com.epam.rd.november2019.assembler.RequestAssembler;
import com.epam.rd.november2019.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RequestServiceImpl implements RequestService {

    private final RequestDao requestDao;
    private final RequestAssembler requestAssembler;

    @Autowired
    public RequestServiceImpl(RequestDao requestDao,
                              RequestAssembler requestAssembler) {
        this.requestDao = requestDao;
        this.requestAssembler = requestAssembler;
    }

    @Override
    public List<RequestViewDto> getAllRequests() {
        List<RequestViewDto> dtoList = new ArrayList<>();
        List<Request> requests = requestDao.getAll();
        for (Request request :
                requests) {
            dtoList.add(requestAssembler.assemble(request));
        }
        return dtoList;
    }
}
