package com.epam.rd.november2019.service;


import com.epam.rd.november2019.dto.FlightCreateDto;
import com.epam.rd.november2019.dto.FlightViewDto;

import java.util.List;
import java.util.UUID;

public interface FlightService {

    List<FlightViewDto> getAllFlights();

    FlightViewDto addFlight(FlightCreateDto dto);

    void removeFlight(UUID flightId);

    FlightViewDto editFlight(FlightCreateDto dto);

    FlightViewDto getFlightById(UUID flightId);
}
