package com.epam.rd.november2019.dto;


import com.epam.rd.november2019.entity.Employee;
import com.epam.rd.november2019.entity.Flight;
import com.epam.rd.november2019.entity.RequestStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestViewDto implements Serializable {

    private static final long serialVersionUID = -6107310279055303125L;

    private UUID requestId;
    private Flight flight;
    private Employee requestBy;
    private String requestDate;
    private RequestStatus requestStatus;
    private String requesterComment;
    private String approverComment;

    @Override
    public String toString() {
        return String.format("RequestViewDto{id=%s : flight=%s : employee=%s : date=%s : status=%s}", requestId, flight.getFlightId(), requestBy.getEmployeeId(), requestDate, requestStatus);
    }
}
