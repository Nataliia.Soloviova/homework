package com.epam.rd.november2019.service.impl;

import com.epam.rd.november2019.converter.UserConverter;
import com.epam.rd.november2019.dto.UserCreateDto;
import com.epam.rd.november2019.dto.UserViewDto;
import com.epam.rd.november2019.entity.User;
import com.epam.rd.november2019.exception.EntityAlreadyExistsException;
import com.epam.rd.november2019.repository.UserRepository;
import com.epam.rd.november2019.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserConverter userConverter;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           UserConverter userConverter) {
        this.userRepository = userRepository;
        this.userConverter = userConverter;
    }

    @Override
    public UserViewDto registerUser(UserCreateDto dto) {
        if (userRepository.findByEmail(dto.getEmail()) != null) {
            throw new EntityAlreadyExistsException("User with this email already exists");
        }

        User user = userConverter.asUser(dto);
        userRepository.save(user);

        return userConverter.asUserDto(user);
    }
}
