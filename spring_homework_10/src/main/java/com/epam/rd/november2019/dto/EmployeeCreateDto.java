package com.epam.rd.november2019.dto;


import com.epam.rd.november2019.entity.EmployeeRole;
import com.epam.rd.november2019.entity.Sex;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeCreateDto implements Serializable {

    private static final long serialVersionUID = -1467003435557820553L;

    private UUID employeeId;
    private String lastName;
    private String firstName;
    private EmployeeRole employeeRole;
    private Sex sex;
    private String city;

    public EmployeeCreateDto(String lastName, String firstName, EmployeeRole employeeRole, Sex sex, String city) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.employeeRole = employeeRole;
        this.sex = sex;
        this.city = city;
    }

    @Override
    public String toString() {
        return String.format("EmployeeCreateDto{id=%s : %s : %s : %s : %s : %s}", employeeId, employeeRole.getDisplayValue(), firstName, lastName, sex, city);
    }
}
