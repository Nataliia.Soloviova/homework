package com.epam.rd.november2019.dao.impl;


import com.epam.rd.november2019.dao.RequestDao;
import com.epam.rd.november2019.entity.Request;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class RequestDaoImpl implements RequestDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    public List<Request> getAll() {
        List<Request> requests = sessionFactory.getCurrentSession().createQuery("from Request").list();

        for (Request request :
                requests) {
            LOGGER.info("Request list: {}", request);
        }

        return requests;
    }
}
