package com.epam.rd.november2019.entity.enums;

public enum Sex {
    MAN("Man"),
    WOMAN("Woman");

    private final String displayValue;

    Sex(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}
