package com.epam.rd.november2019.dto;


import com.epam.rd.november2019.entity.enums.UserRole;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserCreateDto implements Serializable {

    private static final long serialVersionUID = 1108905749709775122L;

    private String email;
    private String userName;
    private String password;
    private UserRole userRole;

    @Override
    public String toString() {
        return String.format("UserCreateDto{email=%s : username=%s : role=%s}", email, userName, userRole);
    }
}
