package com.epam.rd.november2019.assembler.impl;

import com.epam.rd.november2019.converter.DateTimeConverter;
import com.epam.rd.november2019.dto.RequestCreateDto;
import com.epam.rd.november2019.dto.RequestViewDto;
import com.epam.rd.november2019.entity.Request;
import com.epam.rd.november2019.assembler.RequestAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class RequestAssemblerImpl implements RequestAssembler {

    private final DateTimeConverter converter;

    @Autowired
    public RequestAssemblerImpl(DateTimeConverter converter) {
        this.converter = converter;
    }

    @Override
    public Request assemble(RequestCreateDto dto) {
        Request request = new Request();
        request.setRequestId(dto.getRequestId());
        request.setFlight(dto.getFlight());
        request.setRequestBy(dto.getRequestBy());
        request.setRequestDate(dto.getRequestDate());
        request.setRequesterComment(dto.getRequesterComment());
        request.setApproverComment(dto.getApproverComment());
        request.setRequestStatus(dto.getRequestStatus());

        return request;
    }

    @Override
    public RequestViewDto assemble(Request entity) {
        RequestViewDto dto = new RequestViewDto();
        dto.setRequestId(entity.getRequestId());
        dto.setFlight(entity.getFlight());
        dto.setRequestBy(entity.getRequestBy());
        dto.setRequestDate(converter.toViewString(entity.getRequestDate()));
        dto.setRequesterComment(entity.getRequesterComment());
        dto.setApproverComment(entity.getApproverComment());
        dto.setRequestStatus(entity.getRequestStatus());

        return dto;
    }
}
