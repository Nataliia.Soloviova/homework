package com.epam.rd.november2019.service;


import com.epam.rd.november2019.dto.EmployeeCreateDto;
import com.epam.rd.november2019.dto.EmployeeViewDto;

import java.util.List;
import java.util.UUID;

public interface EmployeeService {

    List<EmployeeViewDto> getAllEmployees();

    EmployeeViewDto addEmployee(EmployeeCreateDto dto);

    void removeEmployee(UUID employeeId);

    void editEmployee(EmployeeCreateDto dto);

    EmployeeViewDto getEmployeeById(UUID employeeId);
}
