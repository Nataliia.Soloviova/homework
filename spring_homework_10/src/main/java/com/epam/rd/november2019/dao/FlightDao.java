package com.epam.rd.november2019.dao;


import com.epam.rd.november2019.entity.Flight;

import java.util.List;
import java.util.UUID;

public interface FlightDao {

    void add(Flight flight);

    List<Flight> getAll();

    void remove(UUID flightId);

    Flight getById(UUID flightId);
}
