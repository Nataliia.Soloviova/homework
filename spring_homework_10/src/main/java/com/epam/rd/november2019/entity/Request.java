package com.epam.rd.november2019.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
@Table(name = "requests")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Request {

    @Id
    @Column(name = "request_id", updatable = false, nullable = false)
    private UUID requestId = UUID.randomUUID();

    @ManyToOne
    @JoinColumn(name = "employee_id", nullable = false, updatable = false)
    private Employee requestBy;

    @Column(name = "request_date", nullable = false)
    private Timestamp requestDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "request_status")
    private RequestStatus requestStatus;

    @Column(name = "requester_comment", nullable = false)
    private String requesterComment;

    @Column(name = "approver_comment")
    private String approverComment;

    @ManyToOne
    @JoinColumn(name = "flight_id", nullable = false)
    private Flight flight;

    @Override
    public String toString() {
        return String.format("Request{id=%s : flight=%s : employee=%s : date=%s : status=%s}", requestId, flight.getFlightId(), requestBy.getEmployeeId(), requestDate, requestStatus);
    }
}
