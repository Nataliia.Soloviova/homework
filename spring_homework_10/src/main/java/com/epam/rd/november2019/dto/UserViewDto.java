package com.epam.rd.november2019.dto;


import com.epam.rd.november2019.entity.enums.UserRole;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserViewDto implements Serializable {

    private static final long serialVersionUID = 5461630002087134088L;

    private String email;
    private String userName;
    private UserRole userRole;

    @Override
    public String toString() {
        return String.format("UserViewDto{email=%s : username=%s : role=%s}", email, userName, userRole);
    }
}
