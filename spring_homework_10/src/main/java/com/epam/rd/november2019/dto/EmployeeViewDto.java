package com.epam.rd.november2019.dto;


import com.epam.rd.november2019.entity.EmployeeRole;
import com.epam.rd.november2019.entity.Sex;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeViewDto implements Serializable {

    private static final long serialVersionUID = 1853084038109202289L;

    private UUID employeeId;
    private String lastName;
    private String firstName;
    private EmployeeRole employeeRole;
    private Sex sex;
    private String city;

    @Override
    public String toString() {
        return String.format("EmployeeViewDto{id=%s : %s : %s : %s : %s : %s}", employeeId, employeeRole.getDisplayValue(), firstName, lastName, sex, city);
    }
}
