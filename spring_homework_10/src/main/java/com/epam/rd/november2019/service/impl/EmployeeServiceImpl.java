package com.epam.rd.november2019.service.impl;


import com.epam.rd.november2019.dao.EmployeeDao;
import com.epam.rd.november2019.dto.EmployeeCreateDto;
import com.epam.rd.november2019.dto.EmployeeViewDto;
import com.epam.rd.november2019.entity.Employee;
import com.epam.rd.november2019.assembler.EmployeeAssembler;
import com.epam.rd.november2019.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeDao employeeDao;
    private final EmployeeAssembler employeeAssembler;

    @Autowired
    public EmployeeServiceImpl(EmployeeDao employeeDao,
                               EmployeeAssembler employeeAssembler) {
        this.employeeDao = employeeDao;
        this.employeeAssembler = employeeAssembler;
    }

    @Override
    public List<EmployeeViewDto> getAllEmployees() {
        List<EmployeeViewDto> dtoList = new ArrayList<>();
        List<Employee> employees = employeeDao.getAll();
        for (Employee employee :
                employees) {
            dtoList.add(employeeAssembler.assemble(employee));
        }

        return dtoList;
    }

    @Override
    public EmployeeViewDto addEmployee(EmployeeCreateDto dto) {
        Employee employee = assemble(dto);
        employeeDao.add(employee);

        return employeeAssembler.assemble(employee);
    }

    private Employee assemble(EmployeeCreateDto dto) {
        Employee employee = new Employee();
        employee.setFirstName(dto.getFirstName());
        employee.setLastName(dto.getLastName());
        employee.setEmployeeRole(dto.getEmployeeRole());
        employee.setSex(dto.getSex());
        employee.setCity(dto.getCity());

        return employee;
    }

    @Override
    @Transactional
    public void editEmployee(EmployeeCreateDto dto) {
        Employee entity = employeeDao.getById(dto.getEmployeeId());
        Employee updatedEntity = employeeAssembler.assemble(dto);

        performUpdate(entity, updatedEntity);
    }

    private void performUpdate(Employee persistentEntity, Employee newEntity) {
        persistentEntity.setEmployeeRole(newEntity.getEmployeeRole());
        persistentEntity.setFirstName(newEntity.getFirstName());
        persistentEntity.setLastName(newEntity.getLastName());
        persistentEntity.setSex(newEntity.getSex());
        persistentEntity.setCity(newEntity.getCity());
    }

    @Override
    @Transactional
    public void removeEmployee(UUID employeeId) {
        employeeDao.remove(employeeId);
    }


    @Override
    @Transactional
    public EmployeeViewDto getEmployeeById(UUID employeeId) {
        Employee employee = employeeDao.getById(employeeId);

        return employeeAssembler.assemble(employee);
    }
}
