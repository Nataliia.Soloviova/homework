package com.epam.rd.november2019.service;

import com.epam.rd.november2019.dto.RequestViewDto;

import java.util.List;

public interface RequestService {

    List<RequestViewDto> getAllRequests();
}
