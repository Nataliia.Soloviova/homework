package com.epam.rd.november2019.assembler.impl;

import com.epam.rd.november2019.assembler.EmployeeAssembler;
import com.epam.rd.november2019.dao.EmployeeDao;
import com.epam.rd.november2019.dto.EmployeeCreateDto;
import com.epam.rd.november2019.dto.EmployeeViewDto;
import com.epam.rd.november2019.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EmployeeAssemblerImpl implements EmployeeAssembler {

    private final EmployeeDao employeeDao;

    @Autowired
    public EmployeeAssemblerImpl(EmployeeDao employeeDao) {
        this.employeeDao = employeeDao;
    }

    @Override
    public Employee assemble(EmployeeCreateDto dto) {
        Employee employee = new Employee();
        employee.setEmployeeId(dto.getEmployeeId());
        employee.setFirstName(dto.getFirstName());
        employee.setLastName(dto.getLastName());
        employee.setEmployeeRole(dto.getEmployeeRole());
        employee.setSex(dto.getSex());
        employee.setCity(dto.getCity());
        return employee;
    }

    @Override
    public EmployeeViewDto assemble(Employee employee) {
        EmployeeViewDto dto = new EmployeeViewDto();
        dto.setEmployeeId(employee.getEmployeeId());
        dto.setEmployeeRole(employee.getEmployeeRole());
        dto.setFirstName(employee.getFirstName());
        dto.setLastName(employee.getLastName());
        dto.setSex(employee.getSex());
        dto.setCity(employee.getCity());
        return dto;
    }
}
