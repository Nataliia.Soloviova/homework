package com.epam.rd.november2019.assembler.impl;


import com.epam.rd.november2019.converter.DateTimeConverter;
import com.epam.rd.november2019.dto.FlightCreateDto;
import com.epam.rd.november2019.dto.FlightViewDto;
import com.epam.rd.november2019.dto.RequestCreateDto;
import com.epam.rd.november2019.dto.RequestViewDto;
import com.epam.rd.november2019.entity.Flight;
import com.epam.rd.november2019.entity.Request;
import com.epam.rd.november2019.assembler.FlightAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class FlightAssemblerImpl implements FlightAssembler {

    private final DateTimeConverter converter;

    @Autowired
    public FlightAssemblerImpl(DateTimeConverter converter) {
        this.converter = converter;
    }

    @Override
    public Flight assemble(FlightCreateDto dto) {
        Flight flight = new Flight();
        flight.setFlightId(dto.getFlightId());
        flight.setDepartureCity(dto.getDepartureCity());
        flight.setDepartureAirport(dto.getDepartureAirport());
        flight.setDepartureDate(dto.getDepartureDate());
        flight.setArrivalCity(dto.getArrivalCity());
        flight.setArrivalAirport(dto.getArrivalAirport());
        flight.setArrivalDate(dto.getArrivalDate());
        flight.setStatus(dto.getStatus());

        for (RequestCreateDto requestDto :
                dto.getRequests()) {
            Request request = assemble(requestDto);
            flight.addRequest(request);
        }

        return flight;
    }

    @Override
    public FlightViewDto assemble(Flight flight) {
        FlightViewDto dto = new FlightViewDto();
        dto.setFlightId(flight.getFlightId());
        dto.setDepartureCity(flight.getDepartureCity());
        dto.setDepartureAirport(flight.getDepartureAirport());
        dto.setDepartureDate(converter.toDateTimeString(flight.getDepartureDate()));
        dto.setArrivalCity(flight.getArrivalCity());
        dto.setArrivalAirport(flight.getArrivalAirport());
        dto.setArrivalDate(converter.toDateTimeString(flight.getArrivalDate()));
        dto.setStatus(flight.getStatus());

        List<RequestViewDto> dtoList = flight.getRequests()
                .stream()
                .map(this::assemble)
                .collect(Collectors.toList());

        dto.setRequests(dtoList);

        return dto;
    }

    private Request assemble(RequestCreateDto dto) {
        Request request = new Request();
        request.setRequestId(null);
        request.setFlight(dto.getFlight());
        request.setApproverComment(dto.getApproverComment());
        request.setRequestBy(dto.getRequestBy());
        request.setRequestDate(dto.getRequestDate());
        request.setRequesterComment(dto.getRequesterComment());
        request.setRequestStatus(dto.getRequestStatus());

        return request;
    }

    private RequestViewDto assemble(Request request) {
        RequestViewDto dto = new RequestViewDto();
        dto.setRequestId(request.getRequestId());
        dto.setFlight(request.getFlight());
        dto.setApproverComment(request.getApproverComment());
        dto.setRequestBy(request.getRequestBy());
        dto.setRequestDate(converter.toDateTimeString(request.getRequestDate()));
        dto.setRequesterComment(request.getRequesterComment());
        dto.setRequestStatus(request.getRequestStatus());

        return dto;
    }
}
