package com.epam.rd.november2019.assembler;

import com.epam.rd.november2019.dto.EmployeeCreateDto;
import com.epam.rd.november2019.dto.EmployeeViewDto;
import com.epam.rd.november2019.entity.Employee;

public interface EmployeeAssembler {

    Employee assemble(EmployeeCreateDto dto);

    EmployeeViewDto assemble(Employee entity);
}
