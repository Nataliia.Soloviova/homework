package com.epam.rd.november2019.dao.impl;


import com.epam.rd.november2019.dao.EmployeeDao;
import com.epam.rd.november2019.entity.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Repository
public class EmployeeDaoImpl implements EmployeeDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public void add(Employee employee) {
        Session session = sessionFactory.getCurrentSession();
        session.save(employee);
        LOGGER.info("Employee has been successfully saved. Employee details: {}", employee);
    }

    @Override
    public void remove(UUID employeeId) {
        Employee employee = getById(employeeId);
        sessionFactory.getCurrentSession().delete(employee);
        LOGGER.info("Employee has been successfully deleted. Employee details: {}", employee);
    }

    @Override
    @Transactional
    public Employee getById(UUID employeeId) {
        Session session = sessionFactory.getCurrentSession();
        Employee employee = session.get(Employee.class, employeeId);
        Objects.requireNonNull(employee, "Employee isn't found by id: " + employeeId);
        LOGGER.info("Employee has been successfully got. Employee details: {}", employee);
        return employee;
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    public List<Employee> getAll() {
        List<Employee> employees = sessionFactory.getCurrentSession().createQuery("from Employee").list();

        for (Employee employee :
                employees) {
            LOGGER.info("Employee list: {}", employee);
        }

        return employees;
    }
}
