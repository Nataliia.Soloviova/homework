package com.epam.rd.november2019.exception;

public class EntityDoesNotExistException extends RuntimeException {

    public EntityDoesNotExistException() {
    }

    public EntityDoesNotExistException(String message) {
        super(message);
    }
}
