package com.epam.rd.november2019.assembler;

import com.epam.rd.november2019.dto.RequestCreateDto;
import com.epam.rd.november2019.dto.RequestViewDto;
import com.epam.rd.november2019.entity.Request;

public interface RequestAssembler {

    Request assemble(RequestCreateDto dto);

    RequestViewDto assemble(Request entity);
}
