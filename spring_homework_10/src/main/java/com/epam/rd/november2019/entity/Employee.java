package com.epam.rd.november2019.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "employees")
@DynamicInsert
@DynamicUpdate
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee {

    @Id
    @Column(name = "employee_id", length = 16, nullable = false, updatable = false)
    private UUID employeeId = UUID.randomUUID();

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Enumerated(EnumType.STRING)
    @Column(name = "employee_role", nullable = false)
    private EmployeeRole employeeRole;

    @Enumerated(EnumType.STRING)
    @Column(name = "sex", nullable = false)
    private Sex sex;

    @Column(name = "city", nullable = false)
    private String city;

    public Employee(String lastName, String firstName, EmployeeRole employeeRole, Sex sex, String city) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.employeeRole = employeeRole;
        this.sex = sex;
        this.city = city;
    }

    @Override
    public String toString() {
        return String.format("Employee{id=%s : %s : %s : %s : %s : %s}", employeeId, employeeRole.getDisplayValue(), firstName, lastName, sex, city);
    }
}
