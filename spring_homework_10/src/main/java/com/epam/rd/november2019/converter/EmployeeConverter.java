package com.epam.rd.november2019.converter;


import com.epam.rd.november2019.dto.EmployeeCreateDto;
import com.epam.rd.november2019.dto.EmployeeViewDto;
import com.epam.rd.november2019.entity.Employee;
import com.epam.rd.november2019.entity.Flight;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class EmployeeConverter {

    public Employee asEmployee(EmployeeCreateDto dto) {
        Employee employee = new Employee();
        employee.setEmployeeId(dto.getEmployeeId());
        employee.setFirstName(dto.getFirstName());
        employee.setLastName(dto.getLastName());
        employee.setEmployeeRole(dto.getEmployeeRole());
        employee.setSex(dto.getSex());
        employee.setCity(dto.getCity());

        return employee;
    }

    public EmployeeViewDto asDto(Employee employee) {
        EmployeeViewDto dto = new EmployeeViewDto();
        dto.setEmployeeId(employee.getEmployeeId());
        dto.setEmployeeRole(employee.getEmployeeRole());
        dto.setFirstName(employee.getFirstName());
        dto.setLastName(employee.getLastName());
        dto.setSex(employee.getSex());
        dto.setCity(employee.getCity());

        Set<UUID> flightIds = employee.getFlights()
                .stream()
                .map(Flight::getId)
                .collect(Collectors.toSet());
        dto.setFlights(flightIds);

        return dto;
    }
}
