package com.epam.rd.november2019.dao;


import com.epam.rd.november2019.entity.Employee;

import java.util.List;
import java.util.UUID;

public interface EmployeeDao {

    void add(Employee employee);

    void remove(UUID employeeId);

    Employee getById(UUID employeeId);

    List<Employee> getAll();
}
