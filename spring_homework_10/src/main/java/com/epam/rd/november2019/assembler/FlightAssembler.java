package com.epam.rd.november2019.assembler;


import com.epam.rd.november2019.dto.FlightCreateDto;
import com.epam.rd.november2019.dto.FlightViewDto;
import com.epam.rd.november2019.entity.Flight;

public interface FlightAssembler {

    Flight assemble(FlightCreateDto dto);

    FlightViewDto assemble(Flight entity);
}
