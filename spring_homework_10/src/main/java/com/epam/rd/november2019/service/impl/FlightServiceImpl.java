package com.epam.rd.november2019.service.impl;


import com.epam.rd.november2019.dao.FlightDao;
import com.epam.rd.november2019.dao.RequestDao;
import com.epam.rd.november2019.dto.FlightCreateDto;
import com.epam.rd.november2019.dto.FlightViewDto;
import com.epam.rd.november2019.dto.RequestCreateDto;
import com.epam.rd.november2019.entity.Flight;
import com.epam.rd.november2019.entity.Request;
import com.epam.rd.november2019.assembler.FlightAssembler;
import com.epam.rd.november2019.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.UUID.randomUUID;

@Service
public class FlightServiceImpl implements FlightService {

    private final FlightDao flightDao;
    private final RequestDao requestDao;
    private final FlightAssembler flightAssembler;

    @Autowired
    public FlightServiceImpl(FlightDao flightDao,
                             RequestDao requestDao,
                             FlightAssembler flightAssembler) {
        this.flightDao = flightDao;
        this.requestDao = requestDao;
        this.flightAssembler = flightAssembler;
    }

    @Override
    @Transactional(readOnly = true)
    public List<FlightViewDto> getAllFlights() {
        List<FlightViewDto> dtoList = new ArrayList<>();
        List<Flight> flights = flightDao.getAll();
        for (Flight flight :
                flights) {
            dtoList.add(flightAssembler.assemble(flight));
        }

        return dtoList;
    }

    @Override
    public FlightViewDto addFlight(FlightCreateDto dto) {
        Flight flight = assemble(dto);
        flightDao.add(flight);

        return flightAssembler.assemble(flight);
    }

    private Flight assemble(FlightCreateDto dto) {
        Flight flight = new Flight();
        flight.setDepartureCity(dto.getDepartureCity());
        flight.setDepartureAirport(dto.getDepartureAirport());
        flight.setDepartureDate(dto.getDepartureDate());
        flight.setArrivalCity(dto.getArrivalCity());
        flight.setArrivalAirport(dto.getArrivalAirport());
        flight.setArrivalDate(dto.getArrivalDate());
        flight.setStatus(dto.getStatus());

        for (RequestCreateDto requestDto :
                dto.getRequests()) {
            Request request = assemble(requestDto);
            flight.addRequest(request);
        }

        return flight;
    }

    private Request assemble(RequestCreateDto dto) {
        Request request = new Request();
        request.setFlight(dto.getFlight());
        request.setApproverComment(dto.getApproverComment());
        request.setRequestBy(dto.getRequestBy());
        request.setRequestDate(dto.getRequestDate());
        request.setRequesterComment(dto.getRequesterComment());
        request.setRequestStatus(dto.getRequestStatus());

        return request;
    }

    @Override
    @Transactional
    public FlightViewDto editFlight(FlightCreateDto dto) {
        Flight entity = flightDao.getById(dto.getFlightId());
        Flight updatedEntity = flightAssembler.assemble(dto);

        performUpdate(entity, updatedEntity);

        return flightAssembler.assemble(entity);
    }

    private void performUpdate(Flight persistentEntity, Flight newEntity) {
        persistentEntity.setDepartureCity(newEntity.getDepartureCity());
        persistentEntity.setDepartureAirport(newEntity.getDepartureAirport());
        persistentEntity.setDepartureDate(newEntity.getDepartureDate());
        persistentEntity.setArrivalCity(newEntity.getArrivalCity());
        persistentEntity.setArrivalAirport(newEntity.getArrivalAirport());
        persistentEntity.setArrivalDate(newEntity.getArrivalDate());
        persistentEntity.setStatus(newEntity.getStatus());

        updateRequests(persistentEntity.getRequests(), newEntity.getRequests());
    }

    private void updateRequests(List<Request> persistentRequests, List<Request> newRequests) {
        Map<UUID, Request> stillExistentRequests = newRequests
                .stream()
                .filter(j -> Objects.nonNull(j.getRequestId()))
                .collect(Collectors.toMap(Request::getRequestId, Function.identity()));

        List<Request> requestsToAdd = newRequests
                .stream()
                .filter(j -> Objects.isNull(j.getRequestId()))
                .map(j -> {
                    j.setRequestId(randomUUID());
                    return j;
                })
                .collect(Collectors.toList());

        Iterator<Request> persistentIterator = persistentRequests.iterator();
        while (persistentIterator.hasNext()) {
            Request persistentRequest = persistentIterator.next();
            if (stillExistentRequests.containsKey(persistentRequest.getRequestId())) {
                Request updatedRequest = stillExistentRequests.get(persistentRequest.getRequestId());
                updateRequest(persistentRequest, updatedRequest);
            } else {
                persistentIterator.remove();
                persistentRequest.setFlight(null);
            }
        }

        persistentRequests.addAll(requestsToAdd);
    }

    private void updateRequest(Request persistentEntity, Request newEntity) {
        persistentEntity.setFlight(newEntity.getFlight());
        persistentEntity.setApproverComment(newEntity.getApproverComment());
        persistentEntity.setRequestBy(newEntity.getRequestBy());
        persistentEntity.setRequestDate(newEntity.getRequestDate());
        persistentEntity.setRequesterComment(newEntity.getRequesterComment());
        persistentEntity.setRequestStatus(newEntity.getRequestStatus());
    }


    @Override
    @Transactional
    public void removeFlight(UUID flightId) {
        flightDao.remove(flightId);
    }


    @Override
    @Transactional(readOnly = true)
    public FlightViewDto getFlightById(UUID flightId) {
        Flight flight = flightDao.getById(flightId);
        return flightAssembler.assemble(flight);
    }
}
