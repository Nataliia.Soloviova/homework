package com.epam.rd.november2019.dto;

import com.epam.rd.november2019.entity.Employee;
import com.epam.rd.november2019.entity.Flight;
import com.epam.rd.november2019.entity.RequestStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestCreateDto implements Serializable {

    private UUID requestId;
    private Flight flight;
    private Employee requestBy;
    private Timestamp requestDate;
    private RequestStatus requestStatus;
    private String requesterComment;
    private String approverComment;

    @Override
    public String toString() {
        return String.format("RequestCreateDto{id=%s : flight=%s : employee=%s : date=%s : status=%s}", requestId, flight.getFlightId(), requestBy.getEmployeeId(), requestDate, requestStatus);
    }
}
