package com.epam.rd.november2019;

import org.hibernate.SessionFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.metamodel.EntityType;
import java.util.List;
import java.util.Set;

@ExtendWith(value = {SpringExtension.class})
@ContextConfiguration(classes = HibernateConfig.class)
public class ConfigurationTest {

    @Autowired
    private SessionFactory sessionFactory;

    @Test
    public void testConfigurationWorks() {
        Set<EntityType<?>> entities = sessionFactory.getMetamodel().getEntities();
        for (EntityType<?> entity : entities) {
            System.out.println(entity.getName());
        }
    }
}
