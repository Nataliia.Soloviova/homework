package com.epam.rd.november2019.service;

import com.epam.rd.november2019.HibernateConfig;
import com.epam.rd.november2019.dao.EmployeeDao;
import com.epam.rd.november2019.dto.EmployeeCreateDto;
import com.epam.rd.november2019.dto.EmployeeViewDto;
import com.epam.rd.november2019.entity.Employee;
import com.epam.rd.november2019.entity.EmployeeRole;
import com.epam.rd.november2019.entity.Sex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(value = {SpringExtension.class})
@ContextConfiguration(classes = {HibernateConfig.class})
public class EmployeeServiceImplIT {

    private static final String LAST_NAME = "Johnson";
    private static final String FIRST_NAME = "John";
    private static final EmployeeRole EMPLOYEE_ROLE = EmployeeRole.RADIO_OPERATOR;
    private static final Sex SEX = Sex.MAN;
    private static final String CITY = "New York";

    private final EmployeeService employeeService;
    private final EmployeeDao employeeDao;

    private Employee employee;
    private EmployeeCreateDto createDto;

    @Autowired
    public EmployeeServiceImplIT(EmployeeService employeeService, EmployeeDao employeeDao) {
        this.employeeService = employeeService;
        this.employeeDao = employeeDao;
    }

    @BeforeEach
    public void init() {
        employee = new Employee();
        employee.setFirstName(FIRST_NAME);
        employee.setLastName(LAST_NAME);
        employee.setSex(SEX);
        employee.setEmployeeRole(EMPLOYEE_ROLE);
        employee.setCity(CITY);

        createDto = new EmployeeCreateDto();
        createDto.setFirstName(FIRST_NAME);
        createDto.setLastName(LAST_NAME);
        createDto.setSex(SEX);
        createDto.setEmployeeRole(EMPLOYEE_ROLE);
        createDto.setCity(CITY);
    }

    @Test
    public void testAdd() {
        //Given

        //When
        EmployeeViewDto result = employeeService.addEmployee(createDto);

        //Then
        assertEquals(LAST_NAME, result.getLastName());
        assertEquals(FIRST_NAME, result.getFirstName());
        assertEquals(EMPLOYEE_ROLE, result.getEmployeeRole());
        assertEquals(CITY, result.getCity());
        assertEquals(SEX, result.getSex());
    }

    @Test
    public void testRemove() {
        //Given
        UUID id = employee.getEmployeeId();
        employeeDao.add(employee);

        //When
        employeeService.removeEmployee(id);
        NullPointerException ex = assertThrows(NullPointerException.class, () -> {
            employeeService.removeEmployee(id);
        });

        //Then
        assertTrue(ex.getMessage().contains("Employee isn't found by id: " + id));
    }

    @Test
    public void testGetById() {
        //Given
        UUID id = employee.getEmployeeId();
        employeeDao.add(employee);

        //When
        EmployeeViewDto result = employeeService.getEmployeeById(id);

        //Then
        assertEquals(employee.getFirstName(), result.getFirstName());
        assertEquals(employee.getLastName(), result.getLastName());
        assertEquals(employee.getCity(), result.getCity());
        assertEquals(employee.getEmployeeRole(), result.getEmployeeRole());
        assertEquals(employee.getSex(), result.getSex());
    }

    @Test
    public void testGetAll() {
        //Given
        List<Employee> expected = employeeDao.getAll();

        //When
        List<EmployeeViewDto> result = employeeService.getAllEmployees();

        //Then
        assertEquals(result.size(), expected.size());
    }

    @Test
    public void testEditEmployee() {
        //Given
        UUID id = employee.getEmployeeId();
        employeeDao.add(employee);

        String newFirstName = "Jack";
        createDto.setFirstName(newFirstName);
        createDto.setEmployeeId(id);

        //When
        employeeService.editEmployee(createDto);

        //Then
        assertEquals(newFirstName, employeeDao.getById(id).getFirstName());
    }
}
