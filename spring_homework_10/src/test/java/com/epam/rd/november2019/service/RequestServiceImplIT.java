package com.epam.rd.november2019.service;

import com.epam.rd.november2019.HibernateConfig;
import com.epam.rd.november2019.dao.RequestDao;
import com.epam.rd.november2019.dto.RequestViewDto;
import com.epam.rd.november2019.entity.Request;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(value = {SpringExtension.class})
@ContextConfiguration(classes = {HibernateConfig.class})
public class RequestServiceImplIT {

    private final RequestDao requestDao;
    private final RequestService requestService;

    @Autowired
    public RequestServiceImplIT(RequestDao requestDao,
                                RequestService requestService) {
        this.requestDao = requestDao;
        this.requestService = requestService;
    }

    @Test
    public void testGetAll() {
        //Given
        List<Request> expected = requestDao.getAll();

        //When
        List<RequestViewDto> result = requestService.getAllRequests();

        //Then
        assertEquals(result.size(), expected.size());
    }
}
