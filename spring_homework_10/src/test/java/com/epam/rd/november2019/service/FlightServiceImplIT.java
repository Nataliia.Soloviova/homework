package com.epam.rd.november2019.service;

import com.epam.rd.november2019.HibernateConfig;
import com.epam.rd.november2019.converter.DateTimeConverter;
import com.epam.rd.november2019.dao.EmployeeDao;
import com.epam.rd.november2019.dao.FlightDao;
import com.epam.rd.november2019.dto.FlightCreateDto;
import com.epam.rd.november2019.dto.FlightViewDto;
import com.epam.rd.november2019.dto.RequestCreateDto;
import com.epam.rd.november2019.entity.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(value = {SpringExtension.class})
@ContextConfiguration(classes = {HibernateConfig.class})
public class FlightServiceImplIT {

    private static final String DEPARTURE_CITY = "London";
    private static final String DEPARTURE_AIRPORT = "Laa";
    private static final String ARRIVAL_CITY = "Paris";
    private static final String ARRIVAL_AIRPORT = "Paa";
    private static final FlightStatus FLIGHT_STATUS = FlightStatus.BOARDING;
    private static final String DEPARTURE_DATE = "2020-02-20T12:00";
    private static final String ARRIVAL_DATE = "2020-02-20T15:00";
    private static final String REQUESTER_COMMENT = "It's comment";
    private static final String REQUEST_DATE = "2020-02-01T12:00";

    private final FlightService flightService;
    private final FlightDao flightDao;
    private final DateTimeConverter converter;
    private final EmployeeDao employeeDao;

    private Flight flight;
    private FlightCreateDto createDto;
    private Request request;
    private RequestCreateDto requestDto;
    private Employee requestBy;

    @Autowired
    public FlightServiceImplIT(FlightService flightService,
                               FlightDao flightDao,
                               DateTimeConverter converter,
                               EmployeeDao employeeDao) {
        this.flightService = flightService;
        this.flightDao = flightDao;
        this.converter = converter;
        this.employeeDao = employeeDao;
    }

    @BeforeEach
    public void init() {
        requestBy = new Employee("Johnson", "John", EmployeeRole.RADIO_OPERATOR, Sex.MAN, "New York");
        employeeDao.add(requestBy);

        request = new Request();
        request.setRequestBy(requestBy);
        request.setRequesterComment(REQUESTER_COMMENT);
        request.setRequestDate(converter.toTimestamp(REQUEST_DATE));

        requestDto = new RequestCreateDto();
        requestDto.setRequestBy(requestBy);
        requestDto.setRequesterComment(REQUESTER_COMMENT);
        requestDto.setRequestDate(converter.toTimestamp(REQUEST_DATE));

        flight = new Flight();
        flight.setDepartureCity(DEPARTURE_CITY);
        flight.setDepartureDate(converter.toTimestamp(DEPARTURE_DATE));
        flight.setDepartureAirport(DEPARTURE_AIRPORT);
        flight.setArrivalCity(ARRIVAL_CITY);
        flight.setArrivalDate(converter.toTimestamp(ARRIVAL_DATE));
        flight.setArrivalAirport(ARRIVAL_AIRPORT);
        flight.setStatus(FLIGHT_STATUS);
        flight.addRequest(request);

        createDto = new FlightCreateDto();
        createDto.setDepartureCity(DEPARTURE_CITY);
        createDto.setDepartureDate(converter.toTimestamp(DEPARTURE_DATE));
        createDto.setDepartureAirport(DEPARTURE_AIRPORT);
        createDto.setArrivalCity(ARRIVAL_CITY);
        createDto.setArrivalDate(converter.toTimestamp(ARRIVAL_DATE));
        createDto.setArrivalAirport(ARRIVAL_AIRPORT);
        createDto.setStatus(FLIGHT_STATUS);
        createDto.getRequests().add(requestDto);
        requestDto.setFlight(flight);
    }


    @Test
    public void testAddShouldCreateFlightWithoutAnyRequests() {
        //Given
        createDto.setRequests(new ArrayList<>());

        //When
        FlightViewDto result = flightService.addFlight(createDto);

        //Then
        assertEquals(DEPARTURE_CITY, result.getDepartureCity());
        assertEquals(DEPARTURE_AIRPORT, result.getDepartureAirport());
        assertEquals(DEPARTURE_DATE, result.getDepartureDate());
        assertEquals(ARRIVAL_CITY, result.getArrivalCity());
        assertEquals(ARRIVAL_AIRPORT, result.getArrivalAirport());
        assertEquals(ARRIVAL_DATE, result.getArrivalDate());
        assertEquals(FLIGHT_STATUS, result.getStatus());
        assertEquals(createDto.getRequests().size(), result.getRequests().size());
    }

    @Test
    void testAddShouldCreateFlightWithRequest() {
        //Given

        //When
        FlightViewDto result = flightService.addFlight(createDto);

        //Then
        assertEquals(DEPARTURE_CITY, result.getDepartureCity());
        assertEquals(DEPARTURE_AIRPORT, result.getDepartureAirport());
        assertEquals(DEPARTURE_DATE, result.getDepartureDate());
        assertEquals(ARRIVAL_CITY, result.getArrivalCity());
        assertEquals(ARRIVAL_AIRPORT, result.getArrivalAirport());
        assertEquals(ARRIVAL_DATE, result.getArrivalDate());
        assertEquals(FLIGHT_STATUS, result.getStatus());
        assertEquals(createDto.getRequests().size(), result.getRequests().size());
        assertEquals(requestBy, result.getRequests().get(0).getRequestBy());
        assertEquals(REQUESTER_COMMENT, result.getRequests().get(0).getRequesterComment());
        assertEquals(REQUEST_DATE, result.getRequests().get(0).getRequestDate());
    }

    @Test
    public void testRemoveShouldThrowNullPointerException() {
        //Given
        UUID id = flight.getFlightId();
        flightDao.add(flight);

        //When
        flightService.removeFlight(id);
        NullPointerException ex = assertThrows(NullPointerException.class, () -> {
            flightService.removeFlight(id);
        });

        //Then
        assertTrue(ex.getMessage().contains("Flight isn't found by id: " + id));
    }

    @Test
    public void testGetByIdShouldReturnFlightWithRequest() {
        //Given
        UUID id = flight.getFlightId();
        flightDao.add(flight);

        //When
        FlightViewDto result = flightService.getFlightById(id);

        //Then
        assertEquals(DEPARTURE_CITY, result.getDepartureCity());
        assertEquals(DEPARTURE_AIRPORT, result.getDepartureAirport());
        assertEquals(DEPARTURE_DATE, result.getDepartureDate());
        assertEquals(ARRIVAL_CITY, result.getArrivalCity());
        assertEquals(ARRIVAL_AIRPORT, result.getArrivalAirport());
        assertEquals(ARRIVAL_DATE, result.getArrivalDate());
        assertEquals(FLIGHT_STATUS, result.getStatus());
        assertEquals(flight.getRequests().size(), result.getRequests().size());
        assertEquals(requestBy, result.getRequests().get(0).getRequestBy());
        assertEquals(REQUESTER_COMMENT, result.getRequests().get(0).getRequesterComment());
        assertEquals(REQUEST_DATE, result.getRequests().get(0).getRequestDate());
    }

    @Test
    public void testGetAllShouldReturnAllFlights() {
        //Given
        List<Flight> expected = flightDao.getAll();

        //When
        List<FlightViewDto> result = flightService.getAllFlights();

        //Then
        assertEquals(result.size(), expected.size());
    }

    @Test
    public void testEditFlightShouldUpdateFlightAndRequest() {
        //Given
        UUID flightId = flight.getFlightId();
        UUID requestId = request.getRequestId();

        flightDao.add(flight);

        String newDepartureCity = "Kyiv";
        createDto.setDepartureCity(newDepartureCity);
        createDto.setFlightId(flightId);

        String approverComment = "Well done!";
        createDto.getRequests().get(0).setRequestId(requestId);
        createDto.getRequests().get(0).setApproverComment(approverComment);

        //When
        flightService.editFlight(createDto);
        FlightViewDto result = flightService.getFlightById(flightId);

        //Then
        assertEquals(newDepartureCity, result.getDepartureCity());
        assertEquals(approverComment, result.getRequests().get(0).getApproverComment());
    }

    @Test
    public void testEditFlightShouldAddFirstRequestIntoFlight() {
        //Given
        UUID flightId = flight.getFlightId();
        UUID requestId = request.getRequestId();

        flight.setRequests(new ArrayList<>());
        flightDao.add(flight);

        createDto.setRequests(new ArrayList<>());
        createDto.setFlightId(flightId);
        createDto.getRequests().add(requestDto);
        requestDto.setFlight(flight);

        //When
        FlightViewDto result = flightService.editFlight(createDto);

        //Then
        assertEquals(createDto.getRequests().size(), result.getRequests().size());
        assertEquals(requestBy, result.getRequests().get(0).getRequestBy());
        assertEquals(REQUESTER_COMMENT, result.getRequests().get(0).getRequesterComment());
        assertEquals(REQUEST_DATE, result.getRequests().get(0).getRequestDate());
    }

    @Test
    public void testEditFlightShouldAddSecondRequestIntoFlight() {
        //Given
        UUID flightId = flight.getFlightId();
        UUID requestId = request.getRequestId();

        flight.setRequests(new ArrayList<>());
        flightDao.add(flight);

        createDto.setFlightId(flightId);
        createDto.getRequests().add(requestDto);
        requestDto.setFlight(flight);

        //When
        FlightViewDto result = flightService.editFlight(createDto);

        //Then
        assertEquals(createDto.getRequests().size(), result.getRequests().size());
        assertEquals(requestBy, result.getRequests().get(0).getRequestBy());
        assertEquals(REQUESTER_COMMENT, result.getRequests().get(0).getRequesterComment());
        assertEquals(REQUEST_DATE, result.getRequests().get(0).getRequestDate());
    }
}
