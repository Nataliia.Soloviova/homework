CREATE TABLE employees (
employee_id BINARY(16) NOT NULL,
last_name VARCHAR(50) NOT NULL,
first_name VARCHAR(50) NOT NULL,
city VARCHAR(50) NOT NULL,
sex VARCHAR(50) NOT NULL,
employee_role VARCHAR(50) NOT NULL,
PRIMARY KEY (employee_id)
);

CREATE TABLE flights (
flight_id BINARY(16) NOT NULL,
departure_city VARCHAR(50) NOT NULL,
arrival_city VARCHAR(50) NOT NULL,
departure_date TIMESTAMP DEFAULT ('0000-00-00 00:00:00') NOT NULL,
arrival_date TIMESTAMP DEFAULT ('0000-00-00 00:00:00') NOT NULL,
departure_airport VARCHAR(50) NOT NULL,
arrival_airport VARCHAR(50) NOT NULL,
flight_status VARCHAR(50) NOT NULL,
PRIMARY KEY (flight_id)
);

CREATE TABLE requests (
request_id BINARY(16) NOT NULL,employees
employee_id VARCHAR(50) NOT NULL,
request_date TIMESTAMP DEFAULT ('0000-00-00 00:00:00') NOT NULL,
requester_comment VARCHAR(500) NOT NULL,
approver_comment VARCHAR(500),
flight_id BINARY(16) NOT NULL,
request_status VARCHAR(50),
PRIMARY KEY (request_id)
);