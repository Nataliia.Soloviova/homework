package com.epam.rd.november2019.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AdminController {

    @GetMapping("/admin_menu")
    public String adminMenu() {

        return "admin_menu";
    }
}
