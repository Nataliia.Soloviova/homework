package com.epam.rd.november2019.services;

import com.epam.rd.november2019.model.UserAccount;

public interface SecurityService {

    boolean isCorrectPassword(UserAccount user, String password);
}