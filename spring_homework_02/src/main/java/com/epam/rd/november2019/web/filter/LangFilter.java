package com.epam.rd.november2019.web.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(filterName = "langFilter", urlPatterns = {"/*"})
public class LangFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if (request.getParameter("locale") != null) {
            Cookie cookie = new Cookie("lang", request.getParameter("locale"));
            response.addCookie(cookie);
        }

        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
