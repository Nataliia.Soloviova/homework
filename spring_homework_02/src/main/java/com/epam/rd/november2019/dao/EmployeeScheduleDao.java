package com.epam.rd.november2019.dao;

import com.epam.rd.november2019.model.EmployeeSchedule;
import com.epam.rd.november2019.model.Flight;
import com.epam.rd.november2019.model.FlightTeam;

import java.util.List;

public interface EmployeeScheduleDao {

    List<EmployeeSchedule> getAll();

    boolean changeStartLocation(int employeeId, String newStartLocation, String oldStartLocation);

    EmployeeSchedule add(EmployeeSchedule employeeSchedule);

    boolean removeByEmployeeId(int employeeId);

    boolean removeByFlightId(int flightId);

    EmployeeSchedule getByIds(int employeeId, int flightId);

    List<Integer> getFreeEmployee(Flight flight);

    FlightTeam getFlightTeam(int flightId);
}
