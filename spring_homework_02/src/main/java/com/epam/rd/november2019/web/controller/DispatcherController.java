package com.epam.rd.november2019.web.controller;

import com.epam.rd.november2019.model.FlightStatus;
import com.epam.rd.november2019.services.DispatcherService;
import com.epam.rd.november2019.services.FlightService;
import com.epam.rd.november2019.web.dto.EmployeeViewDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class DispatcherController {

    @Autowired
    private DispatcherService dispatcherService;

    @Autowired
    private FlightService flightService;

    @GetMapping("/dispatcher_menu")
    public String dispatcherMenu() {

        return "dispatcher_menu";
    }

    @GetMapping("/addFlightTeam/{id}")
    public String addFlightTeam(@PathVariable("id") int id, HttpServletRequest req) {
        req.setAttribute("flightId", id);
        List<EmployeeViewDto> employees = dispatcherService.getFreeEmployees(id);
        req.setAttribute("employees", employees);

        return "employees";
    }

    @GetMapping("/freeEmployees")
    public String freeEmployees(HttpServletRequest req) {
        req.setAttribute("flightId", req.getParameter("flightId"));
        List<EmployeeViewDto> employees = new LinkedList<>();
        String employeeRole = req.getParameter("employeeRole");
        employees = dispatcherService.getFreeEmployees(Integer.parseInt(req.getParameter("flightId")));
        if (!employeeRole.equals("getAll")) {
            employees = employees.stream().filter(i -> i.getEmployeeRole().toString().equals(employeeRole)).collect(Collectors.toList());
        }
        req.setAttribute("employees", employees);

        return "employees";
    }

    @PostMapping("/addFlightTeam")
    public String createFlightTeam(HttpServletRequest req) {
        int flightId = Integer.parseInt(req.getParameter("flightId"));
        String[] ids = req.getParameterValues("employeeId");
        List<Integer> list = new LinkedList<>();
        for (String id : ids) {
            list.add(Integer.parseInt(id));
        }
        dispatcherService.createFlightTeam(list, flightId);

        return "redirect:/flights";
    }

    @GetMapping("editStatus/{id}")
    public String editTeamAndStatus(@PathVariable("id") int id, Model model) {
        model.addAttribute("flight", flightService.getById(id));
        model.addAttribute("flights", flightService.getAllFlights());

        return "flights";
    }

    @PostMapping(value = "editStatus/{id}")
    public String editStatus(@PathVariable("id") int id, HttpServletRequest req) {
        FlightStatus flightStatus = FlightStatus.valueOf(req.getParameter("flightStatus").toUpperCase());
        flightService.edit(id, flightStatus);

        return "redirect:/flights";
    }
}
