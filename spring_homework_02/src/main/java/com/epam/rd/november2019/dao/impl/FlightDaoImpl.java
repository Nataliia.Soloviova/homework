package com.epam.rd.november2019.dao.impl;

import com.epam.rd.november2019.annotation.Timed;
import com.epam.rd.november2019.dao.ConnectionDB;
import com.epam.rd.november2019.dao.FlightDao;
import com.epam.rd.november2019.model.Flight;
import com.epam.rd.november2019.model.FlightStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

@Repository
@Timed
public class FlightDaoImpl implements FlightDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(FlightDaoImpl.class);

    private static final String ADD_FLIGHT = "INSERT INTO flights (departure_city, departure_airport, arrival_city, arrival_airport, departure_date, arrival_date, flight_status) VALUES (?, ?, ?, ?, ?, ?, ?)";

    private static final String GET_ID = "SELECT flight_id FROM flights WHERE departure_city = ? AND departure_airport = ? AND arrival_city = ? AND arrival_airport = ? AND departure_date = ? AND arrival_date = ? AND flight_status = ?";

    private static final String GET_BY_ID = "SELECT * FROM flights WHERE flight_id = ?";

    private static final String GET_ALL = "SELECT * FROM flights";

    private static final String REMOVE_FLIGHT = "DELETE FROM flights WHERE flight_id = ?";

    private static final String SORT_BY_ID = "SELECT * FROM flights ORDER BY flight_id";

    private static final String SORT_BY_FLIGHT_NAME = "SELECT * FROM flights ORDER BY departure_date, departure_city, arrival_city, arrival_date";

    private static final StringBuilder FIND_FLIGHT = new StringBuilder("SELECT * FROM flights");

    private static final String EDIT_FLIGHT = "UPDATE flights SET departure_city = ?, departure_airport = ?, arrival_city = ?, arrival_airport = ?, departure_date = ?, arrival_date = ? WHERE flight_id = ?";

    private static final String EDIT_FLIGHT_STATUS = "UPDATE flights SET flight_status = ? WHERE flight_id = ?";

    @Override
    public synchronized Flight add(Flight flight) {
        LOGGER.debug("Trying to add {} to flights table", flight);
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(ADD_FLIGHT, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, flight.getDepartureCity());
            preparedStatement.setString(2, flight.getDepartureAirport());
            preparedStatement.setString(3, flight.getArrivalCity());
            preparedStatement.setString(4, flight.getArrivalAirport());
            preparedStatement.setTimestamp(5, flight.getDepartureDate());
            preparedStatement.setTimestamp(6, flight.getArrivalDate());
            preparedStatement.setString(7, flight.getStatus().toString());
            preparedStatement.executeUpdate();

            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                flight.setFlightId(generatedKeys.getInt("flight_id"));
            }

            LOGGER.debug("Flight {} added to flights table", flight);
        } catch (SQLException e) {
            LOGGER.warn("Flight {} already exist", flight);
        }
        return flight;
    }

    @Override
    public int getId(Flight flight) {
        LOGGER.debug("Trying to find flight from table: flights");
        int result = -1;
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GET_ID)) {
            preparedStatement.setString(1, flight.getDepartureCity());
            preparedStatement.setString(2, flight.getDepartureAirport());
            preparedStatement.setString(3, flight.getArrivalCity());
            preparedStatement.setString(4, flight.getArrivalAirport());
            preparedStatement.setTimestamp(5, flight.getDepartureDate());
            preparedStatement.setTimestamp(6, flight.getArrivalDate());
            preparedStatement.setString(7, flight.getStatus().toString());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getInt("flight_id");
            }
            LOGGER.debug("Employee id is found: {}", result);
        } catch (SQLException e) {
            LOGGER.warn("This flight doesn't exist");
        }
        return result;
    }

    @Override
    public synchronized Flight getById(int flightId) {
        LOGGER.debug("Trying to get flight by id: {}", flightId);
        Flight flight = null;
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GET_BY_ID)) {
            preparedStatement.setInt(1, flightId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                flight = new Flight(
                        resultSet.getInt("flight_id"),
                        resultSet.getString("departure_city"),
                        resultSet.getString("departure_airport"),
                        resultSet.getTimestamp("departure_date"),
                        resultSet.getString("arrival_city"),
                        resultSet.getString("arrival_airport"),
                        resultSet.getTimestamp("arrival_date"),
                        FlightStatus.valueOf(resultSet.getString("flight_status"))
                );
                LOGGER.debug("Flight with id {} is returned", flightId);
            }
        } catch (SQLException e) {
            LOGGER.warn("Flight with id {} doesn't exist", flightId);
        }
        return flight;
    }

    @Override
    public synchronized List<Flight> getAll() {
        LOGGER.debug("Trying to get all elements from table: flights");
        ArrayList<Flight> flights = new ArrayList<>();
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GET_ALL)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                flights.add(new Flight(
                        resultSet.getInt("flight_id"),
                        resultSet.getString("departure_city"),
                        resultSet.getString("departure_airport"),
                        resultSet.getTimestamp("departure_date"),
                        resultSet.getString("arrival_city"),
                        resultSet.getString("arrival_airport"),
                        resultSet.getTimestamp("arrival_date"),
                        FlightStatus.valueOf(resultSet.getString("flight_status"))
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (flights.size() > 0) {
            LOGGER.debug("Return {} flights after get all elements from table: flights", flights.size());
        } else {
            LOGGER.warn("Return empty list after get all elements from table: flights");
        }
        return flights;
    }

    @Override
    public boolean remove(int flightId) {
        LOGGER.debug("Trying to remove flight with id {} from flights table", flightId);
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(REMOVE_FLIGHT)) {
            preparedStatement.setInt(1, flightId);
            preparedStatement.executeUpdate();
            LOGGER.debug("Flight with id {} removed from flights table", flightId);
            return true;
        } catch (SQLException e) {
            LOGGER.warn("Failed to delete flight from DB");
            return false;
        }
    }

    @Override
    public List<Flight> findFlights(Flight flight) {
        List<String> paramsList;
        List<String> paramsListTime;
        Map<String, String> paramsMap = new LinkedHashMap<>();
        Map<String, Timestamp> paramsTime = new LinkedHashMap<>();

        paramsMap.put("flight_status", flight.getStatus().toString());

        if (flight.getDepartureCity().length() > 0) {
            paramsMap.put("departure_city", flight.getDepartureCity());
        }
        if (flight.getDepartureAirport().length() > 0) {
            paramsMap.put("departure_airport", flight.getDepartureAirport());
        }
        if (flight.getArrivalCity().length() > 0) {
            paramsMap.put("arrival_city", flight.getArrivalCity());
        }
        if (flight.getArrivalAirport().length() > 0) {
            paramsMap.put("arrival_airport", flight.getArrivalAirport());
        }
        if (flight.getDepartureDate() != null) {
            paramsTime.put("departure_date", flight.getDepartureDate());
        }
        if (flight.getArrivalDate() != null) {
            paramsTime.put("arrival_date", flight.getArrivalDate());
        }
        paramsList = paramsMap.keySet().stream()
                .map(i -> String.format("%s = ?", i))
                .collect(Collectors.toList());
        paramsListTime = paramsTime.keySet().stream()
                .map(i -> String.format("%s = ?", i))
                .collect(Collectors.toList());
        paramsList.addAll(paramsListTime);
        if (paramsList.size() > 0) {
            FIND_FLIGHT.append(" WHERE ").append(String.join(" AND ", paramsList));
        }
        LOGGER.debug("Trying to find employee from table: flights");
        List<Flight> flights = new LinkedList<>();
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(FIND_FLIGHT.toString())) {
            int i = 1;
            for (Map.Entry<String, String> pair : paramsMap.entrySet()) {
                String value = pair.getValue();
                preparedStatement.setString(i, value);
                i++;
            }
            for (Map.Entry<String, Timestamp> pair : paramsTime.entrySet()) {
                Timestamp value = pair.getValue();
                preparedStatement.setTimestamp(i, value);
                i++;
            }
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                flights.add(new Flight(
                        resultSet.getInt("flight_id"),
                        resultSet.getString("departure_city"),
                        resultSet.getString("departure_airport"),
                        resultSet.getTimestamp("departure_date"),
                        resultSet.getString("arrival_city"),
                        resultSet.getString("arrival_airport"),
                        resultSet.getTimestamp("arrival_date"),
                        FlightStatus.valueOf(resultSet.getString("flight_status"))
                ));
            }
            LOGGER.debug("Flight is found");
        } catch (SQLException e) {
            LOGGER.warn("Flight isn't found");
        }
        if (flights.size() > 0) {
            LOGGER.debug("Return {} flights after searching elements from table: flights", flights.size());
        } else {
            LOGGER.warn("Return empty list after searching elements from table: flights");
        }
        FIND_FLIGHT.delete(FIND_FLIGHT.indexOf("s") + 1, FIND_FLIGHT.length());
        return flights;
    }

    @Override
    public boolean edit(int flightId, Flight flight) {
        LOGGER.debug("Trying to edit flight from table: flights");
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(EDIT_FLIGHT)) {
            preparedStatement.setString(1, flight.getDepartureCity());
            preparedStatement.setString(2, flight.getDepartureAirport());
            preparedStatement.setString(3, flight.getArrivalCity());
            preparedStatement.setString(4, flight.getArrivalAirport());
            preparedStatement.setTimestamp(5, flight.getDepartureDate());
            preparedStatement.setTimestamp(6, flight.getArrivalDate());
            preparedStatement.setInt(7, flightId);
            preparedStatement.executeUpdate();
            LOGGER.debug("Flight is edited");
            return true;
        } catch (SQLException e) {
            LOGGER.warn("Flight isn't edited");
            return false;
        }
    }

    @Override
    public boolean edit(int flightId, FlightStatus flightStatus) {
        LOGGER.debug("Trying to edit flight from table: flights");
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(EDIT_FLIGHT_STATUS)) {
            preparedStatement.setString(1, flightStatus.toString());
            preparedStatement.setInt(2, flightId);
            preparedStatement.executeUpdate();
            LOGGER.debug("Flight is edited");
            return true;
        } catch (SQLException e) {
            LOGGER.warn("Flight isn't edited");
            return false;
        }
    }

    @Override
    public synchronized List<Flight> sortById() {
        LOGGER.debug("Trying to sort list of flights by id");
        List<Flight> flights = new ArrayList<>();
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(SORT_BY_ID)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                flights.add(new Flight(
                        resultSet.getInt("flight_id"),
                        resultSet.getString("departure_city"),
                        resultSet.getString("departure_airport"),
                        resultSet.getTimestamp("departure_date"),
                        resultSet.getString("arrival_city"),
                        resultSet.getString("arrival_airport"),
                        resultSet.getTimestamp("arrival_date"),
                        FlightStatus.valueOf(resultSet.getString("flight_status"))
                ));
            }
        } catch (SQLException e) {
            LOGGER.warn("Can not sort list of flights by id");
        }
        if (flights.size() > 0) {
            LOGGER.debug("Return {} sorted flights by id", flights.size());
        } else {
            LOGGER.warn("Return empty list after sorting by id");
        }
        return flights;
    }

    @Override
    public synchronized List<Flight> sortByFlightName() {
        LOGGER.debug("Trying to sort list of flights by flight name");
        List<Flight> flights = new ArrayList<>();
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(SORT_BY_FLIGHT_NAME)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                flights.add(new Flight(
                        resultSet.getInt("flight_id"),
                        resultSet.getString("departure_city"),
                        resultSet.getString("departure_airport"),
                        resultSet.getTimestamp("departure_date"),
                        resultSet.getString("arrival_city"),
                        resultSet.getString("arrival_airport"),
                        resultSet.getTimestamp("arrival_date"),
                        FlightStatus.valueOf(resultSet.getString("flight_status"))
                ));
            }
        } catch (SQLException e) {
            LOGGER.warn("Can not sort list of flights by flight name");
        }
        if (flights.size() > 0) {
            LOGGER.debug("Return {} sorted flights by flight name", flights.size());
        } else {
            LOGGER.warn("Return empty list after sorting by flight name");
        }
        return flights;
    }
}
