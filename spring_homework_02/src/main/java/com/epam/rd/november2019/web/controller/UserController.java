package com.epam.rd.november2019.web.controller;

import com.epam.rd.november2019.dao.UserDao;
import com.epam.rd.november2019.exception.ValidationException;
import com.epam.rd.november2019.model.UserAccount;
import com.epam.rd.november2019.model.UserRole;
import com.epam.rd.november2019.services.UserService;
import com.epam.rd.november2019.web.dto.UserAccountCreateDto;
import com.epam.rd.november2019.web.dto.UserAccountViewDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserDao userDao;

    @GetMapping("/login")
    public String loginPage(Model model) {
        return "login";
    }

    @PostMapping("/login")
    public String login(HttpServletRequest req) {
        String userName = req.getParameter("loginName");
        String password = req.getParameter("password");
        try {
            UserAccountViewDto accountViewDto = userService.login(userName, password);
            HttpSession session = req.getSession(true);
            session.setAttribute("user", accountViewDto);
            return "redirect:/profile";
        } catch (ValidationException e) {
            req.setAttribute("errorLoginPassMessage", e.getMessage());
            return "login";
        }
    }

    @GetMapping("/registration")
    public String registrationPage() {

        return "registration";
    }

    @PostMapping(value = "/registration")
    public String registration(HttpServletRequest req) {
        try {
            UserAccountCreateDto accountCreateDto = extractUserFromRequest(req);
            UserAccountViewDto accountViewDto = userService.registerUser(accountCreateDto);

            return "redirect:/login";
        } catch (IllegalArgumentException e) {
            req.setAttribute("errorRegisterMessage", e.getMessage());

            return "/login";
        } catch (ValidationException e) {
            req.setAttribute("errorRegisterMessage", e.getMessage());

            return "/registration";
        }
    }

    private UserAccountCreateDto extractUserFromRequest(HttpServletRequest req) {
        String userName = req.getParameter("loginName");
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        UserRole userRole = UserRole.valueOf(req.getParameter("userRole"));
        return new UserAccountCreateDto(userName, email, password, userRole);
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest req) {
        HttpSession session = req.getSession(true);
        session.invalidate();

        return "redirect:/login";
    }

    @GetMapping("/profile")
    public String profilePage() {

        return "profile";
    }

    @PostMapping("/change_password")
    public String changePassword(HttpServletRequest req) {
        String oldPassword = req.getParameter("oldPassword");
        String newPassword1 = req.getParameter("newPassword_1");
        String newPassword2 = req.getParameter("newPassword_2");

        HttpSession session = req.getSession(true);
        UserAccountViewDto dto = (UserAccountViewDto) session.getAttribute("user");

        try {
            UserAccount user = userDao.getById(dto.getId());
            if (!oldPassword.equals(user.getPassword())) {
                throw new ValidationException("Wrong old password!");
            }
            checkNewPassword(user.getPassword(), newPassword1, newPassword2);
            user.setPassword(newPassword1);
            userDao.edit(user);
            req.setAttribute("confirmChangePassMessage", "Successful changing password");
            session.invalidate();
            return "redirect:/login";
        } catch (ValidationException e) {
            req.setAttribute("errorChangePassMessage", e.getMessage());
            return "profile";
        }
    }

    private void checkNewPassword(String oldPass, String pass1, String pass2) {
        if (!pass1.equals(pass2)) {
            throw new ValidationException("Wrong new password! Try again!");
        }
        if (oldPass.equals(pass1)) {
            throw new ValidationException("Create a new password!");
        }
    }

    @GetMapping("/login-error")
    public String loginErrorPage() {

        return "login-error";
    }
}
