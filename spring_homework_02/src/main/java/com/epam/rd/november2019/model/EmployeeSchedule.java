package com.epam.rd.november2019.model;

import java.sql.Timestamp;
import java.util.Objects;

public class EmployeeSchedule {

    private int employeeId;
    private int flightId;
    private Timestamp startWorkDay;
    private Timestamp finishWorkDay;
    private String startLocation;
    private String finishLocation;

    public EmployeeSchedule(int employeeId, String startLocation) {
        this.employeeId = employeeId;
        this.startLocation = startLocation;
        this.startWorkDay = new Timestamp(System.currentTimeMillis() - 43_200_000);
        this.finishWorkDay = this.startWorkDay;
    }

    public EmployeeSchedule(Timestamp startWorkDay, Timestamp finishWorkDay, String startLocation, String finishLocation) {
        this.startWorkDay = startWorkDay;
        this.finishWorkDay = finishWorkDay;
        this.startLocation = startLocation;
        this.finishLocation = finishLocation;
    }

    public EmployeeSchedule(int employeeId, int flightId, Timestamp startWorkDay, Timestamp finishWorkDay, String startLocation, String finishLocation) {
        this.employeeId = employeeId;
        this.flightId = flightId;
        this.startWorkDay = startWorkDay;
        this.finishWorkDay = finishWorkDay;
        this.startLocation = startLocation;
        this.finishLocation = finishLocation;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public Timestamp getStartWorkDay() {
        return startWorkDay;
    }

    public void setStartWorkDay(Timestamp startWorkDay) {
        this.startWorkDay = startWorkDay;
    }

    public Timestamp getFinishWorkDay() {
        return finishWorkDay;
    }

    public void setFinishWorkDay(Timestamp finishWorkDay) {
        this.finishWorkDay = finishWorkDay;
    }

    public String getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(String startLocation) {
        this.startLocation = startLocation;
    }

    public String getFinishLocation() {
        return finishLocation;
    }

    public void setFinishLocation(String finishLocation) {
        this.finishLocation = finishLocation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmployeeSchedule that = (EmployeeSchedule) o;
        return employeeId == that.employeeId &&
                flightId == that.flightId &&
                Objects.equals(startWorkDay, that.startWorkDay) &&
                Objects.equals(finishWorkDay, that.finishWorkDay) &&
                Objects.equals(startLocation, that.startLocation) &&
                Objects.equals(finishLocation, that.finishLocation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(employeeId, flightId, startWorkDay, finishWorkDay, startLocation, finishLocation);
    }

    @Override
    public String toString() {
        return "[Employee id: " + employeeId + " Flight id: " + flightId + "]";
    }
}
