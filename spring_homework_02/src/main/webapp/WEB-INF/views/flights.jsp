<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${cookie['lang'].value}"/>
<fmt:setBundle basename="messages" />

<!DOCTYPE html>

<html lang="${cookie['lang'].value}">
    <head>
         <title>Employees</title>

         <style>
            input, button, select {
                width: 300px;
                height: 30px;
            }

            select {
                width: 200px;
                height: 30px;
            }

            .add, .edit, .delete {
                width: 100px;
                height: 30px;
            }

            .checkbox {
                width: 20px;
                height: 20px;
            }
         </style>
    </head>

    <body>

    <c:if test="${sessionScope.user.userRole eq 'ADMINISTRATOR'}">

    <jsp:include page="admin_menu.jsp"></jsp:include>

    <div align="center">

                <c:if test="${!empty flight.departureCity}">
                    <h2>Edit a flight</h2>
                    <form action="/editFlight/${flight.flightId}" method="post">
                        <p>
                            <input name="flightId" width="30" disabled="true" value="${flight.flightId}" />
                        </p>
                        <p>
                            <input type="text" name="departureCity" width="30" value="${flight.departureCity}" required/>
                        </p>
                        <p>
                            <input type="text" name="departureAirport" width="30" value="${flight.departureAirport}" required/>
                        </p>
                        <p>
                            <input type="datetime-local" name="departureDate" width="30" value="${flight.departureDate}" required/>
                        </p>
                        <p>
                            <input type="text" name="arrivalCity" width="30" value="${flight.arrivalCity}" required/>
                        </p>
                        <p>
                            <input type="text" name="arrivalAirport" width="30" value="${flight.arrivalAirport}" required/>
                        </p>
                        <p>
                            <input type="datetime-local" name="arrivalDate" width="30" value="${flight.arrivalDate}" required/>
                        </p>
                        <p>
                            <input type="submit" value="Edit" />
                        </p>
                    </form>
                </c:if>

                <c:if test="${empty flight.departureCity}">

                    <h2>Find a flight:</h2>

                    <form action="/flights" method="post">
                        <div>
                            <p>
                                <input type="text" name="departureCity" width="30" placeholder="Departure city"/>
                                <input type="text" name="departureAirport" width="30" placeholder="Departure airport"/>
                                <input type="datetime-local" name="departureDate" width="30" placeholder="Departure date"/>
                                <input type="text" name="arrivalCity" width="30" placeholder="Arrival city"/>
                                <input type="text" name="arrivalAirport" width="30" placeholder="Arrival airport"/>
                                <input type="datetime-local" name="arrivalDate" width="30" placeholder="Arrival date"/>
                                <select name="flightStatus" width="50" required>
                                    <option value="">Flight status</option>
                                    <option value="planned">Planned</option>
                                    <option value="formed">Formed</option>
                                    <option value="on_time">On time</option>
                                    <option value="check_in">Check in</option>
                                    <option value="gate_closing">Gate closing</option>
                                    <option value="boarding">Boarding</option>
                                    <option value="cancelled">Cancelled</option>
                                    <option value="delayed">Delayed</option>
                                    <option value="departed">Departed</option>
                                </select>
                            </p>
                            <p>
                                <input type="submit" value="Find" />
                            </p>
                        </div>
                    </form>

            <h2>Add a Flight</h2>

            <form action="/flights/add" method="post">
                <p>
                    <input type="text" name="departureCity" width="30" placeholder="Departure city" required/>
                    <input type="text" name="departureAirport" width="30" placeholder="Departure airport" required/>
                    <input type="datetime-local" name="departureDate" width="30" placeholder="Departure date" required/>
                    <input type="text" name="arrivalCity" width="30" placeholder="Arrival city" required/>
                    <input type="text" name="arrivalAirport" width="30" placeholder="Arrival airport" required/>
                    <input type="datetime-local" name="arrivalDate" width="30" placeholder="Arrival date" required/>
                    <select name="flightStatus" width="50" required>
                            <option value="">Flight status</option>
                            <option value="planned">Planned</option>
                            <option value="formed">Formed</option>
                            <option value="on time">On time</option>
                            <option value="check in">Check in</option>
                            <option value="gate closing">Gate closing</option>
                            <option value="boarding">Boarding</option>
                            <option value="cancelled">Cancelled</option>
                            <option value="delayed">Delayed</option>
                            <option value="departed">Departed</option>
                    </select>
                </p>
                <p>
                    <input type="submit" value="Add" />

                </p>
            </form>
            </c:if>

            </div>

            <p style="color:red">${addingErrorMessage}</p>

            <form action="/sort_flights" method="post">
                <div align="center">
                    <label for="sortBy">Sort by:</label>
                    <select name="sortBy" required>
                        <option value="Get all">Get all</option>
                        <option value="Flight name">Flight name</option>
                        <option value="Flight id">Flight id</option>
                    </select>
                    <p><input type="submit" value="Sort"></p>
                </div>
            </form>

            <div align="center">
                <table border="1" cellpadding="5">
                    <caption><h2>List of flights</h2></caption>

                    <thead>
                        <tr>
                                <th>ID</th>
                                <th>Departure city</th>
                                <th>Departure airport</th>
                                <th>Departure date</th>
                                <th>Arrival city</th>
                                <th>Arrival airport</th>
                                <th>Arrival date</th>
                                <th>Status</th>
                                <th>Flight team</th>
                                <th>Edit</th>
                                <th>Delete</th>
                        </tr>
                    </thead>

                    <tbody>
                        <c:forEach items="${flights}" var="flight">
                            <tr>
                                    <td><c:out value="${flight.flightId}" /></td>
                                    <td><c:out value="${flight.departureCity}" /></td>
                                    <td><c:out value="${flight.departureAirport}" /></td>
                                    <td><c:out value="${flight.departureDate}" /></td>
                                    <td><c:out value="${flight.arrivalCity}" /></td>
                                    <td><c:out value="${flight.arrivalAirport}" /></td>
                                    <td><c:out value="${flight.arrivalDate}" /></td>
                                    <td><c:out value="${flight.status}" /></td>
                                    <td>
                                                <c:choose>
                                                    <c:when test="${not empty flight.flightTeam.employeeIds}">
                                                        <c:out value="${flight.flightTeam}" />
                                                    </c:when>
                                                    <c:otherwise>

                                                    </c:otherwise>
                                                </c:choose>
                                    </td>
                                    <td><a href="<c:url value='editFlight/${flight.flightId}'/>">Edit</a></td>
                                    <td><a href="<c:url value='deleteFlight/${flight.flightId}'/>">Delete</a></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
    </c:if>


    <c:if test="${sessionScope.user.userRole eq 'DISPATCHER'}">

        <jsp:include page="dispatcher_menu.jsp"></jsp:include>

        <div align="center">

                <h2>Find a flight:</h2>

                    <form action="/flights" method="post">
                        <div>
                            <p>
                                <input type="text" name="departureCity" width="30" placeholder="Departure city"/>
                                <input type="text" name="departureAirport" width="30" placeholder="Departure airport"/>
                                <input type="datetime-local" name="departureDate" width="30" placeholder="Departure date"/>
                                <input type="text" name="arrivalCity" width="30" placeholder="Arrival city"/>
                                <input type="text" name="arrivalAirport" width="30" placeholder="Arrival airport"/>
                                <input type="datetime-local" name="arrivalDate" width="30" placeholder="Arrival date"/>
                                <select name="flightStatus" width="50" required>
                                    <option value="">Flight status</option>
                                    <option value="planned">Planned</option>
                                    <option value="formed">Formed</option>
                                    <option value="on_time">On time</option>
                                    <option value="check_in">Check in</option>
                                    <option value="gate_closing">Gate closing</option>
                                    <option value="boarding">Boarding</option>
                                    <option value="cancelled">Cancelled</option>
                                    <option value="delayed">Delayed</option>
                                    <option value="departed">Departed</option>
                                </select>
                            </p>
                            <p>
                                <input type="submit" value="Find" />
                            </p>
                        </div>
                    </form>

        </div>

            <form action="/sort_flights" method="post">
                <div align="center">
                    <label for="sortBy">Sort by:</label>
                    <select name="sortBy" required>
                        <option value="Get all">Get all</option>
                        <option value="Flight name">Flight name</option>
                        <option value="Flight id">Flight id</option>
                    </select>
                    <p><input type="submit" value="Sort"></p>
                </div>
            </form>

            <div align="center">
                <table border="1" cellpadding="5">
                    <caption><h2>List of flights</h2></caption>

                    <thead>
                        <tr>
                                <th>ID</th>
                                <th>Departure city</th>
                                <th>Departure airport</th>
                                <th>Departure date</th>
                                <th>Arrival city</th>
                                <th>Arrival airport</th>
                                <th>Arrival date</th>
                                <th>Status</th>
                                <th>Flight team</th>
                                <th>Edit</th>
                        </tr>
                    </thead>


                <c:if test="${!empty flight.departureCity}">
                    <h2>Edit a flight</h2>
                    <form action="/editStatus/${flight.flightId}" method="post">
                        <p>
                                        <select name="flightStatus" width="50" required>
                                            <option value="">Flight status</option>
                                            <option value="planned">Planned</option>
                                            <option value="formed">Formed</option>
                                            <option value="on_time">On time</option>
                                            <option value="check_in">Check in</option>
                                            <option value="gate_closing">Gate closing</option>
                                            <option value="boarding">Boarding</option>
                                            <option value="cancelled">Cancelled</option>
                                            <option value="delayed">Delayed</option>
                                            <option value="departed">Departed</option>
                                        </select>
                        </p>
                        <p>
                            <input type="submit" value="Edit" />
                        </p>
                    </form>
                </c:if>

                    <tbody>
                        <c:forEach items="${flights}" var="flight">
                            <tr>
                                    <td><c:out value="${flight.flightId}" /></td>
                                    <td><c:out value="${flight.departureCity}" /></td>
                                    <td><c:out value="${flight.departureAirport}" /></td>
                                    <td><c:out value="${flight.departureDate}" /></td>
                                    <td><c:out value="${flight.arrivalCity}" /></td>
                                    <td><c:out value="${flight.arrivalAirport}" /></td>
                                    <td><c:out value="${flight.arrivalDate}" /></td>
                                    <td><c:out value="${flight.status}" /></td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${not empty flight.flightTeam.employeeIds}">
                                                <c:out value="${flight.flightTeam}" />
                                            </c:when>
                                            <c:otherwise>
                                                    <a href="<c:url value='addFlightTeam/${flight.flightId}'/>">Add team</a>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td><a href="<c:url value='editStatus/${flight.flightId}'/>">Edit</a></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>


    </c:if>

    </body>
</html>