<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
    <head>

        <title>Profile</title>

        <style>
            input, button {
                width: 300px;
                height: 30px;
            }
        </style>

    </head>
    <body>




                <jsp:include page="menu.jsp"></jsp:include>

                <c:set var="user" value="${sessionScope.user}" />
                <c:set var="userRole" value="${user.userRole}"/>
                <div>
                    <div>
                        <h1>Login: ${user.userName}</h1>
                    </div>
                </div>
                <h1>: ${user.id}</h1>
                <h1>: ${user.email}</h1>
                <h1>: ${user.userRole}</h1>

                <div>
                    <h3>Changing password</h3>
                    <form action="/change_password" method="post">
                        <p>
                            <label for="password">Old password:</label>
                            <input type="password" name="oldPassword" width="10" placeholder="Old password" required/>
                        </p>
                        <p>
                            <label for="newPassword_1">New password:</label>
                            <input type="password" name="newPassword_1" width="10" placeholder="New password" required/>
                        </p>
                        <p>
                            <label for="newPassword_2">New password:</label>
                            <input type="password" name="newPassword_2" width="10" placeholder="New password" required/>
                        </p>
                        <p>
                            <input type="submit" value="Change password" />
                        </p>
                    </form>

                    <p style="color:red">${requestScope.confirmChangePassMessage}</p>

                    <p style="color:red">${requestScope.errorChangePassMessage}</p>



    </body>
</html>