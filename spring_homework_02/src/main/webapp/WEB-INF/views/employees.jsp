<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${cookie['lang'].value}"/>
<fmt:setBundle basename="messages" />

<!DOCTYPE html>

<html lang="${cookie['lang'].value}">
    <head>
         <title>Employees</title>

         <style>
            input, button, select {
                width: 300px;
                height: 30px;
            }

            .checkbox {
                width: 20px;
                height: 20px;
            }
         </style>
    </head>

    <body>

    <c:if test="${sessionScope.user.userRole eq 'ADMINISTRATOR'}">

    <jsp:include page="admin_menu.jsp"></jsp:include>

        <div align="center">

            <c:if test="${!empty employee.firstName}">
                <h2>Edit an employee</h2>
                <form action="/editEmployee/${employee.employeeId}" method="post">
                    <p>
                        <input name="employeeId" width="30" disabled="true" value="${employee.employeeId}" />
                    </p>
                    <p>
                        <input type="text" name="firstName" width="30" placeholder="First name" value="${employee.firstName}" required/>
                    </p>
                    <p>
                        <input type="text" name="lastName" width="30" placeholder="Last name" value="${employee.lastName}" required/>
                    </p>
                    <p>
                        <select name="employeeRole" width="50" required>
                            <option value="${employee.employeeRole}">${employee.employeeRole}</option>
                            <option value="pilot">Pilot</option>
                            <option value="navigator">Navigator</option>
                            <option value="radio_operator">Radio operator</option>
                            <option value="stewardess">Stewardess</option>
                        </select>

                    </p>
                    <p>
                        <select name="sex" width="50" required>
                            <option value="${employee.sex}">${employee.sex}</option>
                            <option value="man">Man</option>
                            <option value="woman">Woman</option>
                        </select>
                    </p>
                    <p>
                        <input type="text" name="city" width="30" placeholder="City" value="${employee.city}" required/>
                    </p>
                    <p>

                        <input type="submit" value="Edit" />

                    </p>
                </form>
            </c:if>

            <c:if test="${empty employee.firstName}">

                <h2>Find employee:</h2>

                <form action="/employees" method="post">
                    <div>
                        <p>
                            <input type="text" name="lastName" width="30" placeholder="Last name" />
                            <input type="text" name="firstName" width="30" placeholder="First name" />
                            <select name="employeeRole" width="50" required>
                                <option value="">Profession</option>
                                <option value="pilot">Pilot</option>
                                <option value="navigator">Navigator</option>
                                <option value="radio_operator">Radio operator</option>
                                <option value="stewardess">Stewardess</option>
                            </select>
                            <select name="sex" width="50" required>
                                <option value="">Sex</option>
                                <option value="man">Man</option>
                                <option value="woman">Woman</option>
                            </select>
                            <input type="text" name="city" width="30" placeholder="City" />
                        </p>
                        <p>
                            <input type="submit" value="Find" />
                        </p>
                    </div>
                </form>

        <h2>Add an employee</h2>

        <form action="/employees/add" method="post">
            <p>
                <input type="text" name="firstName" width="30" placeholder="First name" value="${employee.firstName}" required/>
                <input type="text" name="lastName" width="30" placeholder="Last name" value="${employee.lastName}" required/>
                <select name="employeeRole" width="50" required>
                    <option value="">Profession</option>
                    <option value="pilot">Pilot</option>
                    <option value="navigator">Navigator</option>
                    <option value="radio_operator">Radio operator</option>
                    <option value="stewardess">Stewardess</option>
                </select>
                <select name="sex" width="50" required>
                    <option value="">Sex</option>
                    <option value="man">Man</option>
                    <option value="woman">Woman</option>
                </select>
                <input type="text" name="city" width="30" placeholder="City" value="${employee.city}" required/>
            </p>
            <p>
                <input type="submit" value="Add" />

            </p>
        </form>
        </c:if>

        </div>

        <p style="color:red">${addingErrorMessage}</p>

        <div align="center">
            <table border="1" cellpadding="5">
                <caption>
                    <h2>List of employees</h2></caption>

                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Last name</th>
                        <th>First name</th>
                        <th>Profession</th>
                        <th>Sex</th>
                        <th>City</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>

                <tbody>
                    <c:forEach items="${employees}" var="employee">
                        <tr>
                            <td>
                                <c:out value="${employee.employeeId}" />
                            </td>
                            <td>
                                <c:out value="${employee.lastName}" />
                            </td>
                            <td>
                                <c:out value="${employee.firstName}" />
                            </td>
                            <td>
                                <c:out value="${employee.employeeRole}" />
                            </td>
                            <td>
                                <c:out value="${employee.sex}" />
                            </td>
                            <td>
                                <c:out value="${employee.city}" />
                            </td>
                            <td><a href="<c:url value='/editEmployee/${employee.employeeId}'/>">Edit</a></td>
                            <td><a href="<c:url value='/deleteEmployee/${employee.employeeId}'/>">Delete</a></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>

    </c:if>

    <c:if test="${sessionScope.user.userRole eq 'DISPATCHER'}">


    <jsp:include page="dispatcher_menu.jsp"></jsp:include>

            <div align="center">
                <h1>Create flight team for flight ${flightId}</h1>
            </div>


            <form action="/freeEmployees" method="get">
            <div align="center">
                <label for="employeeRole">Profession:</label>
                <select name="employeeRole" required>
                    <option value="getAll">Get all</option>
                    <option value="PILOT">Pilot</option>
                    <option value="NAVIGATOR">Navigator</option>
                    <option value="RADIO_OPERATOR">Radio operator</option>
                    <option value="STEWARDESS">Stewardess</option>
                </select>
                <p>
                <button type="submit" value="${flightId}" name="flightId" class="find">
                    Find
                </button>
                </p>
            </div>
            </form>



            <div align="center">
            <form action="/addFlightTeam" method="post">

             <button type="submit" value="${flightId}" name="flightId">
                Add employee into team
             </button>

                    <table border="1" cellpadding="5">
                                        <caption><h2>List of employees</h2></caption>

                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Last name</th>
                                            <th>First name</th>
                                            <th>Profession</th>
                                            <th>Sex</th>
                                            <th>City</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <c:forEach items="${employees}" var="employee">
                                            <tr>
                                                <td><input class="checkbox" type="checkbox" name="employeeId" value="${employee.employeeId}"/></td>
                                                <td><c:out value="${employee.lastName}" /></td>
                                                <td><c:out value="${employee.firstName}" /></td>
                                                <td><c:out value="${employee.employeeRole}" /></td>
                                                <td><c:out value="${employee.sex}" /></td>
                                                <td><c:out value="${employee.city}" /></td>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                </form>
            </div>

    </c:if>

    </body>
</html>