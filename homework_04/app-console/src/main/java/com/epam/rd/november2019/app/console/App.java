package com.epam.rd.november2019.app.console;

import com.epam.rd.november2019.app.core.firstTask.FirstTask;
import com.epam.rd.november2019.app.core.secondTask.City;
import com.epam.rd.november2019.app.core.secondTask.SecondTask;
import com.epam.rd.november2019.app.core.thirdTask.ThirdTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The class {@code App} shows how to work some methods.
 *
 * @see FirstTask
 * @see SecondTask
 * @see ThirdTask
 */
public class App {

    private static final Logger logger = LoggerFactory.getLogger(App.class);

    /**
     * This method shows how to work some methods.
     *
     * @param args not used
     */
    public static void main(String[] args) {

        FirstTask firstTask = new FirstTask();
        logger.info("First task:");
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        for (int i = 0; i < 10; i++) {
            Random random = new Random();
            int number = random.nextInt(100);
            arrayList.add(number);
        }
        logger.info("Created my list: {}", arrayList.toString());
        logger.info("Modified list: [{}]", firstTask.firstMethod(arrayList));

        logger.info("Second task:");
        ArrayList<City> cities = new ArrayList<City>();
        cities.add(new City("City1", "State1", 2300000));
        cities.add(new City("City2", "State3", 840000));
        cities.add(new City("City3", "State1", 3100000));
        cities.add(new City("City4", "State2", 650000));
        cities.add(new City("City5", "State1", 7000000));
        cities.add(new City("City6", "State3", 2100000));
        cities.add(new City("City7", "State1", 3600000));
        cities.add(new City("City8", "State2", 510000));
        cities.add(new City("City9", "State1", 780000));
        cities.add(new City("City10", "State3", 8700000));
        logger.info("Created some cities.");
        SecondTask secondTask = new SecondTask();

        logger.info("Found largest city per state by Stream:");
        secondTask.getLargestCityPerState(cities).forEach((a, b) -> {
            if (b.isPresent()) {
                logger.info("{} : {}", a, b.get());
            } else {
                throw new IllegalStateException();
            }
        });

        logger.info("Found largest city per state by Collection:");
        for (Map.Entry<String, City> entry :
                secondTask.getLargestCityPerStateByCollection(cities).entrySet()) {
            logger.info("{} : {}", entry.getKey(), entry.getValue());
        }

        logger.info("Third task:");
        Integer[] firstList = {0, 2, 4, 6, 8};
        Integer[] secondList = {1, 3, 5, 7, 9};
        Stream<Integer> integerStream1 = Stream.of(firstList);
        Stream<Integer> integerStream2 = Stream.of(secondList);
        logger.info("First stream: {}", Arrays.toString(firstList));
        logger.info("Second stream: {}", Arrays.toString(secondList));
        logger.info("New stream: {}",
                ThirdTask.zip(integerStream1, integerStream2)
                        .collect(Collectors.toList()).toString());
    }
}
