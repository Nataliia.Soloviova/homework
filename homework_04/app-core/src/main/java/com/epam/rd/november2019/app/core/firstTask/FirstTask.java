package com.epam.rd.november2019.app.core.firstTask;

import java.util.ArrayList;

/**
 * This class has one method for first task.
 */
public class FirstTask {

    /**
     * This method returns a comma separated
     * string based on a given list of integers.
     *
     * @param arrayList list of integers
     * @return a comma separated string
     */
    public String firstMethod(ArrayList<Integer> arrayList) {
        return arrayList.stream()
                .map(x -> x % 2 == 0 ? "e" + x : "o" + x)
                .reduce((x, y) -> x + "," + y)
                .orElseThrow(NullPointerException::new);
    }
}
