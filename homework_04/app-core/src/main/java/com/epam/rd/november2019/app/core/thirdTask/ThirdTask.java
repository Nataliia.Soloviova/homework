package com.epam.rd.november2019.app.core.thirdTask;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Class has one static method for third task.
 */
public class ThirdTask {
    /**
     * This method alternates elements from the streams first
     * and second, stopping when one of them runs out of elements.
     *
     * @param first stream
     * @param second stream
     * @param <T> the type of elements in this stream
     * @return stream with alternates elements from the
     *         streams first and second
     */
    public static <T> Stream<T> zip(Stream<T> first, Stream<T> second) {
        Stream.Builder<T> finalStream = Stream.builder();

        List<T> firstList = first.collect(Collectors.toList());
        List<T> secondList = second.collect(Collectors.toList());

        IntStream
                .range(0, Math.min(firstList.size(), secondList.size()))
                .forEach(i -> {
                    finalStream.add(firstList.get(i));
                    finalStream.add(secondList.get(i));
                });

        return finalStream.build();
    }
}