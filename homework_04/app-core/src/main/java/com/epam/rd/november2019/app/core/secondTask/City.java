package com.epam.rd.november2019.app.core.secondTask;

import java.util.Objects;

public class City {
    private String name;
    private String state;
    private long population;

    public City(String name, String state, long population) {
        this.name = name;
        this.state = state;
        this.population = population;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public long getPopulation() {
        return population;
    }

    public void setPopulation(long population) {
        if (population < 0) {
            throw new IllegalArgumentException();
        } else {
            this.population = population;
        }
    }

    @Override
    public String toString() {
        return "[" + name + ", "
                + state  +
                ", population = " + population
                + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City city = (City) o;
        return population == city.population &&
                Objects.equals(name, city.name) &&
                Objects.equals(state, city.state);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, state, population);
    }
}

