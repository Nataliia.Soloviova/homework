package com.epam.rd.november2019.app.core.secondTask;

import java.util.*;
import java.util.stream.Collectors;

/**
 * This class has two methods for second task.
 *
 * @see City
 */
public class SecondTask {

    /**
     * This method produces the largest city (by population)
     * per state with using Stream API.
     *
     * @param cities is collection of City
     * @return map of state's name and city
     */
    public Map<String, Optional<City>> getLargestCityPerState(Collection<City> cities) {
        return cities.stream()
                .collect(Collectors.groupingBy(City::getState,
                        Collectors.maxBy(Comparator.comparing(City::getPopulation))));
    }

    /**
     * This method produces the largest city (by population)
     * per state with using Collection API.
     *
     * @param cities is collection of City
     * @return map of state's name and city
     */
    public Map<String, City> getLargestCityPerStateByCollection(Collection<City> cities) {
        Map<String, City> map = new HashMap<>();
        for (City city :
                cities) {
            if (!map.containsKey(city.getState())) {
                map.put(city.getState(), city);
            } else {
                if (map.get(city.getState()).getPopulation() < city.getPopulation()) {
                    map.put(city.getState(), city);
                }
            }
        }
        return map;
    }
}
