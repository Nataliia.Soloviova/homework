package com.epam.rd.november2019.controller;

import com.epam.rd.november2019.config.TestAppConfig;
import com.epam.rd.november2019.converter.DateTimeConverter;
import com.epam.rd.november2019.model.FlightStatus;
import com.epam.rd.november2019.model.FlightTeam;
import com.epam.rd.november2019.service.EmployeeService;
import com.epam.rd.november2019.service.FlightService;
import com.epam.rd.november2019.web.controller.FlightController;
import com.epam.rd.november2019.web.dto.EmployeeViewDto;
import com.epam.rd.november2019.web.dto.FlightCreateDto;
import com.epam.rd.november2019.web.dto.FlightViewDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ContextConfiguration(classes = TestAppConfig.class)
@WebMvcTest(FlightController.class)
public class FlightControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FlightService flightService;

    @MockBean
    private EmployeeService employeeService;

    @MockBean
    private DateTimeConverter dateTimeConverter;

    @BeforeEach
    public void startMocks() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/templates/");
        viewResolver.setSuffix(".html");

        mockMvc = MockMvcBuilders
                .standaloneSetup(new FlightController(flightService, dateTimeConverter, employeeService))
                .setViewResolvers(viewResolver)
                .build();
    }

    @Test
    public void getAllFlightsShouldReturnFlightsPage() throws Exception {
        //Given
        List<FlightViewDto> viewDtoList = new ArrayList<>();
        viewDtoList.add(new FlightViewDto());
        given(this.flightService.getAllFlights()).willReturn(viewDtoList);
        //When
        //Then
        mockMvc.perform(get("/flights")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("flight"))
                .andExpect(model().attributeExists("flights"))
                .andExpect(view().name("flights"));
    }

    @Test
    void findFlightShouldReturnFlightsPage() throws Exception {
        //Given
        List<FlightViewDto> viewDtoList = new ArrayList<>();
        viewDtoList.add(new FlightViewDto());
        FlightCreateDto createDto = new FlightCreateDto();
        given(this.flightService.findFlights(createDto)).willReturn(viewDtoList);
        //When
        //Then
        mockMvc.perform(post("/flights")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("flights"))
                .andExpect(view().name("/flights"));
    }

    @Test
    void addFlightShouldRedirectToFlightsPage() throws Exception {
        //Given
        FlightCreateDto createDto = new FlightCreateDto();
        FlightViewDto viewDto = new FlightViewDto();
        given(this.flightService.addFlight(createDto)).willReturn(viewDto);
        //When
        //Then
        mockMvc.perform(post("/flights/add")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/flights"));
    }

    @Test
    void removeFlightShouldRedirectToFlightsPage() throws Exception {
        //Given
        int id = 1;
        given(this.flightService.removeFlight(id)).willReturn(true);
        //When
        //Then
        mockMvc.perform(get("/deleteFlight/{id}", id)
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/flights"));
    }

    @Test
    void editFlightShouldReturnFlightDataPage() throws Exception {
        //Given
        int id = 1;
        FlightCreateDto createDto = new FlightCreateDto();
        given(this.flightService.editFlight(id, createDto)).willReturn(true);
        //When
        //Then
        mockMvc.perform(get("/editFlight/{id}", id)
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(view().name("flight_data"));
    }

    @Test
    void editFlightShouldRedirectToFlightsPage() throws Exception {
        //Given
        int id = 1;
        FlightCreateDto createDto = new FlightCreateDto();
        given(this.flightService.editFlight(id, createDto)).willReturn(true);
        //When
        //Then
        mockMvc.perform(post("/editFlight/{id}", id)
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/flights"));
    }

    @Test
    void editStatusShouldRedirectToSpecificFlightPage() throws Exception {
        //Given
        int id = 1;
        FlightStatus status = FlightStatus.CHECK_IN;
        given(this.flightService.edit(id, status)).willReturn(true);
        //When
        //Then
        mockMvc.perform(post("/editStatus/{id}", id)
                .param("status", status.toString())
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/flight/" + id));
    }

    @Test
    void showFlightShouldReturnSpecificFlightPage() throws Exception {
        //Given
        int id = 1;
        FlightViewDto dto = new FlightViewDto();
        dto.setFlightTeam(new FlightTeam(new ArrayList<>()));
        given(this.flightService.getById(id)).willReturn(dto);
        given(this.employeeService.getEmployeesById(dto.getFlightTeam().getEmployeeIds())).willReturn(new ArrayList<EmployeeViewDto>());
        //When
        //Then
        mockMvc.perform(get("/flight/{id}", id)
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("flight"))
                .andExpect(model().attributeExists("employees"))
                .andExpect(view().name("flight_data"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"Flight name", "Flight id", "Get all"})
    void testSortFlightShouldReturnFlightsPage(String sortBy) throws Exception {
        //Given
        given(this.flightService.sortByFlightName()).willReturn(new ArrayList<>());
        given(this.flightService.sortById()).willReturn(new ArrayList<>());
        given(this.flightService.getAllFlights()).willReturn(new ArrayList<>());
        //When
        //Then
        mockMvc.perform(post("/sort_flights")
                .param("sortBy", sortBy)
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(view().name("flights"));
    }
}
