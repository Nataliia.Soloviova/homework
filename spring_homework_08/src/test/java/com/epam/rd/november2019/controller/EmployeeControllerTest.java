package com.epam.rd.november2019.controller;


import com.epam.rd.november2019.config.TestAppConfig;
import com.epam.rd.november2019.service.EmployeeService;
import com.epam.rd.november2019.web.controller.EmployeeController;
import com.epam.rd.november2019.web.dto.EmployeeCreateDto;
import com.epam.rd.november2019.web.dto.EmployeeViewDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ContextConfiguration(classes = TestAppConfig.class)
@WebMvcTest(EmployeeController.class)
public class EmployeeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmployeeService employeeService;

    @BeforeEach
    public void startMocks() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/templates/");
        viewResolver.setSuffix(".html");

        mockMvc = MockMvcBuilders
                .standaloneSetup(new EmployeeController(employeeService))
                .setViewResolvers(viewResolver)
                .build();
    }

    @Test
    public void getAllEmployeesShouldReturnEmployeesPage() throws Exception {
        //Given
        List<EmployeeViewDto> viewDtoList = new ArrayList<>();
        viewDtoList.add(new EmployeeViewDto());
        given(this.employeeService.getAllEmployees()).willReturn(viewDtoList);
        //When
        //Then
        mockMvc.perform(get("/employees")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("employee"))
                .andExpect(model().attributeExists("employees"))
                .andExpect(view().name("employees"));
    }

    @Test
    void findEmployeeShouldReturnEmployeesPage() throws Exception {
        //Given
        List<EmployeeViewDto> viewDtoList = new ArrayList<>();
        viewDtoList.add(new EmployeeViewDto());
        EmployeeCreateDto createDto = new EmployeeCreateDto();
        given(this.employeeService.findEmployees(createDto)).willReturn(viewDtoList);
        //When
        //Then
        mockMvc.perform(post("/employees")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("employees"))
                .andExpect(view().name("employees"));
    }

    @Test
    void addEmployeeShouldRedirectToEmployeesPage() throws Exception {
        //Given
        EmployeeCreateDto createDto = new EmployeeCreateDto();
        EmployeeViewDto viewDto = new EmployeeViewDto();
        given(this.employeeService.addEmployee(createDto)).willReturn(viewDto);
        //When
        //Then
        mockMvc.perform(post("/employees/add")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/employees"));
    }

    @Test
    void removeEmployeeShouldRedirectToEmployeesPage() throws Exception {
        //Given
        int id = 1;
        given(this.employeeService.removeEmployee(id)).willReturn(true);
        //When
        //Then
        mockMvc.perform(get("/deleteEmployee/{id}", id)
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/employees"));
    }

    @Test
    void editEmployeeShouldReturnEmployeeDataPage() throws Exception {
        //Given
        int id = 1;
        EmployeeCreateDto createDto = new EmployeeCreateDto();
        given(this.employeeService.editEmployee(id, createDto)).willReturn(true);
        //When
        //Then
        mockMvc.perform(get("/editEmployee/{id}", id)
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(view().name("employee_data"));
    }

    @Test
    void editEmployeeShouldRedirectToEmployeesPage() throws Exception {
        //Given
        int id = 1;
        EmployeeCreateDto createDto = new EmployeeCreateDto();
        given(this.employeeService.editEmployee(id, createDto)).willReturn(true);
        //When
        //Then
        mockMvc.perform(post("/editEmployee/{id}", id)
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/employees"));
    }
}
