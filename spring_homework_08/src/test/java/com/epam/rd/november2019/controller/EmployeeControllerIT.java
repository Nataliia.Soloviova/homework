package com.epam.rd.november2019.controller;

import com.epam.rd.november2019.SpringIntegrationTest;
import com.epam.rd.november2019.model.EmployeeRole;
import com.epam.rd.november2019.model.Sex;
import com.epam.rd.november2019.web.dto.EmployeeCreateDto;
import com.epam.rd.november2019.web.dto.EmployeeViewDto;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class EmployeeControllerIT extends SpringIntegrationTest {

    @ParameterizedTest
    @ValueSource(strings = {"/employees", "/deleteEmployee/{id}", "/editEmployee/{id}", "/employees/add"})
    public void shouldGetRedirectionToLoginPage(String link) throws Exception {
        //Given
        int id = 1;
        //When
        //Then
        mockMvc.perform(get(link, id)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound());
    }

    @WithMockUser(roles = "DISPATCHER")
    @ParameterizedTest
    @ValueSource(strings = {"/deleteEmployee/{id}", "/editEmployee/{id}", "/employees/add"})
    public void shouldGetForbiddenResponseForDispatcher(String link) throws Exception {
        //Given
        int id = 1;
        //When
        //Then
        mockMvc.perform(get(link, id)
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isForbidden());
    }

    @Disabled("Should prepare database before running test")
    @WithMockUser(roles = {"ADMINISTRATOR", "DISPATCHER"})
    @Test
    void testGetAllEmployeesShouldReturnEmployeesList() throws Exception {
        //Given
        List<EmployeeViewDto> expected = new ArrayList<>();
        expected.add(new EmployeeViewDto(1, "Jackson", "Michael", EmployeeRole.PILOT, Sex.MAN, "London"));
        expected.add(new EmployeeViewDto(2, "Lewis", "Jack", EmployeeRole.NAVIGATOR, Sex.MAN, "London"));
        expected.add(new EmployeeViewDto(3, "Williams", "John", EmployeeRole.RADIO_OPERATOR, Sex.MAN, "London"));
        //When
        //Then
        mockMvc.perform(get("/employees")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("employee"))
                .andExpect(model().attributeExists("employees"))
                .andExpect(model().attribute("employees", isA(List.class)))
                .andExpect(model().attribute("employees", hasSize(expected.size())))
                .andExpect(model().attribute("employees", hasToString(expected.toString())))
                .andExpect(view().name("employees"));
    }


    @WithMockUser(roles = "ADMINISTRATOR")
    @Test
    void testFindEmployeeShouldReturnEmployeesList() throws Exception {
        //Given
        EmployeeViewDto employeeViewDto = new EmployeeViewDto(1, "Jackson", "Michael", EmployeeRole.PILOT, Sex.MAN, "London");
        List<EmployeeViewDto> expected = new ArrayList<>();
        expected.add(employeeViewDto);
        //When
        mockMvc.perform(post("/employees")
                .param("employeeId", String.valueOf(employeeViewDto.getEmployeeId()))
                .param("employeeRole", employeeViewDto.getEmployeeRole().toString())
                .param("sex", employeeViewDto.getSex().toString())
                .param("lastName", employeeViewDto.getLastName())
                .param("firstName", employeeViewDto.getFirstName())
                .param("city", employeeViewDto.getCity())
                .accept(MediaType.TEXT_HTML))
                //Then
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("employees"))
                .andExpect(model().attribute("employees", isA(List.class)))
                .andExpect(model().attribute("employees", hasSize(expected.size())))
                .andExpect(model().attribute("employees", hasToString(expected.toString())))
                .andExpect(view().name("employees"));
    }

    @Disabled("Should prepare database before running test")
    @WithMockUser(roles = "ADMINISTRATOR")
    @Test
    void testAddEmployeeShouldReturnEmployeesPage() throws Exception {
        //Given
        int expectedSize = 4;
        EmployeeCreateDto createDto = new EmployeeCreateDto("Tompson", "Michael", EmployeeRole.PILOT, Sex.MAN, "London");
        List<EmployeeViewDto> expectedList = new ArrayList<>();
        expectedList.add(new EmployeeViewDto(1, "Jackson", "Michael", EmployeeRole.PILOT, Sex.MAN, "London"));
        expectedList.add(new EmployeeViewDto(2, "Lewis", "Jack", EmployeeRole.NAVIGATOR, Sex.MAN, "London"));
        expectedList.add(new EmployeeViewDto(3, "Williams", "John", EmployeeRole.RADIO_OPERATOR, Sex.MAN, "London"));
        expectedList.add(new EmployeeViewDto(4, "Tompson", "Michael", EmployeeRole.PILOT, Sex.MAN, "London"));
        //When
        mockMvc.perform(post("/employees/add")
                .param("employeeRole", createDto.getEmployeeRole().toString())
                .param("sex", createDto.getSex().toString())
                .param("lastName", createDto.getLastName())
                .param("firstName", createDto.getFirstName())
                .param("city", createDto.getCity())
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/employees"));
        //Then
        mockMvc.perform(get("/employees")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("employee"))
                .andExpect(model().attributeExists("employees"))
                .andExpect(model().attribute("employees", isA(List.class)))
                .andExpect(model().attribute("employees", hasSize(expectedSize)))
                .andExpect(model().attribute("employees", hasToString(expectedList.toString())))
                .andExpect(view().name("employees"));
    }

    @Disabled("Should prepare database before running test")
    @WithMockUser(roles = "ADMINISTRATOR")
    @Test
    void testRemoveEmployeeShouldRedirectToEmployeesPage() throws Exception {
        //Given
        int id = 1;
        int expectedSize = 2;
        //When
        mockMvc.perform(get("/deleteEmployee/{id}", id)
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/employees"));
        //Then
        mockMvc.perform(get("/employees")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("employee"))
                .andExpect(model().attributeExists("employees"))
                .andExpect(model().attribute("employees", isA(List.class)))
                .andExpect(model().attribute("employees", hasSize(expectedSize)))
                .andExpect(view().name("employees"));
    }

    @WithMockUser(roles = "ADMINISTRATOR")
    @Test
    void testEditEmployeeShouldReturnEmployeeDataPage() throws Exception {
        //Given
        int id = 1;
        //When
        mockMvc.perform(get("/editEmployee/{id}", id)
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(view().name("employee_data"));
        //Then
        mockMvc.perform(get("/employees")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("employee"))
                .andExpect(model().attributeExists("employees"))
                .andExpect(view().name("employees"));
    }

    @WithMockUser(roles = "ADMINISTRATOR")
    @Test
    void testEditEmployeeShouldRedirectToEmployeesPage() throws Exception {
        //Given
        int id = 2;
        EmployeeCreateDto createDto = new EmployeeCreateDto("Ivanov", "Ivan", EmployeeRole.NAVIGATOR, Sex.MAN, "Paris");
        //When
        mockMvc.perform(post("/editEmployee/{id}", id)
                .param("firstName", createDto.getFirstName())
                .param("employeeRole", createDto.getEmployeeRole().toString())
                .param("sex", createDto.getSex().toString())
                .param("lastName", createDto.getLastName())
                .param("city", createDto.getCity())
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/employees"));
        //Then
        mockMvc.perform(get("/employees")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("employee"))
                .andExpect(model().attributeExists("employees"))
                .andExpect(view().name("employees"));
    }
}
