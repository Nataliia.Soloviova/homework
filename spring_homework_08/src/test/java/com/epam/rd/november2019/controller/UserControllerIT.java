package com.epam.rd.november2019.controller;


import com.epam.rd.november2019.SpringIntegrationTest;
import com.epam.rd.november2019.model.UserRole;
import com.epam.rd.november2019.web.dto.UserCreateDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


public class UserControllerIT extends SpringIntegrationTest {

    @ParameterizedTest
    @ValueSource(strings = {"/profile", "/admin", "/dispatcher", "/403"})
    public void shouldGetRedirectionToLoginPage(String link) throws Exception {
        mockMvc.perform(get(link)
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isFound());
    }

    @Test
    public void postRegistrationShouldReturnRegistrationPageWithMessage() throws Exception {
        //Given
        String expected = "You are successfully registered. Please, log in.";
        UserCreateDto dto = new UserCreateDto("sam_samos_001@corp.com", "sam_samos_001", "samos12345", UserRole.ROLE_ADMINISTRATOR);

        //When
        mockMvc.perform(post("/registration")
                .with(csrf())
                .accept(MediaType.TEXT_HTML)
                .param("userName", dto.getUserName())
                .param("password", dto.getPassword())
                .param("email", dto.getEmail())
                .param("userRole", dto.getUserRole().toString()))
                .andExpect(status().isOk())
                .andExpect(model().attribute("message", expected))
                .andExpect(view().name("registration"));

        //Then
        mockMvc.perform(get("/registration")
                .contentType(MediaType.TEXT_HTML))
                .andExpect(status().isOk());
    }

    @Test
    public void postRegistrationWithIncorrectEmailShouldReturnRegistrationPageWithErrorMessage() throws Exception {
        //Given
        String expected = "Incorrect information! The email must be like surname_name_000@corp.com. Try again!";
        UserCreateDto dto = new UserCreateDto("s@corp.com", "sam_samos_001", "samos12345", UserRole.ROLE_ADMINISTRATOR);

        //When
        mockMvc.perform(post("/registration")
                .with(csrf())
                .accept(MediaType.TEXT_HTML)
                .param("userName", dto.getUserName())
                .param("password", dto.getPassword())
                .param("email", dto.getEmail())
                .param("userRole", dto.getUserRole().toString()))
                .andExpect(status().isOk())
                .andExpect(request().attribute("errorMessage", expected))
                .andExpect(view().name("/registration"));

        //Then
        mockMvc.perform(get("/registration")
                .contentType(MediaType.TEXT_HTML))
                .andExpect(status().isOk());
    }

    @Test
    public void postRegistrationWithIncorrectPasswordShouldReturnRegistrationPageWithErrorMessage() throws Exception {
        //Given
        String expected = "Incorrect information! Password must have 5-20 letters and 5-20 numbers. Try again!";
        UserCreateDto dto = new UserCreateDto("sam_samos_001@corp.com", "sam_samos_001", "qwerty", UserRole.ROLE_ADMINISTRATOR);

        //When
        mockMvc.perform(post("/registration")
                .with(csrf())
                .accept(MediaType.TEXT_HTML)
                .param("userName", dto.getUserName())
                .param("password", dto.getPassword())
                .param("email", dto.getEmail())
                .param("userRole", dto.getUserRole().toString()))
                .andExpect(status().isOk())
                .andExpect(request().attribute("errorMessage", expected))
                .andExpect(view().name("/registration"));

        //Then
        mockMvc.perform(get("/registration")
                .contentType(MediaType.TEXT_HTML))
                .andExpect(status().isOk());
    }

    @Test
    public void postRegistrationWithIncorrectLoginShouldReturnRegistrationPageWithErrorMessage() throws Exception {
        //Given
        String expected = "Incorrect information! The login must be like surname_name_000. Try again!";
        UserCreateDto dto = new UserCreateDto("sam_samos_001@corp.com", "sam", "qwerty12345", UserRole.ROLE_ADMINISTRATOR);

        //When
        mockMvc.perform(post("/registration")
                .with(csrf())
                .accept(MediaType.TEXT_HTML)
                .param("userName", dto.getUserName())
                .param("password", dto.getPassword())
                .param("email", dto.getEmail())
                .param("userRole", dto.getUserRole().toString()))
                .andExpect(status().isOk())
                .andExpect(request().attribute("errorMessage", expected))
                .andExpect(view().name("/registration"));

        //Then
        mockMvc.perform(get("/registration")
                .contentType(MediaType.TEXT_HTML))
                .andExpect(status().isOk());
    }

    @Test
    public void loginShouldBeCorrectAndRedirectToProfile() throws Exception {
        mockMvc.perform(formLogin()
                .user("Ivan_Ivanov_001")
                .password("ivanov12345"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/profile"));
    }

    @Test
    public void loginWithBadCredentialsShouldRedirectToLoginPageWithError() throws Exception {
        mockMvc.perform(formLogin()
                .user("Bob")
                .password("bob12345"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/login?error"));
    }

    @WithMockUser(roles = "ADMINISTRATOR")
    @Test
    public void shouldGetForbiddenResponseForAdmin() throws Exception {
        mockMvc.perform(get("/dispatcher")
                .accept(MediaType.TEXT_HTML_VALUE))
                .andExpect(status().isForbidden());
    }

    @WithMockUser(roles = "DISPATCHER")
    @Test
    public void shouldGetForbiddenResponseForDispatcher() throws Exception {
        mockMvc.perform(get("/admin")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isForbidden());
    }

    @WithMockUser(roles = "DISPATCHER")
    @Test
    public void shouldGetSecretResourceForDispatcher() throws Exception {
        mockMvc.perform(get("/dispatcher")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk());
    }

    @WithMockUser(roles = "ADMINISTRATOR")
    @Test
    public void shouldGetSecretResourceForAdmin() throws Exception {
        mockMvc.perform(get("/admin")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk());
    }
}
