package com.epam.rd.november2019.config;


import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import javax.sql.DataSource;

@TestConfiguration
@ComponentScan(basePackages = "com.epam.rd.november2019")
public class TestAppConfig {

    @Bean
    public DataSource dataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName("org.h2.Driver");
        dataSourceBuilder.url("jdbc:h2:mem:airline;INIT=RUNSCRIPT FROM 'classpath:db.sql'");
        return dataSourceBuilder.build();
    }
}
