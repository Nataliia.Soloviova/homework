package com.epam.rd.november2019.controller;


import com.epam.rd.november2019.config.TestAppConfig;
import com.epam.rd.november2019.service.UserService;
import com.epam.rd.november2019.web.controller.UserController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ContextConfiguration(classes = TestAppConfig.class)
@WebMvcTest(controllers = UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @BeforeEach
    public void startMocks() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/templates/");
        viewResolver.setSuffix(".html");

        mockMvc = MockMvcBuilders
                .standaloneSetup(new UserController(userService))
                .setViewResolvers(viewResolver)
                .build();
    }

    @Test
    public void getMainPageShouldRedirectToLoginPage() throws Exception {
        mockMvc.perform(get("/")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/login"))
                .andReturn();
    }

    @Test
    public void getLoginPageShouldReturnLoginPage() throws Exception {
        mockMvc.perform(get("/login")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void getRegistrationPageShouldReturnRegistrationPage() throws Exception {
        mockMvc.perform(get("/login")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void getProfilePageShouldReturnProfilePage() throws Exception {
        mockMvc.perform(get("/profile")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void getAdminPageShouldReturnAdminPage() throws Exception {
        mockMvc.perform(get("/admin")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void getDispatcherPageShouldReturnDispatcherPage() throws Exception {
        mockMvc.perform(get("/dispatcher")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void getErrorPageShouldReturnErrorPage() throws Exception {
        mockMvc.perform(get("/403")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void postRegistrationShouldReturnRegistrationPage() throws Exception {
        mockMvc.perform(post("/registration")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().is4xxClientError())
                .andReturn();
    }
}
