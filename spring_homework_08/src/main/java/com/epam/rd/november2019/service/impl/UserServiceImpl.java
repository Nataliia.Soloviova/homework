package com.epam.rd.november2019.service.impl;


import com.epam.rd.november2019.converter.UserAccountConverter;
import com.epam.rd.november2019.dao.UserDao;
import com.epam.rd.november2019.exception.UserAlreadyExistException;
import com.epam.rd.november2019.exception.ValidationException;
import com.epam.rd.november2019.model.User;
import com.epam.rd.november2019.service.UserService;
import com.epam.rd.november2019.valid.UserValidator;
import com.epam.rd.november2019.web.dto.UserCreateDto;
import com.epam.rd.november2019.web.dto.UserViewDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private final UserDao userDao;
    private final UserAccountConverter userAccountConverter;
    private final PasswordEncoder passwordEncoder;
    private final UserValidator userValidator;

    @Autowired
    public UserServiceImpl(UserDao userDao,
                           UserAccountConverter userAccountConverter,
                           PasswordEncoder passwordEncoder,
                           UserValidator userValidator) {
        this.userDao = userDao;
        this.userAccountConverter = userAccountConverter;
        this.passwordEncoder = passwordEncoder;
        this.userValidator = userValidator;
    }

    @Override
    public UserViewDto registerUser(UserCreateDto createDto) throws ValidationException {
        userValidator.validateNewUser(createDto);

        if (userDao.getByEmail(createDto.getEmail()) != null) {
            throw new UserAlreadyExistException("There is an account with that email address:" + createDto.getEmail());
        }

        User user = new User();
        user.setUserName(createDto.getUserName());
        user.setPassword(passwordEncoder.encode(createDto.getPassword()));
        user.setEmail(createDto.getEmail());
        user.setUserRole(createDto.getUserRole());
        userDao.add(user);

        return userAccountConverter.asUserDto(user);
    }
}
