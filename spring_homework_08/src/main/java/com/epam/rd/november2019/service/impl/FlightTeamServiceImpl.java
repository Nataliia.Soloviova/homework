package com.epam.rd.november2019.service.impl;

import com.epam.rd.november2019.converter.EmployeeConverter;
import com.epam.rd.november2019.dao.EmployeeDao;
import com.epam.rd.november2019.dao.EmployeeScheduleDao;
import com.epam.rd.november2019.dao.FlightDao;
import com.epam.rd.november2019.model.Employee;
import com.epam.rd.november2019.model.EmployeeSchedule;
import com.epam.rd.november2019.model.Flight;
import com.epam.rd.november2019.model.FlightTeam;
import com.epam.rd.november2019.service.FlightTeamService;
import com.epam.rd.november2019.web.dto.EmployeeViewDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class FlightTeamServiceImpl implements FlightTeamService {

    private final FlightDao flightDao;
    private final EmployeeDao employeeDao;
    private final EmployeeScheduleDao employeeScheduleDao;
    private final EmployeeConverter employeeConverter;

    @Autowired
    public FlightTeamServiceImpl(FlightDao flightDao,
                                 EmployeeDao employeeDao,
                                 EmployeeScheduleDao employeeScheduleDao,
                                 EmployeeConverter employeeConverter) {
        this.flightDao = flightDao;
        this.employeeDao = employeeDao;
        this.employeeScheduleDao = employeeScheduleDao;
        this.employeeConverter = employeeConverter;
    }

    @Override
    public FlightTeam createFlightTeam(List<Integer> employeeIds, int flightId) {
        Flight flight = flightDao.getById(flightId);
        for (Integer employeeId :
                employeeIds) {
            employeeScheduleDao.add(new EmployeeSchedule(
                    employeeId,
                    flightId,
                    flight.getDepartureDate(),
                    flight.getArrivalDate(),
                    flight.getDepartureCity(),
                    flight.getArrivalCity()
            ));
        }
        flight.setFlightTeam(employeeScheduleDao.getFlightTeam(flightId));
        return flight.getFlightTeam();
    }

    @Override
    public List<EmployeeViewDto> getFreeEmployees(int flightId) {
        List<EmployeeViewDto> employees = new LinkedList<>();

        Flight flight = flightDao.getById(flightId);
        flight.setFlightTeam(employeeScheduleDao.getFlightTeam(flightId));
        List<Integer> freeEmployeeIds = employeeScheduleDao.getFreeEmployee(flight);
        List<Integer> employeeIds = flight.getFlightTeam().getEmployeeIds();
        freeEmployeeIds.removeAll(employeeIds);

        for (Integer employeeId :
                freeEmployeeIds) {
            Employee employee = employeeDao.getById(employeeId);
            employees.add(employeeConverter.asEmployeeDto(employee));
        }

        return employees;
    }

    @Override
    public void removeEmployeeFromFlightTeam(int employeeId, int flightId) {
        employeeScheduleDao.removeEmployeeFromFlightTeam(employeeId, flightId);
    }
}
