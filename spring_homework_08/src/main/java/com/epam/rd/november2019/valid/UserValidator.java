package com.epam.rd.november2019.valid;

import com.epam.rd.november2019.exception.ValidationException;
import com.epam.rd.november2019.web.dto.UserCreateDto;

public interface UserValidator {

    void validateUserCredentials(String userName, String password) throws ValidationException;

    void validateNewUser(UserCreateDto createDto) throws ValidationException;
}