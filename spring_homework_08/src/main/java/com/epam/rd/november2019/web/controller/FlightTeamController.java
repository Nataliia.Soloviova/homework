package com.epam.rd.november2019.web.controller;

import com.epam.rd.november2019.service.EmployeeService;
import com.epam.rd.november2019.service.FlightTeamService;
import com.epam.rd.november2019.web.dto.EmployeeViewDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class FlightTeamController {

    private final FlightTeamService flightTeamService;
    private final EmployeeService employeeService;

    @Autowired
    public FlightTeamController(FlightTeamService flightTeamService, EmployeeService employeeService) {
        this.flightTeamService = flightTeamService;
        this.employeeService = employeeService;
    }

    @GetMapping("editFlightTeam/{id}")
    public String getFreeEmployees(@PathVariable("id") int id, HttpServletRequest req) {
        req.setAttribute("flightId", id);
        List<EmployeeViewDto> employees = flightTeamService.getFreeEmployees(id);
        req.setAttribute("employees", employees);

        return "employees";
    }

    @PostMapping("/addIntoFlightTeam")
    public String createFlightTeam(HttpServletRequest req) {
        int flightId = Integer.parseInt(req.getParameter("flightId"));
        String[] ids = req.getParameterValues("employeeId");
        List<Integer> list = new LinkedList<>();
        for (String id : ids) {
            list.add(Integer.parseInt(id));
        }
        flightTeamService.createFlightTeam(list, flightId);

        return "redirect:/flight/" + flightId;
    }

    @RequestMapping("/deleteEmployeeFromTeam/{flight_id}/{employee_id}")
    public String removeEmployee(@PathVariable("employee_id") int employeeId,
                                 @PathVariable("flight_id") int flightId) {
        flightTeamService.removeEmployeeFromFlightTeam(employeeId, flightId);

        return "redirect:/flight/" + flightId;
    }

    @PostMapping("/findFreeEmployees")
    public String freeEmployees(HttpServletRequest req) {
        req.setAttribute("flightId", req.getParameter("flightId"));
        List<EmployeeViewDto> employees = new ArrayList<>();
        String employeeRole = req.getParameter("employeeRole");
        employees = flightTeamService.getFreeEmployees(Integer.parseInt(req.getParameter("flightId")));
        if (!employeeRole.equals("getAll")) {
            employees = employees.stream().filter(i -> i.getEmployeeRole().toString().equals(employeeRole)).collect(Collectors.toList());
        }
        req.setAttribute("employees", employees);

        return "employees";
    }
}
