package com.epam.rd.november2019.dao.impl;

import com.epam.rd.november2019.dao.UserDao;
import com.epam.rd.november2019.model.User;
import com.epam.rd.november2019.model.UserRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class UserDaoImpl implements UserDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDaoImpl.class);
    private static final String ADD_USER = "INSERT INTO users (user_name, user_email, user_password, role_id) VALUES (?, ?, ?, (SELECT role_id FROM user_roles WHERE role_name=?))";
    private static final String GET_BY_EMAIL = "SELECT user_name, user_email, user_password, user_roles.role_name FROM user_roles JOIN users ON users.role_id=user_roles.role_id WHERE user_email = ?";

    @Autowired
    private DataSource dataSource;

    @Override
    public User add(User user) {
        LOGGER.debug("Trying to add user {} to users table", user);
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(ADD_USER)) {
            preparedStatement.setString(1, user.getUserName());
            preparedStatement.setString(2, user.getEmail());
            preparedStatement.setString(3, user.getPassword());
            preparedStatement.setString(4, user.getUserRole().toString());
            preparedStatement.executeUpdate();
            LOGGER.warn("User {} was added into table users", user);
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.warn("Failed to insert user into DB");
        }
        return user;
    }

    @Override
    public User getByEmail(String email) {
        User user = null;
        LOGGER.debug("Trying to get user by email: {}", email);
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GET_BY_EMAIL)) {
            preparedStatement.setString(1, email);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                LOGGER.debug("User with email: {} is returned", email);
                user = new User();
                user.setUserName(resultSet.getString("user_name"));
                user.setEmail(resultSet.getString("user_email"));
                user.setPassword(resultSet.getString("user_password"));
                user.setUserRole(UserRole.valueOf(resultSet.getString("role_name")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.warn("Failed to load user from DB");
        }
        if (user == null) {
            LOGGER.warn("User not found by email: {}", email);
        }
        return user;
    }
}
