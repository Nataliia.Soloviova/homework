package com.epam.rd.november2019.web.dto;


import com.epam.rd.november2019.model.EmployeeRole;
import com.epam.rd.november2019.model.Sex;

import java.io.Serializable;

public class EmployeeCreateDto implements Serializable {

    private static final long serialVersionUID = -1467003435557820553L;

    private int employeeId;
    private String lastName;
    private String firstName;
    private EmployeeRole employeeRole;
    private Sex sex;
    private String city;

    public EmployeeCreateDto() {
    }

    public EmployeeCreateDto(String lastName, String firstName, EmployeeRole employeeRole, Sex sex, String city) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.employeeRole = employeeRole;
        this.sex = sex;
        this.city = city;
    }

    public EmployeeCreateDto(int employeeId, String lastName, String firstName, EmployeeRole employeeRole, Sex sex, String city) {
        this.employeeId = employeeId;
        this.lastName = lastName;
        this.firstName = firstName;
        this.employeeRole = employeeRole;
        this.sex = sex;
        this.city = city;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public EmployeeRole getEmployeeRole() {
        return employeeRole;
    }

    public void setEmployeeRole(EmployeeRole employeeRole) {
        this.employeeRole = employeeRole;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return String.format("[%s : %s : %s : %s : %s : %s]", employeeId, employeeRole, firstName, lastName, sex, city);
    }
}
