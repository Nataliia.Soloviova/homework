package com.epam.rd.november2019.web.controller;


import com.epam.rd.november2019.converter.DateTimeConverter;
import com.epam.rd.november2019.model.FlightStatus;
import com.epam.rd.november2019.service.EmployeeService;
import com.epam.rd.november2019.service.FlightService;
import com.epam.rd.november2019.web.dto.FlightCreateDto;
import com.epam.rd.november2019.web.dto.FlightViewDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Controller
public class FlightController {

    private final FlightService flightService;
    private final DateTimeConverter timeConverter;
    private final EmployeeService employeeService;

    @Autowired
    public FlightController(FlightService flightService, DateTimeConverter timeConverter, EmployeeService employeeService) {
        this.flightService = flightService;
        this.timeConverter = timeConverter;
        this.employeeService = employeeService;
    }

    @GetMapping("/flights")
    public String allFlights(Model model) {
        model.addAttribute("flight", new FlightViewDto());
        model.addAttribute("flights", flightService.getAllFlights());

        return "flights";
    }

    @PostMapping("/flights")
    public String findFlight(HttpServletRequest req, Model model) {
        List<FlightViewDto> flights = flightService.findFlights(extractFlightFromRequest(req));
        model.addAttribute("flights", flights);

        return "/flights";
    }

    @PostMapping(value = "/flights/add")
    public String addFlight(HttpServletRequest req, Model model) {
        FlightCreateDto createDto = extractFlightFromRequest(req);
        flightService.addFlight(createDto);

        return "redirect:/flights";
    }

    @RequestMapping("deleteFlight/{id}")
    public String removeFlight(@PathVariable("id") int id) {
        flightService.removeFlight(id);

        return "redirect:/flights";
    }

    @RequestMapping("editFlight/{id}")
    public String editFlight(@PathVariable("id") int id, Model model) {
        model.addAttribute("flight", flightService.getById(id));
        model.addAttribute("flights", flightService.getAllFlights());

        return "flight_data";
    }

    @PostMapping(value = "/editFlight/{id}")
    public String editFlight(@PathVariable("id") int id, HttpServletRequest req) {
        FlightCreateDto createDto = extractFlightFromRequest(req);
        flightService.editFlight(id, createDto);

        return "redirect:/flights";
    }

    @PostMapping(value = "/sort_flights")
    public String sortFlight(HttpServletRequest req, Model model) {
        String sortBy = req.getParameter("sortBy");
        List<FlightViewDto> flights = new ArrayList<>();
        if (sortBy.equals("Flight name")) {
            flights = flightService.sortByFlightName();
        } else if (sortBy.equals("Flight id")) {
            flights = flightService.sortById();
        } else {
            flights = flightService.getAllFlights();
        }
        model.addAttribute("flights", flights);

        return "flights";
    }

    @RequestMapping("flight/{id}")
    public String showFlight(@PathVariable("id") int id, Model model) {
        FlightViewDto dto = flightService.getById(id);
        model.addAttribute("flight", dto);

        List<Integer> employeeIds = dto.getFlightTeam().getEmployeeIds();
        model.addAttribute("employees", employeeService.getEmployeesById(employeeIds));

        return "flight_data";
    }

    @PostMapping("editStatus/{id}")
    public String editStatus(@PathVariable("id") int id,
                             @RequestParam("status") String status,
                             Model model) {
        flightService.edit(id, FlightStatus.valueOf(status.toUpperCase()));

        return "redirect:/flight/" + id;
    }

    private FlightCreateDto extractFlightFromRequest(HttpServletRequest req) {
        String departureCity = req.getParameter("departureCity");
        String departureAirport = req.getParameter("departureAirport");
        String arrivalCity = req.getParameter("arrivalCity");
        String arrivalAirport = req.getParameter("arrivalAirport");
        FlightStatus flightStatus = null;
        int flightId = 0;
        if (req.getParameter("flightId") != null && !req.getParameter("flightId").isEmpty()) {
            flightId = Integer.parseInt(req.getParameter("flightId"));
        }
        if (req.getParameter("status") != null && !req.getParameter("status").isEmpty()) {
            flightStatus = FlightStatus.valueOf(req.getParameter("status").toUpperCase());
        }
        Timestamp departureDate = null;
        Timestamp arrivalDate = null;
        if (req.getParameter("departureDate") != null && !req.getParameter("departureDate").isEmpty()) {
            departureDate = timeConverter.toTimestamp(req.getParameter("departureDate"));
        }
        if (req.getParameter("arrivalDate") != null && !req.getParameter("arrivalDate").isEmpty()) {
            arrivalDate = timeConverter.toTimestamp(req.getParameter("arrivalDate"));
        }
        return new FlightCreateDto(flightId, departureAirport, arrivalAirport, departureCity, arrivalCity, departureDate, arrivalDate, flightStatus);
    }
}
