package com.epam.rd.november2019.service.impl;

import com.epam.rd.november2019.converter.RequestConverter;
import com.epam.rd.november2019.dao.RequestDao;
import com.epam.rd.november2019.model.Request;
import com.epam.rd.november2019.service.RequestService;
import com.epam.rd.november2019.web.dto.RequestCreateDto;
import com.epam.rd.november2019.web.dto.RequestViewDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RequestServiceImpl implements RequestService {

    private final RequestDao requestDao;
    private final RequestConverter requestConverter;

    @Autowired
    public RequestServiceImpl(RequestDao requestDao,
                              RequestConverter requestConverter) {
        this.requestDao = requestDao;
        this.requestConverter = requestConverter;
    }

    @Override
    public List<RequestViewDto> getAllRequests() {
        List<RequestViewDto> dtoList = new ArrayList<>();
        List<Request> requests = requestDao.getAll();
        for (Request request :
                requests) {
            dtoList.add(requestConverter.asRequestDto(request));
        }
        return dtoList;
    }

    @Override
    public RequestViewDto addRequest(RequestCreateDto dto) {
        Request request = requestConverter.asRequest(dto);
        request = requestDao.add(request);

        return requestConverter.asRequestDto(request);
    }
}
