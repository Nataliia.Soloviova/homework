package com.epam.rd.november2019.web.controller;

import com.epam.rd.november2019.model.UserRole;
import com.epam.rd.november2019.service.UserService;
import com.epam.rd.november2019.web.dto.UserCreateDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }


    @GetMapping("/")
    public String getMainPage() {

        return "redirect:/login";
    }

    @GetMapping("/login")
    public String getLoginPage() {

        return "login";
    }

    @GetMapping("/registration")
    public String getRegistrationPage(Model model) {

        return "registration";
    }

    @PostMapping("/registration")
    public ModelAndView postRegistration(@RequestParam("userName") String userName,
                                         @RequestParam("password") String password,
                                         @RequestParam("email") String email,
                                         @RequestParam("userRole") String userRole,
                                         ModelAndView modelAndView) {
        UserCreateDto userCreateDto = new UserCreateDto(email, userName, password, UserRole.valueOf(userRole.toUpperCase()));
        userService.registerUser(userCreateDto);

        modelAndView.addObject("message", "You are successfully registered. Please, log in.");
        modelAndView.setViewName("registration");

        return modelAndView;
    }

    @GetMapping("/profile")
    public String getProfilePage() {

        return "profile";
    }

    @GetMapping("/admin")
    public String getAdminPage() {

        return "admin_menu";
    }

    @GetMapping("/dispatcher")
    public String getDispatcherPage() {

        return "dispatcher_menu";
    }

    @GetMapping("/403")
    public String getErrorPage() {

        return "error/403";
    }
}
