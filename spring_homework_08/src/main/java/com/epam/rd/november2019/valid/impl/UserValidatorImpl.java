package com.epam.rd.november2019.valid.impl;

import com.epam.rd.november2019.exception.ValidationException;
import com.epam.rd.november2019.valid.UserValidator;
import com.epam.rd.november2019.web.dto.UserCreateDto;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class UserValidatorImpl implements UserValidator {

    private static final String EMAIL_PATTERN = "[a-zA-Z]{2,20}_[a-zA-Z]{2,20}_\\d{3,10}@corp.com";
    private static final String PASSWORD_PATTERN = "[a-zA-Z]{5,20}\\d{5,20}";
    private static final String USERNAME_PATTERN = "[a-zA-Z]{2,20}_[a-zA-Z]{2,20}_\\d{3,10}";

    @Override
    public void validateUserCredentials(String userName, String password) throws ValidationException {
        checkName(userName);
        checkPassword(password);
    }

    @Override
    public void validateNewUser(UserCreateDto createDto) throws ValidationException {
        validateUserCredentials(createDto.getUserName(), createDto.getPassword());
        checkEmail(createDto.getEmail());
    }

    private void checkEmail(String email) throws ValidationException {
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        if (!matcher.matches()) {
            throw new ValidationException("Incorrect information! The email must be like surname_name_000@corp.com. Try again!");
        }
    }

    private void checkName(String userName) throws ValidationException {
        Pattern pattern = Pattern.compile(USERNAME_PATTERN);
        Matcher matcher = pattern.matcher(userName);
        if (!matcher.matches() || StringUtils.isEmpty(userName)) {
            throw new ValidationException("Incorrect information! The login must be like surname_name_000. Try again!");
        }
    }

    private void checkPassword(String password) throws ValidationException {
        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password);
        if (!matcher.matches() || StringUtils.isEmpty(password)) {
            throw new ValidationException("Incorrect information! Password must have 5-20 letters and 5-20 numbers. Try again!");
        }
    }
}