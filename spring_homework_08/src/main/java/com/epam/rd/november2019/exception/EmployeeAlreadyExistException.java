package com.epam.rd.november2019.exception;

public class EmployeeAlreadyExistException extends ApplicationException {

    public EmployeeAlreadyExistException(String message) {
        super(message);
    }
}
