package com.epam.rd.november2019.dao;

import com.epam.rd.november2019.model.Request;

import java.util.List;

public interface RequestDao {

    List<Request> getAll();

    Request add(Request request);
}
