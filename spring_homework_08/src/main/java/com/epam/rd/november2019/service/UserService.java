package com.epam.rd.november2019.service;

import com.epam.rd.november2019.web.dto.UserCreateDto;
import com.epam.rd.november2019.web.dto.UserViewDto;

public interface UserService {

    UserViewDto registerUser(UserCreateDto createDto);
}