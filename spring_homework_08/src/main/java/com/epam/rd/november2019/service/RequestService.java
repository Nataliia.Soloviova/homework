package com.epam.rd.november2019.service;

import com.epam.rd.november2019.web.dto.RequestCreateDto;
import com.epam.rd.november2019.web.dto.RequestViewDto;

import java.util.List;

public interface RequestService {

    List<RequestViewDto> getAllRequests();

    RequestViewDto addRequest(RequestCreateDto dto);
}
