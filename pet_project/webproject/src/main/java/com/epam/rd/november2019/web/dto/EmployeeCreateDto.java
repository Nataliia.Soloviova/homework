package com.epam.rd.november2019.web.dto;

import com.epam.rd.november2019.pojo.EmployeeRole;
import com.epam.rd.november2019.pojo.Sex;

import java.io.Serializable;

public class EmployeeCreateDto implements Serializable {

    private static final long serialVersionUID = -1467003435557820553L;

    private String lastName;
    private String firstName;
    private EmployeeRole employeeRole;
    private Sex sex;
    private String city;

    public EmployeeCreateDto(String lastName, String firstName, EmployeeRole employeeRole, Sex sex, String city) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.employeeRole = employeeRole;
        this.sex = sex;
        this.city = city;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public EmployeeRole getEmployeeRole() {
        return employeeRole;
    }

    public Sex getSex() {
        return sex;
    }

    public String getCity() {
        return city;
    }

    @Override
    public String toString() {
        return "EmployeeCreateDto{" +
                "lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", employeeRole=" + employeeRole +
                ", sex=" + sex +
                ", city='" + city + '\'' +
                '}';
    }
}
