package com.epam.rd.november2019.converter;

import com.epam.rd.november2019.pojo.Employee;
import com.epam.rd.november2019.web.dto.EmployeeCreateDto;
import com.epam.rd.november2019.web.dto.EmployeeViewDto;

public class EmployeeConverter {

    public Employee asEmployee(EmployeeCreateDto dto) {
        Employee employee = new Employee();
        employee.setFirstName(dto.getFirstName());
        employee.setLastName(dto.getLastName());
        employee.setEmployeeRole(dto.getEmployeeRole());
        employee.setSex(dto.getSex());
        employee.setCity(dto.getCity());
        return employee;
    }

    public EmployeeViewDto asEmployeeDto(Employee employee) {
        EmployeeViewDto dto = new EmployeeViewDto();
        dto.setEmployeeId(employee.getEmployeeId());
        dto.setEmployeeRole(employee.getEmployeeRole());
        dto.setFirstName(employee.getFirstName());
        dto.setLastName(employee.getLastName());
        dto.setSex(employee.getSex());
        dto.setCity(employee.getCity());
        return dto;
    }
}
