package com.epam.rd.november2019.web.servlet.flight;

import com.epam.rd.november2019.pojo.FlightStatus;
import com.epam.rd.november2019.services.impl.AdminServiceImpl;
import com.epam.rd.november2019.web.dto.FlightCreateDto;
import com.epam.rd.november2019.web.dto.UserAccountViewDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet (name = "editFlight", urlPatterns = "/editFlight")
public class EditFlightServlet extends HttpServlet {

    private AdminServiceImpl adminService = new AdminServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/findFlight.jsp").include(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int flightId = Integer.parseInt(req.getParameter("flightId"));
        FlightCreateDto createDto = null;
        boolean result;
        UserAccountViewDto dto = (UserAccountViewDto) req.getSession().getAttribute("user");
        if (dto.getUserRole().toString().equals("ADMINISTRATOR")) {
            createDto = ExtractObjectFromReq.extractFlight(req);
            result = adminService.editFlight(flightId, createDto);
        } else {
            FlightStatus flightStatus = FlightStatus.valueOf(req.getParameter("flightStatus").toUpperCase());
            result = adminService.editFlight(flightId, flightStatus);
        }
        if (result) {
            req.setAttribute("editMessage", "The editing was successful");
        } else {
            req.setAttribute("editMessage", "The editing wasn't successful");
        }
        req.getRequestDispatcher("/findFlight.jsp").forward(req, resp);
    }
}
