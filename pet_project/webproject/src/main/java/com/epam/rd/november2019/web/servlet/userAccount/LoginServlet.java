package com.epam.rd.november2019.web.servlet.userAccount;

import com.epam.rd.november2019.exception.ValidationException;
import com.epam.rd.november2019.services.impl.UserServiceImpl;
import com.epam.rd.november2019.web.dto.UserAccountViewDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "loginPage", urlPatterns = "/login")
public class LoginServlet extends HttpServlet {

    private UserServiceImpl userService = new UserServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/login.jsp").include(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userName = req.getParameter("loginName");
        String password = req.getParameter("password");
        try {
            UserAccountViewDto accountViewDto = userService.login(userName, password);
            HttpSession session = req.getSession(true);
            session.setAttribute("user", accountViewDto);
            resp.sendRedirect(req.getContextPath() + "/profile");
        } catch (ValidationException e) {
            req.setAttribute("errorLoginPassMessage", e.getMessage());
            req.getRequestDispatcher("/login.jsp").forward(req, resp);
        }
    }
}
