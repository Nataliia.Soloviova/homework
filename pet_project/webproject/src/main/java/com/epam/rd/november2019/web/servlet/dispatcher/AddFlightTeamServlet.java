package com.epam.rd.november2019.web.servlet.dispatcher;

import com.epam.rd.november2019.services.DispatcherService;
import com.epam.rd.november2019.services.impl.DispatcherServiceImpl;
import com.epam.rd.november2019.web.dto.EmployeeViewDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@WebServlet(name = "addFlightTeam", urlPatterns = "/addFlightTeam")
public class AddFlightTeamServlet extends HttpServlet {

    private DispatcherService dispatcherService = new DispatcherServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("flightId", req.getParameter("flightId"));
        List<EmployeeViewDto> employees = dispatcherService.getFreeEmployees(Integer.parseInt(req.getParameter("flightId")));
        req.setAttribute("employees", employees);
        req.getRequestDispatcher("/addFlightTeam.jsp").include(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int flightId = Integer.parseInt(req.getParameter("flightId"));
        String[] ids = req.getParameterValues("employeeId");
        List<Integer> list = new LinkedList<>();
        for (String id : ids) {
            list.add(Integer.parseInt(id));
        }
        dispatcherService.createFlightTeam(list, flightId);
        resp.sendRedirect("/flights");
    }
}
