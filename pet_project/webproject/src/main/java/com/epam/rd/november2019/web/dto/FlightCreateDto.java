package com.epam.rd.november2019.web.dto;

import com.epam.rd.november2019.pojo.FlightStatus;
import com.epam.rd.november2019.pojo.FlightTeam;

import java.io.Serializable;
import java.sql.Timestamp;

public class FlightCreateDto implements Serializable {

    private static final long serialVersionUID = 3582227784241194540L;

    private String departureAirport;
    private String arrivalAirport;
    private String departureCity;
    private String arrivalCity;
    private Timestamp departureDate;
    private Timestamp arrivalDate;
    private FlightStatus status;
    private FlightTeam flightTeam;

    public FlightCreateDto(FlightStatus status) {
        this.status = status;
    }

    public FlightCreateDto(String departureAirport, String arrivalAirport, String departureCity, String arrivalCity, Timestamp departureDate, Timestamp arrivalDate, FlightStatus status) {
        this.departureAirport = departureAirport;
        this.arrivalAirport = arrivalAirport;
        this.departureCity = departureCity;
        this.arrivalCity = arrivalCity;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.status = status;
    }

    public String getDepartureAirport() {
        return departureAirport;
    }

    public String getArrivalAirport() {
        return arrivalAirport;
    }

    public String getDepartureCity() {
        return departureCity;
    }

    public String getArrivalCity() {
        return arrivalCity;
    }

    public Timestamp getDepartureDate() {
        return departureDate;
    }

    public Timestamp getArrivalDate() {
        return arrivalDate;
    }

    public FlightStatus getStatus() {
        return status;
    }

    public FlightTeam getFlightTeam() {
        return flightTeam;
    }

    @Override
    public String toString() {
        return "FlightCreateDto{" +
                "departureAirport='" + departureAirport + '\'' +
                ", arrivalAirport='" + arrivalAirport + '\'' +
                ", departureCity='" + departureCity + '\'' +
                ", arrivalCity='" + arrivalCity + '\'' +
                ", departureDate=" + departureDate +
                ", arrivalDate=" + arrivalDate +
                ", status=" + status +
                ", flightTeam=" + flightTeam +
                '}';
    }
}
