package com.epam.rd.november2019.web.servlet.employee;

import com.epam.rd.november2019.services.AdminService;
import com.epam.rd.november2019.services.impl.AdminServiceImpl;
import com.epam.rd.november2019.web.dto.EmployeeViewDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "employees", urlPatterns = {"/employees"})
public class GetAllEmployeesServlet extends HttpServlet {

    private AdminService adminService = new AdminServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<EmployeeViewDto> employees = adminService.getAllEmployees();
        req.setAttribute("employees", employees);
        req.getRequestDispatcher("/employees.jsp").include(req, resp);
    }
}
