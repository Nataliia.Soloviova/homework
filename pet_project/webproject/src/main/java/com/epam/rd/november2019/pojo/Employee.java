package com.epam.rd.november2019.pojo;

import java.util.Objects;

public class Employee {

    private int employeeId;
    private String lastName;
    private String firstName;
    private EmployeeRole employeeRole;
    private Sex sex;
    private String city;

    public Employee() {
    }

    public Employee(String lastName, String firstName, EmployeeRole employeeRole, Sex sex, String city) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.employeeRole = employeeRole;
        this.sex = sex;
        this.city = city;
    }

    public Employee(int employeeId, String lastName, String firstName, EmployeeRole employeeRole, Sex sex, String city) {
        this.employeeId = employeeId;
        this.lastName = lastName;
        this.firstName = firstName;
        this.employeeRole = employeeRole;
        this.sex = sex;
        this.city = city;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public EmployeeRole getEmployeeRole() {
        return employeeRole;
    }

    public void setEmployeeRole(EmployeeRole employeeRole) {
        this.employeeRole = employeeRole;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String address) {
        this.city = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return employeeId == employee.employeeId &&
                Objects.equals(lastName, employee.lastName) &&
                Objects.equals(firstName, employee.firstName) &&
                employeeRole == employee.employeeRole &&
                sex == employee.sex &&
                Objects.equals(city, employee.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(employeeId, lastName, firstName, employeeRole, sex, city);
    }

    @Override
    public String toString() {
        return String.format("[%s : %s : %s : %s : %s : %s]", employeeId, employeeRole, firstName, lastName, sex, city);
    }
}
