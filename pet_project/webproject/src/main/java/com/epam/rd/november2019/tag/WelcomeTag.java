package com.epam.rd.november2019.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.time.LocalTime;

public class WelcomeTag extends TagSupport {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int doStartTag() throws JspException {
        String prefix = "Good morning, ";
        if (LocalTime.now().isAfter(LocalTime.NOON)) {
            prefix = "Good afternoon, ";
        }
        try {
            pageContext.getOut().write(prefix + name + "!");
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }

    @Override
    public void release() {
        super.release();
        this.name = null;
    }
}
