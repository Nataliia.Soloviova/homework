package com.epam.rd.november2019.web.servlet.employee;

import com.epam.rd.november2019.services.AdminService;
import com.epam.rd.november2019.services.impl.AdminServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "deleteEmployee", urlPatterns = {"/deleteEmployee"})
public class RemoveEmployeeServlet extends HttpServlet {

    private AdminService adminService = new AdminServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/findEmployee.jsp").include(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int employeeId = Integer.parseInt(req.getParameter("employeeId"));
        if (adminService.removeEmployee(employeeId)) {
            req.setAttribute("deleteMessage", "The deleting was successful");
        } else {
            req.setAttribute("deleteMessage", "The deleting wasn't successful");
        }
        req.getRequestDispatcher("/findEmployee.jsp").include(req, resp);
    }
}
