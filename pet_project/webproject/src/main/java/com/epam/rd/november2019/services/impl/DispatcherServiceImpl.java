package com.epam.rd.november2019.services.impl;

import com.epam.rd.november2019.converter.EmployeeConverter;
import com.epam.rd.november2019.dao.EmployeeDao;
import com.epam.rd.november2019.dao.EmployeeScheduleDao;
import com.epam.rd.november2019.dao.FlightDao;
import com.epam.rd.november2019.dao.impl.EmployeeDaoImpl;
import com.epam.rd.november2019.dao.impl.EmployeeScheduleDaoImpl;
import com.epam.rd.november2019.dao.impl.FlightDaoImpl;
import com.epam.rd.november2019.pojo.Employee;
import com.epam.rd.november2019.pojo.EmployeeSchedule;
import com.epam.rd.november2019.pojo.Flight;
import com.epam.rd.november2019.pojo.FlightTeam;
import com.epam.rd.november2019.services.DispatcherService;
import com.epam.rd.november2019.web.dto.EmployeeViewDto;

import java.util.LinkedList;
import java.util.List;

public class DispatcherServiceImpl implements DispatcherService {

    private final FlightDao flightDao;
    private final EmployeeDao employeeDao;
    private final EmployeeScheduleDao employeeScheduleDao;
    private final EmployeeConverter employeeConverter;

    public DispatcherServiceImpl() {
        flightDao = new FlightDaoImpl();
        employeeDao = new EmployeeDaoImpl();
        employeeScheduleDao = new EmployeeScheduleDaoImpl();
        employeeConverter = new EmployeeConverter();
    }

    @Override
    public FlightTeam createFlightTeam(List<Integer> employeeIds, int flightId) {
        Flight flight = flightDao.getById(flightId);
        for (Integer employeeId :
                employeeIds) {
            employeeScheduleDao.add(new EmployeeSchedule(
                    employeeId,
                    flightId,
                    flight.getDepartureDate(),
                    flight.getArrivalDate(),
                    flight.getDepartureCity(),
                    flight.getArrivalCity()
            ));
        }
        flight.setFlightTeam(employeeScheduleDao.getFlightTeam(flightId));
        return flight.getFlightTeam();
    }

    @Override
    public List<EmployeeViewDto> getFreeEmployees(int flightId) {
        List<EmployeeViewDto> employees = new LinkedList<>();
        List<Integer> freeEmployeeId = employeeScheduleDao.getFreeEmployee(flightDao.getById(flightId));
        for (Integer employeeId :
                freeEmployeeId) {
            Employee employee = employeeDao.getById(employeeId);
            employees.add(employeeConverter.asEmployeeDto(employee));
        }
        return employees;
    }
}
