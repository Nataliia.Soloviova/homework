package com.epam.rd.november2019.converter;

import com.epam.rd.november2019.pojo.Flight;
import com.epam.rd.november2019.web.dto.FlightCreateDto;
import com.epam.rd.november2019.web.dto.FlightViewDto;

public class FlightConverter {

    public Flight asFlight(FlightCreateDto dto) {
        Flight flight = new Flight();
        flight.setDepartureCity(dto.getDepartureCity());
        flight.setDepartureAirport(dto.getDepartureAirport());
        flight.setDepartureDate(dto.getDepartureDate());
        flight.setArrivalCity(dto.getArrivalCity());
        flight.setArrivalAirport(dto.getArrivalAirport());
        flight.setArrivalDate(dto.getArrivalDate());
        flight.setStatus(dto.getStatus());
        flight.setFlightTeam(dto.getFlightTeam());
        return flight;
    }

    public FlightViewDto asFlightDto(Flight flight) {
        FlightViewDto dto = new FlightViewDto();
        dto.setFlightId(flight.getFlightId());
        dto.setDepartureCity(flight.getDepartureCity());
        dto.setDepartureAirport(flight.getDepartureAirport());
        dto.setDepartureDate(flight.getDepartureDate());
        dto.setArrivalCity(flight.getArrivalCity());
        dto.setArrivalAirport(flight.getArrivalAirport());
        dto.setArrivalDate(flight.getArrivalDate());
        dto.setStatus(flight.getStatus());
        dto.setFlightTeam(flight.getFlightTeam());
        return dto;
    }
}
