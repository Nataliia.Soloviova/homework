package com.epam.rd.november2019.services.impl;

import com.epam.rd.november2019.pojo.UserAccount;
import com.epam.rd.november2019.services.SecurityService;

public class SecurityServiceImpl implements SecurityService {

    @Override
    public boolean isCorrectPassword(UserAccount user, String password) {
        return user.getPassword().equals(password);
    }
}
