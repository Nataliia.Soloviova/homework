package com.epam.rd.november2019.pojo;

public enum RequestStatus {
    COMPLETED, REJECTED;
}
