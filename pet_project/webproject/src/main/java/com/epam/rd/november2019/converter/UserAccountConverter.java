package com.epam.rd.november2019.converter;

import com.epam.rd.november2019.pojo.UserAccount;
import com.epam.rd.november2019.web.dto.UserAccountCreateDto;
import com.epam.rd.november2019.web.dto.UserAccountViewDto;

public class UserAccountConverter {

    public UserAccountViewDto asUserDto(UserAccount userAccount) {
        UserAccountViewDto userAccountViewDto = new UserAccountViewDto();
        userAccountViewDto.setId(userAccount.getId());
        userAccountViewDto.setEmail(userAccount.getEmail());
        userAccountViewDto.setUserName(userAccount.getUserName());
        userAccountViewDto.setUserRole(userAccount.getUserRole());
        return userAccountViewDto;
    }

    public UserAccount asUserAccount(UserAccountCreateDto accountCreateDto) {
        UserAccount userAccount = new UserAccount();
        userAccount.setEmail(accountCreateDto.getEmail());
        userAccount.setUserName(accountCreateDto.getUserName());
        userAccount.setPassword(accountCreateDto.getPassword());
        userAccount.setUserRole(accountCreateDto.getUserRole());
        return userAccount;
    }
}
