package com.epam.rd.november2019.web.servlet.flight;

import com.epam.rd.november2019.services.impl.AdminServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "deleteFlight", urlPatterns = "/deleteFlight")
public class RemoveFlightServlet extends HttpServlet {

    private AdminServiceImpl adminService = new AdminServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/findFlight.jsp").include(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int flightId = Integer.parseInt(req.getParameter("flightId"));
        if (adminService.removeFlight(flightId)) {
            req.setAttribute("deleteMessage", "The deleting was successful");
        } else {
            req.setAttribute("deleteMessage", "The deleting wasn't successful");
        }
        req.getRequestDispatcher("/findFlight.jsp").include(req, resp);
    }
}
