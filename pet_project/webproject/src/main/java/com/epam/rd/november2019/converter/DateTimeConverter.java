package com.epam.rd.november2019.converter;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTimeConverter {

    public Timestamp toTimestamp(String param) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        try {
            Date date = simpleDateFormat.parse(param);
            return new Timestamp(date.getTime());
        } catch (ParseException e) {
            throw new RuntimeException("Can not parse");
        }
    }
}
