package com.epam.rd.november2019.web.servlet.flight;

import com.epam.rd.november2019.services.impl.AdminServiceImpl;
import com.epam.rd.november2019.web.dto.FlightCreateDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "addFlight", urlPatterns = "/addFlight")
public class AddFlightServlet extends HttpServlet {

    private AdminServiceImpl adminService = new AdminServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/addFlight.jsp").include(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            FlightCreateDto createDto = ExtractObjectFromReq.extractFlight(req);
            adminService.addFlight(createDto);
            req.setAttribute("addingMessage", "Added new flight");
            req.getRequestDispatcher("/addFlight.jsp").forward(req, resp);
        } catch (IllegalArgumentException e) {
            req.setAttribute("addingMessage", e.getMessage());
            req.getRequestDispatcher("/addFlight.jsp").forward(req, resp);
        }
    }
}
