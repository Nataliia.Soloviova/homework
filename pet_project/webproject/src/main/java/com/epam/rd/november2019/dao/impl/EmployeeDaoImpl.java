package com.epam.rd.november2019.dao.impl;

import com.epam.rd.november2019.dao.ConnectionDB;
import com.epam.rd.november2019.dao.EmployeeDao;
import com.epam.rd.november2019.pojo.Employee;
import com.epam.rd.november2019.pojo.EmployeeRole;
import com.epam.rd.november2019.pojo.Sex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class EmployeeDaoImpl implements EmployeeDao {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeDaoImpl.class);

    private static final String ADD_EMPLOYEE = "INSERT INTO employees (lastName, firstName, employeeRole, sex, city) VALUES (?, ?, ?, ?, ?)";

    private static final String REMOVE_EMPLOYEE = "DELETE FROM employees WHERE employeeId = ?";

    private static final String GET_BY_ID = "SELECT * FROM employees WHERE employeeId = ?";

    private static final String GET_ID = "SELECT employeeId FROM employees WHERE firstName = ? AND employeeRole = ? AND lastName = ? AND sex = ? AND city = ?";

    private static final String GET_ALL = "SELECT * FROM employees";

    private static final StringBuilder FIND_EMPLOYEE = new StringBuilder("SELECT * FROM employees");

    private static final StringBuilder EDIT_EMPLOYEE = new StringBuilder("UPDATE employees SET ");

    @Override
    public synchronized Employee add(Employee employee) {
        logger.debug("Trying to add employee {} to employees table", employee);
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(ADD_EMPLOYEE, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, employee.getLastName());
            preparedStatement.setString(2, employee.getFirstName());
            preparedStatement.setString(3, employee.getEmployeeRole().toString());
            preparedStatement.setString(4, employee.getSex().toString());
            preparedStatement.setString(5, employee.getCity());
            preparedStatement.executeUpdate();

            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                employee.setEmployeeId(generatedKeys.getInt("employee_id"));
            }
        } catch (SQLException e) {
            logger.warn("Failed to insert employee into DB");
        }
        return employee;
    }

    @Override
    public synchronized boolean edit(int employeeId, String firstName, String lastName, EmployeeRole employeeRole, Sex sex, String city) {
        List<String> paramsList;
        Map<String, String> paramsMap = new LinkedHashMap<>();
        if (getById(employeeId).getEmployeeRole() != employeeRole) {
            paramsMap.put("employeeRole", employeeRole.toString());
        }
        if (getById(employeeId).getSex() != sex) {
            paramsMap.put("sex", sex.toString());
        }
        if (firstName.length() > 0) {
            paramsMap.put("firstName", firstName);
        }
        if (lastName.length() > 0) {
            paramsMap.put("lastName", lastName);
        }
        if (city.length() > 0) {
            paramsMap.put("city", city);
        }

        paramsList = paramsMap.keySet().stream()
                .map(i -> String.format("%s = ?", i))
                .collect(Collectors.toList());
        if (paramsList.size() > 0) {
            EDIT_EMPLOYEE.append(String.join(", ", paramsList)).append(" WHERE employeeId = ?");
        }
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(EDIT_EMPLOYEE.toString())) {
            int i = 1;
            for (Map.Entry<String, String> pair : paramsMap.entrySet()) {
                String value = pair.getValue();
                preparedStatement.setString(i, value);
                i++;
            }
            preparedStatement.setInt(i, employeeId);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    @Override
    public synchronized boolean remove(int employeeId) {
        logger.debug("Trying to remove employee with id {} from employees table", employeeId);
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(REMOVE_EMPLOYEE)) {
            preparedStatement.setInt(1, employeeId);
            preparedStatement.executeUpdate();
            logger.debug("Employee with id {} removed from employees table", employeeId);
            return true;
        } catch (SQLException e) {
            logger.warn("Failed to delete employee from DB");
            return false;
        }
    }

    @Override
    public synchronized Employee getById(int employeeId) {
        logger.debug("Trying to get employee by id: {}", employeeId);
        Employee employee = null;
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GET_BY_ID)) {
            preparedStatement.setInt(1, employeeId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                employee = new Employee(
                        resultSet.getInt("employeeId"),
                        resultSet.getString("lastName"),
                        resultSet.getString("firstName"),
                        EmployeeRole.valueOf(resultSet.getString("employeeRole")),
                        Sex.valueOf(resultSet.getString("sex")),
                        resultSet.getString("city")
                );
                logger.debug("Employee with id: {} is returned", employeeId);
            }
        } catch (SQLException e) {
            logger.warn("Employee with id {} doesn't exist", employeeId);
        }
        return employee;
    }

    @Override
    public synchronized List<Employee> getAll() {
        logger.debug("Trying to get all employees from table: employees");
        ArrayList<Employee> employees = new ArrayList<>();
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GET_ALL)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                employees.add(new Employee(
                        resultSet.getInt("employeeId"),
                        resultSet.getString("lastName"),
                        resultSet.getString("firstName"),
                        EmployeeRole.valueOf(resultSet.getString("employeeRole")),
                        Sex.valueOf(resultSet.getString("sex")),
                        resultSet.getString("city")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (employees.size() > 0) {
            logger.debug("Return {} employees after get all elements from table: employees", employees.size());
        } else {
            logger.warn("Return empty list after get all elements from table: employees");
        }
        return employees;
    }

    public synchronized int getId(Employee employee) {
        logger.debug("Trying to find employee from table: employees");
        int result = -1;
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GET_ID)) {
            preparedStatement.setString(1, employee.getFirstName());
            preparedStatement.setString(2, employee.getEmployeeRole().toString());
            preparedStatement.setString(3, employee.getLastName());
            preparedStatement.setString(4, employee.getSex().toString());
            preparedStatement.setString(5, employee.getCity());
            ResultSet resultSet = preparedStatement.executeQuery();
            logger.warn("Employee is found");
            if (resultSet.next()) {
                result = resultSet.getInt("employeeId");
            }
            logger.debug("Employee id is found: {}", result);
        } catch (SQLException e) {
            logger.warn("Employee {} doesn't have id", employee);
        }
        return result;
    }

    @Override
    public synchronized List<Employee> findEmployee(String firstName, String lastName, EmployeeRole employeeRole, Sex sex, String city) {
        List<String> paramsList;
        Map<String, String> paramsMap = new LinkedHashMap<>();

        paramsMap.put("employeeRole", employeeRole.toString());
        paramsMap.put("sex", sex.toString());

        if (firstName.length() > 0) {
            paramsMap.put("firstName", firstName);
        }
        if (lastName.length() > 0) {
            paramsMap.put("lastName", lastName);
        }
        if (city.length() > 0) {
            paramsMap.put("city", city);
        }
        paramsList = paramsMap.keySet().stream()
                .map(i -> String.format("%s = ?", i))
                .collect(Collectors.toList());
        if (paramsList.size() > 0) {
            FIND_EMPLOYEE.append(" WHERE ").append(String.join(" AND ", paramsList));
        }
        logger.debug("Trying to find employee from table: employees");
        ArrayList<Employee> employees = new ArrayList<>();
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(FIND_EMPLOYEE.toString())) {
            int i = 1;
            for (Map.Entry<String, String> pair : paramsMap.entrySet()) {
                String value = pair.getValue();
                preparedStatement.setString(i, value);
                i++;
            }
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                employees.add(new Employee(
                        resultSet.getInt("employeeId"),
                        resultSet.getString("lastName"),
                        resultSet.getString("firstName"),
                        EmployeeRole.valueOf(resultSet.getString("employeeRole")),
                        Sex.valueOf(resultSet.getString("sex")),
                        resultSet.getString("city")
                ));
            }
            logger.debug("Employees is found");
        } catch (SQLException e) {
            logger.warn("Any employee isn't found");
        }
        return employees;
    }
}
