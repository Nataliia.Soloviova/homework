package com.epam.rd.november2019.services.impl;

import com.epam.rd.november2019.converter.EmployeeConverter;
import com.epam.rd.november2019.converter.FlightConverter;
import com.epam.rd.november2019.dao.EmployeeDao;
import com.epam.rd.november2019.dao.EmployeeScheduleDao;
import com.epam.rd.november2019.dao.FlightDao;
import com.epam.rd.november2019.dao.impl.EmployeeDaoImpl;
import com.epam.rd.november2019.dao.impl.EmployeeScheduleDaoImpl;
import com.epam.rd.november2019.dao.impl.FlightDaoImpl;
import com.epam.rd.november2019.pojo.*;
import com.epam.rd.november2019.services.AdminService;
import com.epam.rd.november2019.web.dto.EmployeeCreateDto;
import com.epam.rd.november2019.web.dto.EmployeeViewDto;
import com.epam.rd.november2019.web.dto.FlightCreateDto;
import com.epam.rd.november2019.web.dto.FlightViewDto;

import java.util.LinkedList;
import java.util.List;

public class AdminServiceImpl implements AdminService {

    private final FlightDao flightDao;
    private final EmployeeDao employeeDao;
    private final EmployeeScheduleDao employeeScheduleDao;
    private final EmployeeConverter employeeConverter;
    private final FlightConverter flightConverter;

    public AdminServiceImpl() {
        flightDao = new FlightDaoImpl();
        employeeDao = new EmployeeDaoImpl();
        employeeScheduleDao = new EmployeeScheduleDaoImpl();
        employeeConverter = new EmployeeConverter();
        flightConverter = new FlightConverter();
    }

    @Override
    public EmployeeViewDto addEmployee(EmployeeCreateDto dto) {
        Employee employee = employeeConverter.asEmployee(dto);
        if (employeeDao.getId(employee) != -1) {
            throw new IllegalArgumentException("This employee already exist!");
        }
        employee = employeeDao.add(employee);
        employeeScheduleDao.add(new EmployeeSchedule(employee.getEmployeeId(), employee.getCity()));
        return employeeConverter.asEmployeeDto(employee);
    }

    @Override
    public List<EmployeeViewDto> getAllEmployees() {
        List<EmployeeViewDto> dtoList = new LinkedList<>();
        List<Employee> employees = employeeDao.getAll();
        for (Employee employee :
                employees) {
            dtoList.add(employeeConverter.asEmployeeDto(employee));
        }
        return dtoList;
    }

    @Override
    public boolean removeEmployee(int employeeId) {
        if (employeeDao.remove(employeeId)) {
            return employeeScheduleDao.removeByEmployeeId(employeeId);
        } else {
            return false;
        }
    }

    @Override
    public boolean editEmployee(int employeeId, String firstName, String lastName, EmployeeRole employeeRole, Sex sex, String city) {
        String oldStartLocation = employeeDao.getById(employeeId).getCity();
        if (employeeDao.edit(employeeId, firstName, lastName, employeeRole, sex, city)) {
            if (city != null) {
                return employeeScheduleDao.changeStartLocation(employeeId, city, oldStartLocation);
            }
            return true;
        }
        return false;
    }

    @Override
    public List<EmployeeViewDto> findEmployees(String firstName, String lastName, EmployeeRole employeeRole, Sex sex, String city) {
        List<EmployeeViewDto> dtoList = new LinkedList<>();
        List<Employee> employees = employeeDao.findEmployee(firstName, lastName, employeeRole, sex, city);
        for (Employee employee :
                employees) {
            dtoList.add(employeeConverter.asEmployeeDto(employee));
        }
        return dtoList;
    }

    @Override
    public FlightViewDto addFlight(FlightCreateDto dto) {
        Flight flight = flightConverter.asFlight(dto);
        if (flightDao.getId(flight) != -1) {
            throw new IllegalArgumentException("This flight already exist!");
        }
        flight = flightDao.add(flight);
        return flightConverter.asFlightDto(flight);
    }

    @Override
    public List<FlightViewDto> getAllFlights() {
        List<FlightViewDto> dtoList = new LinkedList<>();
        List<Flight> flights = flightDao.getAll();
        for (Flight flight :
                flights) {
            dtoList.add(flightConverter.asFlightDto(flight));
        }
        return dtoList;
    }

    @Override
    public boolean removeFlight(int flightId) {
        if (flightDao.remove(flightId)) {
            return employeeScheduleDao.removeByFlightId(flightId);
        } else {
            return false;
        }
    }

    @Override
    public List<FlightViewDto> findFlights(FlightCreateDto dto) {
        List<FlightViewDto> dtoList = new LinkedList<>();
        Flight searchFlight = flightConverter.asFlight(dto);
        List<Flight> flights = flightDao.findFlights(searchFlight);
        for (Flight flight :
                flights) {
            dtoList.add(flightConverter.asFlightDto(flight));
        }
        return dtoList;
    }

    @Override
    public boolean editFlight(int flightId, FlightCreateDto dto) {
        Flight newFlight = flightConverter.asFlight(dto);
        if (flightDao.edit(flightId, newFlight)) {
            return employeeScheduleDao.removeByFlightId(flightId);
        }
        return false;
    }

    @Override
    public boolean editFlight(int flightId, FlightStatus flightStatus) {
        return flightDao.edit(flightId, flightStatus);
    }
}
