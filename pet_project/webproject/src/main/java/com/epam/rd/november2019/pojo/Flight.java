package com.epam.rd.november2019.pojo;

import java.sql.Timestamp;
import java.util.Objects;

public class Flight {

    private int flightId;
    private String departureCity;
    private String departureAirport;
    private Timestamp departureDate;
    private String arrivalCity;
    private String arrivalAirport;
    private Timestamp arrivalDate;
    private FlightStatus status;
    private FlightTeam flightTeam;

    public Flight() {
    }

    public Flight(String departureCity, String departureAirport, Timestamp departureDate, String arrivalCity, String arrivalAirport, Timestamp arrivalDate, FlightStatus status) {
        this.departureCity = departureCity;
        this.departureAirport = departureAirport;
        this.departureDate = departureDate;
        this.arrivalCity = arrivalCity;
        this.arrivalAirport = arrivalAirport;
        this.arrivalDate = arrivalDate;
        this.status = status;
    }

    public Flight(int flightId, String departureCity, String departureAirport, Timestamp departureDate, String arrivalCity, String arrivalAirport, Timestamp arrivalDate, FlightStatus status) {
        this.flightId = flightId;
        this.departureCity = departureCity;
        this.departureAirport = departureAirport;
        this.departureDate = departureDate;
        this.arrivalCity = arrivalCity;
        this.arrivalAirport = arrivalAirport;
        this.arrivalDate = arrivalDate;
        this.status = status;
    }

    public String getDepartureAirport() {
        return departureAirport;
    }

    public void setDepartureAirport(String departureAirport) {
        this.departureAirport = departureAirport;
    }

    public String getArrivalAirport() {
        return arrivalAirport;
    }

    public void setArrivalAirport(String arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public String getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(String departureCity) {
        this.departureCity = departureCity;
    }

    public String getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public Timestamp getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Timestamp departureDate) {
        this.departureDate = departureDate;
    }

    public Timestamp getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Timestamp arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public FlightStatus getStatus() {
        return status;
    }

    public void setStatus(FlightStatus status) {
        this.status = status;
    }

    public FlightTeam getFlightTeam() {
        return flightTeam;
    }

    public void setFlightTeam(FlightTeam flightTeam) {
        this.flightTeam = flightTeam;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flight flight = (Flight) o;
        return flightId == flight.flightId &&
                Objects.equals(departureCity, flight.departureCity) &&
                Objects.equals(departureAirport, flight.departureAirport) &&
                Objects.equals(departureDate, flight.departureDate) &&
                Objects.equals(arrivalCity, flight.arrivalCity) &&
                Objects.equals(arrivalAirport, flight.arrivalAirport) &&
                Objects.equals(arrivalDate, flight.arrivalDate) &&
                status == flight.status &&
                Objects.equals(flightTeam, flight.flightTeam);
    }

    @Override
    public int hashCode() {
        return Objects.hash(flightId, departureCity, departureAirport, departureDate, arrivalCity, arrivalAirport, arrivalDate, status, flightTeam);
    }

    @Override
    public String toString() {
        return String.format("[%s - %s : %s : %s - %s : %s : %s]", flightId, departureDate, departureCity, departureAirport, arrivalCity, arrivalAirport, arrivalDate);
    }
}
