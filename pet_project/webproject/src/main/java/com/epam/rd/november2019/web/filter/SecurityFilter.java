package com.epam.rd.november2019.web.filter;

import com.epam.rd.november2019.config.SecurityConfig;
import com.epam.rd.november2019.pojo.UserRole;
import com.epam.rd.november2019.web.dto.UserAccountViewDto;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Set;

public class SecurityFilter implements Filter {

    private SecurityConfig securityConfig = new SecurityConfig();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String url = request.getRequestURL().toString();
        if (shouldBeProtected(url)) {
            HttpSession currentSession = request.getSession();
            if (currentSession == null || currentSession.getAttribute("user") == null) {
                response.sendRedirect(request.getContextPath() + "/login");
            } else if (shouldBeForAdmin(url)) {
                UserAccountViewDto dto = (UserAccountViewDto) currentSession.getAttribute("user");
                if (!dto.getUserRole().equals(UserRole.ADMINISTRATOR)) {
                    response.sendRedirect(request.getContextPath() + "/login-error.jsp");
                }
            } else if (shouldBeForDispatcher(url)) {
                UserAccountViewDto dto = (UserAccountViewDto) currentSession.getAttribute("user");
                if (!dto.getUserRole().equals(UserRole.DISPATCHER)) {
                    response.sendRedirect(request.getContextPath() + "/login-error.jsp");
                }
            }
        }

        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }

    private boolean shouldBeProtected(String url) {
        Set<String> protectedURLs = securityConfig.getProtectedURLs();
        return protectedURLs.stream().anyMatch(url::endsWith);
    }

    private boolean shouldBeForAdmin(String url) {
        Set<String> adminURLs = securityConfig.getAdminURLs();
        return adminURLs.stream().anyMatch(url::endsWith);
    }

    private boolean shouldBeForDispatcher(String url) {
        Set<String> dispatcherURLs = securityConfig.getDispatcherURLs();
        return dispatcherURLs.stream().anyMatch(url::endsWith);
    }
}
