package com.epam.rd.november2019.web.servlet.employee;

import com.epam.rd.november2019.pojo.EmployeeRole;
import com.epam.rd.november2019.pojo.Sex;
import com.epam.rd.november2019.services.AdminService;
import com.epam.rd.november2019.services.impl.AdminServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "editEmployee", urlPatterns = {"/editEmployee"})
public class EditEmployeeServlet extends HttpServlet {

    private AdminService adminService = new AdminServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/findEmployee.jsp").include(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int employeeId = Integer.parseInt(req.getParameter("employeeId"));
        String lastName = req.getParameter("lastName");
        String firstName = req.getParameter("firstName");
        EmployeeRole employeeRole = EmployeeRole.valueOf(req.getParameter("employeeRole").toUpperCase());
        Sex sex = Sex.valueOf(req.getParameter("sex").toUpperCase());
        String city = req.getParameter("city");
        if (adminService.editEmployee(employeeId, firstName, lastName, employeeRole, sex, city)) {
            req.setAttribute("editMessage", "The editing was successful");
        } else {
            req.setAttribute("editMessage", "The editing wasn't successful");
        }
        req.getRequestDispatcher("/findEmployee.jsp").include(req, resp);
    }
}
