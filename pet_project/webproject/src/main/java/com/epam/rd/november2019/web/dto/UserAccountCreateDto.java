package com.epam.rd.november2019.web.dto;

import com.epam.rd.november2019.pojo.UserRole;

import java.io.Serializable;

public class UserAccountCreateDto implements Serializable {

    private static final long serialVersionUID = 1108905749709775122L;

    private String userName;
    private String email;
    private String password;
    private UserRole userRole;

    public UserAccountCreateDto(String userName, String email, String password, UserRole userRole) {
        this.userName = userName;
        this.email = email;
        this.password = password;
        this.userRole = userRole;
    }

    public String getUserName() {
        return userName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    @Override
    public String toString() {
        return "UserAccountCreateDto{" +
                "userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", userRole=" + userRole +
                '}';
    }
}
