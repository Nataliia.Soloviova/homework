package com.epam.rd.november2019.valid;

import com.epam.rd.november2019.exception.ValidationException;
import com.epam.rd.november2019.web.dto.UserAccountCreateDto;

public interface UserValidator {

    void validateUserCredentials(String userName, String password) throws ValidationException;

    void validateNewUser(UserAccountCreateDto createDto) throws ValidationException;
}