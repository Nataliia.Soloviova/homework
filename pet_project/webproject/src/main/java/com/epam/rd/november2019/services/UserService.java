package com.epam.rd.november2019.services;

import com.epam.rd.november2019.web.dto.UserAccountCreateDto;
import com.epam.rd.november2019.web.dto.UserAccountViewDto;

public interface UserService {

    UserAccountViewDto login(String email, String password);

    UserAccountViewDto registerUser(UserAccountCreateDto createDto);
}