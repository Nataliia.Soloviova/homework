package com.epam.rd.november2019.web.servlet.flight;

import com.epam.rd.november2019.services.impl.AdminServiceImpl;
import com.epam.rd.november2019.web.dto.FlightCreateDto;
import com.epam.rd.november2019.web.dto.FlightViewDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "findFlight", urlPatterns = "/findFlight")
public class FindFlightServlet extends HttpServlet {

    private AdminServiceImpl adminService = new AdminServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/findFlight.jsp").include(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        FlightCreateDto createDto = ExtractObjectFromReq.extractFlight(req);
        List<FlightViewDto> flights = adminService.findFlights(createDto);
        req.setAttribute("flights", flights);
        req.getRequestDispatcher("/findFlight.jsp").forward(req, resp);
    }
}
