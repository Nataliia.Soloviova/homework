package com.epam.rd.november2019.web.servlet.flight;

import com.epam.rd.november2019.services.FlightService;
import com.epam.rd.november2019.services.impl.FlightServiceImpl;
import com.epam.rd.november2019.web.dto.FlightViewDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@WebServlet(name = "flights", urlPatterns = "/flights")
public class GetAllFlightsServlet extends HttpServlet {

    private FlightService flightService = new FlightServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<FlightViewDto> flights = flightService.getAllFlights();
        req.setAttribute("flights", flights);
        req.getRequestDispatcher("/flights.jsp").include(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
        String sortBy = req.getParameter("sortBy");
        List<FlightViewDto> flights = new LinkedList<>();
        if (sortBy.equals("Flight name")) {
            flights = flightService.sortByFlightName();
        } else if (sortBy.equals("Flight id")) {
            flights = flightService.sortById();
        } else {
            flights = flightService.getAllFlights();
        }
        req.setAttribute("flights", flights);
        req.getRequestDispatcher("/flights.jsp").forward(req, resp);
    }
}
