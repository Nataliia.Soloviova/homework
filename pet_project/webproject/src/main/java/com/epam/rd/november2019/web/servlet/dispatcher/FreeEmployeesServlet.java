package com.epam.rd.november2019.web.servlet.dispatcher;

import com.epam.rd.november2019.pojo.EmployeeRole;
import com.epam.rd.november2019.services.DispatcherService;
import com.epam.rd.november2019.services.impl.DispatcherServiceImpl;
import com.epam.rd.november2019.web.dto.EmployeeViewDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet(name = "freeEmployees", urlPatterns = "/freeEmployees")
public class FreeEmployeesServlet extends HttpServlet {

    private DispatcherService dispatcherService = new DispatcherServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("flightId", req.getParameter("flightId"));
        List<EmployeeViewDto> employees = new LinkedList<>();
        String employeeRole = req.getParameter("employeeRole");
        employees = dispatcherService.getFreeEmployees(Integer.parseInt(req.getParameter("flightId")));
        if (!employeeRole.equals("getAll")) {
            employees = employees.stream().filter(i -> i.getEmployeeRole().toString().equals(employeeRole)).collect(Collectors.toList());
        }
        req.setAttribute("employees", employees);
        req.getRequestDispatcher("/addFlightTeam.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
        List<EmployeeViewDto> employees = new LinkedList<>();
        EmployeeRole employeeRole = EmployeeRole.valueOf(req.getParameter("employeeRole"));
        employees = dispatcherService.getFreeEmployees(Integer.parseInt(req.getParameter("flightId")));
        if (employeeRole == EmployeeRole.PILOT || employeeRole == EmployeeRole.NAVIGATOR || employeeRole == EmployeeRole.RADIO_OPERATOR || employeeRole == EmployeeRole.STEWARDESS) {
            employees = employees.stream().filter(i -> i.getEmployeeRole() == employeeRole).collect(Collectors.toList());
        }
        req.setAttribute("employees", employees);
        req.getRequestDispatcher("/addFlightTeam.jsp").forward(req, resp);
    }
}
