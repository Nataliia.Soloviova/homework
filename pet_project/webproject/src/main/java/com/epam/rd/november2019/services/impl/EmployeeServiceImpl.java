package com.epam.rd.november2019.services.impl;

import com.epam.rd.november2019.converter.EmployeeConverter;
import com.epam.rd.november2019.dao.EmployeeDao;
import com.epam.rd.november2019.dao.EmployeeScheduleDao;
import com.epam.rd.november2019.dao.impl.EmployeeDaoImpl;
import com.epam.rd.november2019.dao.impl.EmployeeScheduleDaoImpl;
import com.epam.rd.november2019.pojo.Employee;
import com.epam.rd.november2019.pojo.EmployeeRole;
import com.epam.rd.november2019.pojo.EmployeeSchedule;
import com.epam.rd.november2019.pojo.Sex;
import com.epam.rd.november2019.services.EmployeeService;
import com.epam.rd.november2019.web.dto.EmployeeCreateDto;
import com.epam.rd.november2019.web.dto.EmployeeViewDto;

import java.util.LinkedList;
import java.util.List;

public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeDao employeeDao;
    private final EmployeeScheduleDao employeeScheduleDao;
    private final EmployeeConverter employeeConverter;

    public EmployeeServiceImpl() {
        employeeScheduleDao = new EmployeeScheduleDaoImpl();
        employeeConverter = new EmployeeConverter();
        employeeDao = new EmployeeDaoImpl();
    }

    @Override
    public List<EmployeeViewDto> getAllEmployees() {
        List<EmployeeViewDto> dtoList = new LinkedList<>();
        List<Employee> employees = employeeDao.getAll();
        for (Employee employee :
                employees) {
            dtoList.add(employeeConverter.asEmployeeDto(employee));
        }
        return dtoList;
    }

    @Override
    public EmployeeViewDto addEmployee(EmployeeCreateDto dto) {
        Employee employee = employeeConverter.asEmployee(dto);
        if (employeeDao.getId(employee) != -1) {
            throw new IllegalArgumentException("This employee already exist!");
        }
        employee = employeeDao.add(employee);
        employeeScheduleDao.add(new EmployeeSchedule(employee.getEmployeeId(), employee.getCity()));

        return employeeConverter.asEmployeeDto(employee);
    }

    @Override
    public boolean removeEmployee(int employeeId) {
        if (employeeDao.remove(employeeId)) {
            return employeeScheduleDao.removeByEmployeeId(employeeId);
        } else {
            return false;
        }
    }

    @Override
    public boolean editEmployee(int employeeId, String firstName, String lastName, EmployeeRole employeeRole, Sex sex, String city) {
        String oldStartLocation = employeeDao.getById(employeeId).getCity();
        if (employeeDao.edit(employeeId, firstName, lastName, employeeRole, sex, city)) {
            if (city != null) {
                return employeeScheduleDao.changeStartLocation(employeeId, city, oldStartLocation);
            }
            return true;
        }
        return false;
    }
}
