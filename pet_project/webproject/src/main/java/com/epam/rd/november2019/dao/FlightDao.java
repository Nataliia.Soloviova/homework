package com.epam.rd.november2019.dao;

import com.epam.rd.november2019.pojo.Flight;
import com.epam.rd.november2019.pojo.FlightStatus;

import java.util.List;

public interface FlightDao {

    Flight add(Flight flight);

    int getId(Flight flight);

    List<Flight> getAll();

    boolean remove(int flightId);

    List<Flight> findFlights(Flight flight);

    Flight getById(int flightId);

    boolean edit(int flightId, Flight flight);

    boolean edit(int flightId, FlightStatus flightStatus);

    List<Flight> sortByFlightName();

    List<Flight> sortById();
}
