package com.epam.rd.november2019.services;

import com.epam.rd.november2019.pojo.FlightTeam;
import com.epam.rd.november2019.web.dto.EmployeeViewDto;

import java.util.List;

public interface DispatcherService {

    FlightTeam createFlightTeam(List<Integer> employeeIds, int flightId);

    List<EmployeeViewDto> getFreeEmployees(int flightId);
}
