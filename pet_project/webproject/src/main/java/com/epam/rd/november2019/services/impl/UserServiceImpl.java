package com.epam.rd.november2019.services.impl;


import com.epam.rd.november2019.converter.UserAccountConverter;
import com.epam.rd.november2019.dao.UserDao;
import com.epam.rd.november2019.dao.impl.UserDaoImpl;
import com.epam.rd.november2019.exception.ValidationException;
import com.epam.rd.november2019.pojo.UserAccount;
import com.epam.rd.november2019.services.SecurityService;
import com.epam.rd.november2019.services.UserService;
import com.epam.rd.november2019.valid.UserValidator;
import com.epam.rd.november2019.valid.UserValidatorImpl;
import com.epam.rd.november2019.web.dto.UserAccountCreateDto;
import com.epam.rd.november2019.web.dto.UserAccountViewDto;

public class UserServiceImpl implements UserService {

    private final UserDao appUserDao;
    private final UserValidator userValidator;
    private final UserAccountConverter userAccountConverter;
    private final SecurityService securityService;

    public UserServiceImpl() {
        appUserDao = new UserDaoImpl();
        userValidator = new UserValidatorImpl();
        userAccountConverter = new UserAccountConverter();
        securityService = new SecurityServiceImpl();
    }

    @Override
    public UserAccountViewDto login(String userName, String password) throws ValidationException {
        userValidator.validateUserCredentials(userName, password);
        UserAccount userAccount = appUserDao.getByUserName(userName);
        if (userAccount == null) {
            throw new ValidationException("User with this login doesn't exist!");
        }
        if (!securityService.isCorrectPassword(userAccount, password)) {
            throw new ValidationException("Wrong password! Try again!");
        }
        return userAccountConverter.asUserDto(userAccount);
    }

    @Override
    public UserAccountViewDto registerUser(UserAccountCreateDto createDto) throws ValidationException {
        userValidator.validateNewUser(createDto);
        UserAccount userAccount = userAccountConverter.asUserAccount(createDto);
        if (appUserDao.getByUserName(createDto.getUserName()) != null || appUserDao.getByEmail(createDto.getEmail()) != null) {
            throw new IllegalArgumentException("This user already exist! Please login!");
        }
        userAccount = appUserDao.add(userAccount);
        return userAccountConverter.asUserDto(userAccount);
    }
}
