package com.epam.rd.november2019.services;

import com.epam.rd.november2019.pojo.EmployeeRole;
import com.epam.rd.november2019.pojo.FlightStatus;
import com.epam.rd.november2019.pojo.Sex;
import com.epam.rd.november2019.web.dto.EmployeeCreateDto;
import com.epam.rd.november2019.web.dto.EmployeeViewDto;
import com.epam.rd.november2019.web.dto.FlightCreateDto;
import com.epam.rd.november2019.web.dto.FlightViewDto;

import java.util.List;

public interface AdminService {

    List<EmployeeViewDto> getAllEmployees();

    EmployeeViewDto addEmployee(EmployeeCreateDto dto);

    boolean removeEmployee(int employeeId);

    boolean editEmployee(int employeeId, String firstName, String lastName, EmployeeRole employeeRole, Sex sex, String city);

    List<EmployeeViewDto> findEmployees(String firstName, String lastName, EmployeeRole employeeRole, Sex sex, String city);

    FlightViewDto addFlight(FlightCreateDto dto);

    List<FlightViewDto> getAllFlights();

    boolean removeFlight(int flightId);

    List<FlightViewDto> findFlights(FlightCreateDto dto);

    boolean editFlight(int flightId, FlightCreateDto dto);

    boolean editFlight(int flightId, FlightStatus flightStatus);
}
