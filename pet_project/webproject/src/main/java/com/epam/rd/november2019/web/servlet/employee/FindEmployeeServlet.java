package com.epam.rd.november2019.web.servlet.employee;

import com.epam.rd.november2019.pojo.EmployeeRole;
import com.epam.rd.november2019.pojo.Sex;
import com.epam.rd.november2019.services.AdminService;
import com.epam.rd.november2019.services.impl.AdminServiceImpl;
import com.epam.rd.november2019.web.dto.EmployeeViewDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "findEmployee", urlPatterns = {"/findEmployee"})
public class FindEmployeeServlet extends HttpServlet {

    private AdminService adminService = new AdminServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/findEmployee.jsp").include(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String lastName = req.getParameter("lastName");
        String firstName = req.getParameter("firstName");
        EmployeeRole employeeRole = EmployeeRole.valueOf(req.getParameter("employeeRole").toUpperCase());
        Sex sex = Sex.valueOf(req.getParameter("sex").toUpperCase());
        String city = req.getParameter("city");

        List<EmployeeViewDto> employees = adminService.findEmployees(firstName, lastName, employeeRole, sex, city);
        req.setAttribute("employees", employees);
        req.getRequestDispatcher("/findEmployee.jsp").include(req, resp);
    }
}
