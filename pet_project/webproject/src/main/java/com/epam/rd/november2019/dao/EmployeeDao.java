package com.epam.rd.november2019.dao;

import com.epam.rd.november2019.pojo.Employee;
import com.epam.rd.november2019.pojo.EmployeeRole;
import com.epam.rd.november2019.pojo.Sex;

import java.util.List;

public interface EmployeeDao {

    Employee add(Employee employee);

    boolean edit(int employeeId, String firstName, String lastName, EmployeeRole employeeRole, Sex sex, String city);

    boolean remove(int employeeId);

    Employee getById(int employeeId);

    List<Employee> getAll();

    int getId(Employee employee);

    List<Employee> findEmployee(String firstName, String lastName, EmployeeRole employeeRole, Sex sex, String city);
}
