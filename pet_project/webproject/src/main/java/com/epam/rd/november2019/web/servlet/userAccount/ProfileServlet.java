package com.epam.rd.november2019.web.servlet.userAccount;

import com.epam.rd.november2019.dao.impl.UserDaoImpl;
import com.epam.rd.november2019.exception.ValidationException;
import com.epam.rd.november2019.pojo.UserAccount;
import com.epam.rd.november2019.web.dto.UserAccountViewDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "profile", urlPatterns = "/profile")
public class ProfileServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/profile.jsp").include(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String newPassword = req.getParameter("newPassword");
        String oldPassword1 = req.getParameter("oldPassword_1");
        String oldPassword2 = req.getParameter("oldPassword_2");

        HttpSession session = req.getSession(true);
        UserAccountViewDto dto = (UserAccountViewDto) session.getAttribute("user");

        try {
            UserDaoImpl userDao = new UserDaoImpl();
            UserAccount user = userDao.getById(dto.getId());
            checkOldPasswords(user.getPassword(), oldPassword1, oldPassword2);
            user.setPassword(newPassword);
            userDao.edit(user);
            req.setAttribute("confirmChangePassMessage", "Successful changing password");
            session.invalidate();
            req.getRequestDispatcher("/login.jsp").forward(req, resp);
        } catch (ValidationException e) {
            req.setAttribute("errorChangePassMessage", e.getMessage());
            req.getRequestDispatcher("/profile.jsp").forward(req, resp);
        }
    }

    private void checkOldPasswords(String oldPass, String pass1, String pass2) {
        if (!pass1.equals(pass2) || !oldPass.equals(pass1)) {
            throw new ValidationException("Wrong old password! Try again!");
        }
    }
}
