package com.epam.rd.november2019.dao.impl;

import com.epam.rd.november2019.dao.ConnectionDB;
import com.epam.rd.november2019.dao.UserDao;
import com.epam.rd.november2019.pojo.UserAccount;
import com.epam.rd.november2019.pojo.UserRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class UserDaoImpl implements UserDao {

    private static final Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);

    private static final String ADD_USER = "INSERT INTO users (user_name, user_email, user_password, user_role) VALUES (?, ?, ?, ?)";

    private static final String EDIT_USER = "UPDATE users SET user_name = ?, user_email = ?, user_password = ?, user_role = ? WHERE user_id = ?";

    private static final String GET_BY_ID = "SELECT * FROM users WHERE user_id = ?";

    private static final String GET_BY_EMAIL = "SELECT * FROM users WHERE user_email = ?";

    private static final String GET_BY_NAME = "SELECT * FROM users WHERE user_name = ?";

    @Override
    public UserAccount add(UserAccount userAccount) {
        logger.debug("Trying to add user account {} to users table", userAccount);
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(ADD_USER, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, userAccount.getUserName());
            preparedStatement.setString(2, userAccount.getEmail());
            preparedStatement.setString(3, userAccount.getPassword());
            preparedStatement.setString(4, userAccount.getUserRole().toString());
            preparedStatement.executeUpdate();

            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                userAccount.setId(generatedKeys.getInt("user_id"));
            }
            logger.warn("User {} was added into table users", userAccount);
        } catch (SQLException e) {
            e.printStackTrace();
            logger.warn("Failed to insert user into DB");
        }
        return userAccount;
    }

    @Override
    public boolean edit(UserAccount userAccount) {
        logger.debug("Trying to edit user account: {}", userAccount);
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(EDIT_USER)) {
            preparedStatement.setString(1, userAccount.getUserName());
            preparedStatement.setString(2, userAccount.getEmail());
            preparedStatement.setString(3, userAccount.getPassword());
            preparedStatement.setString(4, userAccount.getUserRole().toString());
            preparedStatement.setInt(5, userAccount.getId());
            preparedStatement.executeUpdate();
            logger.debug("Editing user account was successful");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            logger.warn("Can't edit user account {}. It doesn't exist", userAccount);
            return false;
        }
    }

    @Override
    public UserAccount getById(int userId) {
        logger.debug("Trying to get user account by id: {}", userId);
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GET_BY_ID)) {
            preparedStatement.setInt(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                logger.debug("Employee with id: {} is returned", userId);
                UserAccount userAccount = new UserAccount();
                userAccount.setId(userId);
                userAccount.setUserName(resultSet.getString("user_name"));
                userAccount.setEmail(resultSet.getString("user_email"));
                userAccount.setPassword(resultSet.getString("user_password"));
                userAccount.setUserRole(UserRole.valueOf(resultSet.getString("user_role")));
                return userAccount;
            } else {
                logger.warn("User account with id {} doesn't exist", userId);
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public UserAccount getByEmail(String email) {
        UserAccount userAccount = null;
        logger.debug("Trying to get user account by email: {}", email);
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GET_BY_EMAIL)) {
            preparedStatement.setString(1, email);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                logger.debug("Employee with id: {} is returned", email);
                userAccount = new UserAccount();
                userAccount.setId(resultSet.getInt("user_id"));
                userAccount.setUserName(resultSet.getString("user_name"));
                userAccount.setEmail(resultSet.getString("user_email"));
                userAccount.setPassword(resultSet.getString("user_password"));
                userAccount.setUserRole(UserRole.valueOf(resultSet.getString("user_role")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            logger.warn("Failed to load user from DB");
        }
        if (userAccount == null) {
            logger.warn("User not found by email: {}", email);
        }
        return userAccount;
    }

    @Override
    public UserAccount getByUserName(String userName) {
        UserAccount userAccount = null;
        logger.debug("Trying to get user account by name: {}", userName);
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GET_BY_NAME)) {
            preparedStatement.setString(1, userName);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                userAccount = new UserAccount();
                userAccount.setId(resultSet.getInt("user_id"));
                userAccount.setUserName(resultSet.getString("user_name"));
                userAccount.setEmail(resultSet.getString("user_email"));
                userAccount.setPassword(resultSet.getString("user_password"));
                userAccount.setUserRole(UserRole.valueOf(resultSet.getString("user_role")));
                logger.debug("Employee with id: {} is returned", userName);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            logger.warn("Failed to load user from DB");
        }
        if (userAccount == null) {
            logger.warn("User not found by user name: {}", userName);
        }
        return userAccount;
    }
}
