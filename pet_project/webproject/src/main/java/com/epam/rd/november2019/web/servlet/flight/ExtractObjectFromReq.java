package com.epam.rd.november2019.web.servlet.flight;

import com.epam.rd.november2019.converter.DateTimeConverter;
import com.epam.rd.november2019.pojo.FlightStatus;
import com.epam.rd.november2019.web.dto.FlightCreateDto;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;

public class ExtractObjectFromReq {

    private static DateTimeConverter timeConverter = new DateTimeConverter();

    public static FlightCreateDto extractFlight(HttpServletRequest req) {
        String departureCity = req.getParameter("departureCity");
        String departureAirport = req.getParameter("departureAirport");
        String arrivalCity = req.getParameter("arrivalCity");
        String arrivalAirport = req.getParameter("arrivalAirport");
        FlightStatus flightStatus = null;
        if (req.getParameter("flightStatus") != null) {
            flightStatus = FlightStatus.valueOf(req.getParameter("flightStatus").toUpperCase());
        }
        Timestamp departureDate = null;
        Timestamp arrivalDate = null;
        if (!req.getParameter("departureDate").isEmpty()) {
            departureDate = timeConverter.toTimestamp(req.getParameter("departureDate"));
        }
        if (!req.getParameter("arrivalDate").isEmpty()) {
            arrivalDate = timeConverter.toTimestamp(req.getParameter("arrivalDate"));
        }
        return new FlightCreateDto(departureAirport, arrivalAirport, departureCity, arrivalCity, departureDate, arrivalDate, flightStatus);
    }

    public static FlightCreateDto extractOnlyStatus(HttpServletRequest req) {
        FlightStatus flightStatus = FlightStatus.valueOf(req.getParameter("flightStatus").toUpperCase());
        return new FlightCreateDto(flightStatus);
    }
}
