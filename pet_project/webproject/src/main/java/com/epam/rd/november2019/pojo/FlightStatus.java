package com.epam.rd.november2019.pojo;


public enum FlightStatus {

    PLANNED, // when flight is created
    FORMED, // when flight team is created
    ON_TIME, // registration will be open on time
    CHECK_IN, // registration has been started
    GATE_CLOSING, // registration will be closed in few minutes
    BOARDING, // boarding now
    CANCELLED, // flight is cancelled
    DELAYED, // flight is delayed
    DEPARTED; // flight already departed
}
