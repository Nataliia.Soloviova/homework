package com.epam.rd.november2019.config;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class SecurityConfig {

    private Set<String> protectedURLs = new HashSet<>();
    private Set<String> adminURLs = new HashSet<>();
    private Set<String> dispatcherURLs = new HashSet<>();

    public SecurityConfig() {
        adminURLs.add("/admin");
        adminURLs.add("/addFlight");
        adminURLs.add("/deleteFlight");
        adminURLs.add("/addEmployee");
        adminURLs.add("/editEmployee");
        adminURLs.add("/employees");

        dispatcherURLs.add("/dispatcher");
        dispatcherURLs.add("/freeEmployees");
        dispatcherURLs.add("/addFlightTeam");

        protectedURLs.add("/findFlight");
        protectedURLs.add("/editFlight");
        protectedURLs.add("/flights");
        protectedURLs.add("/findEmployee");
        protectedURLs.add("/deleteEmployee");
        protectedURLs.add("/profile");
        protectedURLs.addAll(adminURLs);
        protectedURLs.addAll(dispatcherURLs);
    }

    public Set<String> getProtectedURLs() {
        return Collections.unmodifiableSet(protectedURLs);
    }

    public Set<String> getAdminURLs() {
        return Collections.unmodifiableSet(adminURLs);
    }

    public Set<String> getDispatcherURLs() {
        return Collections.unmodifiableSet(dispatcherURLs);
    }
}
