package com.epam.rd.november2019.web.servlet.userAccount;

import com.epam.rd.november2019.exception.ValidationException;
import com.epam.rd.november2019.pojo.UserRole;
import com.epam.rd.november2019.services.impl.UserServiceImpl;
import com.epam.rd.november2019.web.dto.UserAccountCreateDto;
import com.epam.rd.november2019.web.dto.UserAccountViewDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "registrationPage", urlPatterns = "/registration")
public class RegistrationServlet extends HttpServlet {

    private UserServiceImpl userService = new UserServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/registration.jsp").include(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            UserAccountCreateDto accountCreateDto = extractUserFromRequest(req);
            UserAccountViewDto accountViewDto = userService.registerUser(accountCreateDto);

            HttpSession session = req.getSession(true);
            session.setAttribute("user", accountViewDto);
            req.getRequestDispatcher("/profile.jsp").include(req, resp);
        } catch (IllegalArgumentException e) {
            req.setAttribute("errorRegisterMessage", e.getMessage());
            req.getRequestDispatcher("login.jsp").forward(req, resp);
        } catch (ValidationException e) {
            req.setAttribute("errorRegisterMessage", e.getMessage());
            req.getRequestDispatcher("/registration.jsp").forward(req, resp);
        }
    }

    private UserAccountCreateDto extractUserFromRequest(HttpServletRequest req) {
        String userName = req.getParameter("loginName");
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        UserRole userRole = UserRole.valueOf(req.getParameter("userRole"));
        return new UserAccountCreateDto(userName, email, password, userRole);
    }
}
