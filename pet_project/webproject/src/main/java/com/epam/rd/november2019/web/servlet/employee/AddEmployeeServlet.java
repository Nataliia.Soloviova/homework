package com.epam.rd.november2019.web.servlet.employee;

import com.epam.rd.november2019.pojo.EmployeeRole;
import com.epam.rd.november2019.pojo.Sex;
import com.epam.rd.november2019.services.impl.AdminServiceImpl;
import com.epam.rd.november2019.web.dto.EmployeeCreateDto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "addEmployee", urlPatterns = {"/addEmployee"})
public class AddEmployeeServlet extends HttpServlet {

    private AdminServiceImpl adminService = new AdminServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/addEmployee.jsp").include(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            EmployeeCreateDto createDto = extractEmployeeFromRequest(req);
            adminService.addEmployee(createDto);
            req.setAttribute("addingMessage", "Added new employee");
            req.getRequestDispatcher("/addEmployee.jsp").forward(req, resp);
        } catch (IllegalArgumentException e) {
            req.setAttribute("addingMessage", e.getMessage());
            req.getRequestDispatcher("/addEmployee.jsp").forward(req, resp);
        }
    }

    private EmployeeCreateDto extractEmployeeFromRequest(HttpServletRequest req) {
        String lastName = req.getParameter("lastName");
        String firstName = req.getParameter("firstName");
        EmployeeRole employeeRole = EmployeeRole.valueOf(req.getParameter("employeeRole").toUpperCase());
        Sex sex = Sex.valueOf(req.getParameter("sex").toUpperCase());
        String city = req.getParameter("city");
        return new EmployeeCreateDto(lastName, firstName, employeeRole, sex, city);
    }
}
