<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${cookie['lang'].value}"/>
<fmt:setBundle basename="messages" />

<!DOCTYPE html>

<html lang="${cookie['lang'].value}">
    <head>
         <title>Add employee</title>

         <style>
            input, button, select {
                width: 300px;
                height: 30px;
            }
            hr {
                border: none;
                background-color: red;
                color: red;
                height: 2px;
               }
         </style>
    </head>

    <body>

    <c:if test="${sessionScope.user.userRole eq 'ADMINISTRATOR'}">

        <jsp:include page="admin_menu.jsp"></jsp:include>

        <hr>

        <h2>First, find flight:</h2>

        <div align="center">

            <form action="/findFlight" method="post">
                <div>
                    <p>
                        <input type="text" name="departureCity" width="30" placeholder="Departure city"/>
                        <input type="text" name="departureAirport" width="30" placeholder="Departure airport"/>
                        <input type="datetime-local" name="departureDate" width="30" placeholder="Departure date"/>
                        <input type="text" name="arrivalCity" width="30" placeholder="Arrival city"/>
                        <input type="text" name="arrivalAirport" width="30" placeholder="Arrival airport"/>
                        <input type="datetime-local" name="arrivalDate" width="30" placeholder="Arrival date"/>
                        <select name="flightStatus" width="50" required>
                            <option value="">Flight status</option>
                            <option value="planned">Planned</option>
                            <option value="formed">Formed</option>
                            <option value="on_time">On time</option>
                            <option value="check_in">Check in</option>
                            <option value="gate_closing">Gate closing</option>
                            <option value="boarding">Boarding</option>
                            <option value="cancelled">Cancelled</option>
                            <option value="delayed">Delayed</option>
                            <option value="departed">Departed</option>
                        </select>
                    </p>
                    <p>
                        <input type="submit" value="Find flights" />
                    </p>
                </div>
            </form>
        </div>

        <div align="center">
            <table border="1" cellpadding="5">
                <caption>
                    <h2>List of flights</h2>
                </caption>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Departure city</th>
                        <th>Departure airport</th>
                        <th>Departure date</th>
                        <th>Arrival city</th>
                        <th>Arrival airport</th>
                        <th>Arrival date</th>
                        <th>Status</th>
                    </tr>
                    </thead>

                    <tbody>
                    <c:forEach items="${flights}" var="flight">
                        <tr>
                            <td><c:out value="${flight.flightId}" /></td>
                            <td><c:out value="${flight.departureCity}" /></td>
                            <td><c:out value="${flight.departureAirport}" /></td>
                            <td><c:out value="${flight.departureDate}" /></td>
                            <td><c:out value="${flight.arrivalCity}" /></td>
                            <td><c:out value="${flight.arrivalAirport}" /></td>
                            <td><c:out value="${flight.arrivalDate}" /></td>
                            <td><c:out value="${flight.status}" /></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>

        <hr>

        <h2>Choose flight id and enter fields:</h2>

        <form action="/editFlight" method="post">
            <div align="center">
                <select name="flightId" required>
                    <option value="">Choose flight id</option>
                    <c:forEach items="${flights}" var="flight">
                        <option>
                            <c:out value="${flight.flightId}" />
                        </option>
                    </c:forEach>
                </select>

                <div>
                    <p>
                        <input type="text" name="departureCity" width="30" placeholder="Departure city"/>
                        <input type="text" name="departureAirport" width="30" placeholder="Departure airport"/>
                        <input type="datetime-local" name="departureDate" width="30" placeholder="Departure date"/>
                        <input type="text" name="arrivalCity" width="30" placeholder="Arrival city"/>
                        <input type="text" name="arrivalAirport" width="30" placeholder="Arrival airport"/>
                        <input type="datetime-local" name="arrivalDate" width="30" placeholder="Arrival date"/>

                    </p>
                    <p>
                        <input type="submit" value="Edit" />
                    </p>
                </div>
        </form>
        <p style="color:red">${requestScope.editMessage}</p>
        </div>

        <hr>

        <h2>Choose flight:</h2>

        <form action="/deleteFlight" method="post">
            <div align="center">
                <select name="flightId" required>
                    <option value="">Choose flight id</option>
                    <c:forEach items="${flights}" var="flight">
                        <option>
                            <c:out value="${flight.flightId}" />
                        </option>
                    </c:forEach>
                </select>
            </div>
            <div align="center">
                <input type="submit" value="Delete" />
                <p style="color:red">${requestScope.deleteMessage}</p>
            </div>
        </form>

    </c:if>


        <c:if test="${sessionScope.user.userRole eq 'DISPATCHER'}">
            <jsp:include page="dispatcher_menu.jsp"></jsp:include>

                <hr>

                <h2>First, find flight:</h2>

                <div align="center">

                    <form action="/findFlight" method="post">
                        <div>
                            <p>

                                <input type="text" name="departureCity" width="30" placeholder="Departure city"/>
                                <input type="text" name="departureAirport" width="30" placeholder="Departure airport"/>
                                <input type="datetime-local" name="departureDate" width="30" placeholder="Departure date"/>
                                <input type="text" name="arrivalCity" width="30" placeholder="Arrival city"/>
                                <input type="text" name="arrivalAirport" width="30" placeholder="Arrival airport"/>
                                <input type="datetime-local" name="arrivalDate" width="30" placeholder="Arrival date"/>
                                <select name="flightStatus" width="50" required>
                                    <option value="">Flight status</option>
                                    <option value="planned">Planned</option>
                                    <option value="formed">Formed</option>
                                    <option value="on_time">On time</option>
                                    <option value="check_in">Check in</option>
                                    <option value="gate_closing">Gate closing</option>
                                    <option value="boarding">Boarding</option>
                                    <option value="cancelled">Cancelled</option>
                                    <option value="delayed">Delayed</option>
                                    <option value="departed">Departed</option>
                                </select>
                            </p>
                            <p>
                                <input type="submit" value="Find flights" />
                            </p>
                        </div>
                    </form>
                </div>

                <div align="center">
                    <table border="1" cellpadding="5">
                        <caption>
                            <h2>List of flights</h2>
                        </caption>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Departure city</th>
                                <th>Departure airport</th>
                                <th>Departure date</th>
                                <th>Arrival city</th>
                                <th>Arrival airport</th>
                                <th>Arrival date</th>
                                <th>Status</th>
                            </tr>
                            </thead>

                            <tbody>
                            <c:forEach items="${flights}" var="flight">
                                <tr>
                                    <td><c:out value="${flight.flightId}" /></td>
                                    <td><c:out value="${flight.departureCity}" /></td>
                                    <td><c:out value="${flight.departureAirport}" /></td>
                                    <td><c:out value="${flight.departureDate}" /></td>
                                    <td><c:out value="${flight.arrivalCity}" /></td>
                                    <td><c:out value="${flight.arrivalAirport}" /></td>
                                    <td><c:out value="${flight.arrivalDate}" /></td>
                                    <td><c:out value="${flight.status}" /></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>

                <hr>

                        <h2>Choose flight id and enter all fields:</h2>

                        <form action="/editFlight" method="post">
                            <div align="center">
                                <select name="flightId" required>
                                    <option value="">Choose flight id</option>
                                    <c:forEach items="${flights}" var="flight">
                                        <option>
                                            <c:out value="${flight.flightId}" />
                                        </option>
                                    </c:forEach>
                                </select>

                                <div>
                                    <p>

                                        <select name="flightStatus" width="50" required>
                                            <option value="">Flight status</option>
                                            <option value="planned">Planned</option>
                                            <option value="formed">Formed</option>
                                            <option value="on_time">On time</option>
                                            <option value="check_in">Check in</option>
                                            <option value="gate_closing">Gate closing</option>
                                            <option value="boarding">Boarding</option>
                                            <option value="cancelled">Cancelled</option>
                                            <option value="delayed">Delayed</option>
                                            <option value="departed">Departed</option>
                                        </select>
                                    </p>
                                    <p>
                                        <input type="submit" value="Edit" />
                                    </p>
                                </div>
                        </form>
                        <p style="color:red">${requestScope.editMessage}</p>
                        </div>
        </c:if>


    </body>
</html>