<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
    <head>

        <title>AdministratorTask</title>

    </head>
    <body>

        <jsp:include page="menu.jsp"></jsp:include>

        <h1>This is dispatcher page!</h1>

        <h2>Choose action:</h2>

        <div>
            <ul>

                                <li>
                                    <a href="${pageContext.request.contextPath}/flights">
                                        Create flight team
                                    </a>
                                </li>

                                <li>
                                                                    <a href="${pageContext.request.contextPath}/editFlight">
                                                                        Edit flight status
                                                                    </a>
                                                                </li>
            </ul>
        </div>

    </body>
</html>