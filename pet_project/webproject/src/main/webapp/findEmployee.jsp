<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${cookie['lang'].value}"/>
<fmt:setBundle basename="messages" />

<!DOCTYPE html>

<html lang="${cookie['lang'].value}">
    <head>
         <title>Add employee</title>

         <style>
            input, button, select {
                width: 300px;
                height: 30px;
            }
            hr {
                border: none;
                background-color: red;
                color: red;
                height: 2px;
               }
         </style>
    </head>

    <body>

        <jsp:include page="admin_menu.jsp"></jsp:include>

        <hr>

        <h2>First, find employee:</h2>

            <div align="center">

                <form action="/findEmployee" method="post">
                    <div>
                        <p>
                            <input type="text" name="lastName" width="30" placeholder="Last name"/>
                            <input type="text" name="firstName" width="30" placeholder="First name"/>
                            <select name="employeeRole" width="50" required>
                                <option value="">Profession</option>
                                <option value="pilot">Pilot</option>
                                <option value="navigator">Navigator</option>
                                <option value="radio_operator">Radio operator</option>
                                <option value="stewardess">Stewardess</option>
                            </select>
                            <select name="sex" width="50" required>
                                <option value="">Sex</option>
                                <option value="man">Man</option>
                                <option value="woman">Woman</option>
                            </select>
                            <input type="text" name="city" width="30" placeholder="City"/>
                        </p>
                        <p>
                            <input type="submit" value="Find employees" />
                        </p>
                    </div>
                </form>
            </div>

            <div align="center">
                            <table border="1" cellpadding="5">
                                <caption><h2>List of employees</h2></caption>

                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Last name</th>
                                    <th>First name</th>
                                    <th>Profession</th>
                                    <th>Sex</th>
                                    <th>City</th>
                                </tr>
                                </thead>

                                <tbody>
                                <c:forEach items="${employees}" var="employee">
                                    <tr>
                                        <td><c:out value="${employee.employeeId}" /></td>
                                        <td><c:out value="${employee.lastName}" /></td>
                                        <td><c:out value="${employee.firstName}" /></td>
                                        <td><c:out value="${employee.employeeRole}" /></td>
                                        <td><c:out value="${employee.sex}" /></td>
                                        <td><c:out value="${employee.city}" /></td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                    </div>


            <hr>

            <h2>Choose employee id and enter fields:</h2>

                    <form action="/editEmployee" method="post">
                     <div align="center">
                                <select name="employeeId" required>
                                <option value="">Choose employee id</option>
                                <c:forEach items="${employees}" var="employee">
                                    <option><c:out value="${employee.employeeId}" /></option>
                                </c:forEach>
                                </select>

                                        <div>
                                            <p>
                                                <input type="text" name="lastName" width="30" placeholder="Last name"/>
                                                <input type="text" name="firstName" width="30" placeholder="First name"/>
                                                <select name="employeeRole" width="50" required>
                                                    <option value="">Profession</option>
                                                    <option value="pilot">Pilot</option>
                                                    <option value="navigator">Navigator</option>
                                                    <option value="radio_operator">Radio operator</option>
                                                    <option value="stewardess">Stewardess</option>
                                                </select>
                                                <select name="sex" width="50" required>
                                                    <option value="">Sex</option>
                                                    <option value="man">Man</option>
                                                    <option value="woman">Woman</option>
                                                </select>
                                                <input type="text" name="city" width="30" placeholder="City"/>
                                            </p>
                                            <p>
                                                <input type="submit" value="Edit" />
                                            </p>
                                        </div>
                                    </form>
                        <p style="color:red">${requestScope.editMessage}</p>
                    </div>

                    <hr >

                    <h2>Choose employee:</h2>

                             <form action="/deleteEmployee" method="post">
                             <div align="center">
                                                 <select name="employeeId" required>
                                                 <option value="">Choose employee id</option>
                                                 <c:forEach items="${employees}" var="employee">
                                                     <option><c:out value="${employee.employeeId}" /></option>
                                                 </c:forEach>
                                                 </select>
                                </div>
                                <div align="center">
                                <input type="submit" value="Delete" />
                                <p style="color:red">${requestScope.deleteMessage}</p>
                                </div>
                             </form>

    </body>
</html>