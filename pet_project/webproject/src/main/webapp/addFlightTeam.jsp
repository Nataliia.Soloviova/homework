<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${cookie['lang'].value}"/>
<fmt:setBundle basename="messages" />

<!DOCTYPE html>

<html lang="${cookie['lang'].value}">
    <head>
         <title>Employees</title>

         <style>
            input, button, select {
                width: 300px;
                height: 30px;
            }

            select {
                width: 200px;
                height: 30px;
            }

            .find {
                width: 100px;
                height: 30px;
            }

            .checkbox {
                width: 20px;
                height: 20px;
            }
         </style>
    </head>

    <body>

    <c:if test="${sessionScope.user.userRole != null}">

        <jsp:include page="dispatcher_menu.jsp"></jsp:include>

        <div align="center">
            <h1>Create flight team for flight ${flightId}</h1>
        </div>


        <form action="/freeEmployees" method="get">
        <div align="center">
            <label for="employeeRole">Profession:</label>
            <select name="employeeRole" required>
                <option value="getAll">Get all</option>
                <option value="PILOT">Pilot</option>
                <option value="NAVIGATOR">Navigator</option>
                <option value="RADIO_OPERATOR">Radio operator</option>
                <option value="STEWARDESS">Stewardess</option>
            </select>
            <p>
            <button type="submit" value="${flightId}" name="flightId" class="find">
                Find
            </button>
            </p>
        </div>
        </form>



        <div align="center">
        <form action="/addFlightTeam" method="post">

         <button type="submit" value="${flightId}" name="flightId">
            Add employee into team
         </button>

                <table border="1" cellpadding="5">
                                    <caption><h2>List of employees</h2></caption>

                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Last name</th>
                                        <th>First name</th>
                                        <th>Profession</th>
                                        <th>Sex</th>
                                        <th>City</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <c:forEach items="${employees}" var="employee">
                                        <tr>
                                            <td><input class="checkbox" type="checkbox" name="employeeId" value="${employee.employeeId}"/><c:out value="${employee.employeeId}" /></td>
                                            <td><c:out value="${employee.lastName}" /></td>
                                            <td><c:out value="${employee.firstName}" /></td>
                                            <td><c:out value="${employee.employeeRole}" /></td>
                                            <td><c:out value="${employee.sex}" /></td>
                                            <td><c:out value="${employee.city}" /></td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
            </form>
        </div>

</c:if>

    </body>
</html>