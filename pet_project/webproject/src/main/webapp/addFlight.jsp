<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${cookie['lang'].value}"/>
<fmt:setBundle basename="messages" />

<!DOCTYPE html>

<html lang="${cookie['lang'].value}">
    <head>
         <title>Add employee</title>

         <style>
            input, button, select {
                width: 300px;
                height: 30px;
            }
         </style>
    </head>

    <body>

        <jsp:include page="admin_menu.jsp"></jsp:include>

                <div>
                    <form action="/addFlight" method="post">
                    <p>
                        <input type="text" name="departureCity" width="30" placeholder="Departure city" required/>
                    </p>
                    <p>
                        <input type="text" name="departureAirport" width="30" placeholder="Departure airport" required/>
                    </p>
                    <p>
                        <input type="datetime-local" name="departureDate" width="30" placeholder="Departure date" required/>
                    </p>
                    <p>
                        <input type="text" name="arrivalCity" width="30" placeholder="Arrival city" required/>
                    </p>
                    <p>
                        <input type="text" name="arrivalAirport" width="30" placeholder="Arrival airport" required/>
                    </p>
                    <p>
                        <input type="datetime-local" name="arrivalDate" width="30" placeholder="Arrival date" required/>
                    </p>
                    <p>
                        <select name="flightStatus" width="50" required>
                            <option value="">Flight status</option>
                            <option value="planned">Planned</option>
                            <option value="formed">Formed</option>
                            <option value="on time">On time</option>
                            <option value="check in">Check in</option>
                            <option value="gate closing">Gate closing</option>
                            <option value="boarding">Boarding</option>
                            <option value="cancelled">Cancelled</option>
                            <option value="delayed">Delayed</option>
                            <option value="departed">Departed</option>
                        </select>
                    </p>

                    <p>
                        <input type="submit" value="Add flight" />
                    </p>
                    </form>

                    <p style="color:red">${requestScope.addingMessage}</p>

                </div>


    </body>
</html>