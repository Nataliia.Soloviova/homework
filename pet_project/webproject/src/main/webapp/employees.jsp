<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${cookie['lang'].value}"/>
<fmt:setBundle basename="messages" />

<!DOCTYPE html>

<html lang="${cookie['lang'].value}">
    <head>
         <title>Employees</title>

         <style>
            input, button {
                width: 300px;
                height: 30px;
            }
         </style>
    </head>

    <body>

        <jsp:include page="admin_menu.jsp"></jsp:include>

        <div align="center">
                <table border="1" cellpadding="5">
                    <caption><h2>List of employees</h2></caption>

                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Last name</th>
                        <th>First name</th>
                        <th>Profession</th>
                        <th>Sex</th>
                        <th>City</th>
                    </tr>
                    </thead>

                    <tbody>
                    <c:forEach items="${employees}" var="employee">
                        <tr>
                            <td><c:out value="${employee.employeeId}" /></td>
                            <td><c:out value="${employee.lastName}" /></td>
                            <td><c:out value="${employee.firstName}" /></td>
                            <td><c:out value="${employee.employeeRole}" /></td>
                            <td><c:out value="${employee.sex}" /></td>
                            <td><c:out value="${employee.city}" /></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
        </div>



    </body>
</html>