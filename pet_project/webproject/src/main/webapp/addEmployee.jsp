<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${cookie['lang'].value}"/>
<fmt:setBundle basename="messages" />

<!DOCTYPE html>

<html lang="${cookie['lang'].value}">
    <head>
         <title>Add employee</title>

         <style>
            input, button, select {
                width: 300px;
                height: 30px;
            }
         </style>
    </head>

    <body>

        <jsp:include page="admin_menu.jsp"></jsp:include>

                <div>
                    <form action="/addEmployee" method="post">
                    <p>
                        <input type="text" name="firstName" width="30" placeholder="First name" required/>
                    </p>
                    <p>
                        <input type="text" name="lastName" width="30" placeholder="Last name" required/>
                    </p>
                    <p>
                        <select name="employeeRole" width="50" required>
                            <option value="">Profession</option>
                            <option value="pilot">Pilot</option>
                            <option value="navigator">Navigator</option>
                            <option value="radio_operator">Radio operator</option>
                            <option value="stewardess">Stewardess</option>
                        </select>
                    </p>
                    <p>
                        <select name="sex" width="50" required>
                            <option value="">Sex</option>
                            <option value="man">Man</option>
                            <option value="woman">Woman</option>
                        </select>
                    </p>
                    <p>
                        <input type="text" name="city" width="30" placeholder="City" required/>
                    </p>
                    <p>
                        <input type="submit" value="Add employee" />
                    </p>
                    </form>

                    <p style="color:red">${requestScope.addingMessage}</p>

                </div>


    </body>
</html>