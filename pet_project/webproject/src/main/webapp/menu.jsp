<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
    <head>

        <title>DispatcherTask</title>

    </head>
    <body>

        <div>
            <a href="${pageContext.request.contextPath}/admin">
                Administrator Task
            </a>
            ||
            <a href="${pageContext.request.contextPath}/dispatcher">
                Dispatcher Task
            </a>
            ||
            <a href="${pageContext.request.contextPath}/profile">
                Profile
            </a>
            ||
            <a href="${pageContext.request.contextPath}/logout">
                Logout
            </a>
        </div>


    </body>
</html>