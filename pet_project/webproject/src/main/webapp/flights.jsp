<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${cookie['lang'].value}"/>
<fmt:setBundle basename="messages" />

<!DOCTYPE html>

<html lang="${cookie['lang'].value}">
    <head>
         <title>Employees</title>

         <style>
            input, button, select {
                width: 300px;
                height: 30px;
            }

            select {
                width: 200px;
                height: 30px;
            }

            .add, .edit, .delete {
                width: 100px;
                height: 30px;
            }

            .checkbox {
                width: 20px;
                height: 20px;
            }
         </style>
    </head>

    <body>

    <c:if test="${sessionScope.user.userRole eq 'DISPATCHER'}">

        <jsp:include page="dispatcher_menu.jsp"></jsp:include>

        <form action="/flights" method="post">
        <div align="center">
            <label for="sortBy">Sort by:</label>
            <select name="sortBy" required>
                <option value="Get all">Get all</option>
                <option value="Flight name">Flight name</option>
                <option value="Flight id">Flight id</option>
            </select>
            <p><input type="submit" value="Sort"></p>
        </div>
        </form>

        <div align="center">
            <form action="/addFlightTeam" method="get">

                <table border="1" cellpadding="5">
                    <caption><h2>List of flights</h2></caption>

                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Departure city</th>
                        <th>Departure airport</th>
                        <th>Departure date</th>
                        <th>Arrival city</th>
                        <th>Arrival airport</th>
                        <th>Arrival date</th>
                        <th>Status</th>
                        <th>Flight team</th>
                    </tr>
                    </thead>

                    <tbody>
                    <c:forEach items="${flights}" var="flight">
                        <tr>
                            <td><c:out value="${flight.flightId}" /></td>
                            <td><c:out value="${flight.departureCity}" /></td>
                            <td><c:out value="${flight.departureAirport}" /></td>
                            <td><c:out value="${flight.departureDate}" /></td>
                            <td><c:out value="${flight.arrivalCity}" /></td>
                            <td><c:out value="${flight.arrivalAirport}" /></td>
                            <td><c:out value="${flight.arrivalDate}" /></td>
                            <td><c:out value="${flight.status}" /></td>
                            <td>
                            <c:choose>
                                <c:when test="${not empty flight.flightTeam.employeeIds}">
                                    <c:out value="${flight.flightTeam}" />
                                </c:when>
                                <c:otherwise>
                                        <button type="submit" value="${flight.flightId}" name="flightId" class="add">
                                            Add team
                                        </button>
                                </c:otherwise>
                            </c:choose>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </form>

        </div>

    </c:if>

    <c:if test="${sessionScope.user.userRole eq 'ADMINISTRATOR'}">

         <jsp:include page="admin_menu.jsp"></jsp:include>

                <form action="/flights" method="post">
                <div align="center">
                    <label for="sortBy">Sort by:</label>
                    <select name="sortBy" required>
                        <option value="Get all">Get all</option>
                        <option value="Flight name">Flight name</option>
                        <option value="Flight id">Flight id</option>
                    </select>
                    <p><input type="submit" value="Sort"></p>
                </div>
                </form>

                <div align="center">


                        <table border="1" cellpadding="5">
                            <caption><h2>List of flights</h2></caption>

                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Departure city</th>
                                <th>Departure airport</th>
                                <th>Departure date</th>
                                <th>Arrival city</th>
                                <th>Arrival airport</th>
                                <th>Arrival date</th>
                                <th>Status</th>
                                <th>Flight team</th>

                            </tr>
                            </thead>

                            <tbody>
                            <c:forEach items="${flights}" var="flight">
                                <tr>
                                    <td><c:out value="${flight.flightId}" /></td>
                                    <td><c:out value="${flight.departureCity}" /></td>
                                    <td><c:out value="${flight.departureAirport}" /></td>
                                    <td><c:out value="${flight.departureDate}" /></td>
                                    <td><c:out value="${flight.arrivalCity}" /></td>
                                    <td><c:out value="${flight.arrivalAirport}" /></td>
                                    <td><c:out value="${flight.arrivalDate}" /></td>
                                    <td><c:out value="${flight.status}" /></td>
                                    <td>
                                    <c:choose>
                                        <c:when test="${not empty flight.flightTeam.employeeIds}">
                                            <c:out value="${flight.flightTeam}" />
                                        </c:when>
                                        <c:otherwise>

                                        </c:otherwise>
                                    </c:choose>
                                    </td>


                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>


                </div>


    </c:if>


    </body>
</html>