<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
    <head>

        <title>AdministratorTask</title>

    </head>
    <body>

        <jsp:include page="menu.jsp"></jsp:include>

        <h1>This is administrator page!</h1>

        <h2>Choose action:</h2>

        <div>
            <ul>
                <li>
                    <a href="${pageContext.request.contextPath}/addEmployee">
                        Add employee
                    </a>
                </li>
                <li>
                    <a href="${pageContext.request.contextPath}/employees">
                        All employees
                    </a>
                </li>
                <li>
                    <a href="${pageContext.request.contextPath}/editEmployee">
                        Edit employee
                    </a>
                </li>
                <li>
                                    <a href="${pageContext.request.contextPath}/addFlight">
                                        Add flight
                                    </a>
                                </li>
                                <li>
                                    <a href="${pageContext.request.contextPath}/flights">
                                        All flights
                                    </a>
                                </li>
                                <li>
                                    <a href="${pageContext.request.contextPath}/editFlight">
                                        Edit flight
                                    </a>
                                </li>
            </ul>
        </div>

    </body>
</html>