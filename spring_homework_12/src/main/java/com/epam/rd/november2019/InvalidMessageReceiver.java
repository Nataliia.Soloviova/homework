package com.epam.rd.november2019;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import static com.epam.rd.november2019.ActiveMQConfig.INVALID_MESSAGES;

@Component
public class InvalidMessageReceiver {

    private static final Logger LOGGER = LoggerFactory.getLogger(InvalidMessageReceiver.class);

    @JmsListener(destination = INVALID_MESSAGES)
    public void receive(@Payload String message) {
        LOGGER.warn(message);
    }
}
