package com.epam.rd.november2019;

import lombok.Data;

import java.io.Serializable;
import java.util.UUID;

@Data
public class Message implements Serializable {

    private static final long serialVersionUID = -8649210610521994353L;

    private UUID correlationId = UUID.randomUUID();
    private int firstNumber;
    private int secondNumber;
    private int result;

    public Message() {
    }

    @SuppressWarnings("all")
    public Message(int firstNumber, int secondNumber) {
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
    }
}
