package com.epam.rd.november2019;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import static com.epam.rd.november2019.ActiveMQConfig.REQUEST_CHANNEL;
import static com.epam.rd.november2019.ActiveMQConfig.RESPONSE_CHANNEL;
import static com.epam.rd.november2019.Application.MESSAGE_QUANTITY;

@Component
public class Responder {

    private static final Logger LOGGER = LoggerFactory.getLogger(Responder.class);
    private final JmsTemplate jmsTemplate;
    private Set<Message> messages;

    @Autowired
    public Responder(JmsTemplate jmsTemplate, Set<Message> messages) {
        this.jmsTemplate = jmsTemplate;
        this.messages = new HashSet<>();
    }

    @JmsListener(destination = REQUEST_CHANNEL)
    public void saveMessage(@Payload Message message) {
        messages.add(message);
        if (messages.size() == MESSAGE_QUANTITY) {
            LOGGER.info("Received {} messages", MESSAGE_QUANTITY);
            process();
        }
    }

    public void process() {
        int result = 404;
        for (Message message :
                this.messages) {
            int randomNumber = new Random().nextInt(3);
            if (randomNumber == 0) {
                result = Calculator.sum(message.getFirstNumber(), message.getSecondNumber());
            }
            if (randomNumber == 2) {
                message.setCorrelationId(UUID.randomUUID());
            }
            message.setResult(result);
            jmsTemplate.convertAndSend(RESPONSE_CHANNEL, message);
        }
    }
}
