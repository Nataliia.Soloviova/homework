package com.epam.rd.november2019;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Random;

@SpringBootApplication
public class Application implements ApplicationRunner {

    public static final int MESSAGE_QUANTITY = 10;

    @Autowired
    private Sender sender;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        for (int i = 0; i < MESSAGE_QUANTITY; i++) {
            int firstNumber = new Random().nextInt(20);
            int secondNumber = new Random().nextInt(20);
            sender.send(new Message(firstNumber, secondNumber));
        }
    }
}
