package com.epam.rd.november2019.dao.impl;

import com.epam.rd.november2019.dao.ConnectionDB;
import com.epam.rd.november2019.dao.EmployeeDao;
import com.epam.rd.november2019.pojo.Employee;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class that implements {@code EmployeeDao} interface.
 *
 * @see EmployeeDao
 * @see Employee
 */
public class EmployeeDaoImpl implements EmployeeDao {

    private static final String GET_ALL = "SELECT * FROM employees";

    private static final String GET_BY_ID =
            "SELECT * FROM employees WHERE id = ?";

    private static final String EDIT_EMPLOYEE = "UPDATE employees " +
            "SET employeeName = ?, employeeRole = ? " +
            "WHERE id = ?";

    private static final String ADD_EMPLOYEE = "INSERT INTO employees " +
            "(id, employeeName, employeeRole)" +
            "VALUES (?, ?, ?)";

    private static final String REMOVE_EMPLOYEE = "DELETE FROM employees " +
            "WHERE id = ?";

    private static final String CLEAR = "DELETE FROM employees";

    @Override
    public Employee getById(int id) throws SQLException, IOException {
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GET_BY_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return new Employee(resultSet.getInt("id"),
                        resultSet.getString("employeeName"),
                        resultSet.getString("employeeRole"));
            }
        }
        return null;
    }

    @Override
    public List<Employee> getAll() throws SQLException, IOException {
        ArrayList<Employee> employees = new ArrayList<Employee>();
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GET_ALL)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                employees.add(new Employee(resultSet.getInt("id"),
                        resultSet.getString("employeeName"),
                        resultSet.getString("employeeRole")));
            }
        }
        return employees;
    }

    @Override
    public boolean edit(Employee employee) throws IOException, SQLException {
        if (getById(employee.getId()) != null) {
            try (Connection conn = ConnectionDB.getConnection();
                 PreparedStatement preparedStatement = conn.prepareStatement(EDIT_EMPLOYEE)) {
                preparedStatement.setString(1, employee.getFirstName());
                preparedStatement.setString(2, employee.getEmployeeRole());
                preparedStatement.setInt(3, employee.getId());
                preparedStatement.executeUpdate();
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean edit(List<Employee> employees) throws IOException, SQLException {
        for (Employee employee :
                employees) {
            if (!edit(employee)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Employee add(Employee employee) throws SQLException, IOException {
        if (getById(employee.getId()) == null) {
            try (Connection conn = ConnectionDB.getConnection();
                 PreparedStatement preparedStatement = conn.prepareStatement(ADD_EMPLOYEE)) {
                preparedStatement.setInt(1, employee.getId());
                preparedStatement.setString(2, employee.getFirstName());
                preparedStatement.setString(3, employee.getEmployeeRole());
                preparedStatement.executeUpdate();
            }
        } else {
            throw new IllegalArgumentException("Employee with this ID already exists!");
        }
        return employee;
    }

    @Override
    public List<Employee> add(List<Employee> employees) throws SQLException, IOException {
        for (Employee employee :
                employees) {
            add(employee);
        }
        return employees;
    }

    @Override
    public Employee remove(Employee employee) throws IOException, SQLException {
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(REMOVE_EMPLOYEE)) {
            preparedStatement.setInt(1, employee.getId());
            preparedStatement.executeUpdate();
        }
        return employee;
    }

    @Override
    public List<Employee> remove(List<Employee> employees) throws IOException, SQLException {
        for (Employee employee :
                employees) {
            remove(employee);
        }
        return employees;
    }

    @Override
    public void clear() throws SQLException, IOException {
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(CLEAR)) {
            preparedStatement.executeUpdate();
        }
    }
}
