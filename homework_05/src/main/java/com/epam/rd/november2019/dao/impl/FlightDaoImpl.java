package com.epam.rd.november2019.dao.impl;

import com.epam.rd.november2019.dao.ConnectionDB;
import com.epam.rd.november2019.dao.FlightDao;
import com.epam.rd.november2019.pojo.Flight;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Class that implements {@code FlightDao} interface.
 *
 * @see FlightDao
 * @see Flight
 */
public class FlightDaoImpl implements FlightDao {

    private static final String GET_ALL = "SELECT * FROM flights";

    private static final String GET_BY_ID =
            "SELECT * FROM flights WHERE id = ?";

    private static final String EDIT_FLIGHT = "UPDATE flights " +
            "SET flightName = ?, departureCity = ?, arrivalCity = ?, departureDate = ?, arrivalDate = ? " +
            "WHERE id = ?";

    private static final String ADD_FLIGHT = "INSERT INTO flights " +
            "(id, flightName, departureCity, arrivalCity, departureDate, arrivalDate)" +
            "VALUES (?, ?, ?, ?, ?, ?)";

    private static final String REMOVE_FLIGHT = "DELETE FROM flights " +
            "WHERE id = ?";

    private static final String CLEAR = "DELETE FROM flights";

    @Override
    public List<Flight> getAll() throws SQLException, IOException {
        ArrayList<Flight> flights = new ArrayList<Flight>();
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GET_ALL)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                flights.add(new Flight(resultSet.getInt("id"),
                        resultSet.getString("departureCity"),
                        resultSet.getString("arrivalCity"),
                        resultSet.getString("departureDate"),
                        resultSet.getString("arrivalDate")));
            }
        }
        return flights;
    }

    @Override
    public Flight getById(int id) throws SQLException, IOException {
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GET_BY_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return new Flight(resultSet.getInt("id"),
                        resultSet.getString("departureCity"),
                        resultSet.getString("arrivalCity"),
                        resultSet.getString("departureDate"),
                        resultSet.getString("arrivalDate"));
            }
        }
        return null;
    }

    @Override
    public boolean edit(Flight flight) throws IOException, SQLException {
        if (getById(flight.getId()) != null) {
            try (Connection conn = ConnectionDB.getConnection();
                 PreparedStatement preparedStatement = conn.prepareStatement(EDIT_FLIGHT)) {
                preparedStatement.setString(1, flight.getFlightName());
                preparedStatement.setString(2, flight.getDepartureCity());
                preparedStatement.setString(3, flight.getArrivalCity());
                preparedStatement.setString(4, flight.getDepartureDate());
                preparedStatement.setString(5, flight.getArrivalDate());
                preparedStatement.setInt(6, flight.getId());
                preparedStatement.executeUpdate();
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean edit(List<Flight> flights) throws IOException, SQLException {
        for (Flight flight :
                flights) {
            if (!edit(flight)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Flight add(Flight flight) throws SQLException, IOException {
        if (getById(flight.getId()) == null) {
            try (Connection conn = ConnectionDB.getConnection();
                 PreparedStatement preparedStatement = conn.prepareStatement(ADD_FLIGHT)) {
                preparedStatement.setInt(1, flight.getId());
                preparedStatement.setString(2, flight.getFlightName());
                preparedStatement.setString(3, flight.getDepartureCity());
                preparedStatement.setString(4, flight.getArrivalCity());
                preparedStatement.setString(5, flight.getDepartureDate());
                preparedStatement.setString(6, flight.getArrivalDate());
                preparedStatement.executeUpdate();
            }
        } else {
            throw new IllegalArgumentException("Employee with this ID already exists!");
        }
        return flight;
    }

    @Override
    public List<Flight> add(List<Flight> flights) throws SQLException, IOException {
        for (Flight flight :
                flights) {
            add(flight);
        }
        return flights;
    }

    @Override
    public Flight remove(Flight flight) throws IOException, SQLException {
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(REMOVE_FLIGHT)) {
            preparedStatement.setInt(1, flight.getId());
            preparedStatement.executeUpdate();
        }
        return flight;
    }

    @Override
    public List<Flight> remove(List<Flight> flights) throws IOException, SQLException {
        for (Flight flight :
                flights) {
            remove(flight);
        }
        return flights;
    }

    @Override
    public void clear() throws SQLException, IOException {
        try (Connection conn = ConnectionDB.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(CLEAR)) {
            preparedStatement.executeUpdate();
        }
    }
}
