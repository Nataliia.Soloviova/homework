package com.epam.rd.november2019.dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

/**
 * This class has one method for connecting to database.
 *
 */
public class ConnectionDB {

    /**
     * Method that returns connection to database.
     *
     * @return connection to database
     * @throws SQLException
     * @throws IOException
     */
    public static Connection getConnection() throws SQLException, IOException {
        Properties props = new Properties();
        try (InputStream in =
                     ConnectionDB.class.getClassLoader().getResourceAsStream("db.properties")) {
            props.load(in);
        }
        String drivers = props.getProperty("db.driver");
        if (drivers != null) {
            System.setProperty("db.driver", drivers);
        }
        String url = props.getProperty("db.url");
        String username = props.getProperty("db.user");
        String password = props.getProperty("db.password");

        return DriverManager.getConnection(url, username, password);
    }

}
