package com.epam.rd.november2019.pojo;

public enum EmployeeRole {
    PILOT, NAVIGATOR, RADIO_OPERATOR, STEWARDESS;
}
