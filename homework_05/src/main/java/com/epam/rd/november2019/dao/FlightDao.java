package com.epam.rd.november2019.dao;

import com.epam.rd.november2019.dao.impl.FlightDaoImpl;
import com.epam.rd.november2019.pojo.Flight;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Interface that has methods for list of flights.
 *
 * @see FlightDaoImpl
 * @see Flight
 */
public interface FlightDao {

    /**
     * Method for finding flight with special number from this table.
     *
     * @param id special number of the flight.
     * @return flight with special number.
     * @throws SQLException
     * @throws IOException
     */
    Flight getById(int id) throws SQLException, IOException;

    /**
     * Method that returns list of all flights from this table.
     *
     * @return list of flights.
     * @throws SQLException
     * @throws IOException
     */
    List<Flight> getAll() throws SQLException, IOException;

    /**
     * Method for editing some flight from this table. Finding happens
     * by special number of flight.
     *
     * @param flight that has to edit.
     * @return true if editing was successful.
     * @throws IOException
     * @throws SQLException
     */
    boolean edit(Flight flight) throws IOException, SQLException;

    /**
     * Method for editing list of flights from this table. It works
     * like editing one flight.
     *
     * @param flights list that has to edit.
     * @return true if editing was successful.
     * @throws IOException
     * @throws SQLException
     */
    boolean edit(List<Flight> flights) throws IOException, SQLException;

    /**
     * Appends the flight to the end of this table.
     *
     * @param flight to be appended to this table.
     * @return flight that added to this table.
     * @throws SQLException
     * @throws IOException
     */
    Flight add(Flight flight) throws SQLException, IOException;

    /**
     * Appends the list of flights to the end of table.
     *
     * @param flights list to be appended to this table.
     * @return list of flights that added to this table.
     * @throws SQLException
     * @throws IOException
     */
    List<Flight> add(List<Flight> flights) throws SQLException, IOException;

    /**
     * Removes the flight at the specified position in table.
     *
     * @param flight to be removed
     * @return flight that removed from database.
     * @throws IOException
     * @throws SQLException
     */
    Flight remove(Flight flight) throws IOException, SQLException;

    /**
     * Removes the list of flights in this table.
     *
     * @param flights list to be removed
     * @return list of flights that removed from table.
     * @throws IOException
     * @throws SQLException
     */
    List<Flight> remove(List<Flight> flights) throws IOException, SQLException;

    /**
     * Method that clears this table in database completely.
     *
     * @throws SQLException
     * @throws IOException
     */
    void clear() throws SQLException, IOException;
}
