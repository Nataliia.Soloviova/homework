package com.epam.rd.november2019.pojo;

import java.util.Objects;

public class Employee {

    private int id;
    private String firstName;
    private String employeeRole;

    public Employee(int id, String firstName, String employeeRole) {
        this.firstName = firstName;
        this.employeeRole = employeeRole;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmployeeRole() {
        return employeeRole;
    }

    public void setEmployeeRole(String employeeRole) {
        this.employeeRole = employeeRole;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return id == employee.id &&
                Objects.equals(firstName, employee.firstName) &&
                employeeRole == employee.employeeRole;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, employeeRole);
    }

    @Override
    public String toString() {
        return String.format("[%s : %s : %s]", id, firstName, employeeRole);
    }
}
