package com.epam.rd.november2019.dao;

import com.epam.rd.november2019.dao.impl.EmployeeDaoImpl;
import com.epam.rd.november2019.pojo.Employee;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Interface that has methods for list of employees.
 *
 * @see EmployeeDaoImpl
 * @see Employee
 */
public interface EmployeeDao {

    /**
     * Method for finding employee with special number from database.
     *
     * @param id special number of the employee.
     * @return employee with special number.
     * @throws SQLException
     * @throws IOException
     */
    Employee getById(int id) throws SQLException, IOException;

    /**
     * Method that returns list of all employees from database.
     *
     * @return list of employees.
     * @throws SQLException
     * @throws IOException
     */
    List<Employee> getAll() throws SQLException, IOException;

    /**
     * Method for editing some employee from database. Finding happens
     * by special number of employee.
     *
     * @param employee that has to edit.
     * @return true if editing was successful.
     * @throws IOException
     * @throws SQLException
     */
    boolean edit(Employee employee) throws IOException, SQLException;

    /**
     * Method for editing list of employees from database. It works
     * like editing one employee.
     *
     * @param employees list that has to edit.
     * @return true if editing was successful.
     * @throws IOException
     * @throws SQLException
     */
    boolean edit(List<Employee> employees) throws IOException, SQLException;

    /**
     * Appends the employee to the end of database.
     *
     * @param employee to be appended to database.
     * @return employee that added to database.
     * @throws SQLException
     * @throws IOException
     * @throws IllegalArgumentException
     */
    Employee add(Employee employee) throws SQLException, IOException;

    /**
     * Appends the list of employees to the end of database.
     *
     * @param employees list to be appended to database.
     * @return list of employees that added to database.
     * @throws SQLException
     * @throws IOException
     * @throws IllegalArgumentException
     */
    List<Employee> add(List<Employee> employees) throws SQLException, IOException;

    /**
     * Removes the employee at the specified position in database.
     *
     * @param employee to be removed
     * @return employee that removed from database.
     * @throws IOException
     * @throws SQLException
     */
    Employee remove(Employee employee) throws IOException, SQLException;

    /**
     * Removes the list of employees in database.
     *
     * @param employees list to be removed
     * @return list of employees that removed from database.
     * @throws IOException
     * @throws SQLException
     */
    List<Employee> remove(List<Employee> employees) throws IOException, SQLException;

    /**
     * Method that clears database completely.
     *
     * @throws SQLException
     * @throws IOException
     */
    void clear() throws SQLException, IOException;
}
