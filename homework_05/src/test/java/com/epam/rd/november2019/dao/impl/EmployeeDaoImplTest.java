package com.epam.rd.november2019.dao.impl;

import com.epam.rd.november2019.pojo.Employee;
import com.epam.rd.november2019.pojo.EmployeeRole;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

class EmployeeDaoImplTest {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeDaoImplTest.class);

    @BeforeEach
    void setUp() throws SQLException, IOException {
        EmployeeDaoImpl employeeDao = new EmployeeDaoImpl();
        employeeDao.add(new Employee(1, "Jack", EmployeeRole.PILOT.toString()));
        employeeDao.add(new Employee(2, "John", EmployeeRole.PILOT.toString()));
        employeeDao.add(new Employee(3, "Tom", EmployeeRole.NAVIGATOR.toString()));
        employeeDao.add(new Employee(4, "Susan", EmployeeRole.RADIO_OPERATOR.toString()));
        employeeDao.add(new Employee(5, "Kate", EmployeeRole.STEWARDESS.toString()));
        employeeDao.add(new Employee(6, "Sam", EmployeeRole.STEWARDESS.toString()));
        employeeDao.add(new Employee(7, "Ann", EmployeeRole.STEWARDESS.toString()));
        employeeDao.add(new Employee(8, "Jeffry", EmployeeRole.PILOT.toString()));
        employeeDao.add(new Employee(9, "Mark", EmployeeRole.NAVIGATOR.toString()));
        employeeDao.add(new Employee(10, "Sean", EmployeeRole.RADIO_OPERATOR.toString()));
        logger.info("Added 10 employees to DataBase.");
    }

    @AfterEach
    void tearDown() throws IOException, SQLException {
        EmployeeDaoImpl employeeDao = new EmployeeDaoImpl();
        employeeDao.clear();
        logger.info("Deleted all employees in the DataBase.");
    }

    @Test
    void addTestShouldReturnAddedFlight() throws SQLException, IOException {
        EmployeeDaoImpl employeeDao = new EmployeeDaoImpl();
        Employee employee = new Employee(11, "Harry", EmployeeRole.PILOT.toString());
        logger.info("Added one employee: " + employeeDao.add(employee));
    }

    @Test
    void addTestShouldReturnIllegalArgumentException() throws SQLException, IOException {
        EmployeeDaoImpl employeeDao = new EmployeeDaoImpl();
        Employee employee = new Employee(5, "Harry", EmployeeRole.PILOT.toString());
        try {
            logger.info("Added one employee: " + employeeDao.add(employee));
        } catch (IllegalArgumentException ex) {
            logger.error("Tried to add employee with id that already exists: " + ex);
        }
    }

    @Test
    void addTestShouldReturnAddedFlightsList() throws SQLException, IOException {
        EmployeeDaoImpl employeeDao = new EmployeeDaoImpl();
        ArrayList<Employee> employees = new ArrayList<>();
        employees.add(new Employee(11, "Lena", EmployeeRole.STEWARDESS.toString()));
        employees.add(new Employee(12, "Natalie", EmployeeRole.STEWARDESS.toString()));
        employees.add(new Employee(13, "Jason", EmployeeRole.NAVIGATOR.toString()));
        employees.add(new Employee(14, "Richard", EmployeeRole.PILOT.toString()));
        employees.add(new Employee(15, "Peter", EmployeeRole.RADIO_OPERATOR.toString()));
        logger.info("Added list of employees: " + employeeDao.add(employees));
    }

    @Test
    void removeTestShouldReturnRemovedFlight() throws IOException, SQLException {
        EmployeeDaoImpl employeeDao = new EmployeeDaoImpl();
        Employee employee = new Employee(11, "Stephen", EmployeeRole.PILOT.toString());
        employeeDao.add(employee);
        logger.info("Added and removed one employee in the DataBase: " + employeeDao.remove(employee));
    }

    @Test
    void removeTestShouldReturnRemovedFlightsList() throws IOException, SQLException {
        EmployeeDaoImpl employeeDao = new EmployeeDaoImpl();
        ArrayList<Employee> employees = new ArrayList<>();
        employees.add(new Employee(11, "Joe", EmployeeRole.NAVIGATOR.toString()));
        employees.add(new Employee(12, "Jacob", EmployeeRole.RADIO_OPERATOR.toString()));
        employees.add(new Employee(13, "Sophie", EmployeeRole.STEWARDESS.toString()));
        employeeDao.add(employees);
        logger.info("Added and removed one list of employees: " + employeeDao.remove(employees));
    }

    @Test
    void getAllTestShouldReturnFlightsList() throws IOException, SQLException {
        EmployeeDaoImpl employeeDao = new EmployeeDaoImpl();
        logger.info("Got list of employees from DataBase: " + employeeDao.getAll());
    }

    @Test
    void getByIdTestShouldReturnFlight() throws IOException, SQLException {
        EmployeeDaoImpl employeeDao = new EmployeeDaoImpl();
        logger.info("Got one employee by ID: " + employeeDao.getById(1));
    }


    @Test
    void editTestShouldReturnTrueAfterEditFlight() throws IOException, SQLException {
        EmployeeDaoImpl employeeDao = new EmployeeDaoImpl();
        Employee employee = new Employee(5, "Rose", EmployeeRole.STEWARDESS.toString());
        logger.info("Edited employee with ID = 5: " + employeeDao.edit(employee));
    }

    @Test
    void editTestShouldReturnTrueAfterEditFlightsList() throws IOException, SQLException {
        EmployeeDaoImpl employeeDao = new EmployeeDaoImpl();
        ArrayList<Employee> employees = new ArrayList<>();
        employees.add(new Employee(7, "Michael", EmployeeRole.PILOT.toString()));
        employees.add(new Employee(3, "Hannah", EmployeeRole.STEWARDESS.toString()));
        employees.add(new Employee(8, "Charles", EmployeeRole.RADIO_OPERATOR.toString()));
        logger.info("Edited list of employees with ID = 3, 7, 8: " + employeeDao.edit(employees));
    }
}
