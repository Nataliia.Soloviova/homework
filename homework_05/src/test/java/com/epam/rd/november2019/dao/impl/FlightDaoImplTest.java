package com.epam.rd.november2019.dao.impl;

import com.epam.rd.november2019.pojo.Flight;
import org.junit.jupiter.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

class FlightDaoImplTest {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeDaoImplTest.class);

    @BeforeEach
    void setUp() throws SQLException, IOException {
        FlightDaoImpl flightDao = new FlightDaoImpl();
        flightDao.add(new Flight(1, "Dnipro", "Kyiv", "2019-12-20", "2019-12-20"));
        flightDao.add(new Flight(2, "Odessa", "Kyiv", "2019-12-29", "2019-12-29"));
        flightDao.add(new Flight(3, "Berlin", "Dresden", "2019-12-01", "2019-12-01"));
        flightDao.add(new Flight(4, "Warsaw", "Lviv", "2019-12-09", "2019-12-09"));
        flightDao.add(new Flight(5, "Dresden", "Warsaw", "2019-12-03", "2019-12-03"));
        flightDao.add(new Flight(6, "Dnipro", "Odessa", "2019-12-14", "2019-12-14"));
        flightDao.add(new Flight(7, "Dnipro", "Prague", "2019-12-01", "2019-12-01"));
        flightDao.add(new Flight(8, "Prague", "Vienna", "2019-12-12", "2019-12-12"));
        flightDao.add(new Flight(9, "Vienna", "London", "2019-12-26", "2019-12-26"));
        flightDao.add(new Flight(10, "Kyiv", "Berlin", "2019-12-15", "2019-12-15"));
    }

    @AfterEach
    void tearDown() throws IOException, SQLException {
        FlightDaoImpl flightDao = new FlightDaoImpl();
        flightDao.clear();
    }

    @Test
    void addTestShouldReturnAddedFlight() throws SQLException, IOException {
        FlightDaoImpl flightDao = new FlightDaoImpl();
        Flight flight = new Flight(11, "Dnipro", "Kyiv", "2019-12-31", "2019-12-31");
        logger.info("Added one flight: " + flightDao.add(flight));
    }

    @Test
    void addTestShouldReturnIllegalArgumentException() throws SQLException, IOException {
        FlightDaoImpl flightDao = new FlightDaoImpl();
        Flight employee = new Flight(5, "Dnipro", "Kyiv", "2019-12-31", "2019-12-31");
        try {
            logger.info("Added one flight: " + flightDao.add(employee));
        } catch (IllegalArgumentException ex) {
            logger.error("Tried to add flight with ID that already exists: " + ex);
        }
    }

    @Test
    void addTestShouldReturnAddedFlightsList() throws SQLException, IOException {
        FlightDaoImpl flightDao = new FlightDaoImpl();
        ArrayList<Flight> flights = new ArrayList<>();
        flights.add(new Flight(11, "Dnipro", "Odessa", "2019-12-14", "2019-12-14"));
        flights.add(new Flight(12, "Dnipro", "Prague", "2019-12-01", "2019-12-01"));
        flights.add(new Flight(13, "Prague", "Vienna", "2019-12-12", "2019-12-12"));
        flights.add(new Flight(14, "Vienna", "London", "2019-12-26", "2019-12-26"));
        flights.add(new Flight(15, "Kyiv", "Berlin", "2019-12-15", "2019-12-15"));
        logger.info("Added list of flights: " + flightDao.add(flights));
    }

    @Test
    void removeTestShouldReturnRemovedFlight() throws IOException, SQLException {
        FlightDaoImpl flightDao = new FlightDaoImpl();
        Flight flight = new Flight(11, "Dnipro", "London", "2019-12-19", "2019-12-19");
        flightDao.add(flight);
        logger.info("Added and removed one flight in the DataBase: " + flightDao.remove(flight));
    }

    @Test
    void removeTestShouldReturnRemovedFlightsList() throws IOException, SQLException {
        FlightDaoImpl flightDao = new FlightDaoImpl();
        ArrayList<Flight> flights = new ArrayList<>();
        flights.add(new Flight(11, "Dnipro", "Odessa", "2019-12-14", "2019-12-14"));
        flights.add(new Flight(12, "Dnipro", "Prague", "2019-12-01", "2019-12-01"));
        flights.add(new Flight(13, "Prague", "Vienna", "2019-12-12", "2019-12-12"));
        flightDao.add(flights);
        logger.info("Added and removed one list of flights: " + flightDao.remove(flights));
    }

    @Test
    void getAllTestShouldReturnFlightsList() throws IOException, SQLException {
        FlightDaoImpl flightDao = new FlightDaoImpl();
        logger.info("Got list of flights from DataBase: " + flightDao.getAll());
    }

    @Test
    void getByIdTestShouldReturnFlight() throws IOException, SQLException {
        FlightDaoImpl flightDao = new FlightDaoImpl();
        logger.info("Got one flight by ID: " + flightDao.getById(1));
    }


    @Test
    void editTestShouldReturnTrueAfterEditFlight() throws IOException, SQLException {
        FlightDaoImpl flightDao = new FlightDaoImpl();
        Flight flight = new Flight(5, "Kyiv", "Prague", "2019-12-05", "2019-12-05");
        logger.info("Edited flight with ID = 5: " + flightDao.edit(flight));
    }

    @Test
    void editTestShouldReturnTrueAfterEditFlightsList() throws IOException, SQLException {
        FlightDaoImpl flightDao = new FlightDaoImpl();
        ArrayList<Flight> flights = new ArrayList<>();
        flights.add(new Flight(7, "Dnipro", "Odessa", "2019-12-14", "2019-12-14"));
        flights.add(new Flight(3, "Dnipro", "Prague", "2019-12-01", "2019-12-01"));
        flights.add(new Flight(8, "Prague", "Vienna", "2019-12-12", "2019-12-12"));
        logger.info("Edited list of flights with ID = 3, 7, 8: " + flightDao.edit(flights));
    }
}
