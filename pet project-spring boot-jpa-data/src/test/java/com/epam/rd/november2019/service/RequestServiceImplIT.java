package com.epam.rd.november2019.service;

import com.epam.rd.november2019.SpringIntegrationTest;
import com.epam.rd.november2019.converter.DateTimeConverter;
import com.epam.rd.november2019.dto.RequestCreateDto;
import com.epam.rd.november2019.dto.RequestViewDto;
import com.epam.rd.november2019.entity.Flight;
import com.epam.rd.november2019.entity.Request;
import com.epam.rd.november2019.entity.User;
import com.epam.rd.november2019.entity.enums.UserRole;
import com.epam.rd.november2019.entity.enums.FlightStatus;
import com.epam.rd.november2019.repository.FlightRepository;
import com.epam.rd.november2019.repository.RequestRepository;
import com.epam.rd.november2019.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class RequestServiceImplIT extends SpringIntegrationTest {

    private static final String REQUESTER_COMMENT = "It's comment";
    private static final String REQUEST_DATE = "2020-02-01T12:00";

    private static final String DEPARTURE_CITY = "London";
    private static final String DEPARTURE_AIRPORT = "Laa";
    private static final String ARRIVAL_CITY = "Paris";
    private static final String ARRIVAL_AIRPORT = "Paa";
    private static final FlightStatus FLIGHT_STATUS = FlightStatus.BOARDING;
    private static final String DEPARTURE_DATE = "2020-02-20T12:00";
    private static final String ARRIVAL_DATE = "2020-02-20T15:00";

    private static final String USER_NAME = "John";
    private static final String USER_PASSWORD = "qwerty";
    private static final String USER_EMAIL = "john@corp.com";
    private static final UserRole USER_ROLE = UserRole.ROLE_ADMINISTRATOR;

    private final RequestRepository requestRepository;
    private final RequestService requestService;
    private final FlightRepository flightRepository;
    private final UserRepository userRepository;
    private final DateTimeConverter converter;

    private RequestCreateDto requestCreateDto;
    private Request request;

    @Autowired
    public RequestServiceImplIT(RequestRepository requestRepository,
                                RequestService requestService,
                                FlightRepository flightRepository,
                                UserRepository userRepository,
                                DateTimeConverter converter) {
        this.requestRepository = requestRepository;
        this.requestService = requestService;
        this.flightRepository = flightRepository;
        this.userRepository = userRepository;
        this.converter = converter;
    }

    @BeforeEach
    void setUp() {
        Flight flight = new Flight();
        flight.setDepartureCity(DEPARTURE_CITY);
        flight.setDepartureDate(converter.toTimestamp(DEPARTURE_DATE));
        flight.setDepartureAirport(DEPARTURE_AIRPORT);
        flight.setArrivalCity(ARRIVAL_CITY);
        flight.setArrivalDate(converter.toTimestamp(ARRIVAL_DATE));
        flight.setArrivalAirport(ARRIVAL_AIRPORT);
        flight.setStatus(FLIGHT_STATUS);
        flightRepository.save(flight);

        User user = new User();
        user.setUserName(USER_NAME);
        user.setEmail(USER_EMAIL);
        user.setPassword(USER_PASSWORD);
        user.setUserRole(USER_ROLE);
        userRepository.save(user);

        request = new Request();
        request.setRequestBy(user);
        request.setFlight(flight);
        request.setRequesterComment(REQUESTER_COMMENT);
        request.setRequestDate(converter.toTimestamp(REQUEST_DATE));

        requestCreateDto = new RequestCreateDto();
        requestCreateDto.setRequestBy(user.getEmail());
        requestCreateDto.setFlight(flight.getId());
        requestCreateDto.setRequesterComment(REQUESTER_COMMENT);
        requestCreateDto.setRequestDate(converter.toTimestamp(REQUEST_DATE));
    }

    @Test
    void testAddRequestShouldReturnRequest() {
        //Given

        //When
        RequestViewDto expected = requestService.addRequest(requestCreateDto);
        Request result = requestRepository.getOne(expected.getRequestId());

        //Then
        assertNotNull(expected.getRequestId());
        assertRequestsEquals(expected, result);
    }

    @Test
    public void testGetAllShouldReturnRequestList() {
        //Given
        List<Request> expected = requestRepository.findAll();

        //When
        List<RequestViewDto> result = requestService.getAllRequests();

        //Then
        assertEquals(result.size(), expected.size());
    }

    @Test
    void testEditRequestShouldUpdateRequest() {
        //Given
        requestRepository.save(request);
        String newApproverComment = "Well done!";

        requestCreateDto.setApproverComment(newApproverComment);
        requestCreateDto.setRequestId(request.getRequestId());

        //When
        requestService.editRequest(requestCreateDto);
        RequestViewDto result = requestService.getRequestById(request.getRequestId());

        //Then
        assertEquals(newApproverComment, result.getApproverComment());
    }

    @Test
    public void testGetByIdShouldReturnRequest() {
        //Given
        requestRepository.save(request);

        //When
        RequestViewDto result = requestService.getRequestById(request.getRequestId());

        //Then
        assertNotNull(result.getRequestId());
        assertRequestsEquals(result, request);
    }

    @Test
    public void testRemoveShouldThrowException() {
        //Given
        requestRepository.save(request);

        //When
        requestService.removeRequest(request.getRequestId());
        EmptyResultDataAccessException ex = assertThrows(EmptyResultDataAccessException.class, () -> {
            requestService.removeRequest(request.getRequestId());
        });

        //Then
    }

    @Test
    public void testGetByFlightIdShouldReturnRequest() {
        //Given
        requestRepository.save(request);
        int expected = 1;

        //When
        List<RequestViewDto> result = requestService.getRequestByFlightId(request.getFlight().getId());

        //Then
        assertNotNull(request.getFlight().getId());
        assertEquals(result.size(), expected);
    }

    private void assertRequestsEquals(RequestViewDto viewDto, Request request) {
        assertEquals(viewDto.getRequestId(), request.getRequestId());
        assertEquals(viewDto.getRequestBy(), request.getRequestBy().getEmail());
        assertEquals(viewDto.getFlight(), request.getFlight().getId());
        assertEquals(viewDto.getRequestDate(), converter.toViewString(request.getRequestDate()));
        assertEquals(viewDto.getRequesterComment(), request.getRequesterComment());
        assertEquals(viewDto.getRequestStatus(), request.getRequestStatus());
        assertEquals(viewDto.getApproverComment(), request.getApproverComment());
    }
}
