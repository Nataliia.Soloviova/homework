package com.epam.rd.november2019.service;

import com.epam.rd.november2019.SpringIntegrationTest;
import com.epam.rd.november2019.dto.EmployeeCreateDto;
import com.epam.rd.november2019.dto.EmployeeViewDto;
import com.epam.rd.november2019.entity.Employee;
import com.epam.rd.november2019.entity.enums.EmployeeRole;
import com.epam.rd.november2019.entity.enums.Sex;
import com.epam.rd.november2019.repository.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class EmployeeServiceImplIT extends SpringIntegrationTest {

    private static final String LAST_NAME = "Johnson";
    private static final String FIRST_NAME = "John";
    private static final EmployeeRole EMPLOYEE_ROLE = EmployeeRole.RADIO_OPERATOR;
    private static final Sex SEX = Sex.MAN;
    private static final String CITY = "New York";

    private final EmployeeService employeeService;
    private final EmployeeRepository employeeRepository;

    private Employee employee;
    private EmployeeCreateDto createDto;

    @Autowired
    public EmployeeServiceImplIT(EmployeeService employeeService,
                                 EmployeeRepository employeeRepository) {
        this.employeeService = employeeService;
        this.employeeRepository = employeeRepository;
    }

    @BeforeEach
    public void init() {
        employee = new Employee();
        employee.setFirstName(FIRST_NAME);
        employee.setLastName(LAST_NAME);
        employee.setSex(SEX);
        employee.setEmployeeRole(EMPLOYEE_ROLE);
        employee.setCity(CITY);

        createDto = new EmployeeCreateDto();
        createDto.setFirstName(FIRST_NAME);
        createDto.setLastName(LAST_NAME);
        createDto.setSex(SEX);
        createDto.setEmployeeRole(EMPLOYEE_ROLE);
        createDto.setCity(CITY);
    }

    @Test
    public void testAddEmployeeShouldReturnEmployee() {
        //Given

        //When
        EmployeeViewDto expected = employeeService.addEmployee(createDto);
        Employee result = employeeRepository.getOne(expected.getEmployeeId());

        //Then
        assertEmployeesEquals(result, expected);
    }

    @Test
    public void testGetById() {
        //Given
        employeeRepository.save(employee);

        //When
        EmployeeViewDto result = employeeService.getEmployeeById(employee.getEmployeeId());

        //Then
        assertEmployeesEquals(employee, result);
    }

    @Test
    public void testGetAll() {
        //Given
        List<Employee> expected = employeeRepository.findAll();

        //When
        List<EmployeeViewDto> result = employeeService.getAllEmployees();

        //Then
        assertEquals(result.size(), expected.size());
    }

    @Test
    public void testEditEmployee() {
        //Given
        employeeRepository.save(employee);

        String newFirstName = "Jack";
        createDto.setFirstName(newFirstName);
        createDto.setEmployeeId(employee.getEmployeeId());

        //When
        employeeService.editEmployee(createDto);
        EmployeeViewDto result = employeeService.getEmployeeById(employee.getEmployeeId());

        //Then
        assertEquals(newFirstName, result.getFirstName());
    }

    @Test
    void testRemoveEmployeeShouldThrowException() {
        //Given
        employeeRepository.save(employee);

        //When
        employeeService.removeEmployee(employee.getEmployeeId());
        EmptyResultDataAccessException ex = assertThrows(EmptyResultDataAccessException.class, () -> {
            employeeService.removeEmployee(employee.getEmployeeId());
        });

        //Then
    }

    private void assertEmployeesEquals(Employee employee, EmployeeViewDto viewDto) {
        assertEquals(employee.getLastName(), viewDto.getLastName());
        assertEquals(employee.getFirstName(), viewDto.getFirstName());
        assertEquals(employee.getEmployeeRole(), viewDto.getEmployeeRole());
        assertEquals(employee.getCity(), viewDto.getCity());
        assertEquals(employee.getSex(), viewDto.getSex());
        assertEquals(employee.getFlights().size(), viewDto.getFlights().size());
    }
}
