package com.epam.rd.november2019.service;

import com.epam.rd.november2019.SpringIntegrationTest;
import com.epam.rd.november2019.converter.DateTimeConverter;
import com.epam.rd.november2019.dto.FlightCreateDto;
import com.epam.rd.november2019.dto.FlightViewDto;
import com.epam.rd.november2019.entity.Flight;
import com.epam.rd.november2019.entity.enums.FlightStatus;
import com.epam.rd.november2019.repository.FlightRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class FlightServiceImplIT extends SpringIntegrationTest {

    private static final String DEPARTURE_CITY = "London";
    private static final String DEPARTURE_AIRPORT = "Laa";
    private static final String ARRIVAL_CITY = "Paris";
    private static final String ARRIVAL_AIRPORT = "Paa";
    private static final FlightStatus FLIGHT_STATUS = FlightStatus.BOARDING;
    private static final String DEPARTURE_DATE = "2020-02-20T12:00";
    private static final String ARRIVAL_DATE = "2020-02-20T15:00";

    private final FlightService flightService;
    private final FlightRepository flightRepository;
    private final DateTimeConverter converter;

    private Flight flight;
    private FlightCreateDto flightCreateDto;

    @Autowired
    public FlightServiceImplIT(FlightService flightService, FlightRepository flightRepository, DateTimeConverter converter) {
        this.flightService = flightService;
        this.flightRepository = flightRepository;
        this.converter = converter;
    }

    @BeforeEach
    public void init() {
        flight = new Flight();
        flight.setDepartureCity(DEPARTURE_CITY);
        flight.setDepartureDate(converter.toTimestamp(DEPARTURE_DATE));
        flight.setDepartureAirport(DEPARTURE_AIRPORT);
        flight.setArrivalCity(ARRIVAL_CITY);
        flight.setArrivalDate(converter.toTimestamp(ARRIVAL_DATE));
        flight.setArrivalAirport(ARRIVAL_AIRPORT);
        flight.setStatus(FLIGHT_STATUS);

        flightCreateDto = new FlightCreateDto();
        flightCreateDto.setDepartureCity(DEPARTURE_CITY);
        flightCreateDto.setDepartureDate(converter.toTimestamp(DEPARTURE_DATE));
        flightCreateDto.setDepartureAirport(DEPARTURE_AIRPORT);
        flightCreateDto.setArrivalCity(ARRIVAL_CITY);
        flightCreateDto.setArrivalDate(converter.toTimestamp(ARRIVAL_DATE));
        flightCreateDto.setArrivalAirport(ARRIVAL_AIRPORT);
        flightCreateDto.setStatus(FLIGHT_STATUS);
    }


    @Test
    public void testAddShouldCreateFlight() {
        //Given

        //When
        FlightViewDto expected = flightService.addFlight(flightCreateDto);
        Flight result = flightRepository.getOne(expected.getFlightId());

        //Then
        assertNotNull(expected.getFlightId());
        assertFlightsEquals(expected, result);
    }

    @Test
    public void testRemoveShouldThrowNullPointerException() {
        //Given
        flightRepository.save(flight);

        //When
        flightService.removeFlight(flight.getId());
        EmptyResultDataAccessException ex = assertThrows(EmptyResultDataAccessException.class, () -> {
            flightService.removeFlight(flight.getId());
        });

        //Then
    }

    @Test
    public void testGetByIdShouldReturnFlight() {
        //Given
        flightRepository.save(flight);

        //When
        FlightViewDto result = flightService.getFlightById(flight.getId());

        //Then
        assertNotNull(result.getFlightId());
        assertFlightsEquals(result, flight);
    }

    @Test
    public void testGetAllShouldReturnAllFlights() {
        //Given
        List<Flight> expected = flightRepository.findAll();

        //When
        List<FlightViewDto> result = flightService.getAllFlights();

        //Then
        assertEquals(result.size(), expected.size());
    }

    @Test
    public void testEditFlightShouldUpdateFlight() {
        //Given
        flightRepository.save(flight);

        String newDepartureCity = "Kyiv";
        flightCreateDto.setDepartureCity(newDepartureCity);
        flightCreateDto.setFlightId(flight.getId());

        //When
        flightService.editFlight(flightCreateDto);
        FlightViewDto result = flightService.getFlightById(flight.getId());

        //Then
        assertEquals(newDepartureCity, result.getDepartureCity());
    }

    private void assertFlightsEquals(FlightViewDto viewDto, Flight flight) {
        assertEquals(viewDto.getDepartureCity(), flight.getDepartureCity());
        assertEquals(viewDto.getDepartureAirport(), flight.getDepartureAirport());
        assertEquals(viewDto.getDepartureDate(), converter.toDateTimeString(flight.getDepartureDate()));
        assertEquals(viewDto.getArrivalCity(), flight.getArrivalCity());
        assertEquals(viewDto.getArrivalAirport(), flight.getArrivalAirport());
        assertEquals(viewDto.getArrivalDate(), converter.toDateTimeString(flight.getArrivalDate()));
        assertEquals(viewDto.getStatus(), flight.getStatus());
        assertEquals(viewDto.getRequests().size(), flight.getRequests().size());
        assertEquals(viewDto.getEmployees().size(), flight.getEmployees().size());
    }
}
