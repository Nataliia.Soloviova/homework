package com.epam.rd.november2019.service;


import com.epam.rd.november2019.SpringIntegrationTest;
import com.epam.rd.november2019.dto.UserCreateDto;
import com.epam.rd.november2019.dto.UserViewDto;
import com.epam.rd.november2019.entity.User;
import com.epam.rd.november2019.entity.enums.UserRole;
import com.epam.rd.november2019.exception.EntityAlreadyExistsException;
import com.epam.rd.november2019.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

public class UserServiceImplIT extends SpringIntegrationTest {

    private static final String USER_NAME = "Jack";
    private static final String USER_PASSWORD = "qwerty";
    private static final String USER_EMAIL = "jack@corp.com";
    private static final UserRole USER_ROLE = UserRole.ROLE_ADMINISTRATOR;

    private final UserService userService;
    private final UserRepository userRepository;

    private UserCreateDto user;

    @Autowired
    public UserServiceImplIT(UserService userService,
                             UserRepository userRepository) {
        this.userService = userService;
        this.userRepository = userRepository;
    }

    @BeforeEach
    void setUp() {
        user = new UserCreateDto();
        user.setUserName(USER_NAME);
        user.setEmail(USER_EMAIL);
        user.setPassword(USER_PASSWORD);
        user.setUserRole(USER_ROLE);
    }

    @Test
    void testRegisterUserShouldReturnUser() {
        //Given

        //When
        UserViewDto result = userService.registerUser(user);
        User user = userRepository.findByEmail(USER_EMAIL);

        //Then
        assertEquals(USER_EMAIL, result.getEmail());
        assertEquals(USER_NAME, result.getUserName());
        assertEquals(USER_ROLE, result.getUserRole());
        assertEquals(user.getEmail(), result.getEmail());
        assertEquals(user.getUserName(), result.getUserName());
        assertEquals(user.getUserRole(), result.getUserRole());
    }

    @Test
    void testRegisterUserShouldThrowException() {
        //Given
        user.setEmail("user@corp.com");
        userService.registerUser(user);

        //When
        EntityAlreadyExistsException ex = assertThrows(EntityAlreadyExistsException.class, () -> {
            userService.registerUser(user);
        });

        //Then
        assertTrue(ex.getMessage().contains("User with this email already exists"));
    }
}
