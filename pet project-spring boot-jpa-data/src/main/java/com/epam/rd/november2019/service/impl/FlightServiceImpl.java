package com.epam.rd.november2019.service.impl;


import com.epam.rd.november2019.converter.FlightConverter;
import com.epam.rd.november2019.dto.FlightCreateDto;
import com.epam.rd.november2019.dto.FlightViewDto;
import com.epam.rd.november2019.entity.Flight;
import com.epam.rd.november2019.exception.EntityDoesNotExistException;
import com.epam.rd.november2019.repository.FlightRepository;
import com.epam.rd.november2019.repository.RequestRepository;
import com.epam.rd.november2019.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class FlightServiceImpl implements FlightService {

    private final FlightRepository flightRepository;
    private final FlightConverter flightConverter;

    @Autowired
    public FlightServiceImpl(FlightRepository flightRepository,
                             FlightConverter flightConverter) {
        this.flightRepository = flightRepository;
        this.flightConverter = flightConverter;
    }

    @Override
    @Transactional
    public List<FlightViewDto> getAllFlights() {
        List<FlightViewDto> dtoList = new ArrayList<>();
        List<Flight> flights = flightRepository.findAll();
        for (Flight flight :
                flights) {
            dtoList.add(flightConverter.asDto(flight));
        }

        return dtoList;
    }

    @Override
    public FlightViewDto addFlight(FlightCreateDto dto) {
        Flight flight = flightConverter.asFlight(dto);
        flightRepository.save(flight);

        return flightConverter.asDto(flight);
    }

    @Override
    public FlightViewDto editFlight(FlightCreateDto dto) {
        Flight updatedEntity = flightConverter.asFlight(dto);
        if (flightRepository.getOne(dto.getFlightId()) == null) {
            throw new EntityDoesNotExistException(String.format("Flight with id: %s doesn't exist", dto.getFlightId()));
        }
        flightRepository.save(updatedEntity);
        return flightConverter.asDto(updatedEntity);
    }

    @Override
    public void removeFlight(UUID flightId) {
        flightRepository.deleteById(flightId);
    }

    @Override
    @Transactional
    public FlightViewDto getFlightById(UUID flightId) {
        Flight flight = flightRepository.getOne(flightId);

        return flightConverter.asDto(flight);
    }
}
