package com.epam.rd.november2019.service;

import com.epam.rd.november2019.dto.UserCreateDto;
import com.epam.rd.november2019.dto.UserViewDto;


public interface UserService {

    UserViewDto registerUser(UserCreateDto dto);
}
