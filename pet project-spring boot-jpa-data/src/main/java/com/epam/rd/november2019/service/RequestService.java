package com.epam.rd.november2019.service;

import com.epam.rd.november2019.dto.RequestCreateDto;
import com.epam.rd.november2019.dto.RequestViewDto;

import java.util.List;
import java.util.UUID;

public interface RequestService {

    List<RequestViewDto> getAllRequests();

    RequestViewDto addRequest(RequestCreateDto dto);

    void removeRequest(UUID requestId);

    RequestViewDto editRequest(RequestCreateDto dto);

    RequestViewDto getRequestById(UUID requestId);

    List<RequestViewDto> getRequestByFlightId(UUID flightId);
}
