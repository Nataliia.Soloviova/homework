package com.epam.rd.november2019.entity;


import com.epam.rd.november2019.entity.enums.EmployeeRole;
import com.epam.rd.november2019.entity.enums.Sex;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "employees")
@DynamicInsert
@DynamicUpdate
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "employee_id", length = 16, nullable = false, updatable = false, columnDefinition = "BINARY(16)")
    private UUID employeeId;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Enumerated(EnumType.STRING)
    @Column(name = "employee_role", nullable = false)
    private EmployeeRole employeeRole;

    @Enumerated(EnumType.STRING)
    @Column(name = "sex", nullable = false)
    private Sex sex;

    @Column(name = "city", nullable = false)
    private String city;

    @ManyToMany(mappedBy = "employees")
    private Set<Flight> flights = new HashSet<>();

    public Employee(String lastName, String firstName, EmployeeRole employeeRole, Sex sex, String city) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.employeeRole = employeeRole;
        this.sex = sex;
        this.city = city;
    }

    @Override
    public String toString() {
        return String.format("Employee{id=%s : %s : %s : %s : %s : %s}", employeeId, employeeRole.getDisplayValue(), firstName, lastName, sex, city);
    }
}
