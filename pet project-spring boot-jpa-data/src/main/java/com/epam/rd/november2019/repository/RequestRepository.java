package com.epam.rd.november2019.repository;

import com.epam.rd.november2019.entity.Request;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface RequestRepository extends JpaRepository<Request, UUID> {

    List<Request> findRequestByFlightId(UUID id);
}
