package com.epam.rd.november2019.dto;

import com.epam.rd.november2019.entity.enums.FlightStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FlightViewDto implements Serializable {

    private static final long serialVersionUID = 4414342883098637633L;

    private UUID flightId;
    private String departureAirport;
    private String arrivalAirport;
    private String departureCity;
    private String arrivalCity;
    private String departureDate;
    private String arrivalDate;
    private FlightStatus status;
    private Set<UUID> requests = new HashSet<>();
    private Set<UUID> employees = new HashSet<>();

    public FlightViewDto(UUID flightId, String departureAirport, String arrivalAirport, String departureCity, String arrivalCity, String departureDate, String arrivalDate, FlightStatus status) {
        this.flightId = flightId;
        this.departureAirport = departureAirport;
        this.arrivalAirport = arrivalAirport;
        this.departureCity = departureCity;
        this.arrivalCity = arrivalCity;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.status = status;
    }

    @Override
    public String toString() {
        return String.format("FlightViewDto{id=%s : %s : %s : %s : %s : %s : %s : status=%s : requests=%s}",
                flightId, departureCity, departureAirport, departureDate, arrivalCity, arrivalAirport, arrivalDate, status, requests.size());
    }
}
