package com.epam.rd.november2019.entity;

import com.epam.rd.november2019.entity.enums.RequestStatus;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "requests")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Request {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "request_id", length = 16, nullable = false, updatable = false, columnDefinition = "BINARY(16)")
    private UUID requestId;

    @ManyToOne
    @JoinColumn(name = "user_email", nullable = false, updatable = false)
    private User requestBy;

    @Column(name = "request_date", nullable = false)
    private Timestamp requestDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "request_status")
    private RequestStatus requestStatus;

    @Column(name = "requester_comment", nullable = false)
    private String requesterComment;

    @Column(name = "approver_comment")
    private String approverComment;

    @ManyToOne
    @JoinColumn(name = "flight_id", nullable = false)
    private Flight flight;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Request request = (Request) o;
        return Objects.equals(requestId, request.requestId) &&
                Objects.equals(requestBy, request.requestBy) &&
                Objects.equals(requestDate, request.requestDate) &&
                requestStatus == request.requestStatus &&
                Objects.equals(requesterComment, request.requesterComment) &&
                Objects.equals(approverComment, request.approverComment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestId, requestBy, requestDate, requestStatus, requesterComment, approverComment);
    }

    @Override
    public String toString() {
        return String.format("Request{id=%s : flight=%s : by=%s : date=%s : status=%s}", requestId, flight.getId(), requestBy.getEmail(), requestDate, requestStatus);
    }
}
