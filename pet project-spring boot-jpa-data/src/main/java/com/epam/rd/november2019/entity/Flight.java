package com.epam.rd.november2019.entity;

import com.epam.rd.november2019.entity.enums.FlightStatus;
import lombok.*;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "flights")
@DynamicInsert
@DynamicUpdate
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Flight {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "flight_id", length = 16, nullable = false, updatable = false, columnDefinition = "BINARY(16)")
    private UUID id;

    @Column(name = "departure_city", nullable = false)
    private String departureCity;

    @Column(name = "departure_airport", nullable = false)
    private String departureAirport;

    @Column(name = "departure_date", nullable = false)
    private Timestamp departureDate;

    @Column(name = "arrival_city", nullable = false)
    private String arrivalCity;

    @Column(name = "arrival_airport", nullable = false)
    private String arrivalAirport;

    @Column(name = "arrival_date", nullable = false)
    private Timestamp arrivalDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "flight_status", nullable = false)
    private FlightStatus status;

    @OneToMany(orphanRemoval = true, mappedBy = "flight")
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private Set<Request> requests = new HashSet<>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "flight_employee",
            joinColumns = {@JoinColumn(name = "flight_id")},
            inverseJoinColumns = {@JoinColumn(name = "employee_id")})
    private Set<Employee> employees = new HashSet<>();

    public Flight(String departureCity, String departureAirport, Timestamp departureDate, String arrivalCity, String arrivalAirport, Timestamp arrivalDate, FlightStatus status) {
        this.departureCity = departureCity;
        this.departureAirport = departureAirport;
        this.departureDate = departureDate;
        this.arrivalCity = arrivalCity;
        this.arrivalAirport = arrivalAirport;
        this.arrivalDate = arrivalDate;
        this.status = status;
    }

    public Flight(UUID id, String departureCity, String departureAirport, Timestamp departureDate, String arrivalCity, String arrivalAirport, Timestamp arrivalDate, FlightStatus status) {
        this.id = id;
        this.departureCity = departureCity;
        this.departureAirport = departureAirport;
        this.departureDate = departureDate;
        this.arrivalCity = arrivalCity;
        this.arrivalAirport = arrivalAirport;
        this.arrivalDate = arrivalDate;
        this.status = status;
    }

    public void addRequest(Request request) {
        this.requests.add(request);
        request.setFlight(this);
    }

    public void addEmployee(Employee employee) {
        this.employees.add(employee);
        employee.getFlights().add(this);
    }

    public void removeEmployee(Employee employee) {
        this.employees.remove(employee);
        employee.getFlights().remove(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flight flight = (Flight) o;
        return Objects.equals(id, flight.id) &&
                Objects.equals(departureCity, flight.departureCity) &&
                Objects.equals(departureAirport, flight.departureAirport) &&
                Objects.equals(departureDate, flight.departureDate) &&
                Objects.equals(arrivalCity, flight.arrivalCity) &&
                Objects.equals(arrivalAirport, flight.arrivalAirport) &&
                Objects.equals(arrivalDate, flight.arrivalDate) &&
                status == flight.status &&
                Objects.equals(employees, flight.employees);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, departureCity, departureAirport, departureDate, arrivalCity, arrivalAirport, arrivalDate, status, employees);
    }

    @Override
    public String toString() {
        return String.format("Flight{id=%s : %s : %s : %s : %s : %s : %s : status=%s : requests=%s}",
                id, departureCity, departureAirport, departureDate, arrivalCity, arrivalAirport, arrivalDate, status, requests.size());
    }
}