package com.epam.rd.november2019.dto;


import com.epam.rd.november2019.entity.enums.FlightStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FlightCreateDto implements Serializable {

    private static final long serialVersionUID = 3582227784241194540L;

    private UUID flightId;
    private String departureAirport;
    private String arrivalAirport;
    private String departureCity;
    private String arrivalCity;
    private Timestamp departureDate;
    private Timestamp arrivalDate;
    private FlightStatus status;
    private Set<UUID> requests = new HashSet<>();
    private Set<UUID> employees = new HashSet<>();

    public FlightCreateDto(UUID flightId, String departureAirport, String arrivalAirport, String departureCity, String arrivalCity, Timestamp departureDate, Timestamp arrivalDate, FlightStatus status) {
        this.flightId = flightId;
        this.departureAirport = departureAirport;
        this.arrivalAirport = arrivalAirport;
        this.departureCity = departureCity;
        this.arrivalCity = arrivalCity;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.status = status;
    }

    @Override
    public String toString() {
        return String.format("FlightCreateDto{id=%s : %s : %s : %s : %s : %s : %s : status=%s : requests=%s}",
                flightId, departureCity, departureAirport, departureDate, arrivalCity, arrivalAirport, arrivalDate, status, requests.size());
    }
}
