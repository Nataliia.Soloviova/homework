package com.epam.rd.november2019.entity.enums;

public enum RequestStatus {
    APPROVED("Approved"),
    DISAPPROVED("Disapproved");

    private final String displayValue;

    RequestStatus(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}
