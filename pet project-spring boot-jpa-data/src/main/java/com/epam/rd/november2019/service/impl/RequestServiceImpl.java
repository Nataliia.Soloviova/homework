package com.epam.rd.november2019.service.impl;


import com.epam.rd.november2019.converter.RequestConverter;
import com.epam.rd.november2019.dto.RequestCreateDto;
import com.epam.rd.november2019.dto.RequestViewDto;
import com.epam.rd.november2019.entity.Flight;
import com.epam.rd.november2019.entity.Request;
import com.epam.rd.november2019.exception.EntityDoesNotExistException;
import com.epam.rd.november2019.repository.FlightRepository;
import com.epam.rd.november2019.repository.RequestRepository;
import com.epam.rd.november2019.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class RequestServiceImpl implements RequestService {

    private final RequestRepository requestRepository;
    private final RequestConverter requestConverter;
    private final FlightRepository flightRepository;

    @Autowired
    public RequestServiceImpl(RequestRepository requestRepository,
                              RequestConverter requestConverter,
                              FlightRepository flightRepository) {
        this.requestRepository = requestRepository;
        this.requestConverter = requestConverter;
        this.flightRepository = flightRepository;
    }

    @Override
    public List<RequestViewDto> getAllRequests() {
        List<RequestViewDto> dtoList = new ArrayList<>();
        List<Request> requests = requestRepository.findAll();
        for (Request request :
                requests) {
            dtoList.add(requestConverter.asDto(request));
        }
        return dtoList;
    }

    @Override
    public RequestViewDto addRequest(RequestCreateDto dto) {
        Request request = requestConverter.asRequest(dto);
        Flight flight = flightRepository.getOne(dto.getFlight());
        flight.addRequest(request);
        requestRepository.save(request);

        return requestConverter.asDto(request);
    }

    @Override
    public void removeRequest(UUID requestId) {
        requestRepository.deleteById(requestId);
    }

    @Override
    public RequestViewDto editRequest(RequestCreateDto dto) {
        getRequestById(dto.getRequestId());
        Request request = requestConverter.asRequest(dto);
        request.setFlight(flightRepository.getOne(dto.getFlight()));
        requestRepository.save(request);

        return requestConverter.asDto(request);
    }

    @Override
    @Transactional
    public RequestViewDto getRequestById(UUID requestId) {
        Request request = requestRepository.findById(requestId).orElseThrow(EntityDoesNotExistException::new);

        return requestConverter.asDto(request);
    }

    @Override
    @Transactional
    public List<RequestViewDto> getRequestByFlightId(UUID flightId) {
        List<Request> requests = requestRepository.findRequestByFlightId(flightId);

        return requestConverter.asDtos(requests);
    }
}
