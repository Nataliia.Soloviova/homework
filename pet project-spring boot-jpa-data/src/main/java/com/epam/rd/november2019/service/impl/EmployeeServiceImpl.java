package com.epam.rd.november2019.service.impl;


import com.epam.rd.november2019.converter.EmployeeConverter;
import com.epam.rd.november2019.dto.EmployeeCreateDto;
import com.epam.rd.november2019.dto.EmployeeViewDto;
import com.epam.rd.november2019.entity.Employee;
import com.epam.rd.november2019.exception.EntityDoesNotExistException;
import com.epam.rd.november2019.repository.EmployeeRepository;
import com.epam.rd.november2019.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final EmployeeConverter employeeConverter;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository,
                               EmployeeConverter employeeConverter) {
        this.employeeRepository = employeeRepository;
        this.employeeConverter = employeeConverter;
    }

    @Override
    @Transactional
    public List<EmployeeViewDto> getAllEmployees() {
        List<EmployeeViewDto> dtoList = new ArrayList<>();
        List<Employee> employees = employeeRepository.findAll();
        for (Employee employee :
                employees) {
            dtoList.add(employeeConverter.asDto(employee));
        }

        return dtoList;
    }

    @Override
    public EmployeeViewDto addEmployee(EmployeeCreateDto dto) {
        Employee employee = employeeConverter.asEmployee(dto);
        employeeRepository.save(employee);

        return employeeConverter.asDto(employee);
    }

    @Override
    public void editEmployee(EmployeeCreateDto dto) {
        getEmployeeById(dto.getEmployeeId());
        Employee updatedEntity = employeeConverter.asEmployee(dto);
        employeeRepository.save(updatedEntity);
    }

    @Override
    public void removeEmployee(UUID employeeId) {
        employeeRepository.deleteById(employeeId);
    }


    @Override
    @Transactional
    public EmployeeViewDto getEmployeeById(UUID employeeId) {
        Employee employee = employeeRepository.findById(employeeId).orElseThrow(EntityDoesNotExistException::new);

        return employeeConverter.asDto(employee);
    }
}
