package com.epam.rd.november2019.converter;


import com.epam.rd.november2019.dto.FlightCreateDto;
import com.epam.rd.november2019.dto.FlightViewDto;
import com.epam.rd.november2019.entity.Employee;
import com.epam.rd.november2019.entity.Flight;
import com.epam.rd.november2019.entity.Request;
import com.epam.rd.november2019.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class FlightConverter {

    private final DateTimeConverter converter;
    private final UserRepository userRepository;

    @Autowired
    public FlightConverter(DateTimeConverter converter, UserRepository userRepository) {
        this.converter = converter;
        this.userRepository = userRepository;
    }

    public Flight asFlight(FlightCreateDto dto) {
        Flight flight = new Flight();
        flight.setId(dto.getFlightId());
        flight.setDepartureCity(dto.getDepartureCity());
        flight.setDepartureAirport(dto.getDepartureAirport());
        flight.setDepartureDate(dto.getDepartureDate());
        flight.setArrivalCity(dto.getArrivalCity());
        flight.setArrivalAirport(dto.getArrivalAirport());
        flight.setArrivalDate(dto.getArrivalDate());
        flight.setStatus(dto.getStatus());

        return flight;
    }

    public FlightViewDto asDto(Flight flight) {
        FlightViewDto dto = new FlightViewDto();
        dto.setFlightId(flight.getId());
        dto.setDepartureCity(flight.getDepartureCity());
        dto.setDepartureAirport(flight.getDepartureAirport());
        dto.setDepartureDate(converter.toDateTimeString(flight.getDepartureDate()));
        dto.setArrivalCity(flight.getArrivalCity());
        dto.setArrivalAirport(flight.getArrivalAirport());
        dto.setArrivalDate(converter.toDateTimeString(flight.getArrivalDate()));
        dto.setStatus(flight.getStatus());

        Set<UUID> requestIds = flight.getRequests()
                .stream()
                .map(Request::getRequestId)
                .collect(Collectors.toSet());
        dto.setRequests(requestIds);

        Set<UUID> employeeIds = flight.getEmployees()
                .stream()
                .map(Employee::getEmployeeId)
                .collect(Collectors.toSet());
        dto.setEmployees(employeeIds);

        return dto;
    }

    public Set<Flight> asFlights(Set<FlightCreateDto> createDtos) {
        return createDtos
                .stream()
                .map(this::asFlight)
                .collect(Collectors.toSet());
    }

    public Set<FlightViewDto> asDtos(Set<Flight> flights) {
        return flights
                .stream()
                .map(this::asDto)
                .collect(Collectors.toSet());
    }
}
