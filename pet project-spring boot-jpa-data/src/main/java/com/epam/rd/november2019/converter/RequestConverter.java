package com.epam.rd.november2019.converter;


import com.epam.rd.november2019.dto.RequestCreateDto;
import com.epam.rd.november2019.dto.RequestViewDto;
import com.epam.rd.november2019.entity.Request;
import com.epam.rd.november2019.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class RequestConverter {

    private final DateTimeConverter converter;
    private final UserRepository userRepository;

    @Autowired
    public RequestConverter(DateTimeConverter converter, UserRepository userRepository) {
        this.converter = converter;
        this.userRepository = userRepository;
    }

    public Request asRequest(RequestCreateDto dto) {
        Objects.requireNonNull(dto, "Request DTO is null");
        Request request = new Request();
        request.setRequestId(dto.getRequestId());
        request.setRequestBy(userRepository.findByEmail(dto.getRequestBy()));
        request.setRequestDate(dto.getRequestDate());
        request.setRequesterComment(dto.getRequesterComment());
        request.setApproverComment(dto.getApproverComment());
        request.setRequestStatus(dto.getRequestStatus());

        return request;
    }

    public RequestViewDto asDto(Request request) {
        RequestViewDto dto = new RequestViewDto();
        dto.setRequestId(request.getRequestId());
        dto.setFlight(request.getFlight().getId());
        dto.setRequestBy(request.getRequestBy().getEmail());
        dto.setRequestDate(converter.toViewString(request.getRequestDate()));
        dto.setRequesterComment(request.getRequesterComment());
        dto.setApproverComment(request.getApproverComment());
        dto.setRequestStatus(request.getRequestStatus());

        return dto;
    }

    public List<RequestViewDto> asDtos(List<Request> requests) {
        return requests.stream()
                .map(this::asDto)
                .collect(Collectors.toList());
    }
}
