# Creational patterns 

### Builder
* creates an object of a particular class in various ways
> Creates short and long flight teams and can creates other teams depends on the plane type. 

### Factory Method
* subclass of object that is instantiated
> Creates employees for flight team. There are three options for implementing this method:
> 1. with one static method;
> 2. generic;
> 3. simple.

# Structural patterns

### Decorator
* responsibilities of an object without subclassing
> Every employee has two abilities: open and close task. But everyone has more different abilities.

# Behavior patterns

### Command
* when and how a request is fulfilled
> Shows how admin works with four commands: add, read, update, delete.

### Strategy
* an algorithm
> There are two services for working with flights and employees. This classes implements one interface.

### Template method
* steps of an algorithm
> There is abstract class that contains steps of the task-algorithm. Implementations of this algorithm are different for each task.