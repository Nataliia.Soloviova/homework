package com.epam.rd.november2019.creational.factoryMethod.genericFactoryMethod.core;

import com.epam.rd.november2019.creational.factoryMethod.genericFactoryMethod.pojo.RadioOperator;

public class RadioOperatorFactory extends AbstractEmployeeFactory<RadioOperator> {

    @Override
    public RadioOperator createInstance() {
        return new RadioOperator();
    }
}
