package com.epam.rd.november2019.behavioral.strategy.pojo;

public class Stewardess extends AbstractEmployee {

    public Stewardess() {
    }

    public Stewardess(int id, String firstName) {
        super(id, firstName);
    }

    @Override
    public void perform() {
        System.out.println("I am a stewardess.");
    }
}
