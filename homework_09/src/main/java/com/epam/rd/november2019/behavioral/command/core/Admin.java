package com.epam.rd.november2019.behavioral.command.core;

public class Admin {

    public void action(CommandType commandType) {
        switch (commandType) {
            case ADDING:
                AddingCommand addingCommand = new AddingCommand();
                addingCommand.execute();
                break;
            case READING:
                ReadingCommand readingCommand = new ReadingCommand();
                readingCommand.execute();
                break;
            case DELETING:
                DeletingCommand deletingCommand = new DeletingCommand();
                deletingCommand.execute();
                break;
            case UPDATING:
                UpdatingCommand updatingCommand = new UpdatingCommand();
                updatingCommand.execute();
                break;
        }
    }
}
