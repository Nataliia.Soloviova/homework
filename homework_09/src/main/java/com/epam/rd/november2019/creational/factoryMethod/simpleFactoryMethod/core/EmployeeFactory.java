package com.epam.rd.november2019.creational.factoryMethod.simpleFactoryMethod.core;

import com.epam.rd.november2019.creational.factoryMethod.genericFactoryMethod.pojo.Navigator;
import com.epam.rd.november2019.creational.factoryMethod.genericFactoryMethod.pojo.Pilot;
import com.epam.rd.november2019.creational.factoryMethod.genericFactoryMethod.pojo.RadioOperator;
import com.epam.rd.november2019.creational.factoryMethod.genericFactoryMethod.pojo.Stewardess;

public class EmployeeFactory {

    public Pilot createPilot() {
        return new Pilot();
    }

    public Navigator createNavigator() {
        return new Navigator();
    }

    public Stewardess createStewardess() {
        return new Stewardess();
    }

    public RadioOperator createRadioOperator() {
        return new RadioOperator();
    }
}
