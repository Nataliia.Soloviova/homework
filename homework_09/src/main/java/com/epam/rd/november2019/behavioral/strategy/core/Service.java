package com.epam.rd.november2019.behavioral.strategy.core;

public interface Service<T> {

    void create(T element);

    void read(T element);

    void update(T element);

    void delete(T element);
}
