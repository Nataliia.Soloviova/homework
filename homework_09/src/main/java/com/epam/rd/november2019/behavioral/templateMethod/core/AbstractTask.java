package com.epam.rd.november2019.behavioral.templateMethod.core;

public abstract class AbstractTask {

    public abstract void startEvent();

    public abstract void process();

    public abstract void endEvent();

    public abstract void progressPrint();

    public void createEvent() {
        startEvent();
        process();
        endEvent();
        progressPrint();
    }
}
