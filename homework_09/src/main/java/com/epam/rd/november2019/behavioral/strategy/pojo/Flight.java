package com.epam.rd.november2019.behavioral.strategy.pojo;

import java.util.Objects;

public class Flight {

    private int id;
    private String flightName;
    private String departureCity;
    private String arrivalCity;
    private String departureDate;
    private String arrivalDate;

    public Flight() {
    }

    public Flight(int id, String departureCity, String arrivalCity, String departureDate, String arrivalDate) {
        this.id = id;
        this.departureCity = departureCity;
        this.arrivalCity = arrivalCity;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.flightName = departureCity + " - " + arrivalCity;
    }

    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = this.departureCity + " - " + this.arrivalCity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(String departureCity) {
        this.departureCity = departureCity;
    }

    public String getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flight flight = (Flight) o;
        return id == flight.id &&
                Objects.equals(flightName, flight.flightName) &&
                Objects.equals(departureCity, flight.departureCity) &&
                Objects.equals(arrivalCity, flight.arrivalCity) &&
                Objects.equals(departureDate, flight.departureDate) &&
                Objects.equals(arrivalDate, flight.arrivalDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, flightName, departureCity, arrivalCity, departureDate, arrivalDate);
    }

    @Override
    public String toString() {
        return String.format("[%s : %s : %s : %s]", id, flightName, departureDate, arrivalDate);
    }
}
