package com.epam.rd.november2019.behavioral.command.core;

public class AddingCommand implements ICommand {

    @Override
    public void execute() {
        System.out.println("Add flight or employee");
    }
}
