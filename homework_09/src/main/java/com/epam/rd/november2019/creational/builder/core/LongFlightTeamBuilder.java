package com.epam.rd.november2019.creational.builder.core;

import com.epam.rd.november2019.creational.builder.pojo.*;

import java.util.List;
import java.util.Objects;

public class LongFlightTeamBuilder implements Builder {

    private FlightType flightType;
    private List<Pilot> pilots;
    private List<Navigator> navigators;
    private List<Stewardess> stewardesses;
    private List<RadioOperator> radioOperators;

    @Override
    public void setFlightType(FlightType flightType) {
        this.flightType = flightType;
    }

    @Override
    public void setPilots(List<Pilot> pilots) {
        if (pilots.size() == 4) {
            this.pilots = pilots;
        } else {
            throw new RuntimeException("This Flight has to have 4 pilots.");
        }
    }

    @Override
    public void setNavigators(List<Navigator> navigators) {
        if (navigators.size() == 2) {
            this.navigators = navigators;
        } else {
            throw new RuntimeException("This Flight has to have 2 navigator.");
        }
    }

    @Override
    public void setRadioOperator(List<RadioOperator> radioOperators) {
        if (radioOperators.size() == 2) {
            this.radioOperators = radioOperators;
        } else {
            throw new RuntimeException("This Flight has to have 2 radio operator.");
        }
    }

    @Override
    public void setStewardess(List<Stewardess> stewardesses) {
        if (stewardesses.size() == 8) {
            this.stewardesses = stewardesses;
        } else {
            throw new RuntimeException("This Flight has to have 8 stewardesses.");
        }
    }

    public FlightTeam getResult() {
        return new FlightTeam(flightType, pilots, navigators, stewardesses, radioOperators);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LongFlightTeamBuilder that = (LongFlightTeamBuilder) o;
        return flightType == that.flightType &&
                Objects.equals(pilots, that.pilots) &&
                Objects.equals(navigators, that.navigators) &&
                Objects.equals(stewardesses, that.stewardesses) &&
                Objects.equals(radioOperators, that.radioOperators);
    }

    @Override
    public int hashCode() {
        return Objects.hash(flightType, pilots, navigators, stewardesses, radioOperators);
    }

    @Override
    public String toString() {
        return "LongFlightTeamBuilder{" +
                "flightType=" + flightType +
                ", pilots=" + pilots +
                ", navigators=" + navigators +
                ", stewardesses=" + stewardesses +
                ", radioOperators=" + radioOperators +
                '}';
    }
}
