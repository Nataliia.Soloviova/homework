package com.epam.rd.november2019.creational.builder.core;

import com.epam.rd.november2019.creational.builder.pojo.*;

import java.util.ArrayList;
import java.util.List;

public class Dispatcher {

    public void buildShortFlightTeam(Builder builder) {
        List<Pilot> pilotList = new ArrayList<>();
        List<Navigator> navigatorList = new ArrayList<>();
        List<RadioOperator> radioOperatorList = new ArrayList<>();
        List<Stewardess> stewardessList = new ArrayList<>();

        pilotList.add(new Pilot());
        pilotList.add(new Pilot());

        navigatorList.add(new Navigator());

        radioOperatorList.add(new RadioOperator());

        stewardessList.add(new Stewardess());
        stewardessList.add(new Stewardess());
        stewardessList.add(new Stewardess());

        builder.setFlightType(FlightType.SHORT);
        builder.setNavigators(navigatorList);
        builder.setPilots(pilotList);
        builder.setRadioOperator(radioOperatorList);
        builder.setStewardess(stewardessList);
    }

    public void buildLongFlightTeam(Builder builder) {
        List<Pilot> pilotList = new ArrayList<>();
        List<Navigator> navigatorList = new ArrayList<>();
        List<RadioOperator> radioOperatorList = new ArrayList<>();
        List<Stewardess> stewardessList = new ArrayList<>();

        pilotList.add(new Pilot());
        pilotList.add(new Pilot());
        pilotList.add(new Pilot());
        pilotList.add(new Pilot());

        navigatorList.add(new Navigator());
        navigatorList.add(new Navigator());

        radioOperatorList.add(new RadioOperator());
        radioOperatorList.add(new RadioOperator());

        stewardessList.add(new Stewardess());
        stewardessList.add(new Stewardess());
        stewardessList.add(new Stewardess());
        stewardessList.add(new Stewardess());
        stewardessList.add(new Stewardess());
        stewardessList.add(new Stewardess());
        stewardessList.add(new Stewardess());
        stewardessList.add(new Stewardess());

        builder.setFlightType(FlightType.LONG);
        builder.setNavigators(navigatorList);
        builder.setPilots(pilotList);
        builder.setRadioOperator(radioOperatorList);
        builder.setStewardess(stewardessList);
    }
}
