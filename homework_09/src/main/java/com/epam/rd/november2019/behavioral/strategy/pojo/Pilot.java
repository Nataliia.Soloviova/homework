package com.epam.rd.november2019.behavioral.strategy.pojo;

public class Pilot extends AbstractEmployee {

    public Pilot() {
    }

    public Pilot(int id, String firstName) {
        super(id, firstName);
    }

    @Override
    public void perform() {
        System.out.println("I am a pilot.");
    }
}
