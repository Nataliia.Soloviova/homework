package com.epam.rd.november2019.creational.factoryMethod.genericFactoryMethod.core;


import com.epam.rd.november2019.creational.factoryMethod.genericFactoryMethod.pojo.*;

public abstract class AbstractEmployeeFactory<T extends AbstractEmployee> {

    public abstract T createInstance();
}
