package com.epam.rd.november2019.structural.decorator.core;

import com.epam.rd.november2019.structural.decorator.pojo.Employee;
import com.epam.rd.november2019.structural.decorator.pojo.Task;

public abstract class EmployeeDecorator implements IEmployee {

    private Employee employee;

    public EmployeeDecorator(Employee employee) {
        this.employee = employee;
    }

    @Override
    public void open(Task task) {
        employee.open(task);
    }

    @Override
    public void close(Task task) {
        employee.close(task);
    }
}
