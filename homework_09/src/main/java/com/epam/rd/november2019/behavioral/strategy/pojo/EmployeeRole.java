package com.epam.rd.november2019.behavioral.strategy.pojo;

public enum EmployeeRole {
    PILOT, NAVIGATOR, RADIO_OPERATOR, STEWARDESS;
}
