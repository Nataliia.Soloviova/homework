package com.epam.rd.november2019.behavioral.templateMethod.core;

public class ConstructingTask extends AbstractTask {

    @Override
    public void startEvent() {
        System.out.println("Start constructing task");
        // constructing task specific initialization actions
    }

    @Override
    public void endEvent() {
        System.out.println("End constructing task");
        // constructing task specific actions to end a task
    }

    @Override
    public void progressPrint() {
        System.out.println("Progress constructing task");
        // constructing actions to print progress
    }

    @Override
    public void process() {
        System.out.println("Constructing task in process");
        // constructing actions in progress
    }
}
