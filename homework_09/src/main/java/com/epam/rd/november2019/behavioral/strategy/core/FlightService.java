package com.epam.rd.november2019.behavioral.strategy.core;

import com.epam.rd.november2019.behavioral.strategy.pojo.Flight;

public class FlightService implements Service<Flight> {

    @Override
    public void create(Flight element) {
//        add new Flight();
    }

    @Override
    public void read(Flight element) {
//        getFlight();
    }

    @Override
    public void update(Flight element) {
//        update Flight
    }

    @Override
    public void delete(Flight element) {
//        delete Flight
    }
}
