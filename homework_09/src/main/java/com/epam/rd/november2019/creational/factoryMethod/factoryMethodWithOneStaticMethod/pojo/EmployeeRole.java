package com.epam.rd.november2019.creational.factoryMethod.factoryMethodWithOneStaticMethod.pojo;

public enum EmployeeRole {
    PILOT, NAVIGATOR, RADIO_OPERATOR, STEWARDESS;
}
