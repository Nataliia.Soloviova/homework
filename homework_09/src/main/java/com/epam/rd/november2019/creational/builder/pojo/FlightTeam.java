package com.epam.rd.november2019.creational.builder.pojo;

import java.util.List;
import java.util.Objects;

public class FlightTeam {

    private final FlightType flightType;
    private final List<Pilot> pilots;
    private final List<Navigator> navigators;
    private final List<Stewardess> stewardesses;
    private final List<RadioOperator> radioOperators;

    public FlightTeam(FlightType flightType, List<Pilot> pilots, List<Navigator> navigators,
                      List<Stewardess> stewardesses, List<RadioOperator> radioOperators) {
        this.flightType = flightType;
        this.pilots = pilots;
        this.navigators = navigators;
        this.stewardesses = stewardesses;
        this.radioOperators = radioOperators;
    }

    public FlightType getFlightType() {
        return flightType;
    }

    public List<Pilot> getPilots() {
        return pilots;
    }

    public List<Navigator> getNavigators() {
        return navigators;
    }

    public List<Stewardess> getStewardesses() {
        return stewardesses;
    }

    public List<RadioOperator> getRadioOperators() {
        return radioOperators;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FlightTeam that = (FlightTeam) o;
        return flightType == that.flightType &&
                Objects.equals(pilots, that.pilots) &&
                Objects.equals(navigators, that.navigators) &&
                Objects.equals(stewardesses, that.stewardesses) &&
                Objects.equals(radioOperators, that.radioOperators);
    }

    @Override
    public int hashCode() {
        return Objects.hash(flightType, pilots, navigators, stewardesses, radioOperators);
    }

    @Override
    public String toString() {
        return "FlightTeam{" +
                "flightType=" + flightType +
                ", pilots=" + pilots +
                ", navigators=" + navigators +
                ", stewardesses=" + stewardesses +
                ", radioOperators=" + radioOperators +
                '}';
    }
}
