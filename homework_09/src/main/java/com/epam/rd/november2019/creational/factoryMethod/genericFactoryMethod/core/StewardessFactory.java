package com.epam.rd.november2019.creational.factoryMethod.genericFactoryMethod.core;

import com.epam.rd.november2019.creational.factoryMethod.genericFactoryMethod.pojo.Stewardess;

public class StewardessFactory extends AbstractEmployeeFactory<Stewardess> {

    @Override
    public Stewardess createInstance() {
        return new Stewardess();
    }
}
