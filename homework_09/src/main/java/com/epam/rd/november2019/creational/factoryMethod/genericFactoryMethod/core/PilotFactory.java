package com.epam.rd.november2019.creational.factoryMethod.genericFactoryMethod.core;

import com.epam.rd.november2019.creational.factoryMethod.genericFactoryMethod.pojo.Pilot;

public class PilotFactory extends AbstractEmployeeFactory<Pilot> {

    @Override
    public Pilot createInstance() {
        return new Pilot();
    }
}
