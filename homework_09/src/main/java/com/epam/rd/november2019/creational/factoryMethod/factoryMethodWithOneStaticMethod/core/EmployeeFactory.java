package com.epam.rd.november2019.creational.factoryMethod.factoryMethodWithOneStaticMethod.core;

import com.epam.rd.november2019.creational.factoryMethod.factoryMethodWithOneStaticMethod.pojo.*;

public class EmployeeFactory {

    public static AbstractEmployee createEmployeeFromFactory(String role) {

        EmployeeRole employeeRole = EmployeeRole.valueOf(role.toUpperCase());

        switch (employeeRole) {
            case PILOT:
                return new Pilot();
            case NAVIGATOR:
                return new Navigator();
            case STEWARDESS:
                return new Stewardess();
            case RADIO_OPERATOR:
                return new RadioOperator();
            default:
                throw new IllegalArgumentException("Invalid input data! This role does not exist.");
        }
    }

}
