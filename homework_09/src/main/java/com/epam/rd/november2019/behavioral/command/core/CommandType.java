package com.epam.rd.november2019.behavioral.command.core;

public enum CommandType {
    ADDING, DELETING, UPDATING, READING;
}
