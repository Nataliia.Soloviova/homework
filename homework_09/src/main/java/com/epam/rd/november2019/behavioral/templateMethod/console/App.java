package com.epam.rd.november2019.behavioral.templateMethod.console;

import com.epam.rd.november2019.behavioral.templateMethod.core.AbstractTask;
import com.epam.rd.november2019.behavioral.templateMethod.core.ConstructingTask;
import com.epam.rd.november2019.behavioral.templateMethod.core.ModelingTask;

public class App {
    public static void main(String[] args) {

        ConstructingTask constructingTask = new ConstructingTask();
        ModelingTask modelingTask = new ModelingTask();

        constructingTask.createEvent();
        modelingTask.createEvent();
    }
}
