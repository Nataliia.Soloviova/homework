package com.epam.rd.november2019.behavioral.templateMethod.core;

public class ModelingTask extends AbstractTask {

    @Override
    public void startEvent() {
        System.out.println("Start modeling task");
        // modeling task specific initialization actions
    }

    @Override
    public void endEvent() {
        System.out.println("End modeling task");
        // modeling task specific actions to end a task
    }

    @Override
    public void progressPrint() {
        System.out.println("Progress modeling task");
        // specific actions to print progress
    }

    @Override
    public void process() {
        System.out.println("Modeling task in process");
        // specific actions in progress
    }
}
