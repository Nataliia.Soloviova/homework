package com.epam.rd.november2019.creational.factoryMethod.genericFactoryMethod.core;

import com.epam.rd.november2019.creational.factoryMethod.genericFactoryMethod.pojo.Navigator;

public class NavigatorFactory extends AbstractEmployeeFactory<Navigator> {

    @Override
    public Navigator createInstance() {
        return new Navigator();
    }
}
