package com.epam.rd.november2019.creational.factoryMethod.simpleFactoryMethod.console;

import com.epam.rd.november2019.creational.factoryMethod.simpleFactoryMethod.core.EmployeeFactory;

public class SimpleEmployeeFactoryApp {

    public static void main(String[] args) {

        EmployeeFactory employeeFactory = new EmployeeFactory();

        employeeFactory.createNavigator().perform();
        employeeFactory.createPilot().perform();
        employeeFactory.createRadioOperator().perform();
        employeeFactory.createStewardess().perform();
    }
}
