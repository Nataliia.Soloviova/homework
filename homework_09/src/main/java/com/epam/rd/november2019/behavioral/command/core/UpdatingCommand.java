package com.epam.rd.november2019.behavioral.command.core;

public class UpdatingCommand implements ICommand {

    @Override
    public void execute() {
        System.out.println("Set flight or employee");
    }
}
