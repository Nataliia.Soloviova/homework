package com.epam.rd.november2019.creational.builder.pojo;

public class Pilot extends AbstractEmployee {

    public Pilot() {
    }

    public Pilot(int id, String firstName) {
        super(id, firstName);
    }

    @Override
    public void perform() {
        System.out.println("I am a pilot.");
    }
}
