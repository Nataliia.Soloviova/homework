package com.epam.rd.november2019.behavioral.command.core;

public class ReadingCommand implements ICommand {

    @Override
    public void execute() {
        System.out.println("Get flight or employee");
    }
}
