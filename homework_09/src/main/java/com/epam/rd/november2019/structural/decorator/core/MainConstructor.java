package com.epam.rd.november2019.structural.decorator.core;

import com.epam.rd.november2019.structural.decorator.pojo.Employee;
import com.epam.rd.november2019.structural.decorator.pojo.Task;

public class MainConstructor extends EmployeeDecorator {


    public MainConstructor(Employee employee) {
        super(employee);
    }

    @Override
    public void open(Task task) {
        super.open(task);
    }

    @Override
    public void close(Task task) {
        super.close(task);
    }

    public Task create() {
        return new Task();
    }

    public void delete(Task task) {
        task = null;
    }
}
