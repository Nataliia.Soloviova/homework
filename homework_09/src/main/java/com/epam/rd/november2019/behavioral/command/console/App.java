package com.epam.rd.november2019.behavioral.command.console;

import com.epam.rd.november2019.behavioral.command.core.Admin;
import com.epam.rd.november2019.behavioral.command.core.CommandType;

public class App {

    public static void main(String[] args) {

        Admin admin = new Admin();
        admin.action(CommandType.ADDING);
        admin.action(CommandType.DELETING);
        admin.action(CommandType.READING);
        admin.action(CommandType.UPDATING);
    }
}
