package com.epam.rd.november2019.creational.builder.core;

import com.epam.rd.november2019.creational.builder.pojo.*;

import java.util.List;

public interface Builder {

    void setFlightType(FlightType flightType);

    void setPilots(List<Pilot> pilots);

    void setNavigators(List<Navigator> navigators);

    void setRadioOperator(List<RadioOperator> radioOperators);

    void setStewardess(List<Stewardess> stewardesses);
}
