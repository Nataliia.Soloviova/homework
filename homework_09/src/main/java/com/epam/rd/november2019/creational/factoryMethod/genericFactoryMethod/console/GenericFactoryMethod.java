package com.epam.rd.november2019.creational.factoryMethod.genericFactoryMethod.console;

import com.epam.rd.november2019.creational.factoryMethod.genericFactoryMethod.core.NavigatorFactory;
import com.epam.rd.november2019.creational.factoryMethod.genericFactoryMethod.core.PilotFactory;
import com.epam.rd.november2019.creational.factoryMethod.genericFactoryMethod.core.RadioOperatorFactory;
import com.epam.rd.november2019.creational.factoryMethod.genericFactoryMethod.core.StewardessFactory;

public class GenericFactoryMethod {

    public static void main(String[] args) {
        NavigatorFactory navigatorFactory = new NavigatorFactory();
        PilotFactory pilotFactory = new PilotFactory();
        StewardessFactory stewardessFactory = new StewardessFactory();
        RadioOperatorFactory radioOperatorFactory = new RadioOperatorFactory();

        navigatorFactory.createInstance().perform();
        pilotFactory.createInstance().perform();
        stewardessFactory.createInstance().perform();
        radioOperatorFactory.createInstance().perform();
    }
}
