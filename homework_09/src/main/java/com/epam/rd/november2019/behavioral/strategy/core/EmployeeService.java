package com.epam.rd.november2019.behavioral.strategy.core;

import com.epam.rd.november2019.creational.factoryMethod.genericFactoryMethod.pojo.AbstractEmployee;

public class EmployeeService implements Service<AbstractEmployee> {

    @Override
    public void create(AbstractEmployee employee) {
//        add
    }

    @Override
    public void read(AbstractEmployee employee) {
//        get
    }

    @Override
    public void update(AbstractEmployee employee) {
//        update
    }

    @Override
    public void delete(AbstractEmployee employee) {
//        delete
    }
}
