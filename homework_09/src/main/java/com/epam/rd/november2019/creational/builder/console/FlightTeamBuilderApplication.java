package com.epam.rd.november2019.creational.builder.console;

import com.epam.rd.november2019.creational.builder.core.Dispatcher;
import com.epam.rd.november2019.creational.builder.core.LongFlightTeamBuilder;
import com.epam.rd.november2019.creational.builder.core.ShortFlightTeamBuilder;
import com.epam.rd.november2019.creational.builder.pojo.FlightTeam;

public class FlightTeamBuilderApplication {
    public static void main(String[] args) {
        Dispatcher dispatcher = new Dispatcher();

        ShortFlightTeamBuilder shortFlightTeamBuilder = new ShortFlightTeamBuilder();
        dispatcher.buildShortFlightTeam(shortFlightTeamBuilder);
        FlightTeam flightTeam1 = shortFlightTeamBuilder.getResult();
        System.out.println("Flight type: " + flightTeam1.getFlightType());

        LongFlightTeamBuilder longFlightTeamBuilder = new LongFlightTeamBuilder();
        dispatcher.buildLongFlightTeam(longFlightTeamBuilder);
        FlightTeam flightTeam2 = longFlightTeamBuilder.getResult();
        System.out.println("Flight type: " + flightTeam2.getFlightType());
    }
}
