package com.epam.rd.november2019.creational.factoryMethod.simpleFactoryMethod.pojo;

public class Navigator extends AbstractEmployee {

    public Navigator() {
    }

    public Navigator(int id, String firstName) {
        super(id, firstName);
    }

    @Override
    public void perform() {
        System.out.println("I am a navigator.");
    }
}
