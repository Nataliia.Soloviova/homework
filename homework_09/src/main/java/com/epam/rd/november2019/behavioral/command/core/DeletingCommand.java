package com.epam.rd.november2019.behavioral.command.core;

public class DeletingCommand implements ICommand {

    @Override
    public void execute() {
        System.out.println("Delete flight or employee");
    }
}
