package com.epam.rd.november2019.creational.factoryMethod.factoryMethodWithOneStaticMethod.console;

import com.epam.rd.november2019.creational.factoryMethod.factoryMethodWithOneStaticMethod.core.EmployeeFactory;

public class FactoryMethodWithOneStaticMethod {

    public static void main(String[] args) {
        EmployeeFactory.createEmployeeFromFactory("pilot").perform();
        EmployeeFactory.createEmployeeFromFactory("stewardess").perform();
        EmployeeFactory.createEmployeeFromFactory("radio_operator").perform();
        EmployeeFactory.createEmployeeFromFactory("navigator").perform();

        try {
            EmployeeFactory.createEmployeeFromFactory("president");
        } catch (Exception e) {
            System.out.println("Invalid input data! This role does not exist.");
        }
    }
}
