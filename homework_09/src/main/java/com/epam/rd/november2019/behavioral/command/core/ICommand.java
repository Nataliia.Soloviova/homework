package com.epam.rd.november2019.behavioral.command.core;

interface ICommand {

    void execute();
}
