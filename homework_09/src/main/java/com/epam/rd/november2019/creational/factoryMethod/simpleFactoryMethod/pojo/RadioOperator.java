package com.epam.rd.november2019.creational.factoryMethod.simpleFactoryMethod.pojo;

public class RadioOperator extends AbstractEmployee {

    public RadioOperator() {
    }

    public RadioOperator(int id, String firstName) {
        super(id, firstName);
    }

    @Override
    public void perform() {
        System.out.println("I am a radio operator.");
    }
}
