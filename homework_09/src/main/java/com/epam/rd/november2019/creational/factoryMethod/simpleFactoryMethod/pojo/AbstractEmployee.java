package com.epam.rd.november2019.creational.factoryMethod.simpleFactoryMethod.pojo;

import java.util.Objects;

public abstract class AbstractEmployee {

    private int id;
    private String firstName;

    public AbstractEmployee() {
    }

    public AbstractEmployee(int id, String firstName) {
        this.id = id;
        this.firstName = firstName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public abstract void perform();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractEmployee employee = (AbstractEmployee) o;
        return id == employee.id &&
                Objects.equals(firstName, employee.firstName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName);
    }

    @Override
    public String toString() {
        return String.format("[%s : %s : %s]", id, firstName);
    }
}
