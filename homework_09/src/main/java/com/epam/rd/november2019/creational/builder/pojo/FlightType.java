package com.epam.rd.november2019.creational.builder.pojo;

public enum FlightType {
    SHORT, LONG;
}
