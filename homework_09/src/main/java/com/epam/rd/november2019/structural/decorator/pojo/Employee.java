package com.epam.rd.november2019.structural.decorator.pojo;

import com.epam.rd.november2019.structural.decorator.core.IEmployee;

import java.util.Objects;

public class Employee implements IEmployee {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void open(Task task) {
        System.out.println("I can open tasks.");
    }

    @Override
    public void close(Task task) {
        System.out.println("I can close tasks.");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Objects.equals(name, employee.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                '}';
    }
}
