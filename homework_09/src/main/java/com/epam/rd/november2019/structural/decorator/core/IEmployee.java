package com.epam.rd.november2019.structural.decorator.core;

import com.epam.rd.november2019.structural.decorator.pojo.Task;

public interface IEmployee {

    void open(Task task);

    void close(Task task);
}
