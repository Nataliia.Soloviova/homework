package com.epam.rd.november2019.structural.decorator.core;

import com.epam.rd.november2019.structural.decorator.pojo.Employee;
import com.epam.rd.november2019.structural.decorator.pojo.Task;

public class Constructor extends EmployeeDecorator {

    public Constructor(Employee employee) {
        super(employee);
    }

    @Override
    public void open(Task task) {
        super.open(task);
        startEvent();
    }

    @Override
    public void close(Task task) {
        endEvent();
        progress();
        report();
        super.close(task);
    }

    public void startEvent() {
        // some actions
    }

    public void endEvent() {
        // some actions
    }

    public void progress() {
        // some actions
    }

    public void report() {
        // some actions
    }
}
