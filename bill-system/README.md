# JSON Doc:
##### http://localhost:8080/jsondoc-ui.html

# There are:
* Spring Boot
* Spring Testing
* Hibernate
* JPA
* REST
* Lombok
* JSON Doc
* Liquibase
* H2
* MariaDB
* YAML
* XML
* Design Patterns

# How it works:

* mvn clean install
* mvn spring-boot:run
