package com.example.demo.facade.impl;

import com.example.demo.converter.CardConverter;
import com.example.demo.dto.CardCreateDto;
import com.example.demo.dto.CardActionDto;
import com.example.demo.dto.CardViewDto;
import com.example.demo.facade.CardFacade;
import com.example.demo.model.Bill;
import com.example.demo.model.Card;
import com.example.demo.model.Payment;
import com.example.demo.model.enums.CardAction;
import com.example.demo.model.enums.PaymentStatus;
import com.example.demo.repository.BillRepository;
import com.example.demo.repository.CardRepository;
import com.example.demo.repository.PaymentRepository;
import com.example.demo.service.BillService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@AllArgsConstructor
public class CardFacadeImpl implements CardFacade {

    private final BillService billService;
    private final BillRepository billRepository;
    private final CardRepository cardRepository;
    private final PaymentRepository paymentRepository;
    private final CardConverter cardConverter;

    @Override
    @Transactional
    public CardViewDto addCardForBill(CardCreateDto cardCreateDto, UUID billId) {
        Bill bill = billService.findById(billId);
        Card card = cardConverter.convert(cardCreateDto);
        card.setBill(bill);
        card.setId(billId);

        cardRepository.save(card);

        return cardConverter.convert(card);
    }

    @Override
    public void addOrGetMoney(CardActionDto cardActionDto) {
        Card card = cardRepository.findCardByNumber(cardActionDto.getNumber());
        Bill bill = billService.findById(card.getBill().getId());

        Payment payment = new Payment();
        payment.setBill(bill);
        payment.setAmount(cardActionDto.getAmount());

        if (cardActionDto.getCardAction().equals(CardAction.ADD)) {
            payment.setPurpose("Adding money on the card");
            bill.setBalance(bill.getBalance() + cardActionDto.getAmount());
        } else if (cardActionDto.getCardAction().equals(CardAction.GET)) {
            payment.setPurpose("Getting money from the card");
            bill.setBalance(bill.getBalance() - cardActionDto.getAmount());
        }
        billRepository.save(bill);

        payment.setStatus(PaymentStatus.SENT);
        paymentRepository.save(payment);
    }
}
