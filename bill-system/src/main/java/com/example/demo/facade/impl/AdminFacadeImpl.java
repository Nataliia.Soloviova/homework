package com.example.demo.facade.impl;

import com.example.demo.dto.AdminCreateDto;
import com.example.demo.dto.AdminViewDto;
import com.example.demo.facade.AdminFacade;
import com.example.demo.service.AdminService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class AdminFacadeImpl implements AdminFacade {

    private final AdminService adminService;

    @Override
    public AdminViewDto createUserWithRoleAdmin(AdminCreateDto adminCreateDto) {
        return null;
    }

    @Override
    public List<AdminViewDto> getAllAdmins() {
        return null;
    }
}
