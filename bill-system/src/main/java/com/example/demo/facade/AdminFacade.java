package com.example.demo.facade;

import com.example.demo.dto.AdminCreateDto;
import com.example.demo.dto.AdminViewDto;

import java.util.List;

public interface AdminFacade {

    AdminViewDto createUserWithRoleAdmin(AdminCreateDto adminCreateDto);

    List<AdminViewDto> getAllAdmins();
}
