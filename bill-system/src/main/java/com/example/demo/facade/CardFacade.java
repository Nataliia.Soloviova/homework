package com.example.demo.facade;

import com.example.demo.dto.CardCreateDto;
import com.example.demo.dto.CardActionDto;
import com.example.demo.dto.CardViewDto;

import java.util.UUID;

public interface CardFacade {

    CardViewDto addCardForBill(CardCreateDto cardCreateDto, UUID billId);

    void addOrGetMoney(CardActionDto cardActionDto);
}
