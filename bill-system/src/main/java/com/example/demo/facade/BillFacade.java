package com.example.demo.facade;

import com.example.demo.dto.BillCreateDto;
import com.example.demo.dto.BillViewDto;

import java.util.UUID;

public interface BillFacade {

    BillViewDto createBillForClient(BillCreateDto billCreateDto, UUID clientId);

}
