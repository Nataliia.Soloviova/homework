package com.example.demo.facade.impl;

import com.example.demo.converter.BillConverter;
import com.example.demo.dto.BillCreateDto;
import com.example.demo.dto.BillViewDto;
import com.example.demo.facade.BillFacade;
import com.example.demo.model.Bill;
import com.example.demo.model.Client;
import com.example.demo.repository.BillRepository;
import com.example.demo.service.ClientService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@AllArgsConstructor
public class BillFacadeImpl implements BillFacade {

    private final ClientService clientService;
    private final BillConverter billConverter;
    private final BillRepository billRepository;

    @Override
    @Transactional
    public BillViewDto createBillForClient(BillCreateDto billCreateDto, UUID clientId) {
        Client client = clientService.findById(clientId);
        Bill bill = billConverter.convert(billCreateDto);
        client.addBill(bill);
        billRepository.save(bill);

        return billConverter.convert(bill);
    }
}
