package com.example.demo.dto;

import lombok.Data;
import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;

import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

@Data
@ApiObject(name = "ClientViewDto")
public class ClientViewDto implements Serializable {

    private static final long serialVersionUID = -9158720867553164244L;

    @ApiObjectField(description = "Client id")
    private UUID id;

    @ApiObjectField(description = "Client first name")
    private String firstName;

    @ApiObjectField(description = "Client status")
    private Boolean enabled;

    @ApiObjectField(description = "Client card numbers")
    private Set<UUID> cardNumbers;
}
