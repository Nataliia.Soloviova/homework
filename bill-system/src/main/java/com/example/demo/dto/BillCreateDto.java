package com.example.demo.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class BillCreateDto implements Serializable {

    private static final long serialVersionUID = 986665423992411000L;

    private String title;

}
