package com.example.demo.dto;

import com.example.demo.utils.constants.UserConstraints;
import lombok.Data;
import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

@Data
@ApiObject(name = "AdminCreateDto")
public class AdminCreateDto implements Serializable {

    private static final long serialVersionUID = -7740419506827794545L;

    @NotNull
    @Pattern(regexp = "^[a-zA-Z'-]{" + UserConstraints.MIN_NAME_LENGTH + "," + UserConstraints.MAX_NAME_LENGTH + "}$", message = "First name isn't correct")
    @ApiObjectField(description = "Admin first name")
    private String firstName;

    @NotNull
    @Pattern(regexp = "^[a-zA-Z'-]{")
    @ApiObjectField(description = "Admin password")
    private String password;
}
