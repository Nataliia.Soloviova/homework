package com.example.demo.dto;

import com.example.demo.utils.constants.UserConstraints;
import lombok.Data;
import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

@Data
@ApiObject(name = "ClientCreateDto")
public class ClientCreateDto implements Serializable {

    private static final long serialVersionUID = 6701615304590363534L;

    @NotBlank
    @NotNull
    @Pattern(regexp = "^[a-zA-Z'-]{" + UserConstraints.MIN_NAME_LENGTH + "," + UserConstraints.MAX_NAME_LENGTH + "}$", message = "The first name is invalid")
    @ApiObjectField(description = "Admin first name")
    private String firstName;

    @NotBlank
    @NotNull
    @Pattern(regexp = "^[a-zA-Z'-]{")
    @ApiObjectField(description = "Admin password")
    private String password;

    @NotBlank
    @NotNull
    @Pattern(regexp = "[A-Z]2[0-9]4")
    @ApiObjectField(description = "Client passport")
    private String passport;
}
