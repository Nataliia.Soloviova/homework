package com.example.demo.dto;

import lombok.Data;
import org.jsondoc.core.annotation.ApiObjectField;

import java.io.Serializable;

@Data
public class CardCreateDto implements Serializable {

    private static final long serialVersionUID = -5365965779867223989L;

    @ApiObjectField(description = "Card number")
    private String number;

}
