package com.example.demo.dto;

import lombok.Data;
import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;

import java.io.Serializable;
import java.util.UUID;

@Data
@ApiObject(name = "CardViewDto")
public class CardViewDto implements Serializable {

    private static final long serialVersionUID = -665289174897703187L;

    @ApiObjectField(description = "Card id")
    private UUID id;

    @ApiObjectField(description = "Card number")
    private String number;
}
