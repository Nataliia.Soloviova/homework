package com.example.demo.dto;

import lombok.Data;
import org.jsondoc.core.annotation.ApiObjectField;

import java.io.Serializable;
import java.util.UUID;

@Data
public class BillViewDto implements Serializable {

    private static final long serialVersionUID = 5651309484148151864L;

    @ApiObjectField(description = "Bill id")
    private UUID id;

    @ApiObjectField(description = "Bill title")
    private String title;

    @ApiObjectField(description = "Bill balance")
    private Double balance;

    @ApiObjectField(description = "Client id")
    private UUID clientId;
}
