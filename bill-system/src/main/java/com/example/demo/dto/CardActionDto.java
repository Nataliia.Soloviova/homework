package com.example.demo.dto;

import com.example.demo.model.enums.CardAction;
import lombok.Data;
import org.jsondoc.core.annotation.ApiObjectField;

import java.io.Serializable;

@Data
public class CardActionDto implements Serializable {

    private static final long serialVersionUID = -3440520993935829944L;

    @ApiObjectField(description = "Card number")
    private String number;

    @ApiObjectField(description = "Refill amount")
    private Double amount;

    @ApiObjectField(description = "Card action")
    private CardAction cardAction;
}