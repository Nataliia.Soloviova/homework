package com.example.demo.dto;

import com.example.demo.model.enums.UserRole;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiObject(name = "UserCreateDto")
public class UserCreateDto implements Serializable {

    private static final long serialVersionUID = 7088099974891967995L;

    @ApiObjectField(description = "User's first name")
    private String firstName;

    @ApiObjectField(description = "User's first name")
    private String password;

    @ApiObjectField(description = "User's first name")
    private String userRole;
}
