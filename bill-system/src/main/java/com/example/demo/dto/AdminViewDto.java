package com.example.demo.dto;

import lombok.Data;
import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;

import java.io.Serializable;
import java.util.UUID;

@Data
@ApiObject(name = "AdminViewDto")
public class AdminViewDto implements Serializable {

    private static final long serialVersionUID = 5698535348254224607L;

    @ApiObjectField(description = "Admin id")
    private UUID id;

    @ApiObjectField(description = "Admin first name")
    private String firstName;

    @ApiObjectField(description = "Admin status")
    private Boolean enabled;
}
