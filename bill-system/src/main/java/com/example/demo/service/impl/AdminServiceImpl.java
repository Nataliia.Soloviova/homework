package com.example.demo.service.impl;

import com.example.demo.converter.AdminConverter;
import com.example.demo.dto.AdminCreateDto;
import com.example.demo.dto.AdminViewDto;
import com.example.demo.model.Admin;
import com.example.demo.model.enums.UserRole;
import com.example.demo.repository.AdminRepository;
import com.example.demo.service.AdminService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class AdminServiceImpl implements AdminService {

    private final AdminRepository adminRepository;
    private final AdminConverter adminConverter;

    @Override
    public AdminViewDto createUserWithRoleAdmin(AdminCreateDto adminCreateDto) {
        Admin admin = adminConverter.convert(adminCreateDto);
        admin.setUserRole(UserRole.ADMIN);
        adminRepository.save(admin);

        return adminConverter.convert(admin);
    }

    @Override
    public List<AdminViewDto> getAllAdmins() {
        return adminRepository.findAll()
                .stream()
                .map(adminConverter::convert)
                .collect(Collectors.toList());
    }
}
