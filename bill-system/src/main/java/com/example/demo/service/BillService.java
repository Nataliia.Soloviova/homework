package com.example.demo.service;


import com.example.demo.dto.BillViewDto;
import com.example.demo.model.Bill;

import java.util.Set;
import java.util.UUID;

public interface BillService {

    Set<BillViewDto> getAllBillsByClientId(UUID clientId);

    Bill findById(UUID id);
}
