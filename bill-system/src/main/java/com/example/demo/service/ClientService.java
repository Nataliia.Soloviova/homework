package com.example.demo.service;

import com.example.demo.dto.ClientCreateDto;
import com.example.demo.dto.ClientViewDto;
import com.example.demo.model.Client;

import java.util.List;
import java.util.UUID;

public interface ClientService {

    ClientViewDto createUserWithRoleClient(ClientCreateDto clientCreateDto);

    List<ClientViewDto> getAllClients();

    Client findById(UUID id);

}
