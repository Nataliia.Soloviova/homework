package com.example.demo.service;

import com.example.demo.dto.AdminCreateDto;
import com.example.demo.dto.AdminViewDto;

import java.util.List;

public interface AdminService {

    AdminViewDto createUserWithRoleAdmin(AdminCreateDto adminCreateDto);

    List<AdminViewDto> getAllAdmins();
}
