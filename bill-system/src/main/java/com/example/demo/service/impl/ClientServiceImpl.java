package com.example.demo.service.impl;

import com.example.demo.converter.ClientConverter;
import com.example.demo.dto.ClientCreateDto;
import com.example.demo.dto.ClientViewDto;
import com.example.demo.exception.EntityAlreadyExists;
import com.example.demo.exception.EntityNotFoundException;
import com.example.demo.model.Client;
import com.example.demo.model.enums.UserRole;
import com.example.demo.repository.ClientRepository;
import com.example.demo.service.ClientService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;
    private final ClientConverter clientConverter;

    @Override
    public ClientViewDto createUserWithRoleClient(ClientCreateDto clientCreateDto) {
        Client client = clientConverter.convert(clientCreateDto);

        if (clientRepository.findByPassport(client.getPassport()) != null) {
            throw new EntityAlreadyExists("Client with this passport already exists in the system");
        }

        client.setEnabled(Boolean.TRUE);
        client.setUserRole(UserRole.CLIENT);
        clientRepository.save(client);

        return clientConverter.convert(client);
    }

    @Override
    public List<ClientViewDto> getAllClients() {
        return clientRepository.findAll()
                .stream()
                .map(clientConverter::convert)
                .collect(Collectors.toList());
    }

    @Override
    public Client findById(UUID id) {
        return clientRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Client hasn't been found"));
    }
}
