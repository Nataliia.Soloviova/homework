package com.example.demo.service.impl;

import com.example.demo.converter.BillConverter;
import com.example.demo.dto.BillViewDto;
import com.example.demo.exception.EntityNotFoundException;
import com.example.demo.model.Bill;
import com.example.demo.repository.BillRepository;
import com.example.demo.repository.CardRepository;
import com.example.demo.service.BillService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class BillServiceImpl implements BillService {

    private final BillRepository billRepository;
    private final BillConverter billConverter;
    private final CardRepository cardRepository;

    @Override
    public Set<BillViewDto> getAllBillsByClientId(UUID clientId) {
        return billRepository.findAllByClientId(clientId)
                .stream()
                .map(billConverter::convert)
                .collect(Collectors.toSet());
    }

    @Override
    public Bill findById(UUID id) {
        return billRepository.findById(id).orElseThrow(
                () -> new EntityNotFoundException("Bill hasn't been found"));
    }
}
