package com.example.demo.service;

import com.example.demo.model.User;

import java.util.UUID;

public interface UserService {

    User create(User user);

    User update(User user);

    User findById(UUID id);

    void deleteById(UUID id);
}
