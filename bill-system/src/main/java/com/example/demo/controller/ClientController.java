package com.example.demo.controller;

import com.example.demo.dto.BillCreateDto;
import com.example.demo.dto.BillViewDto;
import com.example.demo.facade.BillFacade;
import com.example.demo.service.BillService;
import lombok.AllArgsConstructor;
import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiBodyObject;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiPathParam;
import org.jsondoc.core.annotation.ApiResponseObject;
import org.jsondoc.core.pojo.ApiStage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Set;
import java.util.UUID;

@RestController
@Api(name = "Client API", description = "Provides a list of methods that manage client", stage = ApiStage.ALPHA)
@RequestMapping("/clients")
@AllArgsConstructor
public class ClientController {

    private final BillFacade billFacade;
    private final BillService billService;

    @RequestMapping(value = "{clientId}/bills", method = RequestMethod.POST)
    @ApiMethod(description = "Add new bill for the client")
    public @ApiResponseObject
    @ResponseBody
    ResponseEntity<BillViewDto> createBillForClient(
            @ApiPathParam @PathVariable("clientId") UUID clientId,
            @ApiBodyObject @RequestBody @Valid BillCreateDto billCreateDto) {
        BillViewDto billViewDto = billFacade.createBillForClient(billCreateDto, clientId);

        return new ResponseEntity<>(billViewDto, HttpStatus.OK);
    }

    @RequestMapping(value = "{clientId}/bills", method = RequestMethod.GET)
    @ApiMethod(description = "Get all client bills")
    public @ApiResponseObject
    @ResponseBody
    ResponseEntity<Set<BillViewDto>> getBillsByClientId(@ApiPathParam @PathVariable("clientId") UUID clientId) {

        return new ResponseEntity<>(billService.getAllBillsByClientId(clientId), HttpStatus.OK);
    }
}
