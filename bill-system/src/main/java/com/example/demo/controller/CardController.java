package com.example.demo.controller;

import com.example.demo.dto.CardActionDto;
import com.example.demo.facade.CardFacade;
import lombok.AllArgsConstructor;
import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiBodyObject;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.pojo.ApiStage;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@Api(name = "Card API", description = "Provides a list of methods that manage card", stage = ApiStage.ALPHA)
@RequestMapping("/cards")
@AllArgsConstructor
public class CardController {

    private final CardFacade cardFacade;

    @RequestMapping(value = "action", method = RequestMethod.POST)
    @ApiMethod(description = "Add or get money to/from the card")
    @ResponseStatus(HttpStatus.OK)
    public void addOrGetMoney(@ApiBodyObject @RequestBody @Valid CardActionDto cardActionDto) {
        cardFacade.addOrGetMoney(cardActionDto);
    }
}
