package com.example.demo.controller;

import com.example.demo.dto.AdminCreateDto;
import com.example.demo.dto.AdminViewDto;
import com.example.demo.dto.ClientCreateDto;
import com.example.demo.dto.ClientViewDto;
import com.example.demo.service.AdminService;
import com.example.demo.service.ClientService;
import lombok.AllArgsConstructor;
import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiBodyObject;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiResponseObject;
import org.jsondoc.core.pojo.ApiStage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@Api(name = "User API", description = "Provides a list of methods that manage admins and clients", stage = ApiStage.ALPHA)
@RequestMapping("/users")
@AllArgsConstructor
public class UserController {

    private final AdminService adminService;
    private final ClientService clientService;

    @RequestMapping(value = "/admin", method = RequestMethod.POST)
    @ApiMethod(description = "Add new admin")
    public @ApiResponseObject
    @ResponseBody
    ResponseEntity<AdminViewDto> createUserWithRoleAdmin(@ApiBodyObject @RequestBody @Valid AdminCreateDto adminCreateDto) {
        AdminViewDto adminViewDto = adminService.createUserWithRoleAdmin(adminCreateDto);

        return new ResponseEntity<>(adminViewDto, HttpStatus.OK);
    }

    @RequestMapping(value = "/admins", method = RequestMethod.GET)
    @ApiMethod(description = "Get all admins")
    public @ApiResponseObject
    @ResponseBody
    ResponseEntity<List<AdminViewDto>> getAdmins() {

        return new ResponseEntity<>(adminService.getAllAdmins(), HttpStatus.OK);
    }

    @RequestMapping(value = "/client", method = RequestMethod.POST)
    @ApiMethod(description = "Add new client")
    public @ApiResponseObject
    @ResponseBody
    ResponseEntity<ClientViewDto> createUserWithRoleClient(@ApiBodyObject @RequestBody @Valid ClientCreateDto clientCreateDto) {
        ClientViewDto clientViewDto = clientService.createUserWithRoleClient(clientCreateDto);

        return new ResponseEntity<>(clientViewDto, HttpStatus.OK);
    }

    @RequestMapping(value = "/clients", method = RequestMethod.GET)
    @ApiMethod(description = "Get all clients")
    public @ApiResponseObject
    @ResponseBody
    ResponseEntity<List<ClientViewDto>> getClients() {

        return new ResponseEntity<>(clientService.getAllClients(), HttpStatus.OK);
    }
}
