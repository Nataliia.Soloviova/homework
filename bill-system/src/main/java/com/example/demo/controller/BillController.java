package com.example.demo.controller;

import com.example.demo.dto.CardCreateDto;
import com.example.demo.dto.CardViewDto;
import com.example.demo.facade.CardFacade;
import lombok.AllArgsConstructor;
import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiBodyObject;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiPathParam;
import org.jsondoc.core.annotation.ApiResponseObject;
import org.jsondoc.core.pojo.ApiStage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@Api(name = "Bill API", description = "Provides a list of methods that manage bill", stage = ApiStage.ALPHA)
@RequestMapping("/bills")
@AllArgsConstructor
public class BillController {

    private final CardFacade cardFacade;

    @RequestMapping(value = "{billId}/card", method = RequestMethod.POST)
    @ApiMethod(description = "Add new card to the bill")
    public @ApiResponseObject
    @ResponseBody
    ResponseEntity<CardViewDto> addCardForBill(
            @ApiPathParam @PathVariable("billId") UUID billId,
            @ApiBodyObject @RequestBody @Valid CardCreateDto cardCreateDto) {
        CardViewDto cardViewDto = cardFacade.addCardForBill(cardCreateDto, billId);

        return new ResponseEntity<>(cardViewDto, HttpStatus.OK);
    }
}
