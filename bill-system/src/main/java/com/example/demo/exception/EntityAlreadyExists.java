package com.example.demo.exception;

public class EntityAlreadyExists extends RuntimeException {

    public EntityAlreadyExists(String message) {
        super(message);
    }

}
