package com.example.demo.utils.constants;

public class UserConstraints {

    public static final int MIN_NAME_LENGTH = 2;
    public static final int MAX_NAME_LENGTH = 50;
}
