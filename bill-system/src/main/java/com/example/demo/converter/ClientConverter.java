package com.example.demo.converter;

import com.example.demo.dto.ClientCreateDto;
import com.example.demo.dto.ClientViewDto;
import com.example.demo.model.Bill;
import com.example.demo.model.Client;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class ClientConverter {

    public Client convert(ClientCreateDto clientCreateDto) {
        Client client = new Client();
        client.setPassport(clientCreateDto.getPassport());
        client.setFirstName(clientCreateDto.getFirstName());
        client.setPassword(clientCreateDto.getPassword());

        return client;
    }

    public ClientViewDto convert(Client client) {
        ClientViewDto clientViewDto = new ClientViewDto();
        clientViewDto.setId(client.getId());
        clientViewDto.setFirstName(client.getFirstName());
        clientViewDto.setEnabled(client.getEnabled());
        clientViewDto.setCardNumbers(
                client.getBills()
                        .stream()
                        .map(Bill::getId)
                        .collect(Collectors.toSet())
        );

        return clientViewDto;
    }
}
