package com.example.demo.converter;

import com.example.demo.dto.BillCreateDto;
import com.example.demo.dto.BillViewDto;
import com.example.demo.model.Bill;
import org.springframework.stereotype.Component;

@Component
public class BillConverter {

    public Bill convert(BillCreateDto billCreateDto) {
        Bill bill = new Bill();
        bill.setTitle(billCreateDto.getTitle());

        return bill;
    }

    public BillViewDto convert(Bill bill) {
        BillViewDto billViewDto = new BillViewDto();
        billViewDto.setId(bill.getId());
        billViewDto.setTitle(bill.getTitle());
        billViewDto.setBalance(bill.getBalance());
        billViewDto.setClientId(bill.getClient().getId());

        return billViewDto;
    }
}
