package com.example.demo.converter;

import com.example.demo.dto.AdminCreateDto;
import com.example.demo.dto.AdminViewDto;
import com.example.demo.model.Admin;
import org.springframework.stereotype.Component;

@Component
public class AdminConverter {

    public Admin convert(AdminCreateDto adminCreateDto) {
        Admin admin = new Admin();
        admin.setFirstName(adminCreateDto.getFirstName());
        admin.setPassword(adminCreateDto.getPassword());

        return admin;
    }

    public AdminViewDto convert(Admin admin) {
        AdminViewDto adminViewDto = new AdminViewDto();
        adminViewDto.setId(admin.getId());
        adminViewDto.setFirstName(admin.getFirstName());
        adminViewDto.setEnabled(admin.getEnabled());

        return adminViewDto;
    }
}
