package com.example.demo.converter;

import com.example.demo.dto.CardCreateDto;
import com.example.demo.dto.CardViewDto;
import com.example.demo.model.Card;
import org.springframework.stereotype.Component;

@Component
public class CardConverter {

    public Card convert(CardCreateDto cardCreateDto) {
        Card card = new Card();
        card.setNumber(cardCreateDto.getNumber());

        return card;
    }

    public CardViewDto convert(Card card) {
        CardViewDto cardViewDto = new CardViewDto();
        cardViewDto.setId(card.getBill().getId());
        cardViewDto.setNumber(card.getNumber());

        return cardViewDto;
    }
}
