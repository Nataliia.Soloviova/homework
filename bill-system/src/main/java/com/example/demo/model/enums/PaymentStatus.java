package com.example.demo.model.enums;

import org.jsondoc.core.annotation.ApiObject;

@ApiObject(name = "Payment Status")
public enum PaymentStatus {
    PREPARED,
    SENT;
}
