package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "admin")
@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Admin extends User {
}
