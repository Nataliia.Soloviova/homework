package com.example.demo.model.enums;

import org.jsondoc.core.annotation.ApiObject;

@ApiObject(name = "User role")
public enum UserRole {
    ADMIN,
    CLIENT;
}
