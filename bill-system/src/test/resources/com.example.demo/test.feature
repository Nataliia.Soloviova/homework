Feature: Post Functionality

  Scenario Outline: I want to create a client with valid data
    Given Client with first name <first_name>, password <password>, passport <passport>
    When I call "users/client"
    Then I should see new client

    Examples:
      | first_name | password  | passport |
      | Jack       | pass12345 | AA0000   |

  Scenario Outline: I want to create a client with invalid data
    Given Client with first name <first_name>, password <password>, passport <passport>
    When I call "users/client" with invalid data
    Then I should see an error

    Examples:
      | first_name | password  | passport |
      | null       | pass12345 | AA0001   |
      | Jack       | null      | AA0002   |
      | Jack       | pass12345 | null     |
      | 123354     | 1255rss   | AA0003   |
      | Jack       | pass12345 | 0000AA   |
      |            | pass12345 | AA0004   |
      | Jack       |           | AA0005   |
      | Jack       | pass12345 |          |