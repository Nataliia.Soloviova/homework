package com.example.demo.steps;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
public abstract class IntegrationTest {

    @Autowired
    protected MockMvc mockMvc;
}
