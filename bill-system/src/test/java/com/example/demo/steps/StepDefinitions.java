package com.example.demo.steps;


import com.example.demo.dto.ClientCreateDto;
import com.example.demo.dto.ClientViewDto;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.gherkin.internal.com.eclipsesource.json.Json;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;

import java.io.UnsupportedEncodingException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class StepDefinitions extends IntegrationTest {

    @Autowired
    private ObjectMapper objectMapper;

    private static final String PORT = "http://localhost:8080/";

    private ClientCreateDto client;
    private ResultActions resultActions;

//    @Given("Client name {string}, password {string}, passport id {string}")
//    public void clientNamePasswordPassportId(String username, String password, String passport) {
//        client = new ClientCreateDto();
//        client.setFirstName(username);
//        client.setPassport(passport);
//        client.setPassword(password);
//    }

    @Given("^Client with first name (.*), password (.*), passport (.*)$")
    public void clientFist_namePasswordPassport(String first_name, String password, String passport) {
        client = new ClientCreateDto();
        client.setFirstName(first_name);
        client.setPassword(password);
        client.setPassport(passport);
    }

    @When("I call {string}")
    public void iCall(String path) throws Exception {
        resultActions = mockMvc.perform(post(PORT + path)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(client)))
                .andExpect(status().isOk());
    }

    @Then("I should see new client")
    public void iShouldSeeNewClient() throws UnsupportedEncodingException, JsonProcessingException {
        String response = resultActions.andReturn().getResponse().getContentAsString();
        ClientViewDto result = objectMapper.readValue(response, ClientViewDto.class);
        assertNotNull(result);
        assertNotNull(result.getId());
        assertEquals(client.getFirstName(), result.getFirstName());
        assertTrue(result.getEnabled());
        assertTrue(result.getCardNumbers().isEmpty());
    }

    @When("I call {string} with invalid data")
    public void iCallWithInvalidData(String path) throws Exception {
        resultActions = mockMvc.perform(post(PORT + path)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(client)))
                .andExpect(status().isOk());
    }

    @Then("I should see an error")
    public void iShouldSeeAnError() {

    }
}
