#Checking
###Run services
``` bash
eureka-server, notification, orders, products, users, gateway
```
###Create user
``` bash
curl -X POST http://localhost:9090/users
```
###Create product
``` bash
curl -X POST http://localhost:9090/products
```
###Create order
``` bash
curl --url http://localhost:9090/orders -H "Content-Type: Application/json" -d "{\"userName\": \"SpColHARBlGrV\", \"product\": \"mWtOpPpITYLCXJPI\"}"
```
###Get notifications
``` bash
curl -X GET http://localhost:9090/notifications
```
###Get notifications without Gateway
``` bash
curl -X GET http://localhost:8484/
```


