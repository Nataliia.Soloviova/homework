package com.epam.rd.november2019.pojo;

import java.io.Serializable;

public enum DeliveryType implements Serializable {

    DTH,
    DTS,
    TRANSFER

}
