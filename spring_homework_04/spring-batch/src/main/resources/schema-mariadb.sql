DROP TABLE IF EXISTS users;

CREATE TABLE users (
    email VARCHAR(50) UNIQUE NOT NULL PRIMARY KEY,
    name VARCHAR(20) UNIQUE NOT NULL,
    balance INT(11) NOT NULL DEFAULT '0'
);