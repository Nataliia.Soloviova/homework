package com.epam.rd.november2019.config;

import com.epam.rd.november2019.batch.UserItemProcessor;
import com.epam.rd.november2019.batch.UserItemWriter;
import com.epam.rd.november2019.model.User;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.HibernateCursorItemReader;
import org.springframework.batch.item.database.builder.HibernateCursorItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.mail.internet.MimeMessage;

@Configuration
@EnableBatchProcessing
public class SpringBatchConfig {

    private static final Logger logger = LoggerFactory.getLogger(SpringBatchConfig.class);

    private final String from = "email";
    private final String password = "password";

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Bean
    public HibernateCursorItemReader<User> itemReader() {
        logger.info("Trying to read users which have balance less than 10.00$ from table: users");
        return new HibernateCursorItemReaderBuilder<User>()
                .name("userReader")
                .sessionFactory(sessionFactory)
                .queryString("from User where balance < 10")
                .build();
    }

    @Bean
    public ItemProcessor<User, MimeMessage> itemProcessor() {
        return new UserItemProcessor(from, password);
    }

    @Bean
    public ItemWriter<MimeMessage> itemWriter() {
        return new UserItemWriter(from);
    }

    @Bean
    public Step step1() {
        return stepBuilderFactory.get("step1")
                .<User, MimeMessage>chunk(10)
                .reader(itemReader())
                .processor(itemProcessor())
                .writer(itemWriter())
                .build();
    }

    @Bean(name = "firstBatchJob")
    public Job job(@Qualifier("step1") Step step1) {
        return jobBuilderFactory.get("firstBatchJob").flow(step1).end().build();
    }
}
