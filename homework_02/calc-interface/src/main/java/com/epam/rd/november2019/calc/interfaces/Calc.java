package com.epam.rd.november2019.calc.interfaces;

/**
 * The {@code Calc} interface provides four methods for
 * elementary maths operations.
 *
 * @author unascribed
 * @version 1.0
 */

public interface Calc {

    /**
     * Returns sum of the two numbers.
     *
     * @param a a number.
     * @param b another number.
     * @return result of the addition.
     */
    double addition(double a, double b);

    /**
     * Returns difference of the two numbers.
     *
     * @param a a number.
     * @param b another number.
     * @return result of the subtraction.
     */
    double subtraction(double a, double b);

    /**
     * Returns product of the two numbers.
     *
     * @param a a number.
     * @param b another number.
     * @return result of the multiplication.
     */
    double multiplication(double a, double b);

    /**
     * Returns quotient of the two numbers.
     *
     * @param a a number.
     * @param b another number.
     * @return result of the division.
     */
    double division(double a, double b);
}
