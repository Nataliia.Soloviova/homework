package com.epam.rd.november2019.calc.core;

import com.epam.rd.november2019.calc.interfaces.Calc;

/**
 * The class {@code CalcImpl} contains methods for performing
 * basic numeric operations such as the elementary addition,
 * subtraction, multiplication and division.
 *
 * The class {@code CalcImpl} implementation of the {@code List}
 * interface. Implements and overrides all operations.
 *
 * @author Natalie Soloviova
 * @see Calc
 * @version 1.0
 */

public class CalcImpl implements Calc {
    @Override
    public double addition(double a, double b) {
        return a + b;
    }

    @Override
    public double subtraction(double a, double b) {
        return a - b;
    }

    @Override
    public double multiplication(double a, double b) {
        return a * b;
    }

    @Override
    public double division(double a, double b) {
        return a / b;
    }
}
