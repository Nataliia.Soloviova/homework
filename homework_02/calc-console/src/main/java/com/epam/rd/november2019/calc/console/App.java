package com.epam.rd.november2019.calc.console;

import com.epam.rd.november2019.calc.core.CalcImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The class {@code App} demonstrates the calculating elementary
 * numeric operations such as addition, subtraction, multiplication
 * and division.
 */

public class App {
    private static final Logger logger = LoggerFactory.getLogger(App.class);

    /**
     * This method demonstrates the using of the methods
     * like addition, subtraction, multiplication and division.
     *
     * @param args two numbers and maths operator.
     * @exception NumberFormatException exception of the converting a string to number type.
     * @see NumberFormatException
     */
    public static void main(String[] args) {
        logger.info("Start program");

        double result = 0;
        if (args.length == 3) {
            double number1;
            double number2;
            String operator = args[2];

            try {
                number1 = Double.parseDouble(args[0]);
                number2 = Double.parseDouble(args[1]);
                logger.info("Assign values: number1, number2, operator");
            } catch (NumberFormatException e) {
                logger.error("Invalid input data! Please, enter two numbers first and then maths operator!");
                return;
            }

            CalcImpl calc = new CalcImpl();
            switch (operator) {
                case "+":
                    result = calc.addition(number1, number2);
                    logger.info("Addition");
                    break;
                case "-":
                    result = calc.subtraction(number1, number2);
                    logger.info("Subtraction");
                    break;
                case "*":
                    result = calc.multiplication(number1, number2);
                    logger.info("Multiplication");
                    break;
                case "/":
                    if (number2 == 0) {
                        logger.warn("Division by zero!");
                    }
                    result = calc.division(number1, number2);
                    logger.info("Division");
                    break;
                default:
                    logger.error("Invalid operator! Try again, please!");
                    break;
            }
            logger.info("number1={} number2={} operator={} result={}", number1, number2, operator, result);
            logger.info("Finish program");
        }
        else {
            logger.error("Invalid input data! Please, enter two numbers and maths operator!");
        }
    }
}
