# homework_02
## This program simulates simple calculator

* To build project execute in cmd:
> mvn clean install
* To run application execute in cmd:
> java -jar .\calc-console\target\calculator-jar-with-dependencies.jar number1 number2 operator
