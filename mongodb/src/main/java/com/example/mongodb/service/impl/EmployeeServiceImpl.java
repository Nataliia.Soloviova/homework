package com.example.mongodb.service.impl;

import com.example.mongodb.model.Employee;
import com.example.mongodb.repository.EmployeeRepository;
import com.example.mongodb.service.EmployeeService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    @Override
    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    @Override
    public void create(Employee employee) {
        employeeRepository.save(employee);
    }

    @Override
    public Employee update(Employee employee) {
     findById(employee.getId());
     return employeeRepository.save(employee);
    }

    @Override
    public Employee findById(String id) {
        return employeeRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Employee doesn't exist"));
    }

    @Override
    public void delete(String employeeId) {
        findById(employeeId);
        employeeRepository.deleteById(employeeId);
    }
}
