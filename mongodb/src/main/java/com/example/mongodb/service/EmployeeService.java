package com.example.mongodb.service;

import com.example.mongodb.model.Employee;

import java.util.List;

public interface EmployeeService {

    List<Employee> findAll();

    void create(Employee employee);

    Employee update(Employee employee);

    Employee findById(String id);

    void delete(String employeeId);
}
