package com.example.mongodb.service.impl;

import com.example.mongodb.data.EmployeeDataGenerator;
import com.example.mongodb.model.Employee;
import com.example.mongodb.repository.EmployeeRepository;
import com.example.mongodb.service.EmployeeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class EmployeeServiceImplIT {

    @Autowired
    private EmployeeService sut;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private EmployeeDataGenerator employeeDataGenerator;

    @Test
    void shouldFindAllEmployees() {
        //GIVEN
        Employee employee = employeeDataGenerator.employee();
        employeeRepository.save(employee);

        //WHEN
        List<Employee> result = sut.findAll();

        //THEN
        assertNotNull(result);
        assertEquals(1, result.size());
        assertTrue(result.contains(employee));
    }

    @Test
    void shouldCreateEmployee() {
        //GIVEN
        Employee employee = employeeDataGenerator.employee();

        //WHEN
        sut.create(employee);
        Employee result = employeeRepository.findById(employee.getId()).get();

        //THEN
        assertNotNull(result);
        assertEquals(employee, result);
    }

    @Test
    void shouldUpdateEmployee() {
        //GIVEN
        Employee employee = employeeDataGenerator.employee();
        employeeRepository.save(employee);

        employee.setLastName("Jackson");

        //WHEN
        Employee result = sut.update(employee);

        //THEN
        assertNotNull(result);
        assertEquals(employee, result);
    }

    @Test
    void shouldDeleteEmployee() {
        //GIVEN
        Employee employee = employeeDataGenerator.employee();
        employeeRepository.save(employee);

        //WHEN
        sut.delete(employee.getId());
        IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () ->
            sut.delete(employee.getId()));

        //THEN
        assertEquals("Employee doesn't exist", ex.getMessage());
    }
}