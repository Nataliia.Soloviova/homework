package com.example.mongodb.data;

import com.example.mongodb.model.Address;
import com.example.mongodb.model.Employee;
import org.springframework.stereotype.Component;

@Component
public class EmployeeDataGenerator {

    public Employee employee() {
        Employee employee = new Employee();
        employee.setFirstName("John");
        employee.setLastName("Potter");

        Address address = new Address();
        address.setStreet("Yellow");
        address.setCity("London");
        address.setCountry("United Kingdom");
        employee.setAddress(address);

        return employee;
    }
}
