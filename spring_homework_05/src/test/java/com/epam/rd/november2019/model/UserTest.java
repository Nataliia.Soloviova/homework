package com.epam.rd.november2019.model;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UserTest {

    private final static String DEFAULT_EMAIL = "ivan_ivanov@corp.com";
    private final static String DEFAULT_NAME = "ivan_ivanov";
    private final static int DEFAULT_BALANCE = 25;

    @Test
    public void testLombokAnnotation() {
        User user = new User();
        user.setName(DEFAULT_NAME);
        user.setEmail(DEFAULT_EMAIL);
        user.setBalance(DEFAULT_BALANCE);
        assertEquals(user.getName(), DEFAULT_NAME);
        assertEquals(user.getEmail(), DEFAULT_EMAIL);
        assertEquals(user.getBalance(), DEFAULT_BALANCE);
    }

}