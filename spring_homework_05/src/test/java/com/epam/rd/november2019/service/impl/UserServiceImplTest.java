package com.epam.rd.november2019.service.impl;

import com.epam.rd.november2019.model.User;
import com.epam.rd.november2019.repository.UserRepository;
import com.epam.rd.november2019.service.UserService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class UserServiceImplTest {

    @InjectMocks
    UserServiceImpl userService;

    @Mock
    UserRepository userRepository;

    @BeforeEach
    void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllUsersTest() {
        //GIVEN
        List<User> list = new ArrayList<User>();
        User user1 = new User("ivan_ivanov@gmail.com", "Ivan", 20);
        User user2 = new User("tom_tomas@gmail.com", "Tom", 600);
        User user3 = new User("petr_petrov@gmail.com", "Petr", 250);
        list.add(user1);
        list.add(user2);
        list.add(user3);
        when(userRepository.findAll()).thenReturn(list);
        //WHEN
        List<User> userList = userService.getAllUsers();
        //THEN
        assertEquals(3, userList.size());
        verify(userRepository, times(1)).findAll();
    }
//
//    @Test
//    public void getUserByEmailTest() {
//        //GIVEN
//        when(userRepository.findById("ivan_ivanov@gmail.com")
//                .orElseThrow(() -> new IllegalArgumentException("Invalid user email")))
//                .thenReturn(new User("ivan_ivanov@gmail.com", "Ivan", 12));
//        //WHEN
//        User user = userService.getUserByEmail("ivan_ivanov@gmail.com");
//        //THEN
//        assertEquals("ivan_ivanov@gmail.com", user.getEmail());
//        assertEquals("Ivan", user.getName());
//        assertEquals(12, user.getBalance());
//    }
}