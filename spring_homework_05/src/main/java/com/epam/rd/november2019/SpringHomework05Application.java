package com.epam.rd.november2019;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class SpringHomework05Application {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(SpringHomework05Application.class, args);
	}
}
