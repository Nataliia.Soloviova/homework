package com.epam.rd.november2019.actuator;

import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.health.Health;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.HashMap;

@Component
@Endpoint(id = "my-health-check")
public class HealthCheck {

    @ReadOperation
    public Map<String, String> showParameters() throws IOException, XmlPullParserException {
        Map<String, String> map = new HashMap<>();

        Health health = check() ? Health.up().build() : Health.down().build();

        MavenXpp3Reader reader = new MavenXpp3Reader();
        Model model = reader.read(new FileReader("pom.xml"));

        map.put("Status", health.getStatus().toString());
        map.put("Application:", model.getArtifactId());

        return map;
    }

    public boolean check() {
        // Some logic to check health
        return true;
    }
}
