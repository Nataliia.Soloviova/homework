package com.epam.rd.november2019.model;

import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "users")
@EntityListeners(AuditingEntityListener.class)
@Data
public class User {

    @Id
    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "balance", nullable = false)
    private int balance;

    @Column(name = "name", nullable = false)
    private String name;

    public User() {
    }

    public User(String email, String name, int balance) {
        this.email = email;
        this.balance = balance;
        this.name = name;
    }
}
