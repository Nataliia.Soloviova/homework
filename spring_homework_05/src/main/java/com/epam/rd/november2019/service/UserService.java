package com.epam.rd.november2019.service;

import com.epam.rd.november2019.model.User;

import java.util.List;

public interface UserService {

    List<User> getAllUsers();

    User getUserByEmail(String email);
}
