package com.epam.rd.november2019.controller;

import com.epam.rd.november2019.model.User;
import com.epam.rd.november2019.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/users")
    public List<User> users() {

        return this.userService.getAllUsers();
    }

    @GetMapping("users/{email}")
    public ResponseEntity<User> userByEmail(@PathVariable(value = "email") String email) {

        return ResponseEntity.ok().body(this.userService.getUserByEmail(email));
    }
}
