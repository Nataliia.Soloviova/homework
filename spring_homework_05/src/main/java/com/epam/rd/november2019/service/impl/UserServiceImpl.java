package com.epam.rd.november2019.service.impl;

import com.epam.rd.november2019.model.User;
import com.epam.rd.november2019.repository.UserRepository;
import com.epam.rd.november2019.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User getUserByEmail(String email) {
        return userRepository.findById(email)
                .orElseThrow(() -> new IllegalArgumentException("Invalid user email : " + email));
    }
}
