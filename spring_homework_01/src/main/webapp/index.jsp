<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${cookie['lang'].value}"/>
<fmt:setBundle basename="messages" />

<!DOCTYPE html>

<html lang="${cookie['lang'].value}">
    <head>
         <title>Main page</title>

         <style>
            input, button {
                width: 300px;
                height: 30px;
            }
         </style>
    </head>

    <body>

    <div align="center">
        <p>
            		<button>
            		    <a href="?locale=en_US">
            		        <fmt:message key="label.lang.en" />
            		    </a>
            		</button>
        </p>
        <p>
            		<button>
            		    <a href="?locale=ru_RU">
            		        <fmt:message key="label.lang.ru" />
            		    </a>
            		</button>
        </p>
        <p>
        <c:if test="${not empty param.locale}">
                	<button><a href="${pageContext.request.contextPath}/login">Lets start</a></button>
                </c:if>
        </p>
        </div>

    </body>
</html>