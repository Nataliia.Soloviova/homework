<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
    <head>

        <title>AdministratorTask</title>

    </head>
    <body>

        <jsp:include page="menu.jsp"></jsp:include>

        <h1>This is administrator page!</h1>

                    <div>
                        <ul>
                            <li>
                                <a href="${pageContext.request.contextPath}/employees">
                                    Employees
                                </a>
                            </li>
                            <li>
                                <a href="${pageContext.request.contextPath}/flights">
                                    Flights
                                </a>
                            </li>
                        </ul>
                    </div>

    </body>
</html>