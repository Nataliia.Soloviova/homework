package com.epam.rd.november2019.web.controller;

import com.epam.rd.november2019.model.EmployeeRole;
import com.epam.rd.november2019.model.Sex;
import com.epam.rd.november2019.services.EmployeeService;
import com.epam.rd.november2019.web.dto.EmployeeCreateDto;
import com.epam.rd.november2019.web.dto.EmployeeViewDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/employees")
    public String allEmployees(Model model) {
        model.addAttribute("employee", new EmployeeViewDto());
        model.addAttribute("employees", employeeService.getAllEmployees());

        return "employees";
    }

    @PostMapping(value = "/employees/add")
    public String addEmployee(HttpServletRequest req, Model model) {
        try {
            EmployeeCreateDto createDto = extractEmployeeFromRequest(req);
            if (req.getParameter("employeeId") == null) {
                employeeService.addEmployee(createDto);
            } else {
                employeeService.editEmployee(Integer.parseInt(req.getParameter("employeeId")), createDto);
            }
        } catch (IllegalArgumentException e) {
            model.addAttribute("addingErrorMessage", e.getMessage());
        }

        return "redirect:/employees";
    }

    private EmployeeCreateDto extractEmployeeFromRequest(HttpServletRequest req) {
        String lastName = req.getParameter("lastName");
        String firstName = req.getParameter("firstName");
        EmployeeRole employeeRole = EmployeeRole.valueOf(req.getParameter("employeeRole").toUpperCase());
        Sex sex = Sex.valueOf(req.getParameter("sex").toUpperCase());
        String city = req.getParameter("city");
        return new EmployeeCreateDto(lastName, firstName, employeeRole, sex, city);
    }

    @RequestMapping("/deleteEmployee/{id}")
    public String removeEmployee(@PathVariable("id") int id) {
        employeeService.removeEmployee(id);

        return "redirect:/employees";
    }

    @RequestMapping("editEmployee/{id}")
    public String editEmployee(@PathVariable("id") int id, Model model) {
        model.addAttribute("employee", employeeService.getEmployeeById(id));
        model.addAttribute("employees", employeeService.getAllEmployees());

        return "/employees";
    }

    @PostMapping(value = "/editEmployee/{id}")
    public String editEmployee(@PathVariable("id") int id, HttpServletRequest req, Model model) {
        EmployeeCreateDto createDto = extractEmployeeFromRequest(req);
        employeeService.editEmployee(id, createDto);

        return "redirect:/employees";
    }

    @PostMapping("/employees")
    public String findEmployee(HttpServletRequest req, Model model) {
        List<EmployeeViewDto> employees = employeeService.findEmployees(extractEmployeeFromRequest(req));
        model.addAttribute("employees", employees);

        return "/employees";
    }
}
