package com.epam.rd.november2019.services.impl;

import com.epam.rd.november2019.model.UserAccount;
import com.epam.rd.november2019.services.SecurityService;
import org.springframework.stereotype.Service;

@Service
public class SecurityServiceImpl implements SecurityService {

    @Override
    public boolean isCorrectPassword(UserAccount user, String password) {
        return user.getPassword().equals(password);
    }
}
