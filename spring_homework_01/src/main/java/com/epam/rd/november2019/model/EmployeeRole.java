package com.epam.rd.november2019.model;

public enum EmployeeRole {
    PILOT, NAVIGATOR, RADIO_OPERATOR, STEWARDESS;
}
