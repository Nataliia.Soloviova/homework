package com.epam.rd.november2019.dao;

import com.epam.rd.november2019.exception.ApplicationException;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

@Component
public class ConnectionDB {

    public static Connection getConnection() throws SQLException {
        Properties props = new Properties();
        try (InputStream in =
                     ConnectionDB.class.getClassLoader().getResourceAsStream("db.properties")) {
            props.load(in);
        } catch (IOException e) {
            throw new ApplicationException("Connection isn't correct");
        }
        String drivers = props.getProperty("db.driver");
        if (drivers != null) {
            System.setProperty("db.driver", drivers);
        }
        String url = props.getProperty("db.url");
        String username = props.getProperty("db.user");
        String password = props.getProperty("db.password");

        return DriverManager.getConnection(url, username, password);
    }
}
