package com.epam.rd.november2019.model;

public enum Sex {
    MAN, WOMAN;
}
