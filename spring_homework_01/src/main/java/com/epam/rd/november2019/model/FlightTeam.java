package com.epam.rd.november2019.model;

import java.util.List;
import java.util.Objects;

public class FlightTeam {

    private List<Integer> employeeIds;

    public FlightTeam(List<Integer> employeeIds) {
        this.employeeIds = employeeIds;
    }

    public List<Integer> getEmployeeIds() {
        return employeeIds;
    }

    public void setEmployeeIds(List<Integer> employeeIds) {
        this.employeeIds = employeeIds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FlightTeam that = (FlightTeam) o;
        return Objects.equals(employeeIds, that.employeeIds);
    }

    @Override
    public int hashCode() {
        return Objects.hash(employeeIds);
    }

    @Override
    public String toString() {
        return employeeIds.toString();
    }
}
