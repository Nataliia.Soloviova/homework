package com.epam.rd.november2019.web.dto;

import com.epam.rd.november2019.model.UserRole;

import java.io.Serializable;

public class UserViewDto implements Serializable {

    private static final long serialVersionUID = 5461630002087134088L;

    private String email;
    private String userName;
    private UserRole userRole;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    @Override
    public String toString() {
        return "UserViewDto{" +
                "email='" + email + '\'' +
                ", userName='" + userName + '\'' +
                ", userRole=" + userRole +
                '}';
    }
}
