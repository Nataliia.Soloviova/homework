package com.epam.rd.november2019.web.advice;


import com.epam.rd.november2019.exception.EmployeeAlreadyExistException;
import com.epam.rd.november2019.exception.FlightAlreadyExistException;
import com.epam.rd.november2019.exception.UserAlreadyExistException;
import com.epam.rd.november2019.exception.ValidationException;
import com.epam.rd.november2019.service.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.SQLException;

@ControllerAdvice
public class ExceptionControllerAdvice {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionControllerAdvice.class);

    private EmployeeService employeeService;

    @Autowired
    public ExceptionControllerAdvice(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @ExceptionHandler(ValidationException.class)
    public String handleValidationException(HttpServletRequest req, ValidationException ex) {
        LOGGER.info("Error: " + ex.getMessage());
        req.setAttribute("errorMessage", ex.getMessage());

        return req.getRequestURI();
    }

    @ExceptionHandler(UserAlreadyExistException.class)
    public String handleUserAlreadyExistException(HttpServletRequest req, UserAlreadyExistException ex) {
        req.setAttribute("errorMessage", ex.getMessage());

        return "/login";
    }

    @ExceptionHandler(FlightAlreadyExistException.class)
    public String handleFlightAlreadyExistException(HttpServletRequest req, FlightAlreadyExistException ex) {
        req.setAttribute("errorMessage", ex.getMessage());
//        req.setAttribute("flights", flightService.getAllFlights());

        return "/flights";
    }

    @ExceptionHandler(EmployeeAlreadyExistException.class)
    public String handleEmployeeAlreadyExistException(HttpServletRequest req, EmployeeAlreadyExistException ex) {
        req.setAttribute("errorMessage", ex.getMessage());
        req.setAttribute("employees", employeeService.getAllEmployees());

        return "/employees";
    }

    @ExceptionHandler(SQLException.class)
    public String handleSQLException(HttpServletRequest req, SQLException ex) {
        LOGGER.info("SQLException Occured: {}; URL={}", ex.getMessage(), req.getRequestURL());

        return "database_error";
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "IOException occured")
    @ExceptionHandler(IOException.class)
    public void handleIOException() {
        LOGGER.error("IOException handler executed");
    }
}
