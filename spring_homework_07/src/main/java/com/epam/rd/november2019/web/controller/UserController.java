package com.epam.rd.november2019.web.controller;

import com.epam.rd.november2019.model.UserRole;
import com.epam.rd.november2019.service.UserService;
import com.epam.rd.november2019.web.dto.UserCreateDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Controller
public class UserController implements WebMvcConfigurer {

    @Autowired
    private UserService userService;

    @GetMapping("/")
    public String mainPage() {

        return "redirect:/login";
    }

    @GetMapping("/login")
    public String loginPage() {

        return "login";
    }

    @GetMapping("/registration")
    public String registrationPage(Model model) {

        return "registration";
    }

    @PostMapping("/registration")
    public String registration(@RequestParam("userName") String userName,
                               @RequestParam("password") String password,
                               @RequestParam("email") String email,
                               @RequestParam("userRole") String userRole,
                               Model model) {
        UserCreateDto userCreateDto = new UserCreateDto(email, userName, password, UserRole.valueOf(userRole.toUpperCase()));
        userService.registerUser(userCreateDto);

        model.addAttribute("message", "You are successfully registered. Please, log in.");

        return "registration";
    }

    @GetMapping("/profile")
    public String profilePage() {

        return "profile";
    }

    @GetMapping("/admin")
    public String adminPage() {

        return "admin_menu";
    }

    @GetMapping("/dispatcher")
    public String dispatcherPage() {

        return "dispatcher_menu";
    }

    @GetMapping("/403")
    public String errorPage() {

        return "error/403";
    }
}
