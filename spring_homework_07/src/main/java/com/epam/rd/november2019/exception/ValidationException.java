package com.epam.rd.november2019.exception;

public class ValidationException extends ApplicationException {

    public ValidationException(String message) {
        super(message);
    }
}
