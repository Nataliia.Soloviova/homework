package com.epam.rd.november2019.dao.impl;


import com.epam.rd.november2019.dao.EmployeeDao;
import com.epam.rd.november2019.model.Employee;
import com.epam.rd.november2019.model.EmployeeRole;
import com.epam.rd.november2019.model.Sex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class EmployeeDaoImpl implements EmployeeDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeDaoImpl.class);

    private static final String ADD_EMPLOYEE = "INSERT INTO employees (last_name, first_name, sex, city, role_id) VALUES (?, ?, ?, ?, (SELECT role_id FROM employee_roles WHERE role_name=?))";

    private static final String REMOVE_EMPLOYEE = "DELETE FROM employees WHERE employee_id = ?";

    private static final String GET_BY_ID = "SELECT employee_id, last_name, first_name, city, sex, employee_roles.role_name FROM employee_roles INNER JOIN employees ON employees.role_id=employee_roles.role_id WHERE employee_id = ?;";

    private static final String GET_ID = "SELECT employee_id FROM employees WHERE last_name = ? AND first_name = ? AND sex = ? AND city = ? AND role_id = (SELECT role_id FROM employee_roles WHERE role_name = ?)";

    private static final String GET_ALL = "SELECT employee_id, last_name, first_name, city, sex, employee_roles.role_name FROM employee_roles INNER JOIN employees ON employees.role_id=employee_roles.role_id";

    private static final StringBuilder FIND_EMPLOYEE = new StringBuilder("SELECT employee_id, last_name, first_name, city, sex, employee_roles.role_name FROM employee_roles INNER JOIN employees ON employees.role_id=employee_roles.role_id");

    private static final String POINT = "employee_roles.role_id";

    private static final StringBuilder EDIT_EMPLOYEE = new StringBuilder("UPDATE employees SET first_name = ?, last_name = ?, sex = ?, city = ?, role_id = (SELECT role_id FROM employee_roles WHERE role_name = ?) WHERE employee_id = ?");

    @Autowired
    private DataSource dataSource;

    @Override
    public synchronized Employee add(Employee employee) {
        LOGGER.debug("Trying to add employee {} to employees table", employee);
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(ADD_EMPLOYEE, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, employee.getLastName());
            preparedStatement.setString(2, employee.getFirstName());
            preparedStatement.setString(3, employee.getSex().toString());
            preparedStatement.setString(4, employee.getCity());
            preparedStatement.setString(5, employee.getEmployeeRole().toString());
            preparedStatement.executeUpdate();

            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                employee.setEmployeeId(generatedKeys.getInt("employee_id"));
            }
        } catch (SQLException e) {
            LOGGER.warn("Failed to insert employee into DB");
        }
        return employee;
    }


    @Override
    public synchronized boolean edit(Employee employee) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(EDIT_EMPLOYEE.toString())) {
            preparedStatement.setString(1, employee.getFirstName());
            preparedStatement.setString(2, employee.getLastName());
            preparedStatement.setString(3, employee.getSex().toString());
            preparedStatement.setString(4, employee.getCity());
            preparedStatement.setString(5, employee.getEmployeeRole().toString());
            preparedStatement.setInt(6, employee.getEmployeeId());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    @Override
    public synchronized boolean remove(int employeeId) {
        LOGGER.debug("Trying to remove employee with id {} from employees table", employeeId);
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(REMOVE_EMPLOYEE)) {
            preparedStatement.setInt(1, employeeId);
            preparedStatement.executeUpdate();
            LOGGER.debug("Employee with id {} removed from employees table", employeeId);
            return true;
        } catch (SQLException e) {
            LOGGER.warn("Failed to delete employee from DB");
            return false;
        }
    }

    @Override
    public synchronized Employee getById(int employeeId) {
        LOGGER.debug("Trying to get employee by id: {}", employeeId);
        Employee employee = null;
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GET_BY_ID)) {
            preparedStatement.setInt(1, employeeId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                employee = new Employee(
                        resultSet.getInt("employee_id"),
                        resultSet.getString("last_name"),
                        resultSet.getString("first_name"),
                        EmployeeRole.valueOf(resultSet.getString("role_name")),
                        Sex.valueOf(resultSet.getString("sex")),
                        resultSet.getString("city")
                );
                LOGGER.debug("Employee with id: {} is returned", employeeId);
            }
        } catch (SQLException e) {
            LOGGER.warn("Employee with id {} doesn't exist", employeeId);
        }
        return employee;
    }

    @Override
    public synchronized List<Employee> getAll() {
        LOGGER.debug("Trying to get all employees from table: employees");
        ArrayList<Employee> employees = new ArrayList<>();
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GET_ALL)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                employees.add(new Employee(
                        resultSet.getInt("employee_id"),
                        resultSet.getString("last_name"),
                        resultSet.getString("first_name"),
                        EmployeeRole.valueOf(resultSet.getString("role_name")),
                        Sex.valueOf(resultSet.getString("sex")),
                        resultSet.getString("city")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (employees.size() > 0) {
            LOGGER.debug("Return {} employees after get all elements from table: employees", employees.size());
        } else {
            LOGGER.warn("Return empty list after get all elements from table: employees");
        }
        return employees;
    }

    public synchronized int getId(Employee employee) {
        LOGGER.debug("Trying to find employee from table: employees");
        int result = -1;
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GET_ID)) {
            preparedStatement.setString(1, employee.getLastName());
            preparedStatement.setString(2, employee.getFirstName());
            preparedStatement.setString(3, employee.getSex().toString());
            preparedStatement.setString(4, employee.getCity());
            preparedStatement.setString(5, employee.getEmployeeRole().toString());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getInt("employee_id");
            }
            LOGGER.debug("Employee id is found: {}", result);
        } catch (SQLException e) {
            LOGGER.warn("Employee {} doesn't have id", employee);
        }
        return result;
    }

    @Override
    public synchronized List<Employee> findEmployee(Employee employee) {
        List<String> paramsList;
        Map<String, String> paramsMap = new LinkedHashMap<>();

        if (employee.getEmployeeId() != 0) {
            paramsMap.put("employee_id", String.valueOf(employee.getEmployeeId()));
        }
        if (employee.getEmployeeRole() != null) {
            paramsMap.put("role_name", employee.getEmployeeRole().toString());
        }
        if (employee.getSex() != null) {
            paramsMap.put("sex", employee.getSex().toString());
        }
        if (employee.getFirstName().length() > 0) {
            paramsMap.put("first_name", employee.getFirstName());
        }
        if (employee.getLastName().length() > 0) {
            paramsMap.put("last_name", employee.getLastName());
        }
        if (employee.getCity().length() > 0) {
            paramsMap.put("city", employee.getCity());
        }
        paramsList = paramsMap.keySet().stream()
                .map(i -> String.format("%s = ?", i))
                .collect(Collectors.toList());
        if (paramsList.size() > 0) {
            FIND_EMPLOYEE.append(" WHERE ").append(String.join(" AND ", paramsList));
        }
        LOGGER.debug("Trying to find employee from table: employees");
        ArrayList<Employee> employees = new ArrayList<>();
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(FIND_EMPLOYEE.toString())) {
            int i = 1;
            for (Map.Entry<String, String> pair : paramsMap.entrySet()) {
                String value = pair.getValue();
                preparedStatement.setString(i, value);
                i++;
            }
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                employees.add(new Employee(
                        resultSet.getInt("employee_id"),
                        resultSet.getString("last_name"),
                        resultSet.getString("first_name"),
                        EmployeeRole.valueOf(resultSet.getString("role_name")),
                        Sex.valueOf(resultSet.getString("sex")),
                        resultSet.getString("city")
                ));
            }
            LOGGER.debug("Employees is found");
        } catch (SQLException e) {
            LOGGER.warn("Any employee isn't found");
        }
        FIND_EMPLOYEE.delete(FIND_EMPLOYEE.indexOf(POINT) + POINT.length(), FIND_EMPLOYEE.length());

        return employees;
    }
}
