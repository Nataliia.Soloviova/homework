package com.epam.rd.november2019.dao;

import com.epam.rd.november2019.model.User;

public interface UserDao {

    User add(User user);

    User getByEmail(String email);

//    User getByUserName(String email);

//    boolean edit(User user);
}
